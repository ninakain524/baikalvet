<?php
	require_once("php/config.php");
  require_once("php/timezone.php");
  require_once("php/functions.php");
	if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
	require_once("template/head.html");
	require_once("template/header.php");
	
// 	<!--Редактировать контракт-->
    if (isset($_GET['edit-contract'])) { ?>
    <div id="hide-form-popup-edit-contract" class="hide-form-popup-edit-contract hideform fancy-animate">
        <form id="popup-edit-contract" method="post" enctype="multipart/form-data" action="/php/editContract.php?contract1=<? echo ($_GET['edit-contract']) ?>">
            <?  $contract = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM contract WHERE id=".$_GET['edit-contract']));
                $region = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM region WHERE id=".$contract['id_region']));?>
            
            <h3>Редактирование контракта</h3>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="customer_edit">Заказчик</label>
                <input class="field width-add" type="text" name="customer_edit" id="customer_edit" value="<?php echo $contract['customer']; ?>" />
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">Руководитель заказчика (ФИО)</label>
                <input class="field width-add field-date" type="text" name="person_customer_edit" id="person_customer_edit" value="<?php echo $contract['person_customer']; ?>" autocomplete="on"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="deadline_edit">Срок сдачи</label>
                <input class="field width-add" type="text" name="deadline_edit" id="deadline_edit" value="<?php echo $contract['deadline']; ?>" />
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="term_services_edit">Срок оказания услуг</label>
                <input class="field width-add field-date" type="date" name="term_services_edit" id="term_services_edit" autocomplete="on" value="<?php echo $contract['term_services']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="name">Рабочее наименование</label>
                <input class="field width-add" type="text" name="name" id="name" value="<?php echo $contract['name_contract']; ?>" required/>
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="name">Полное наименование</label>
                <input class="field width-add" type="text" name="full-name_edit" id="full-name_edit" value="<?php echo $contract['full_name']; ?>" />
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="number_edit">Номер контракта</label>
                <input class="field width-add" type="text" name="number_edit" id="number_edit" value="<?php echo $contract['number']; ?>" />
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract_edit">Дата контракта</label>
                <input class="field width-add field-date" type="date" name="date_contract_edit" id="date_contract_edit" autocomplete="on" value="<?php echo $contract['date_contract']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label for="region">Регион</label>
        		    <select class="field" id="region" name="region" required autofocus>
                        <?php
                          $sql_region = "SELECT * FROM `region`;";
                          $res_region = mysqli_query($SERVER, $sql_region) or die("Ошибка " . mysqli_error($SERVER));
                          echo '<option value="'. $contract['id_region'] .'" selected readonly hidden>'.$region['name_region'].'</option>';
        
                          while($row_region = mysqli_fetch_array($res_region))
                          {
                            echo '<option value="'. $row_region['id'] .'">'. $row_region['name_region'] .'</option>';
                          }
                        ?>
                    </select> 
                </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">Название огранизации</label>
                <input class="field width-add field-date" type="text" name="name_organization_edit" id="name_organization_edit" autocomplete="on" value="<?php echo $contract['name_organization']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">Индекс</label>
                <input class="field width-add field-date" type="number" name="index_a_edit" id="index_a_edit" autocomplete="on" value="<?php echo $contract['index_a']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">Адрес</label>
                <input class="field width-add field-date" type="text" name="adress_edit" id="adress_edit" autocomplete="on" value="<?php echo $contract['adress']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">ИНН</label>
                <input class="field width-add field-date" type="number" name="inn_edit" id="inn_edit" autocomplete="on" value="<?php echo $contract['inn']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">КПП</label>
                <input class="field width-add field-date" type="number" name="kpp_edit" id="kpp_edit" autocomplete="on" value="<?php echo $contract['kpp']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">ОГРН</label>
                <input class="field width-add field-date" type="number" name="ogrn_edit" id="ogrn_edit" autocomplete="on" value="<?php echo $contract['ogrn']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">Телефон</label>
                <input class="field width-add field-date" type="text" name="phone_edit" id="phone_edit" autocomplete="on" value="<?php echo $contract['phone']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label for="region">Статус</label>
        		    <select class="field" id="status" name="status"  autofocus>
                        <? echo '<option value="'. $contract['status'] .'" selected readonly hidden>'.$contract['status'].'</option>'; ?>
                        <option value="открыт">открыт</option>
                        <option value="закрыт">закрыт</option>
                    </select> 
                </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="doc">Файл договора</label>
                <input type="file" class="form-control" name="doc_edit" id="doc_edit" accept=".doc, .docx, .pdf" />
              </div>
            </div>

            <div class="col-12 col-sm-12">
              <h3>Данные для печатных форм (верхний правый угол)</h3>
            </div>

            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="num_application_edit">Номер приложения</label>
                <input class="field width-add field-date" type="text" name="num_application_edit" id="num_application_edit"  value="<?php echo $contract['num_application']; ?>"  />  
              </div>
            </div>

            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="name_tz_edit">Название контракта/договора</label>
                <input class="field width-add field-date" type="text" name="name_tz_edit" id="name_tz_edit"  value="<?php echo $contract['name_tz']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-12">
              <div class="form-group">
                <label for="comment_edit">Комментарий</label>
                <textarea class="field width-add" name="comment_edit" id="comment_edit" value="<?php echo $contract['comment']; ?>"></textarea>
              </div>
            </div>
            
            <div class="col-12 col-sm-12">
              <input class="btn button-auth field-submit field-width" type="submit" value="Сохранить" autofocus />
            </div>
            
        </form>
    </div>
    <? }
// <!--/Редактировать контракт-->
	
	if ($_GET['contract'] != null && $_GET['contract'] != "") {
	    $contract = mysqli_fetch_array(mysqli_query($SERVER, 
	        "SELECT contract.id AS id,
            contract.name_contract AS name_contract,
            contract.customer AS customer,
            contract.deadline AS deadline,
            DATE_FORMAT(contract.term_services,'%d.%m.%Y') AS term_services,
            contract.number AS number,
            DATE_FORMAT(contract.date_contract,'%d.%m.%Y') AS date_contract,
            contract.comment AS comment,
            contract.status AS status,
            contract.id_region AS id_region,
            contract.full_name AS full_name,
            contract.person_customer AS person_customer,
            contract.index_a AS index_a,
            contract.adress AS adress,
            contract.inn AS inn,
            contract.kpp AS kpp,
            contract.ogrn AS ogrn,
            contract.phone AS phone,
            contract.num_application AS num_application,
            contract.name_tz AS name_tz,
            contract.name_organization AS name_organization,
            contract.document AS document,
            region.name_region AS name_region
            FROM contract 
            LEFT JOIN region ON contract.id_region = region.id
            WHERE contract.id=".$_GET['contract']));
	}
?>

<section class="content" id="contract" data="<? echo $_GET['contract'] ?>">
	<div class="container">
		<div class="col-md-12">
			<h1>Контракт: <?echo $contract['name_contract'] ?></h1>
		</div>
	    <div class="col-md-12 tab-vet-edit">
	        <div class="row block-content-card">
    			<table class="table table-bordered table-hover table-striped tab-card tab-card">
				  	<tbody>
				  	    <tr>
				    	    <th>Полное наименование: </th>
					      	<td><?echo $contract['full_name']?></span></td>
				    	</tr>
				    	<tr>
				    	    <th>Рабочее наименование: </th>
					      	<td><?echo $contract['name_contract']?></span></td>
				    	</tr>
				    	<tr>
				    	    <th>Регион: </th>
					      	<td><?echo $contract['name_region']?></span></td>
				    	</tr>
				  		<tr>
				      		<th>Заказчик:</th>
					      	<td><span class="edit_element"><?echo $contract['customer']?></span></td>
				    	</tr>	   
				    	<tr>
				    	    <th>Руководитель заказчика: </th>
					      	<td><?echo $contract['person_customer']?></span></td>
				    	</tr>
				    	<tr>
				    	    <th>Наименование организации: </th>
					      	<td><?echo $contract['name_organization']?></span></td>
				    	</tr>
				    	<tr>
				    	    <th>Номер контракта: </th>
					      	<td><?echo $contract['number']?></span></td>
				    	</tr>
				    	<tr>
				    	    <th>Дата контракта: </th>
					      	<td><?echo $contract['date_contract']?></span></td>
				    	</tr>
		    			<tr>
			      			<th>Срок сдачи:</th>
				      		<td><?echo $contract['deadline'] ?>
				    	</tr>
				    	<tr>
					      	<th>Срок оказания услуг: </th>
					      	<td><?echo $contract['term_services']?></td>
				    	</tr>
				    	<tr>
				    	    <th>Индекс: </th>
					      	<td><?echo $contract['index_a']?></span></td>
				    	</tr>
				    	<tr>
				    	    <th>Адрес: </th>
					      	<td><?echo $contract['adress']?></span></td>
				    	</tr>
				    	<tr>
				    	    <th>ИНН: </th>
					      	<td><?echo $contract['inn']?></span></td>
				    	</tr>
				    	<tr>
				    	    <th>КПП: </th>
					      	<td><?echo $contract['kpp']?></span></td>
				    	</tr>
				    	<tr>
				    	    <th>ОГРН: </th>
					      	<td><?echo $contract['ogrn']?></span></td>
				    	</tr>
				    	<tr>
				    	    <th>Телефон: </th>
					      	<td><?echo $contract['phone']?></span></td>
				    	</tr>
				    	<tr>
				    	    <th>Статус: </th>
					      	<td><?echo $contract['status']?></span></td>
				    	</tr>
				    	<tr>
				    	    <th>Комментарий: </th>
					      	<td><?echo $contract['comment']?></span></td>
				    	</tr>
              <tr>
                <th colspan="2" style="padding: 20px 0 !important;">Данные для печатных форм (верхний правый угол)</th>
				    	</tr>
              <tr>
				    	    <th>Номер приложения: </th>
					      	<td><?echo $contract['num_application']?></span></td>
				    	</tr>
				    	<tr>
				    	    <th>Название контракта/договора: </th>
					      	<td><?echo $contract['name_tz']?></span></td>
				    	</tr>
				  	</tbody>
				</table>
		    </div>
		</div>
		<div class="col-12 col-md-12">
		    <center><a href='<?echo $contract["document"]?>' target="_blank" class='btn button-auth field-submit1 btn-card'>Текст контракта</a>
		    <a href='?contract=<?echo $_GET['contract']?>&edit-contract=<?echo $_GET['contract']?>' class='btn button-auth field-submit1 btn-card'>Редактировать</a></center>
		</div>
	</div>
</section>

<? if (isset($_GET['edit-contract'])) { ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-edit-contract',
              type : 'inline'
           });
      </script>
    <? } ?>
    
<? require_once("template/footer.html"); ?>