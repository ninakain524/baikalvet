<?php
  require_once("php/config.php");
  require_once("php/timezone.php");
  if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
  require_once("template/head.html");
  require_once("template/header.php");
  require_once("php/functions.php");
  // Часовой пояс Якутск
  $today_day_time = date("Y-m-d\TH:i:s");
  $today_day = date("Y-m-d");

  // Выбор подстроки из строки
  function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
  }
?>
<section class="content">
  <div class="container">
    <div id="add_application" class="content-form js-form-address"> 
      <div class="row">
        <h1>Новая карточка</h1>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="mediafiles-foto">Фото</label>
            <input  type="file" class="form-control" name="mediafiles-foto" id="mediafiles-foto" accept="image/jpeg, image/png, image/jpg" multiple>
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="mediafiles-video">Видео</label>
            <input  type="file" class="form-control" name="mediafiles-video" id="mediafiles-video" accept="video/mp4, video/avi, video/3gp, video/mov" multiple>
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="id_contract">Основание</label>
            <select class="field width-add" id="reason" name="reason" required>
              <?  if ( $_SESSION['id_users_group'] == "4" )  
                    $sql = "SELECT * FROM `journal` WHERE `show`=1 AND `status`='1' AND `id_region`='".$_SESSION['id_region']."' AND `id_kinolog`='".$_SESSION['id_user']."' ORDER BY `date` DESC;";
                  else {
                    $sql = "SELECT * FROM `journal` WHERE `show`=1 AND `status`='1' AND `id_region`='".$_SESSION['id_region']."' AND `id_kinolog`='".$_SESSION['id_user']."' ORDER BY `date` DESC;";

                }
                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                echo '<option value="wo_reason" selected readonly>Без основания</option>';
                while($row = mysqli_fetch_array($res))
                {
                  echo '<option value="'.$row['id'].'">'.$row['id'].' | '.date("d.m.Y\ H:i", $row['date']).' | '.$row['loc_type'].$row['locality'].' | '.$row['applicant_name'].'</option>';
                }
              ?>
            </select>
          </div>
        </div>

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="date">Дата </label>
            <input class="field width-add field-date check-time" type="datetime-local" name="date" id="date" autocomplete="on" value="<?php echo $today_day_time; ?>" required />  
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
            <div class="form-group">
                <label for="region">Регион отлова</label> <? 
                if ( $_SESSION['id_users_group'] == "1" ) { ?>
        		    <select class="field" id="region" name="region" required autofocus>
                        <?php
                          $sql_region = "SELECT * FROM `region`;";
                          $res_region = mysqli_query($SERVER, $sql_region) or die("Ошибка " . mysqli_error($SERVER));
                        //   echo '<option value='.$_SESSION['id_region'].' selected readonly hidden>'. $_SESSION['region'].'</option>';
                          echo '<option value="" selected readonly hidden>Выберите регион</option>';
        
                          while($row_region = mysqli_fetch_array($res_region))
                          {
                            echo '<option value="'. $row_region['id'] .'">'. $row_region['name_region'] .'</option>';
                          }
                        ?>
                      </select> <?
                }
                else { ?>
                    <input class="field width-add" type="text" name="region" value="<?php echo $_SESSION['region']; ?>" required readonly/> <?
                } ?>
                      
            </div>
        </div>

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="city">Населенный пункт/местность отлова</label>
            <input class="field width-add input" type="text" name="city" id="city" required />
          </div>
        </div>  

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="street">Улица</label>
            <input class="field width-add" type="text" name="street" id="street" value=""/>
          </div>
        </div>  
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="id_contract">Контракт</label>
            <select class="field width-add" id="id_contract" name="id_contract" required>
              <? if ( $_SESSION['id_users_group'] == "4" )  
                    $sql = "SELECT * FROM `contract` WHERE `show_contract`=1 AND status='открыт' AND id_region='".$_SESSION['id_region']."' ORDER BY contract.id DESC;";
                else
                    $sql = "SELECT * FROM `contract` WHERE `show_contract`=1 AND status='открыт' ORDER BY contract.id DESC;";
                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                echo '<option value="" selected readonly hidden>Выберите контракт</option>';

                while($row = mysqli_fetch_array($res))
                {
                  echo '<option value="'. $row['id'] .'">'.$row['name_contract'].'</option>';
                }
              ?>
            </select>
          </div>
        </div>

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="id_kind">Вид животного</label>
            <select class="field width-add" id="id_kind" name="id_kind" required>
              <?php
                $sql = "SELECT * FROM `animal_kind`;";
                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                echo '<option value="2" selected readonly hidden>собака</option>';

                while($row = mysqli_fetch_array($res))
                {
                  echo '<option value="'. $row['id'] .'">'. $row['name_kind'] .'</option>';
                }
              ?>
            </select>
          </div>
        </div>

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="breed">Порода животного</label>
            <input class="field width-add" type="text" name="breed" id="breed" value="б/п" />
          </div>
        </div> 

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="gender">Пол животного</label>
            <select class="field width-add" id="gender" name="gender">
              <?php
                $sql = "SELECT * FROM `animal_gender`;";
                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                echo '<option value="" selected readonly hidden>Выберите пол животного</option>';

                while($row = mysqli_fetch_array($res))
                {
                  echo '<option value="'. $row['id'] .'">'. $row['name_gender'] .'</option>';
                }
              ?>
            </select>
          </div>
        </div> 

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="color">Окрас животного</label>
            <select class="field width-add" id="color" name="color" required>
              <?php
                $sql = "SELECT * FROM `animal_color`;";
                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                echo '<option value="" selected readonly hidden>Выберите окрас</option>';

                while($row = mysqli_fetch_array($res))
                {
                  echo '<option value="'. $row['id'] .'">'. $row['name_color'] .'</option>';
                }
              ?>
            </select>
          </div>
        </div>

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="age">Возраст животного</label>
            <input class="field width-add" type="text" name="age" id="age" />
          </div>
        </div>

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="weight">Вес животного</label>
            <input class="field width-add" type="text" name="weight" id="weight" />
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="height">Высота в холке</label>
            <input class="field width-add" type="text" name="height" id="height" />
          </div>
        </div>

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="shelter">Приют</label>
            <select class="field width-add" id="shelter" name="shelter">
              <?php
                $sql = "SELECT * FROM `animal_shelters` WHERE actual = 1;";
                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                echo '<option value="" selected readonly hidden>Выберите приют</option>';

                while($row = mysqli_fetch_array($res))
                {
                  echo '<option value="'. $row['id'] .'">'. $row['name_shelter'] .'</option>';
                }
              ?>
            </select>
          </div>
        </div> 

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="aviary">№ вольера</label>
            <input class="field width-add" type="text" name="aviary" id="num_aviary" />
          </div>
        </div>
        
        <?php if ( $_SESSION['id_users_group'] == "1" ) { ?>
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="kinolog">Ответственный кинолог</label>
                <select class="field width-add" id="kinolog" name="kinolog">
                  <?php
                    $sql_k = "SELECT * FROM `users` WHERE `show` = 1 AND `podtverzhdeno` = 1 AND `id_users_group` = 4;";
                    $res_k = mysqli_query($SERVER, $sql_k) or die("Ошибка " . mysqli_error($SERVER));
                    echo '<option value="" selected readonly hidden>Выберите кинолога</option>';
                    echo '<option value="">нет</option>';
                    while($row_k = mysqli_fetch_array($res_k))
                    {
                        $name = $row_k['name'];
                        $name1 = mb_substr($name, 0, 1);
                        $patronymic = $row_k['patronymic'];
                        $patronymic1 = mb_substr($patronymic, 0, 1);
                        
                        echo '<option value="'. $row_k['id'] .'">'.$row_k['sourname']." ".$name1.". ".$patronymic1.".".'</option>';
                    }
                  ?>
                </select>
              </div>
            </div> 
        <?php } ?>

        <div class="col-12 col-sm-12">
          <div class="form-group">
            <label for="comment">Комментарий</label>
            <textarea class="field" name="comment" id="comment" autocomplete="off"></textarea>
          </div>
        </div>

        <div class="col-12 col-sm-12">
          <div class="form-group">
            <button id="add_card1" class="btn field-submit width-submit">Сохранить</button>
          </div>
        </div>
      </div>
      <span class="error"><?php echo $_SESSION['error'] ?></span>
    </div>
  </div>
</section>
<script src="/js/add_card.js"></script> 
<?php
  require_once("template/footer.html");
?>