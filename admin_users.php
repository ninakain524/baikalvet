<?php
    require_once("php/config.php");
    if (!isset($_SESSION['timezone'])){
      header("Location: auth/login.php");
      exit();
    }
    require_once("php/timezone.php");
    if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
    require_once("php/update_status.php");
    require_once("template/head.html");
    require_once("template/header.php");
    require_once("php/functions.php");
    $today = date("Y-m-d\TH:i:s");
?>

<!-- Content -->
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="content-right" class="content-right">
          <div id="content-right-table" class="content-right-table table-responsive">
            <div class="menu-table">
              <div class="table-caption">Пользователи<div class="rule-answer"><a href="#">?</a></div></div>  
              
              <div class="menu-table-filter">
                  <ul class='subsubsub'>
                    <li>
                      <a href="add_user.php">Добавить</a>
                    </li>
                  </ul>
                  <button class="btn-deletebtn" id="delete_selected"><i class="fa fa-trash"></i>Удалить</button>
              </div>
            </div>
            <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab">
              <tr class="tab-col1">
                <th>№</th>
                <th>Фамилия</th>
                <th>Имя</th>
                <th>Отчество</th>
                <th>e-mail</th>
                <th>Логин</th>
                <th>Пароль</th>
                <th>Телефон</th>
                <th>Роль</th>
                <th>Регион</th>
                <th>Приют</th>
                <th>Подтверждено</th>
                <th>Действия</th>
              </tr>

              <?php  
                $podtverzhdeno = "";
                $query = "SELECT 
                    users.id AS user_id,
                    users.sourname AS user_sourname,
                    users.name AS user_name,
                    users.patronymic AS user_patronymic,
                    users.phone AS user_phone,
                    users.email AS user_email,
                    users.login AS user_login,
                    users.password AS user_password,
                    users.podtverzhdeno AS user_podtverzhdeno,
                    users_group.name_group AS name_group,
                    region.name_region AS name_region,
                    animal_shelters.name_shelter AS name_shelter
                    FROM users 
                    LEFT JOIN users_group ON users.id_users_group = users_group.id
                    LEFT JOIN region ON users.code_region = region.id
                    LEFT JOIN animal_shelters ON users.shelter_id = animal_shelters.id
                    WHERE id_users_group != 0 AND users.show='1' ORDER BY users.id DESC";

                $res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
                
                $i = 1;
                while ($res = mysqli_fetch_array($res_query)) {
                    if ($res['user_podtverzhdeno'] == 1) {$podtverzhdeno = "Да";}
                    else {$podtverzhdeno = "Нет";}
                    $pass = "*****";
                    if($_SESSION['super_admin'] == "1")
                      $pass = $res['user_password'];
                    
                    echo "<tr data-href='admin_users_edit.php?user_id=".$res['user_id']."' class='application' data='".$res['user_id']."'>
                    <td><div class='id-color'>{$i}</div></td>
                    <td>{$res['user_sourname']}</td>
                    <td>{$res['user_name']}</td>
                    <td>{$res['user_patronymic']}</td>
                    <td>{$res['user_email']}</td>
                    <td>{$res['user_login']}</td>
                    <td>{$pass}</td>
                    <td>{$res['user_phone']}</td>
                    <td>{$res['name_group']}</td>
                    <td>{$res['name_region']}</td>
                    <td>{$res['name_shelter']}</td>
                    <td>{$podtverzhdeno}</td>";
                    if ($res['user_podtverzhdeno'] == 1) 
                    {echo "<td><a href='/admin_users_edit.php?user_id={$res['user_id']}'>Редактировать</a></td>";}
                    else
                    {echo "<td><a href='/php/podtverdit.php?user_id={$res['user_id']}'>Подтвердить</a></td>";}
                    
                    $i = $i + 1;
                }
              ?>
            </table>

          </div>  
        </div>
      </div> 
    </div> 
  </div>
</section>

<script>
    $(document).on("click", 'tr[data-href]',function(e) {
        if ($(this).hasClass('del_app_checkbox')) {
          $(this).css("background-color", "#fff");
          $(this).removeClass('del_app_checkbox');
        }
        else{
          $(this).css("background-color", "#c0ccda");
          $(this).addClass('del_app_checkbox');
        }
    });
  
    $('body').on('click', '#delete_selected', function(){
        var checked = [];
        $('.del_app_checkbox').each(function() {
            checked.push($(this).attr('data'));
        });
        if (checked.length > 0) {
            if (confirm("Удалить выбранных пользователей?")){
                $.ajax({
                  type: 'POST',
                  url: 'php/admin.php?delete_users',
                  dataType: 'json',
                  data: {checked : JSON.stringify(checked)},
                  cache: false,
                  success: function(res){
                    console.log(res)
                    if (res.res == "success") {
                      console.log(res.res);
                      location.reload();
                    }
                    else {
                      alert("Возникла ошибка. Страница будет перезагружена");
                      console.log(res.res);//location.reload();
                    }
                  },
                  error: function(res){
                    console.log(res);
                  }
                });
            }
        }
        else{
            alert("Пользователи не выбраны.");
        }
    })
</script>
<!-- /Content -->

<?php
  require_once("template/footer.html");
?>