<?php
  require_once("php/config.php");
  require_once("php/timezone.php");
  require_once("php/functions.php");
  if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
  require_once("template/head.html");
  require_once("template/header.php");
  // Часовой пояс Якутск
  $today_day_time = date("Y-m-d\TH:i:s");
  $today_day = date("Y-m-d", time());

  // Выбор подстроки из строки
  function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
  }
?>
<section class="content">
  <div class="container">
    <div id="free_application" class="content-form js-form-address"> 
      <div class="row">
        <h1>Файлы свободного выезда</h1> 
        <? if ($_SESSION['id_users_group'] == "4") {?>
          <div class="col-xs-12 col-md-6 ">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="date">Дата </label>
                <input class="field width-add field-date" type="date" name="date" id="date" autocomplete="on" value="<?php echo $today_day; ?>" required />  
              </div>
            </div>   
            <div class="col-sm-12">
              <div class="form-group">
                <label for="city">Населенный пункт/местность отлова</label>
                <input class="field width-add input" type="text" name="city" id="city"/>
              </div>
            </div>  
            <div class="col-sm-12">
              <div class="form-group">
                <label for="street">Улица</label>
                <input class="field width-add input" type="text" name="street" id="street"/>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-md-6">
            
            <div class="col-sm-6">
              <div class="form-group">
                <label for="mediafiles-foto">Фото</label>
                <input  type="file" class="form-control" name="mediafiles-foto" id="mediafiles-foto" accept="image/jpeg, image/png, image/jpg" multiple>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <label for="mediafiles-video">Видео</label>
                <input  type="file" class="form-control" name="mediafiles-video" id="mediafiles-video" accept="video/mp4, video/avi, video/3gp, video/mov" multiple>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="form-group">
              <button id="add_free_application" class="btn field-submit width-submit">Сохранить</button>
            </div>
          </div>
        <? } ?>
      </div>
    </div>
    <? if ($_SESSION['id_users_group'] == "1") {?>
    <div id="parent-table">
      <div id="content-right" class="content-right" data-mode="view">
        <div id="content-right-table" class="content-right-table table-responsive sttab">
          <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab tab-vet">
            <thead>
              <tr class="tab-col1">
                <th>№</th>
                <th>Регион</th>
                <th>Дата</th>
                <th>Кинолог</th>
                <th>Скачать файлы</th>
                <th>Удалить</th>
              </tr>
            </thead>
            <tbody id="free_list">

            </tbody>
          </table>
        </div>  
      </div>
    </div>
    <? } ?>
  </div>
</section>
<script type="text/javascript" charset="utf-8" src="js/free.js"></script>
<?php
  require_once("template/footer.html");
?>