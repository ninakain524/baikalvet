<!-- Top menu -->
  <header class="border-bottom">
    <div class="container">
      <div class="row">
        <div class="col-md-2 col-sm-3 col-xs-12">
          <a href="/" class="logo-link">
              <img src="assets/images/logo2.png" class="logoimg" alt="Логотип Байкалвет">
            <!--<div class="custom-title content-title-logo">Байкал<span>вет</span></div>-->
          </a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-12">
          <!--<p class="location"> -->
            <?
              if ($_SESSION['id_users_group'] == "1") {
                echo          
                "<div class='location' class='btn-group'>
                  <a href='#' class='dropdown-toggle' data-toggle='dropdown'>
                    <i class='fas fa-map-marker-alt'></i>
                    {$_SESSION['region']}
                    <span class=''></span>
                  </a>
                  <ul class='dropdown-menu' role='menu'>";
                    $sql = mysqli_query($SERVER, "SELECT * FROM `region`");
                    while ($row = mysqli_fetch_array($sql)){
                      echo"<li><a class='a_location' href='#' data='".$row['id']."'>".$row['code']." ".$row['name_region']."</a></li>";
                    }
                    echo "<li><a class='a_location' href='#' data='all'>Все регионы</a></li>";
                  echo "</ul>
                </div>";
                }
              if ($_SESSION['id_users_group'] != "1" && $_SESSION['id_users_group'] != "2") {
                echo "<p class='location'><i class='fas fa-map-marker-alt'></i>{$_SESSION['region']}</p>";  
              } 
            ?>
          <!--</p>-->
        </div>
        <div class="col-md-2 col-sm-3 col-xs-12">
          <?php if ( $_SESSION['id_users_group'] == "2") { ?>
            <p class="location shelter"><i class="fas fa-building"></i><?php echo $_SESSION['name_shelter'] ?></p>
            <p class='location shelter'><i class='fas fa-map-marker-alt'></i><?php echo $_SESSION['region'] ?></p>
          <?php } ?>
        </div>
        <div class="col-md-3 hidden-sm hidden-xs">
          <?php if ( $_SESSION['id_users_group'] == "4" ||  $_SESSION['id_users_group'] == "1") { ?>
            <div class="content-top-menu">
              <a href="../add_card.php" class="general-btn"><i class="fa fa-plus" aria-hidden="true"></i> Создать запись</a>
              <? if ($_SESSION['id_users_group'] == "1") { ?>
                <a href="../journal.php" class="general-btn btn-second"><i class="fa fa-plus" aria-hidden="true"></i> Создать заявку</a>
              <? } ?>
            </div>
          <?php } ?>
          <?php if ( $_SESSION['id_users_group'] == "1") {
                    $query_count = $SERVER -> query("SELECT COUNT(*) FROM `users` WHERE `podtverzhdeno` = 0");
                    $row = $query_count->fetch_row();
                    $count = $row[0];
          ?>
            <div class="content-top-menu">

            </div>
          <?php } ?>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
          <!--<button type="button" class="btn btn-default btn-lg" onclick="open_notifications()">
            <span class="glyphicon glyphicon-bell" aria-hidden="true"></span>
          </button>-->
          <div class="menu_icon hidden-xs hidden-sm">
            <!--<img src="../assets/images/icons-user-64.png">-->
            <div class="base" onclick="open_notifications()">
                <div class="indicator">
                    <div class="noti_count"></div>
                </div>
            </div>
            <div class="dropdown">
              <button class="dropbtn btn"><?php echo ($_SESSION['sourname_user']." ".$_SESSION['name_user']) ?> <i class="fa fa-angle-down" aria-hidden="true"></i></button>
              <div class="dropdown-content">
                <!--<a href="/" >Настройки</a>-->
                <? if($_SESSION['id_users_group'] == "1"){ ?>
                    <a href="/admin_users.php">Пользователи <span class="message-alert"><?if($count > 0) echo($count); ?></span></a>
                    <!--<a href="/admin_shelters.php">Приюты</a>-->
                    <a href="/">Записи</a>
                    <a href="/free.php">Файлы свободных выездов</a>
                    <a href="/journal.php">Журнал обращений</a>
                    <a href="/admin_otcheti.php">Реестр</a>
                    <a href="/admin_zaklucheniya.php">Печать документов</a>
                    <a href="#">Выгрузка медиафайлов</a>
                    <a href="/contract.php">Контракты</a>
                    <? if($_SESSION['super_admin'] == "1"){ ?>
                      <a href="/logs.php">Лог запросов</a>
                    <? } ?>
                <? } 
                if($_SESSION['id_users_group'] == "4"){ ?>
                    <!--<a href="../otchet_kinolog.php">Закрыть смену</a>-->
                    <a href="/free.php">Свободный выезд</a>
                    <a href="/journal.php">Мои задачи</a>
                    <a id="close-day">Закрыть смену</a>
                <? } ?>
                <a href="../auth/exit.php"><i class="fas fa-sign-out-alt"></i> Выход</a>
              </div>
            </div>
          </div>
          <div class="menu_icon hidden-md hidden-lg block-icon">
            <div class="base" onclick="open_notifications()">
                <div class="indicator">
                    <div class="noti_count"></div>
                </div>
            </div>
            <div class="dropdown">
              <button class="dropbtn btn"><i class="fa fa-angle-down" aria-hidden="true"></i></button>
              <div class="dropdown-content">
                <? if($_SESSION['id_users_group'] == "1"){ ?>
                    <a href="/admin_users.php">Пользователи <span class="message-alert"><?if($count > 0) echo($count); ?></span></a>
                    <!--<a href="/admin_shelters.php">Приюты</a>-->
                    <a href="/">Записи</a>
                    <a href="/journal.php">Журнал обращений</a>
                    <a href="/admin_otcheti.php">Реестр</a>
                    <a href="/admin_zaklucheniya.php">Печать документов</a>
                    <a href="#">Выгрузка медиафайлов</a>
                    <a href="/contract.php">Контракты</a>
                    <? if($_SESSION['super_admin'] == "1"){ ?>
                      <a href="/logs.php">Лог запросов</a>
                    <? } ?>
                <? } 
                if($_SESSION['id_users_group'] == "4"){ ?>
                    <!--<a href="../otchet_kinolog.php">Закрыть смену</a>-->
                    <a href="/free.php">Свободный выезд</a>
                    <a href="/journal.php">Мои задачи</a>
                    <a id="close-day-mobile">Закрыть смену</a>
                <? } ?>
                <a href="../auth/exit.php"><i class="fas fa-sign-out-alt"></i> Выход</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 hidden-md hidden-lg hidden-xl">
          <?php if ( $_SESSION['id_users_group'] == "4" || $_SESSION['id_users_group'] == "1") { ?>
            <div class="content-top-menu">
              <a href="../add_card.php" class="general-btn"><i class="fa fa-plus" aria-hidden="true"></i> Создать запись</a>
              <? if ($_SESSION['id_users_group'] == "1") { ?>
                <a href="../journal.php" class="general-btn btn-second"><i class="fa fa-plus" aria-hidden="true"></i> Создать заявку</a>
              <? } ?>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </header>
  <div id="notifications" class="notifications hideform fancy-animate">
      <h3>Уведомления</h3>
      <div class="row" id="spisok"></div>
  </div>

  <style type="text/css">
    .Empty{
      border-color: red;
    }
  </style>
  
  <script>
    $("#close-day").click(function(){ 
        if (screen.width > 480) location.href='../otchet_kinolog.php';
        else  location.href='../otchet_kinolog-mobile.php';
    });
    $("#close-day-mobile").click(function(){ 
        if (screen.width > 480) location.href='../otchet_kinolog.php';
        else location.href='../otchet_kinolog-mobile.php';
    });
  </script>