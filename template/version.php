<?php
	require_once("head.html");
?>	
<section class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Что нового</h1>
				<div><b>1.0.1</b> 28.09.2022 <br/>Настроена переадресация с http на https защищённое соединение</div>
				<div><b>1.0</b> 28.07.2022 <br/></div>
			</div>
		</div>	
	</div>       
</section>