<?php
  require_once("php/config.php");
  if (!isset($_SESSION['timezone'])){
    echo "Не определён часовой пояс пользователя. Авторизируйтесь заново <a href='auth/login.php'>ЗДЕСЬ</a>";
    exit();
  }
  require_once("php/timezone.php");
  require_once("php/update_status.php");
  require_once("template/head.html");
  require_once("template/header.php");
  require_once("php/functions.php");
  $_SESSION['num_tr'] = 1; 
  $last_query = " ";
  
  $today = date("Y-m-d\TH:i:s");
  
  function format_interval(DateInterval $interval) {
        $result = "";
        if ($interval->d) { $result .= $interval->format("%d д. "); }
        if ($interval->h) { $result .= $interval->format("%h ч. "); }
        if ($interval->i) { $result .= $interval->format("%i мин. "); }
    
        return $result;
    };
  if ($_SESSION['filter_first_date'] == "first") {
    $sql = "SELECT MIN(`data`) AS filter_first_date FROM `application`";
    $res = mysqli_fetch_array(mysqli_query($SERVER, $sql));
    $filter_first_date = date("Y-m-d\TH:i:s", $res['filter_first_date']);
  }
  else{
    $filter_first_date = $_SESSION['filter_first_date'];
  }

  if ($_SESSION['filter_last_date'] == "last") {
    $sql = "SELECT MAX(`data`) AS filter_last_date FROM `application`";
    $res = mysqli_fetch_array(mysqli_query($SERVER, $sql));
    $filter_last_date = date("Y-m-d\TH:i:s", $res['filter_last_date']);
  }
  else{
    $filter_last_date = $_SESSION['filter_last_date'];
  }
?>

<!-- Content -->
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="head-table">
            <div class="table-caption">Администратор<div class="rule-answer"><a href="#">?</a></div></div>
            <div class="menu-table-filter">
              <div class="check">
                <input type="checkbox" id="highload1" name="highload1">
                <label for="highload1" data-onlabel="редактор" data-offlabel="просмотр" class="lb1"></label>
              </div>
            </div>
        </div>
        <div class="fixed-filters">
          <div class="menu-table1"><!--ПЕРВЫЙ БЛОК ФИЛЬТРОВ -->
            <div class="menu-table-filter">
              <input class="field-filter field-date" id="filter_first_date" name="filter_first_date" onchange="" type="datetime-local" autocomplete="on" value="<?echo $filter_first_date?>">
              <input class="field-filter field-date" id="filter_last_date" name="filter_last_date" onchange="" type="datetime-local" autocomplete="on" value="<?echo $filter_last_date?>">
              <select class="field-filter" id="filter_locality" name="filter_locality" onchange="">
                <?php
                  $sql = "SELECT DISTINCT locality AS filter_locality FROM `application`";
                  $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                  echo '<option value="all">Все населенные пункты</option>';
                  while($row = mysqli_fetch_array($res))
                  {
                    $selected = $_SESSION['filter_locality'] == $row['filter_locality'] ? 'selected' : '';
                    echo '<option value="'.$row['filter_locality'].'" '.$selected.'>'.$row['filter_locality'].'</option>';
                  }
                ?>
              </select>
              
              <select class="field-filter" id="show_status" name="show_status" onchange="">
                <?php
                  $sql = "SELECT * FROM `application_status`;";
                  $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                  echo '<option value="all">Все статусы</option>';
        
                  while($row = mysqli_fetch_array($res))
                  {
                    $selected = $_SESSION['show_status'] == $row['id'] ? 'selected' : '';
                    echo '<option value="'.$row['id'].'" '.$selected.'>'. $row['status_name'] .'</option>';
                  }
                ?>
              </select>


              <select class="field-filter" id="id_shelter" name="id_shelter">
                <?php
                  $sql = "SELECT * FROM `animal_shelters` WHERE `actual`='1';";
                  $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                  echo '<option value="all">Все приюты</option>';
                  while($row = mysqli_fetch_array($res))
                  {
                    $selected = $_SESSION['filter_id_shelter'] == $row['id'] ? 'selected' : '';
                    echo '<option value="'.$row['id'].'" '.$selected.'>'.$row['name_shelter'].'</option>';
                  }
                
                ?>
              </select>  
              <!--<select class="field-filter" id="sorting_aviarys" name="sorting_aviarys">
                <option value="without">Вольеры: без фильтра</option>
                <option value="ASC">Вольеры: по возрастанию</option>
                <option value="DESC">Вольеры: по убыванию</option>
              </select>--> 
              <input type="number" class="field-filter input" placeholder="Все вольеры" id="filter_aviarys" value="<?echo $_SESSION['filter_aviarys']?>">          
            </div>
          </div>
          <div class="menu-table1"><!--ВТОРОЙ БЛОК ФИЛЬТРОВ-->
            <div class="menu-table-filter">
              <select class="field-filter" id="kinolog" name="kinolog" onchange="">
                <? $sql_kinolog = "SELECT * FROM `users` WHERE `show` = 1 AND `podtverzhdeno` = 1 AND `id_users_group` = 4;";
                  $res_kinolog = mysqli_query($SERVER, $sql_kinolog) or die("Ошибка " . mysqli_error($SERVER));
                  echo '<option value="all" selected>Все кинологи</option>';
                  while($row_kinolog = mysqli_fetch_array($res_kinolog))
                  {
                    $name_kinolog = $row_kinolog['name'];
                    $name_kinolog1 = mb_substr($name_kinolog, 0, 1);
                    $patronymic_kinolog = $row_kinolog['patronymic'];
                    $patronymic_kinolog1 = mb_substr($patronymic_kinolog, 0, 1);
                    echo '<option value="'.$row_kinolog['id'].'">'.$row_kinolog['sourname']." ".$name_kinolog1.". ".$patronymic_kinolog1.".".'</option>';
                  }
                ?>
              </select>
              <select class="field-filter" id="show_contract" name="show_contract" onchange="">
                <?php
                  $sql1 = "SELECT 
                        contract.id AS id,
                        contract.name_contract AS name_contract
                        FROM `contract` WHERE `show_contract`=1 ORDER BY contract.id DESC;";
                  $res1 = mysqli_query($SERVER, $sql1) or die("Ошибка " . mysqli_error($SERVER));
                  echo '<option value="all">Все контракты</option>';
        
                  while($row1 = mysqli_fetch_array($res1))
                  {
                    $selected = $_SESSION['show_contract'] == $row1['id'] ? 'selected' : '';
                    echo '<option value="'.$row1['id'].'" '.$selected.'>'.$row1['name_contract'].'</option>';
                  }
                ?>
              </select>
              <label for="order_by">Сортировать по: </label>
              <select class="field-filter" onchange="" id="order_by">
                <option value="1" <? if ($_SESSION['order_by'] == 'data') echo 'selected'; ?>>дата</option>
                <option value="8" <? if ($_SESSION['order_by'] == 'id') echo 'selected'; ?>>ID</option>
                <option value="9" <? if ($_SESSION['order_by'] == 'num_aviary') echo 'selected'; ?>>вольер</option>
                <option value="2" <? if ($_SESSION['order_by'] == 'locality') echo 'selected'; ?>>город</option>
                <option value="3" <? if ($_SESSION['order_by'] == 'street') echo 'selected'; ?>>улица</option>
                <option value="4" <? if ($_SESSION['order_by'] == 'id_kind') echo 'selected'; ?>>вид животного</option>
                <option value="5" <? if ($_SESSION['order_by'] == 'breed') echo 'selected'; ?>>порода</option>
                <option value="6" <? if ($_SESSION['order_by'] == 'age') echo 'selected'; ?>>возраст</option>
                <option value="7" <? if ($_SESSION['order_by'] == 'color') echo 'selected'; ?>>окрас</option>
              </select>
              <label for="sort_by">Направление: </label>
              <select class="field-filter" onchange="" id="sort_by">
                <option value="ASC" <? if ($_SESSION['sort_by'] == 'ASC') echo 'selected'; ?>>по возрастанию</option>
                <option value="DESC" <? if ($_SESSION['sort_by'] == 'DESC') echo 'selected'; ?>>по убыванию</option>
              </select>
              <label for="row_limit" style="display:none;">Показывать: </label>
              <select class="field-filter" onchange="" id="row_limit" style="display:none;">
                <option value="25" <? if ($_SESSION['row_limit'] == '25') echo 'selected'; ?>>25</option>
              </select>
              <button id="apply_filter" onclick="selectMode()">Применить фильтры</button>
              <button id="reset_filter" onclick="reset_filter()">Сбросить фильтры</button>
              <button class="btn-deletebtn" id="delete_selected"><i class="fa fa-trash"></i>Удалить</button>
              <button class="btn-edit" id="edit_selected" style="display:none;"><i class="fa fa-pen"></i>Редактировать</button>
              <button class="btn-save" id="save_selected" style="display:none;"><i class="fa fa-save"></i>Сохранить</button>
              <button class="btn-cancel" id="save_cancel" style="display:none;">Отмена</button>
            </div>
            
          </div>   
          <div class="menu-table1">
            <label for="search_request">Искать: </label>
            <input class="field-filter" id="search_request" name="search_request" type="text" autocomplete="off" value="<?echo $_SESSION['search_request']?>">
            <button class="field-filter admin_search_click"> 
                <span class="glyphicon glyphicon-search"></span>
            </button> 
          </div>
        </div>
        
        <div id="parent-table">
            <div class="arrow-left arrows" onMouseover="move('content-right-table',10)" onMouseout="clearTimeout(move.to)" style="display:none">
                <!--<i class='fa fa-arrow-left'></i>   -->
            </div>
            <div class="arrow-right arrows" onMouseover="move('content-right-table',-10)" onMouseout="clearTimeout(move.to)" style="display:none">
                <!--<i class='fa fa-arrow-right'></i>    -->
            </div>
            <div id="content-right" class="content-right" data-mode="view">
              <div id="content-right-table" class="content-right-table table-responsive sttab">
                <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab tab-vet">
                  <tr class="tab-col1 tr-view">
                    <th rowspan="2">№</th>
                    <th rowspan="2">Фото</th>
                    <th rowspan="2">Дата<br>отлова</th>
                    <th rowspan="2">Дата<br>начала<br>карантина</th>
                    <th rowspan="2">Населенный<br>пункт</th>
                    <th rowspan="2">Улица<br>отлова</th>
                    <th rowspan="2">Приют</th>
                    <th colspan="7" style="text-align: center;">Описание животного</th>
                    <th rowspan="2">Контракт</th>
                    <th rowspan="2">№<br>вольера</th>
                    <th rowspan="2">Ответственный</th>
                    <th rowspan="2">Статус</th>
                    <th rowspan="2">ID</th>
                  </tr>
                  <tr class="tab-col1 tr-view1">
                    <th>Вид</th>
                    <th>Порода</th>
                    <th>Пол</th>
                    <th>Окрас</th>
                    <th>Возраст</th>
                    <th>Вес</th>
                    <th>Высота<br>в холке</th>
                  </tr>
                  
                  <tr class="tab-col1 tr-edit">
                    <th>№</th>
                    <th>Фото</th>
                    <th class="cell-edit">Дата отлова</th>
                    <th class="cell-edit">Дата начала<br>карантина</th>
                    <th class="cell-edit">Регион отлова</th>
                    <th class="cell-edit">Населенный<br>пункт</th>
                    <th class="cell-edit">Улица отлова</th>
                    <th class="cell-edit">Приют</th>
                    <th>№<br>Вольера</th>
                    <th>Вид</th>
                    <th>Порода</th>
                    <th>Пол</th>
                    <th class="cell-edit">Окрас</th>
                    <th>Возраст</th>
                    <th>Вес</th>
                    <th>Высота<br>в холке</th>
                    <th class="cell-edit">Контракт</th>
                    <th class="cell-edit">Комментарий</th>
                    <th class="cell-edit">Статус</th>
                    <th class="cell-edit">Ответственный<br>кинолог</th>
                    <th class="cell-edit">Дата проведения<br>лечебных<br>мероприятий</th>
                    <th class="cell-edit">Ответственный<br>ветеринар</th>
                    <th class="cell-edit">№ метки</th>
                    <th class="cell-edit">№ микрочипа</th>
                    <th class="cell-edit">Дата<br>дегельминтизации</th>
                    <th class="cell-edit">Препарат</th>
                    <th class="cell-edit">Дата<br>вакцинации</th>
                    <th class="cell-edit">Препарат</th>
                    <th class="cell-edit">Дата операции</th>
                    <th class="cell-edit">Отобразить<br>на сайте</th>
                    <th class="cell-edit">ID</th>
                    <!--<th class="cell-edit">Отображение у<br>специалистов<br>(0-нет, 1-да)</th>-->
                    <!--<th class="cell-edit">Отображение<br>на сайте<br>(0-нет, 1-да)</th>-->
                  </tr>
                  
                </table>
                
                </div>  
            </div>
        </div>
        <button hidden id="show_more" onclick="moreSelectMode()">Показать ещё</button>
      </div> 
    </div> 
  </div>
</section>
<!-- /Content -->
<?php
  require_once("template/footer.html");
?>

<script>
  var start_from = 25;

    $(document).on("dblclick", 'tr[data-href]',function(e) {
        let nameMode = document.querySelector("#content-right").dataset.mode;
        if (nameMode == "view")
            //window.open($(this).data('href'), '_self');
            window.open($(this).data('href'), '_blank');
    });
    
    $(document).on("click", 'tr[data-href]',function(e) {
        let nameMode = document.querySelector("#content-right").dataset.mode;
        if (nameMode == "view") {
            if ($(this).hasClass('del_app_checkbox')) {
            //   $(this).css("background-color", "#fff");
              $(this).removeClass('del_app_checkbox');
            }
            else{
            //   $(this).css("background-color", "#dfeeff");
              $(this).addClass('del_app_checkbox');
            }
        }
    });
    
    let selected = "";
    
    $(document).on("click", 'tr[data]',function(e) {
        let nameMode = document.querySelector("#content-right").dataset.mode;
        if (document.querySelector('.Editing') == null) {
            if (nameMode == "edit") {
                if ($(this).hasClass('edit_app_checkbox')) {
                    $(this).removeClass('edit_app_checkbox');
                }
                else{
                    if (selected.length > 0) {
                        selected.removeClass('edit_app_checkbox');  
                    }
                    selected = $(this);
                    selected.addClass('edit_app_checkbox');
                }
            }
        }
    });
  
    $(document).ready(function() {
        selectMode();
    });
    
    $('body').on('click', '#delete_selected', function(){
        var checked = [];
        $('.del_app_checkbox').each(function() {
            checked.push($(this).attr('data'));
        });
        if (checked.length > 0) {
            if (confirm("Удалить выбранные записи?")){
                $.ajax({
                  type: 'POST',
                  url: 'php/admin.php?delete_application',
                  dataType: 'json',
                  data: {checked : JSON.stringify(checked)},
                  cache: false,
                  success: function(res){
                    if (res.res == "success") {
                      gen_table();
                    }
                    else {
                      alert("Возникла ошибка. Перезагрузите страницу. ");
                      console.log(res.res);//location.reload();
                    }
                  },
                  error: function(res){
                    console.log(res);
                  }
                });
            }
        }
        else{
            alert("Записи не выбраны.");
        }
    })
    
    function selectMode() {
        // view edit
        start_from = 25;
        if (document.querySelector('.Editing') == null) {
            let nameMode = document.querySelector("#content-right").dataset.mode;
            if (nameMode == "view")
                apply_filter();
            else
                editMode();
        }
    };
    
    function moreSelectMode() {
        // view edit
        let nameMode = document.querySelector("#content-right").dataset.mode;
        if (nameMode == "view")
            show_more();
        else
            showMoreEditMode();
    };
    
    function gen_table(){
        $.ajax({
          type: 'POST',
          url: 'php/gen_table_adm.php?gen_table',
          dataType: 'json',
          beforeSend: function(){
            //создаем div
            var loading = $("<div>", {
              "class" : "ds-loading"
            });
            $('.ds-loading').fadeIn(1000, function(){
              setTimeout(function(){
                $('.ds-loading').fadeIn(500);
              }, 5000);
            });
            //выравним div по центру страницы
            $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
            //добавляем созданный div в конец документа
            $("body").append(loading);
          },
          success: function(res){
            $('.application').remove();
            $('#table-zayavka').append(res.html);
            if (parseInt(res.row_count) < parseInt($('#row_limit').val()))
              $('#show_more').fadeOut();
            else
              $('#show_more').fadeIn();
              $('.ds-loading').fadeOut(1000, function(){
              setTimeout(function(){
                $('.ds-loading').fadeOut(500);
              }, 5000);
            });
            $(".ds-loading").detach();
          },
          error: function(res){
            console.log("Ошибка генерации таблицы. ");
            console.log(res);
          }
        });
    }
    
    function genEditMode(){
        $.ajax({
          type: 'POST',
          url: 'php/editMode.php?gen_table',
          dataType: 'json',
          beforeSend: function(){
            //создаем div
            var loading = $("<div>", {
              "class" : "ds-loading"
            });
            $('.ds-loading').fadeIn(1000, function(){
              setTimeout(function(){
                $('.ds-loading').fadeIn(500);
              }, 5000);
            });
            //выравним div по центру страницы
            $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
            //добавляем созданный div в конец документа
            $("body").append(loading);
          },
          success: function(res){
            $('.application').remove();
            $('#table-zayavka').append(res.html);
            if (parseInt(res.row_count) < parseInt($('#row_limit').val()))
              $('#show_more').fadeOut();
            else
              $('#show_more').fadeIn();
            //уничтожаем div
            $('.ds-loading').fadeOut(1000, function(){
              setTimeout(function(){
                $('.ds-loading').fadeOut(500);
              }, 5000);
            });
            $(".ds-loading").detach();
          },
        });
    }
  
    function apply_filter(){
        var show_status = $('#show_status').val();
        var show_contract = $('#show_contract').val();
        var kinolog = $('#kinolog').val();
        var id_shelter = $('#id_shelter').val();
        var row_limit = $('#row_limit').val();
        var order_by = $('#order_by').val();
        var sort_by = $('#sort_by').val();
        var filter_first_date = $('#filter_first_date').val();
        var filter_last_date = $('#filter_last_date').val();
        var filter_locality = $('#filter_locality').val();
        var filter_aviarys = $('#filter_aviarys').val();
        $.ajax({
          type: 'POST',
          url: 'php/gen_table_adm.php?update_table_config',
          dataType: 'json',
          data: {
            id_shelter,
            show_status,
            show_contract,
            kinolog,
            row_limit,
            order_by,
            sort_by,
            filter_first_date,
            filter_last_date,
            filter_locality,
            filter_aviarys
          },
          success: function(res){
            if (res.res == "success") {
              $('#show_more').fadeIn();
              gen_table();
            }
            else {
              alert("Возникла ошибка. Страница будет перезагружена");
              location.reload();
            }
          },
          error: function(res){
            console.log(res);
          }
        });
    }
    
    function editMode(){
        var show_status = $('#show_status').val();
        var show_contract = $('#show_contract').val();
        var kinolog = $('#kinolog').val();
        var id_shelter = $('#id_shelter').val();
        var row_limit = $('#row_limit').val();
        var order_by = $('#order_by').val();
        var sort_by = $('#sort_by').val();
        var filter_first_date = $('#filter_first_date').val();
        var filter_last_date = $('#filter_last_date').val();
        var filter_locality = $('#filter_locality').val();
        var filter_aviarys = $('#filter_aviarys').val();
        $.ajax({
          type: 'POST',
          url: 'php/editMode.php?update_table_config',
          dataType: 'json',
          data: {
            id_shelter,
            show_status,
            show_contract,
            kinolog,
            row_limit,
            order_by,
            sort_by,
            filter_first_date,
            filter_last_date,
            filter_locality,
            filter_aviarys
          },
          success: function(res){
            if (res.res == "success") {
              $('#show_more').fadeIn();
              genEditMode();
            }
            else {
              alert("Возникла ошибка. Страница будет перезагружена");
              location.reload();
            }
          },
          error: function(res){
            console.log(res);
          }
        });
    }
  
    $("#highload1").click(function(){
      start_from = 25;
      if (document.querySelector("#highload1").checked) {
          document.querySelector("#content-right").dataset.mode = "edit";
          document.querySelector(".tr-view").style.display = "none";
          document.querySelector(".tr-view1").style.display = "none";
          document.querySelector(".tr-edit").style.display = "revert";
          document.querySelector(".arrow-left").style.display = "block";
          document.querySelector(".arrow-right").style.display = "block";
          window.parent.document.querySelector("#content-right").classList.add("parent-arrow");
          window.parent.document.querySelector("#content-right-table").classList.add("child-arrow");
          document.querySelector("#delete_selected").style.display = "none";
          document.querySelector("#edit_selected").style.display = "block";
          
          editMode();
      }
      else {
          document.querySelector("#content-right").dataset.mode = "view";
          document.querySelector(".tr-edit").style.display = "none";
          document.querySelector(".tr-view").style.display = "revert";
          document.querySelector(".tr-view1").style.display = "revert";
          document.querySelector(".arrow-left").style.display = "none";
          document.querySelector(".arrow-right").style.display = "none";
          window.parent.document.querySelector("#content-right").classList.remove("parent-arrow");
          window.parent.document.querySelector("#content-right-table").classList.remove("child-arrow");
          document.querySelector("#content-right-table").style.left = "0";
          document.querySelector("#edit_selected").style.display = "none";
          document.querySelector("#save_selected").style.display = "none";
          document.querySelector("#save_cancel").style.display = "none";
          document.querySelector("#delete_selected").style.display = "block";
          
          let childArrow = document.querySelector(".child-arrow-edit");
          if(childArrow != null)
              childArrow.classList.remove("child-arrow-edit");
            
          apply_filter();
      }
    });
  
    function show_more(){
        var row_nums = 25;
        $.ajax({
          type: 'POST',
          url: 'php/gen_table_adm.php?gen_table&show_more',
          dataType: 'json',
          data: {
            start_from,
            row_nums
          },
          beforeSend: function(){
            //создаем div
            var loading = $("<div>", {
              "class" : "ds-loading"
            });
            $('.ds-loading').fadeIn(1000, function(){
              setTimeout(function(){
                $('.ds-loading').fadeIn(500);
              }, 5000);
            });
            //выравним div по центру страницы
            $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
            //добавляем созданный div в конец документа
            $("body").append(loading);
          },
          success: function(res){
            if (res.result == "success"){
              $('#table-zayavka').append(res.html);
              start_from = Number(start_from) + 25;
              if (parseInt(res.row_count) < parseInt($('#row_limit').val()))
                $('#show_more').fadeOut();
              else
                $('#show_more').fadeIn();
                //уничтожаем div
              $('.ds-loading').fadeOut(1000, function(){
                setTimeout(function(){
                  $('.ds-loading').fadeOut(500);
                }, 5000);
              });
              $(".ds-loading").detach();
            }
          },
          error: function(res){
            console.log("Ошибка дополнения таблицы. ");
            console.log(res);
          }
        });
    }
    
    function showMoreEditMode(){
        var row_nums = 25;
        $.ajax({
          type: 'POST',
          url: 'php/editMode.php?gen_table&show_more',
          dataType: 'json',
          data: {
            start_from,
            row_nums
          },
          beforeSend: function(){
            //создаем div
            var loading = $("<div>", {
              "class" : "ds-loading"
            });
            $('.ds-loading').fadeIn(1000, function(){
              setTimeout(function(){
                $('.ds-loading').fadeIn(500);
              }, 5000);
            });
            //выравним div по центру страницы
            $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
            //добавляем созданный div в конец документа
            $("body").append(loading);
          },
          success: function(res){
            if (res.result == "success"){
              $('#table-zayavka').append(res.html);
              start_from = Number(start_from) + 25;
              if (parseInt(res.row_count) < parseInt($('#row_limit').val()))
                $('#show_more').fadeOut();
              else
                $('#show_more').fadeIn();
                 //уничтожаем div
              $('.ds-loading').fadeOut(1000, function(){
                setTimeout(function(){
                  $('.ds-loading').fadeOut(500);
                }, 5000);
              });
              $(".ds-loading").detach();
            }
          },
          error: function(res){
            console.log(res);
          }
        });
    }
    
    function move(id,spd) {
        var obj=document.getElementById(id);
        var max=-obj.offsetWidth+obj.parentNode.offsetWidth;
        var left=parseInt(obj.style.left);
        if (!left > 0)
            var left = 0;
        if ((spd>0&&left<=0)||(spd<0&&left>=max)){
            obj.style.left=left+spd+"px";
            move.to=setTimeout(function(){ move(id,spd); },2);
        }
        else 
            obj.style.left=(spd>0?0:max)+"px"; 
    }
    
    $("#edit_selected").click(function(){
        // var loading = $("<div>", { "class" : "ds-loading" });
        // $('.ds-loading').fadeIn(1000, function(){
        //     setTimeout(function(){
        //       $('.ds-loading').fadeIn(500);
        //     }, 5000);
        // });
        // $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
        // $("body").append(loading);
        
        let editTr = document.querySelector(".edit_app_checkbox");
        if (editTr != null) {
        
            document.querySelector("#edit_selected").style.display = "none";
            document.querySelector("#save_selected").style.display = "block";
            document.querySelector("#save_cancel").style.display = "block";
            
            let idTr = editTr.id;
            editTr.classList.add("Editing");
            
            let childArrow = document.querySelector(".child-arrow");
            childArrow.classList.add("child-arrow-edit");
            
            
            
            $(".edit_app_checkbox").children('td').each(function(i) {
                let znachenie = this.innerHTML;
                if (!this.classList.contains('no')) 
                    this.innerHTML = "";
                let input = document.createElement("input");
                if (this.classList.contains('dateTedit')) {
                    if(znachenie != "") {
                        let arrStr = "";
                        let dateString = "";
                        let arrayOfStrings = "";
                        if (znachenie.includes(" ")) {
                            let arrayDateTime = znachenie.split(" ");
                            arrayOfStrings = arrayDateTime[0].split(".");
                            let arrayTime = arrayDateTime[1].split(":");
                            if (arrayOfStrings.length > 2)
                                arrStr = " " + arrayTime[0] + ":" + arrayTime[1] + ":" + arrayTime[2];
                            else
                                arrStr = " " + arrayTime[0] + ":" + arrayTime[1];
                        }
                        else {
                            arrayOfStrings = znachenie.split(".");    
                            arrStr = " 00:00:00";
                        }
                            
                        
                        dateString = arrayOfStrings[2] + "-" + arrayOfStrings[1] + "-" + arrayOfStrings[0] + arrStr;
                        
                        let input = document.createElement("input");
                        input.type = "datetime-local";
                        input.step="1";
                        input.value = dateString;
                        input.classList.add("edit_input");
                        this.appendChild(input);
                    }
                    else {
                        let input = document.createElement("input");
                        input.type = "datetime-local";
                        input.step="1";
                        input.classList.add("edit_input");
                        this.appendChild(input);
                    }
                }
                else if (this.classList.contains('selectTedit')) {
                    let fieldName = this.id;
                    let fieldData = this.dataset.field;
                    let tableName = this.dataset.table;
                    
                    let listOptions = [];
                    let listSelects = [];
                    if (typeof fieldData == 'underfined' || fieldData == null || fieldData == "") 
                        fieldData = "";                    
                    $.ajax({
                        url: 'php/getEditFields.php?idApplication=' + idTr + '&tableName=' + tableName + '&fieldName=' + fieldName + "&fieldData=" + fieldData,
                        type: 'POST',  
                        async:false,
                        beforeSend: function(){
                          //создаем div
                          var loading = $("<div>", {
                            "class" : "ds-loading"
                          });
                          $('.ds-loading').fadeIn(1000, function(){
                            setTimeout(function(){
                              $('.ds-loading').fadeIn(500);
                            }, 5000);
                          });
                          //выравним div по центру страницы
                          $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
                          //добавляем созданный div в конец документа
                          $("body").append(loading);
                        },
                        success: function(res){
                            listOptions = JSON.parse(res);
                            //уничтожаем div
                            $('.ds-loading').fadeOut(1000, function(){
                              setTimeout(function(){
                                $('.ds-loading').fadeOut(500);
                              }, 5000);
                            });
                            $(".ds-loading").detach();
                          },
                        error: function(res){
                            console.log("Error in getEditFields.php");
                        }
                    });
    
                    keysListOptions = Object.keys(listOptions); //ключи
                    
                    let select = document.createElement("select");
                    listSelects.push(select);
                    select.id = fieldName;
                    select.name = fieldName;
                    select.classList.add("edit_input");
                    this.appendChild(select);
                    
                    let currentOption = listOptions[keysListOptions[0]][0]; 
                    
                    let option = document.createElement("option");
                    option.value = 0;
                    option.text = "Выберите";
                    select.add(option);
                    if (currentOption == "") {
                        option.selected = 0;
                    }
                    // заполнить селекты
                    for (let i = 1; i < keysListOptions.length; i++) {
                        const option = document.createElement("option"); 
                        let listOptions1 = listOptions[keysListOptions[i]];
                        let keysListOptions1 = Object.keys(listOptions[keysListOptions[i]]);
                        if (listOptions1[keysListOptions1[0]] == currentOption)
                            option.selected = keysListOptions1[0];
                        option.value = keysListOptions1[0];
                        option.text = listOptions1[keysListOptions1[0]];
                        select.add(option);
                    }
                }
                else if (this.classList.contains('TextTedit')) {
                    let input = document.createElement("input");
                    if (this.id == "num_birka" || this.id == "num_chip") {
                        input.type = "number";
                        input.classList.add("edit_input");
                    }
                    else if (this.id == "id") {
                        input.type = "number";
                        input.readOnly = true;
                        input.classList.add("no_edit_input");
                    }
                    else {
                        input.type = "text";
                        input.classList.add("edit_input");
                    }
                    input.value = znachenie;
                    
                    this.appendChild(input); 
                }
            });
        }
        
        // $('.ds-loading').fadeOut(1000, function(){
        //     setTimeout(function(){
        //       $('.ds-loading').fadeOut(500);
        //     }, 5000);
        // });
        // $(".ds-loading").detach();
    });
    
    $("#save_selected").click(function(){
        document.querySelector("#edit_selected").style.display = "block";
        document.querySelector("#save_selected").style.display = "none";
        document.querySelector("#save_cancel").style.display = "none";
        
        let editTr = document.querySelector(".edit_app_checkbox");
        let idTr = editTr.id;        
        let arrQuery = "";
        let fieldsArr = ["data", "name_region", "locality", "street", "name_shelter", "name_kind", "breed", "name_gender", "name_color", "age", "weight", "height", "customer", "comment", "status_name", "date_start_karantin", "kinolog", "vet_card_data", "sourname", "num_birka", "num_chip", "degel_date", "degel_preparat", "vac_date", "vac_preparat", "o_date", "show_app"];
        for (let i=0; i<fieldsArr.length; i++) {
            //console.log("Значение= "+fieldsArr[i]);
            //console.log("Текст= "+document.querySelector("."+fieldsArr[i]+idTr).querySelector('.edit_input').value);
            arrQuery = arrQuery+"&"+fieldsArr[i]+"="+document.querySelector("."+fieldsArr[i]+idTr).querySelector('.edit_input').value;
        }
        //console.log(arrQuery);
        
        $.ajax({
            url: 'php/applicationEditTab.php?update='+idTr+arrQuery,
            type: 'POST',  
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
              //создаем div
              var loading = $("<div>", {
                "class" : "ds-loading"
              });
              $('.ds-loading').fadeIn(1000, function(){
                setTimeout(function(){
                  $('.ds-loading').fadeIn(500);
                }, 5000);
              });
              //выравним div по центру страницы
              $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
              //добавляем созданный div в конец документа
              $("body").append(loading);
            },
            success: function(res){
              //уничтожаем div
              $('.ds-loading').fadeOut(1000, function(){
                setTimeout(function(){
                  $('.ds-loading').fadeOut(500);
                }, 5000);
              });
              $(".ds-loading").detach();
            },
            error: function(res){
                alert("Произошла ошибка");
                window.location.href = "/admin.php";
            }
        });
        
        editMode();
        
        editTr.classList.remove("Editing");
        editTr.classList.remove("edit_app_checkbox");
        
        let childArrow = document.querySelector(".child-arrow-edit");
        if(childArrow != null)
            childArrow.classList.remove("child-arrow-edit");
            
        document.getElementById("content-right-table").style.left="0px";
    });
    
    $("#save_cancel").click(function(){
        document.querySelector("#edit_selected").style.display = "block";
        document.querySelector("#save_selected").style.display = "none";
        document.querySelector("#save_cancel").style.display = "none";
        
        let editTr = document.querySelector(".edit_app_checkbox");
        let idTr = editTr.id;
        //console.log("idTr= "+idTr);
        
        editMode();
        editTr.classList.remove("Editing");
        editTr.classList.remove("edit_app_checkbox");
        
        let childArrow = document.querySelector(".child-arrow-edit");
        if(childArrow != null)
            childArrow.classList.remove("child-arrow-edit");
            
        document.getElementById("content-right-table").style.left="0px";
    });
    $('.admin_search_click').on('click', function() {
      search_request = $('#search_request').val();
      $.ajax({
          type: 'POST',
          url: 'php/admin.php?admin_search',
          dataType: 'json',
          data:{
            search_request,
          },
          success: function(res){
            let nameMode = document.querySelector("#content-right").dataset.mode;
            if (nameMode == "view")
              gen_table();
            else
              genEditMode();
              
          },
          error: function(res){
              console.log("Ошибка: ")
              console.log(res);
          }
      });
    });
    function reset_filter() {
      $.ajax({
        type: 'POST',
        url: 'php/admin.php?reset_filter',
        success: function(res){
          console.log("Фильтр сброшен")
          location.reload();
        },
        error: function(res){
          alert("Ошибка сброса фильтра: "+res.res);
        }
      });
    }
</script>