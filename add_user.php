<?php
  require_once("php/config.php");
  require_once("php/timezone.php");
  if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
  require_once("template/head.html");
  require_once("template/header.php");
  require_once("php/functions.php");
  $today_day = date("Y-m-d");
  $_SESSION['error'] = "";
  
  if(isset($_POST['submit'])) {

      $err = [];
    
      // проверяем, не сущестует ли пользователя с таким именем
      $query = mysqli_query($SERVER, "SELECT id FROM users WHERE login='".mysqli_real_escape_string($SERVER, $_POST['login'])."'");
      if(mysqli_num_rows($query) > 0)
      {
          $err[] = "Пользователь с таким логином уже существует";
      }
    
      // Если нет ошибок, то добавляем в БД нового пользователя
      if(count($err) == 0)
      {
        $res_id = $SERVER -> query("SELECT id FROM users ORDER BY id DESC LIMIT 1");
        $row_id = mysqli_fetch_array($res_id);
        $id = $row_id['id']+1;
    
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $users_group = $_POST['users_group'];
        $sourname = $_POST['sourname'];
        $name = $_POST['name'];
        $patronymic = $_POST['patronymic'];
        $region = $_POST['region'];
        $shelter = 0;
        if ($_POST['shelter'] != "")
          $shelter = $_POST['shelter'];
        // var_dump($shelter);
        $login = $_POST['login'];
        $password = $_POST['password'];
    
        // Убираем лишние пробелы и делаем двойное хеширование
        // $password = md5(md5(trim($_POST['password'])));
        $query = "INSERT INTO users SET
        id = '".$id."', 
        sourname = '".$sourname."',
        name ='".$name."',
        patronymic = '".$patronymic."',
        email='".$email."', 
        login='".$login."',
        password='".$password."',
        id_users_group ='".$users_group."',
        phone='".$phone."', 
        code_region='".$region."',
        shelter_id='".$shelter."',
        podtverzhdeno='1'";

        if (mysqli_query($SERVER, $query)) {
          addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Добавил пользователя - " . $query);
        }
        
        mail($email,
            "Регистрация на сайте",
            "Вы успешно зарегистрированы на сайте https://baikalvet.ru". 
            "\nФИО: ".$sourname." ".$name." ".$patronymic. 
            "\nЛогин: ".$login.
            "\nПароль: ".$password);
    
        $SERVER -> close();
        echo "<script>window.location.href='/admin_users.php';</script>";
        exit();
      }
      else
      {
        foreach($err AS $error)
        {
          $_SESSION['error'] = $error."<br>";
        }
      }
    }

?>

<section class="content">
  <div class="container">
    <div id="add_application" class="content-form js-form-address"> 
      <div class="row">
        <form id="card_user" method="post" action="">
            <h1>Новый пользователь</h1>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="sourname">Фамилия</label>
                <input class="field width-add" type="text" name="sourname"  pattern="^[А-Яа-я0-9\s]{2,80}" title="Поле не может содержать латинские буквы и знаки пунктуации" required/>  
              </div>
            </div>
    
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="name">Имя</label>
                <input class="field width-add input" type="text" name="name" pattern="^[А-Яа-я0-9\s]{2,80}" title="Поле не может содержать латинские буквы и знаки пунктуации" required />
              </div>
            </div>  
             
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="patronymic">Отчество</label>
                <input class="field width-add field-date" type="text" name="patronymic" pattern="^[А-Яа-я0-9\s]{2,80}" title="Поле не может содержать латинские буквы и знаки пунктуации" />  
              </div>
            </div>
    
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="email">e-mail</label>
                <input class="field width-add" type="email" name="email"  required />
              </div>
            </div>  
    
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="phone">Телефон</label>
                <input class="field width-add" type="tel" name="phone" id="phone"/>
              </div>
            </div> 

            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="users_group">Роль</label>
                <select class="field width-add" name="users_group" required>
                  <?php
                    $sql = "SELECT * FROM `users_group`;";
                    $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                    echo '<option value="" selected readonly hidden>Выберите роль пользователя</option>';
    
                    while($row = mysqli_fetch_array($res))
                    {
                      echo '<option value="'. $row['id'] .'">'. $row['name_group'] .'</option>';
                    }
                  ?>
                </select>
              </div>
            </div> 
    
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="region">Регион</label>
                <select class="field width-add" name="region">
                  <?php
                    $sql = "SELECT * FROM `region`;";
                    $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                    echo '<option value="" selected readonly hidden>Выберите регион</option>';
    
                    while($row = mysqli_fetch_array($res))
                    {
                      echo '<option value="'. $row['id'] .'">'. $row['name_region'] .'</option>';
                    }
                  ?>
                </select>
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="shelter">Приют (для админа приюта)</label>
                <select class="field width-add" name="shelter">
                  <?php
                    $sql = "SELECT * FROM animal_shelters WHERE actual = 1;";
                    $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                    echo '<option value="" selected readonly hidden>Выберите приют админа приюта</option>';
    
                    while($row = mysqli_fetch_array($res))
                    {
                      echo '<option value="'. $row['id'] .'">'. $row['name_shelter'] .'</option>';
                    }
                  ?>
                </select>
              </div>
            </div> 
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="login">Логин</label>
                <input class="field width-add field-date" type="text" name="login" pattern="^[A-Za-z0-9\s]{3,80}" title="Поле может содержать только латинские буквы и цифры" required/>  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
                <div class="form-group">
                  <label for="password">Пароль</label>
                  <div class="form-group custom-input">
                    <div class="password">
                    <input class="field pass" type="password" id="password-input1" name="password"  minlength="8" title="Пароль должен содержать минимум 8 символов" required autocomplete="off" />
                      <a href="#" class="password-control"></a>
                      <!--<p id="generate2" class="generate">Сгенерировать</p>-->
                    </div>
                  </div>
                </div>
            </div>

            <div class="col-12 col-sm-12">
              <div class="form-group">
                <input class="btn button-auth field-submit" name="submit" type="submit" value="Сохранить" autofocus />
              </div>
            </div>
        </form>
        <span class="error"><?php echo $_SESSION['error'] ?></span>
      </div>
    </div>
  </div>
</section>
<script>
    $(document).ready(function() {
    
      // Генерация пароля
      $("#generate").click(function(){
        $("#password-input").val("");
        $("#password-input").val(gen_password());
        return false;
      });
    
      function gen_password(){
        len = 8;
        var password = "";
        var symbols = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz0123456789";
        for (var i = 0; i < len; i++){
            password += symbols.charAt(Math.floor(Math.random() * symbols.length));     
        }
        return password;
      }
      // /Генерация пароля
    
    });
    
    // Phone
window.addEventListener("DOMContentLoaded", function() {
	function setCursorPosition(pos, elem) {
		elem.focus();
		if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
		else if (elem.createTextRange) {
			var range = elem.createTextRange();
			range.collapse(true);
			range.moveEnd("character", pos);
			range.moveStart("character", pos);
			range.select()
		}
	}
	function mask(event) {
		var matrix = "+7 (___) ___-__-__",
		i = 0,
		def = matrix.replace(/\D/g, ""),
		val = this.value.replace(/\D/g, "");
		if (def.length >= val.length) val = def;
		this.value = matrix.replace(/./g, function(a) {
			return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
		});
		if (event.type == "blur") {
			if (this.value.length == 2) this.value = ""
		} else setCursorPosition(this.value.length, this)
	};	

	var input = document.querySelector("#phone");
	input.addEventListener("input", mask, false);
	input.addEventListener("focus", mask, false);
	input.addEventListener("blur", mask, false);
});
// /Phone
</script>
<?php
  require_once("template/footer.html");
  
?>