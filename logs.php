<?php
    require_once("php/config.php");
    if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
    require_once("php/update_status.php");
    require_once("template/head.html");
    require_once("template/header.php");
    require_once("php/functions.php");
    $today = date("Y-m-d");
?>

<!-- Content -->
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="content-right" class="content-right">
          <div id="content-right-table" class="content-right-table table-responsive">
            <div class="menu-table">
              <div class="table-caption">Запросы</div>
            </div>
            <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab tab-contract">
              <tr class="tab-col1">
                <th>Дата</th>
                <th>ip</th>
                <th>Пользователь</th>
                <th>Запрос</th>
              </tr>

              <?php  
                $link = array();
                $content = file($_SERVER['DOCUMENT_ROOT']."/"."php/logs/logsForAdmin.txt", FILE_IGNORE_NEW_LINES);
                foreach($content as $line) {

                  $data = mb_split("\n", $line);
                  // var_dump($data);
                  foreach ($data as $item) {
                    $str = mb_split(" ; ", $item);
                    echo "<tr class='application'>";
                    foreach ($str as $name) {
                      echo "<td>{$name}</td>";
                    }
                    echo "</tr>";
                  }
                  
                }
              ?>
            </table>

          </div>  
        </div>
      </div> 
    </div> 
  </div>
</section>
<!-- /Content -->

<?php
  require_once("template/footer.html");
?>