<?php
  require_once("../php/config.php");
  function generateCode($length=6) {
      $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHI JKLMNOPRQSTUVWXYZ0123456789";
      $code = "";
      $clen = strlen($chars) - 1;
      while (strlen($code) < $length) {
        $code .= $chars[mt_rand(0,$clen)];
      }
      return $code;
  }
  // Вытаскиваем из БД запись, у которой логин равняется введенному
  $query = mysqli_query($SERVER, "SELECT * FROM users WHERE email='".mysqli_real_escape_string($SERVER, $_POST['email'])."' LIMIT 1");
  $user = mysqli_fetch_assoc($query);
  if ($user['podtverzhdeno'] == 0){
    $_SESSION['error'] = "Учётная запись не найдена";
    header('location: restore.php' );
    exit;
  }

  else{
    $new_password = generate_random_string(8);
    $md5_password = md5(md5($new_password));
    if (mail($_POST['email'], "Восстановление пароля.", "Добрый день! Ваш новый пароль на сайте https://baikalvet.sgmcode.ru: \n".$new_password)){
      mysqli_query($SERVER, "UPDATE `users` SET `password`='".$md5_password."' WHERE `id` = ".$user['id']);
      $_SESSION['error'] = "Письмо со ссылкой отправлено. Проверьте входящие и спам";
      header('location: login.php' );
      exit;
    }
    else {
      $_SESSION['error'] = "Письмо не отправлено. Обратитесь к администратору ресурса.";
      header('location: restore.php' );
      exit;
    }
  }
?>