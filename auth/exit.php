<?php
	require_once("../php/config.php");

	// Удаляем куки - не надо пожалуйста (((
	setcookie("id", "", time() - 3600*24*30*12, "/");
	setcookie("hash", "", time() - 3600*24*30*12, "/",null,null,true); // httponly !!!

	session_unset();
	session_destroy();
	header('location: ../' ); 
	exit; 
 ?>
