<?php
  require_once("../php/config.php");
  require_once("../template/head.html");
  
  $_SESSION['error'] = "";
?>

<style>
  body {
    background: linear-gradient(-45deg, #373b44, #4286f4, #23a6d5, #23d5ab);
    background-size: 400% 400%;
    animation: gradient 15s ease infinite;
    height: 100vh;
  }
  @keyframes gradient {
    0% {
      background-position: 0% 50%;
    }
    50% {
      background-position: 100% 50%;
    }
    100% {
      background-position: 0% 50%;
    }
  }
</style>

<?
  if(isset($_POST['submit'])) {

    $err = [];

    // проверяем, не сущестует ли пользователя с таким именем
    $query = mysqli_query($SERVER, "SELECT id FROM users WHERE login='".mysqli_real_escape_string($SERVER, $_POST['login'])."'");
    if(mysqli_num_rows($query) > 0)
    {
        $err[] = "Пользователь с такой почтой уже существует";
    }

    // Если нет ошибок, то добавляем в БД нового пользователя
    if(count($err) == 0)
    {
      $res_id = $SERVER -> query("SELECT id FROM users ORDER BY id DESC LIMIT 1");
      $row_id = mysqli_fetch_array($res_id);
      $id = $row_id['id']+1;

      $email = $_POST['email'];
      $login = $_POST['login'];
      $password = $_POST['password'];
      $region_code = $_POST['region_code'];
      $role = $_POST['role'];
      $sourname_user = $_POST['sourname_user'];
      $name_user = $_POST['name_user'];
      $patronymic_user = $_POST['patronymic_user'];
      $phone = $_POST['phone'];

      // Убираем лишние пробелы и делаем двойное хеширование
    //   $password = md5(md5(trim($_POST['password'])));

      mysqli_query($SERVER, "INSERT INTO users SET
        id = '".$id."', 
        email='".$email."', 
        login='".$login."', 
        password='".$password."',
        code_region='".$region_code."',
        sourname = '".$sourname_user."',
        name ='".$name_user."',
        patronymic = '".$patronymic_user."',
        phone='".$phone."',
        podtverzhdeno='0',
        id_users_group='".$role."'");
      
      mail($email,
          "Регистрация кинолога на сайте",
          "Вы успешно зарегистрировались на сайте https://baikalvet.ru". 
          "\nФИО: ".$sourname_user." ".$name_user." ".$patronymic_user. 
          "\nЛогин: ".$login.
          "\nПароль: ".$_POST['password'].
          "\nДля начала работы дождитесь подтверждения учетной записи администратором");

      $SERVER -> close();
      echo "<script>window.location.href='login.php';</script>";
      // header("Location: login.php"); 
      exit();
    }
    else
    {
      foreach($err AS $error)
      {
        $_SESSION['error'] = $error."<br>";
      }
    }
  }
?>

  <div class="container">
    <div class="row bottom-reg">
      <div class="col-md-12">

        <div class="auth-reg-form" id="authForm"> 
            <form id="auth" class="auth-reg-card" method="post" action="">
            <div class="login-text">Регистрация</div>
            <div class="form-group">
              <label for="region_code">Ваш регион</label>
              <select class="field" id="region_code" name="region_code" required autofocus>
                <?php
                  $sql_region = "SELECT * FROM `region`;";
                  $res_region = mysqli_query($SERVER, $sql_region) or die("Ошибка " . mysqli_error($SERVER));
                  echo '<option value="" selected readonly hidden>Выберите регион</option>';

                  while($row_region = mysqli_fetch_array($res_region))
                  {
                    echo '<option value="'. $row_region['id'] .'">'. $row_region['name_region'] .'</option>';
                  }
                ?>
              </select>
            </div>

            <div class="form-group">
              <label for="role">Роль пользователя</label>
              <select class="field" id="role" name="role" required>
                <?php
                  $sql_role = "SELECT * FROM `users_group` WHERE `id` != 1 ;";
                  $res_role = mysqli_query($SERVER, $sql_role) or die("Ошибка " . mysqli_error($SERVER));
                  echo '<option value="" selected readonly hidden>Выберите роль</option>';

                  while($row_role = mysqli_fetch_array($res_role))
                  {
                    echo '<option value="'. $row_role['id'] .'">'. $row_role['name_group'] .'</option>';
                  }
                ?>
              </select>
            </div>

            <div class="form-group">
              <label for="sourname_user">Фамилия</label>
              <input class="field" type="text" name="sourname_user" id="sourname_user" pattern="^[А-Яа-я0-9\s]{2,80}" title="Поле не может содержать латинские буквы и знаки пунктуации" required />  
            </div>
            <div class="form-group">
              <label for="name_user">Имя</label>
              <input class="field" type="text" name="name_user" id="name_user" pattern="^[А-Яа-я0-9\s]{2,80}" title="Поле не может содержать латинские буквы и знаки пунктуации" required />  
            </div>
            <div class="form-group">
              <label for="patronymic_user">Отчество</label>
              <input class="field" type="text" name="patronymic_user" id="patronymic_user" pattern="^[А-Яа-я0-9\s]{2,80}" title="Поле не может содержать латинские буквы и знаки пунктуации" required />  
            </div>
            <div class="form-group">
              <label for="phone">Телефон</label>
              <input class="field" type="tel" name="phone" id="phone" required />  
            </div>
            <div class="form-group">
              <label for="email">E-mail</label>
              <input class="field" type="email" name="email" id="email_reg" autocomplete="off" />  
            </div>
            <div class="form-group">
              <label for="patronymic_user">Логин</label>
              <input class="field" type="text" name="login" id="login" pattern="^[A-Za-z0-9\s]{3,80}" title="Поле может содержать только латинские буквы и цифры" required />  
            </div>
            <div class="form-group">
              <label for="password">Пароль</label>
              
              <!--<div class="form-text">-->
              <!--  <input type="checkbox" name="toggler" class="password-checkbox">-->
              <!--  <label for="toggler">Показать пароль</label>-->
              <!--</div>-->
              
              <div class="form-group custom-input">
                <div class="password">
                <input class="field pass" type="password" id="password-input1" name="password"  minlength="8" title="Пароль должен содержать минимум 8 символов" required autocomplete="off" />
                  <a href="#" class="password-control"></a>
                  <button id="generate" class="generate">Сгенерировать</button>
                </div>
              </div>
          
              <!--<input class="field pass" type="password" id="password-input" name="password" minlength="8" title="Пароль должен содержать минимум 8 символов" required />-->
              

            </div>
            <div class="form-group">
              <input id="ButReg" class="btn button-auth field-submit" name="submit" type="submit" value="Зарегистрироваться" />
            </div>
            <div class="form-group">
              <div class="second-but">
                <a href="../auth/login.php" id="ButReg" class="button-reg text-center">У меня есть аккаунт</a>  
              </div>
            </div>
          </form>
        </div>
        <div class="auth-reg-footer">
            Только для участников проекта "Байкалвет"
        </div>
        <span class="error"><?php echo $_SESSION['error'] ?></span>
      </div>    
    </div>
  </div>
  <footer>
      <script src="/js/common.js"></script>  
  </footer>
</body>
<script>
// Phone
    window.addEventListener("DOMContentLoaded", function() {
    	function setCursorPosition(pos, elem) {
    		elem.focus();
    		if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
    		else if (elem.createTextRange) {
    			var range = elem.createTextRange();
    			range.collapse(true);
    			range.moveEnd("character", pos);
    			range.moveStart("character", pos);
    			range.select()
    		}
    	}
    	function mask(event) {
    		var matrix = "+7 (___) ___-__-__",
    		i = 0,
    		def = matrix.replace(/\D/g, ""),
    		val = this.value.replace(/\D/g, "");
    		if (def.length >= val.length) val = def;
    		this.value = matrix.replace(/./g, function(a) {
    			return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
    		});
    		if (event.type == "blur") {
    			if (this.value.length == 2) this.value = ""
    		} else setCursorPosition(this.value.length, this)
    	};	
    
    	var input = document.querySelector("#phone");
    	input.addEventListener("input", mask, false);
    	input.addEventListener("focus", mask, false);
    	input.addEventListener("blur", mask, false);
    });
// /Phone
</script>
</html>
<?php
  $_SESSION['error'] = ""; 
?>