<?php
  require_once("../php/config.php");
  require_once("../template/head.html");
  if (!isset($_SESSION['error']))
    {
      $_SESSION['error'] = " ";
    }
  
?>
<style>
    body {
        background: linear-gradient(-45deg, #373b44, #4286f4, #23a6d5, #23d5ab);
        background-size: 400% 400%;
        animation: gradient 15s ease infinite;
        height: 100vh;
}
@keyframes gradient {
	0% {
		background-position: 0% 50%;
	}
	50% {
		background-position: 100% 50%;
	}
	100% {
		background-position: 0% 50%;
	}
}
</style>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="auth-reg-form" id="authForm"> 
        <form id="auth" class="auth-reg-card" method="post" action="restore_script.php">
          <img class="auth-icon" src="../assets/images/icon_lock_login.png">
          <div class="login-text">Восстановление пароля</div>
          <div class="form-group">
            <input class="field" type="email" name="email" id="email" placeholder="E-mail" autocomplete="on" required />  
          </div>
          <div class="form-group">
            <input id="ButRestore" name="submit" type="submit" class="btn button-auth field-submit field-width" value="Восстановить"/>
          </div>
        </form>
      </div>
      <div class="auth-reg-footer">
          Только для участников проекта "Байкалвет"
      </div>
      <span class="error"><?php echo $_SESSION['error'] ?></span>
    </div>    
  </div>
</div>
<? $_SESSION['error'] = " "; ?>