<?php
  require_once("../php/config.php");
  require_once("../template/head.html");
  if (!isset($_SESSION['error']))
    {
      $_SESSION['error'] = " ";
    }
?>

<style>
  body {
    background: linear-gradient(-45deg, #373b44, #4286f4, #23a6d5, #23d5ab);
    background-size: 400% 400%;
    animation: gradient 15s ease infinite;
    height: 100vh;
  }
  @keyframes gradient {
    0% {
      background-position: 0% 50%;
    }
    50% {
      background-position: 100% 50%;
    }
    100% {
      background-position: 0% 50%;
    }
  }
</style>

<script src="../js/common.js" type="text/javascript" charset="utf-8" async defer></script>
<div class="container">
  <div class="row">
    <div class="col-md-12">

      <div class="auth-reg-form" id="authForm"> 
          <form id="auth" class="auth-reg-card" method="post" action="auth.php">
          <!--<div class="custom-title content-title text-center">Байкал<span>вет</span></div>-->
          <img class="auth-icon" src="../assets/images/icon_lock_login.png">
          <div class="login-text">Войдите с нужными правами</div>
          <div class="form-group">
            <input class="field" type="text" name="login" id="login" placeholder="Логин" autocomplete="on" required />  
          </div>
          <div class="form-group custom-input">
            <div class="password">
            <input class="field pass" type="password" id="password-input1" name="password" placeholder="Пароль" autocomplete="on" required />
              <a href="restore.php">Забыли пароль?</a>
              <a href="#" class="password-control"></a>
            </div>
          </div>

          <div class="form-group">
            <input id="ButAuth" name="submit" type="submit" class="btn button-auth field-submit field-width" value="Войти" autofocus />
          </div>

          <div class="form-group">
            <div class="second-but">
              <a href="registration.php" id="ButReg" class="button-reg text-center">Зарегистрироваться</a>  
            </div>
          </div>
        </form>
      </div>
      <div class="auth-reg-footer">
          Только для участников проекта "Байкалвет"
      </div>
      <span class="error"><?php echo $_SESSION['error'] ?></span>
    </div>    
  </div>
</div>
<script>
    var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
  $.ajax({
    type: 'POST',
    url: '../php/timezone.php?apply_timezone',
    dataType: 'json',
    data: {
      timezone,
    },
    success: function(res){
      console.log("Часовой пояс обработан: "+timezone)
    },
    error: function(res){
      console.log("Ошибка часового пояса: "+res.res)
    }
  });</script>
<? $_SESSION['error'] = " "; ?>