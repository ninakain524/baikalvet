<?php
  require_once("../php/config.php");

  // Функция для генерации случайной строки
  function generateCode($length=6) {
      $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHI JKLMNOPRQSTUVWXYZ0123456789";
      $code = "";
      $clen = strlen($chars) - 1;
      while (strlen($code) < $length) {
        $code .= $chars[mt_rand(0,$clen)];
      }
      return $code;
  }

  // Вытаскиваем из БД запись, у которой логин равняется введенному
  $query = mysqli_query($SERVER, "SELECT * FROM users WHERE login='".mysqli_real_escape_string($SERVER, $_POST['login'])."' LIMIT 1");
  $user = mysqli_fetch_assoc($query);
    if ($user['podtverzhdeno'] == 0)
    {
        $_SESSION['error'] = "Учетная запись еще не подтверждена администратором";
        header('location: login.php' );
        exit;
    }
  // Сравниваем пароли
//   if($user['password'] == md5(md5($_POST['password']))){
  if($user['password'] == $_POST['password']){
    
    // Генерируем случайное число и шифруем его
    $hash = md5(generateCode(10));

    // Привязка к IP
    // Переводим IP в строку
    $insip = "INET_ATON('".$_SERVER['REMOTE_ADDR']."')";

    // Записываем в БД новый хеш авторизации и IP
    mysqli_query($SERVER, "UPDATE users SET hash='".$hash."', user_ip = ".$insip." WHERE id='".$user['id']."'");

    // // Ставим куки
    setcookie("id", $user['id'], time()+60*60*24*30, "/");
    setcookie("hash", $hash, time()+60*60*24*30, "/", null, null, true); // httponly !!!

    // Группа пользователей
    $result_group = $SERVER -> query("SELECT * FROM `users_group` WHERE `id` = '".$user['id_users_group']."';") ;
    $users_group = $result_group -> fetch_assoc();

    // Регион (Заб край или Иркутск)
    $result_region = $SERVER -> query("SELECT * FROM `region` WHERE `id` = '".$user['code_region']."';") ;
    $region = $result_region -> fetch_assoc();

    if ($user['shelter_id'] != NULL && $user['shelter_id'] != "" && $user['shelter_id'] != 0)
    {
        // приют для админа приюта
        $result_shelter = $SERVER -> query("SELECT * FROM `animal_shelters` WHERE `id` = '".$user['shelter_id']."';") ;
        $shelter = $result_shelter -> fetch_assoc();
        $_SESSION['name_shelter'] = $shelter['name_shelter']; // Название приюта
    } 


    // Переменные сессии
    $_SESSION['id_user'] = $user['id']; // Идентификатор пользователя
    $_SESSION['sourname_user'] = $user['sourname']; // Фамилия пользователя
    $_SESSION['name_user'] = $user['name']; // Имя пользователя
    $_SESSION['patronymic_user'] = $user['patronymic']; // Отчество пользователя
    $_SESSION['email_user'] = $user['email']; // Email пользователя
    $_SESSION['email'] = $user['login']; // Email пользователя
    $_SESSION['code'] = $user['code_region']; // код региона

    $_SESSION['super_admin'] = $user['super_admin'];
    $_SESSION['id_users_group'] = $users_group['id']; // Идентификатор группы пользователей
    $_SESSION['users_group'] = $users_group['name_group']; // Название группы пользователей
    $_SESSION['id_region'] = $region['id']; // Идентификатор региона  
    $_SESSION['region'] = $region['name_region']; // Название региона 

    $_SESSION['shelter_id'] = $user['shelter_id']; // id приюта
    // header('location: ../index.php' );//переменные для сохранения фильтров в сессии
    $_SESSION['order_by'] = 'data'; // сортировка по столбцам, по дефолту - по дате
    $_SESSION['sort_by'] = 'ASC'; // сортировка по > или <, по дефолту - по возрастанию
    $_SESSION['show_status'] = 'all'; // отображение по статусам, по дефолту - все статусы
    $_SESSION['row_limit'] = '25'; // количество отображаемых записей, по дефолту - 25
    $_SESSION['default_shelter'] = 'all'; // приют по умолчанию для админа, по дефолту - все
    $_SESSION['filter_id_shelter'] = 'all';// приют по умолчанию для админа, по дефолту - все
    $_SESSION['show_contract'] = 'all';
    $_SESSION['filter_kinolog'] = 'all';
    $_SESSION['show_contract_otchet'] = 'all';
    $_SESSION['num_tr'] = 0;
    $_SESSION['filter_locality'] = 'all';//населенный пункт
    $_SESSION['filter_first_date'] = 'first';
    $_SESSION['filter_last_date'] = 'last';
    $_SESSION['filter_aviarys'] = '';//вольер
    $_SESSION['search_request'] = '';
    
    $_SESSION['journal_order_by']= 'date';
    $_SESSION['journal_sort_by']= 'ASC';
    $_SESSION['journal_row_limit'] = '50';
    $_SESSION['journal_filter_first_date'] = 'first';
    $_SESSION['journal_filter_last_date'] = 'last';
    $_SESSION['journal_start_from'] = '0';
    $_SESSION['journal_current_page'] = '1';
    $_SESSION['journal_search_request'] = "";
    $_SESSION['journal_id_executor'] = "without";
    $_SESSION['journal_status'] = "all";
 	header("Location: check.php"); exit();
  }
  else{
    $_SESSION['error'] = "Неверно указан логин или пароль";
    header('location: login.php' );
    exit;
  }

	$SERVER -> close();

?>