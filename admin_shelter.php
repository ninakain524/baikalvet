<?php
  require_once("php/config.php");
  require_once("php/timezone.php");
  if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
  require_once("php/update_status.php");
  require_once("template/head.html");
  require_once("template/header.php");
  require_once("php/functions.php");
  $_SESSION['num_tr'] = 1;

  $today = date("Y-m-d\TH:i:s");
  
  function format_interval(DateInterval $interval) {
        $result = "";
        if ($interval->d) { $result .= $interval->format("%d д. "); }
        if ($interval->h) { $result .= $interval->format("%h ч. "); }
        if ($interval->i) { $result .= $interval->format("%i мин. "); }
    
        return $result;
    };
?>

<!-- Content -->
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="content-right" class="content-right">
          <div id="content-right-table" class="content-right-table table-responsive">
            <div class="menu-table">  
            <div class="table-caption">Админ. приюта<div class="rule-answer"><a href="#">?</a></div></div>
            <div class="menu-table-filter">
              <select class="field-filter" id="show_status" onchange="apply_filter()">
                    <option onchange="apply_filter()" value="0" <? if ($_SESSION['show_status'] == 0) echo 'selected'; ?>> Все статусы</option>
                    <option onchange="apply_filter()" value="2" <? if ($_SESSION['show_status'] == 2) echo 'selected'; ?>> На карантине</option>
                    <option onchange="apply_filter()" value="10" <? if ($_SESSION['show_status'] == 10) echo 'selected'; ?>> Карантин завершен</option>
                    <option onchange="apply_filter()" value="3" <? if ($_SESSION['show_status'] == 3) echo 'selected'; ?>> На восстановлении</option>
                    <option onchange="apply_filter()" value="11" <? if ($_SESSION['show_status'] == 11) echo 'selected'; ?>> Готово к выпуску</option>
                    <option onchange="apply_filter()" value="4" <? if ($_SESSION['show_status'] == 4) echo 'selected'; ?>> Выпущено</option>
                    <option onchange="apply_filter()" value="7" <? if ($_SESSION['show_status'] == 7) echo 'selected'; ?>> Погибло</option>
                    <option onchange="apply_filter()" value="8" <? if ($_SESSION['show_status'] == 8) echo 'selected'; ?>> Умерщвлено</option>
                    <option onchange="apply_filter()" value="9" <? if ($_SESSION['show_status'] == 9) echo 'selected'; ?>> Захоронено</option>
                    <option onchange="apply_filter()" value="5" <? if ($_SESSION['show_status'] == 5) echo 'selected'; ?>> Возвращено владельцу</option>
                    <option onchange="apply_filter()" value="12" <? if ($_SESSION['show_status'] == 12) echo 'selected'; ?>> Передано в приют</option>
                  </select>
              <label for="order_by">Сортировать по: </label>
              <select class="field-filter" onchange="apply_filter()" id="order_by">
                <option value="1" <? if ($_SESSION['order_by'] == 'data') echo 'selected'; ?>>дата</option>
                <option value="2" <? if ($_SESSION['order_by'] == 'locality') echo 'selected'; ?>>город</option>
                <option value="3" <? if ($_SESSION['order_by'] == 'street') echo 'selected'; ?>>улица</option>
                <option value="4" <? if ($_SESSION['order_by'] == 'id_kind') echo 'selected'; ?>>вид животного</option>
                <option value="5" <? if ($_SESSION['order_by'] == 'breed') echo 'selected'; ?>>порода</option>
                <option value="6" <? if ($_SESSION['order_by'] == 'age') echo 'selected'; ?>>возраст</option>
                <option value="7" <? if ($_SESSION['order_by'] == 'color') echo 'selected'; ?>>окрас</option>
                <option value="9" <? if ($_SESSION['order_by'] == 'num_aviary') echo 'selected'; ?>>вольер</option>
              </select>

              <label for="sort_by">Направление сортировки: </label>
              <select class="field-filter" onchange="apply_filter()" id="sort_by">
                <option value="DESC" <? if ($_SESSION['sort_by'] == 'DESC') echo 'selected'; ?>>по убыванию</option>
                <option value="ASC" <? if ($_SESSION['sort_by'] == 'ASC') echo 'selected'; ?>>по возрастанию</option>
              </select>

              <label for="row_limit">Показывать: </label>
              <select class="field-filter" onchange="apply_filter()" id="row_limit">
                <option value="25" <? if ($_SESSION['row_limit'] == '25') echo 'selected'; ?>>25</option>
                <!--<option value="50" <? if ($_SESSION['row_limit'] == '50') echo 'selected'; ?>>50</option>-->
                <!--<option value="75" <? if ($_SESSION['row_limit'] == '75') echo 'selected'; ?>>75</option>-->
                <!--<option value="100" <? if ($_SESSION['row_limit'] == '100') echo 'selected'; ?>>100</option>-->
              </select>
            </div>
            </div>
            <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab tab-vet">
              <tr class="tab-col1">
                <th rowspan="2">№</th>
                <th rowspan="2">Дата отлова, поступления в приют для животных, заключения о клиническом состоянии животного</th>
                <th rowspan="2">Вольер</th>
                <th rowspan="2">Населенный пункт отлова</th>
                <th rowspan="2">Улица отлова</th>
                <th colspan="6">Описание животного (вид, порода, пол, окрас, приблизительный возраст, вес, визуальная характеристика состояния животного, а также иные данные, позволяющие ее идентифицировать)</th>
                <th rowspan="2" width="170">Статус</th>
              </tr>
              <tr class="tab-col1">
                <th>Вид</th>
                <th>Порода</th>
                <th>Пол</th>
                <th>Окрас</th>
                <th>Возраст</th>
                <th>Вес</th>
              </tr>
            </table>
            <button hidden id="show_more" onclick="show_more()">Показать ещё</button>
          </div>  
        </div>
      </div> 
    </div> 
  </div>
</section>
<!-- /Content -->

<?php
  require_once("template/footer.html");
?>
<script type="text/javascript" charset="utf-8">
  $(document).on("click", 'tr[data-href]',function() {
    document.location = $(this).data('href');
  });
  $(document).ready(function() {
    gen_table();

  });
  var start_from = $('#row_limit').val();
  function gen_table(){
    $.ajax({
      type: 'POST',
      url: 'php/gen_table_adm_shelter.php?gen_table',
      dataType: 'json',
      beforeSend: function(){
        //создаем div
        var loading = $("<div>", {
          "class" : "ds-loading"
        });
        $('.ds-loading').fadeIn(1000, function(){
          setTimeout(function(){
            $('.ds-loading').fadeIn(500);
          }, 5000);
        });
        //выравним div по центру страницы
        $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
        //добавляем созданный div в конец документа
        $("body").append(loading);
      },
      success: function(res){
        $('.application').remove();
        $('#table-zayavka').append(res.html);
        if (parseInt(res.row_count) < parseInt($('#row_limit').val()))
          $('#show_more').fadeOut();
        else
          $('#show_more').fadeIn();
        //уничтожаем div
        $('.ds-loading').fadeOut(1000, function(){
          setTimeout(function(){
            $('.ds-loading').fadeOut(500);
          }, 5000);
        });
        $(".ds-loading").detach();
      },
    });
  }
  function apply_filter(){
    var show_status = $('#show_status').val();
    var id_shelter = $('#id_shelter').val();
    var row_limit = $('#row_limit').val();
    var order_by = $('#order_by').val();
    var sort_by = $('#sort_by').val();
    $.ajax({
      type: 'POST',
      url: 'php/gen_table_adm_shelter.php?update_table_config',
      dataType: 'json',
      data: {
        id_shelter,
        show_status,
        row_limit,
        order_by,
        sort_by
      },
      success: function(res){
        if (res.res == "success") {
          $('#show_more').fadeIn();
          gen_table();
        }
        else {
          alert("Возникла ошибка. Страница будет перезагружена");
          location.reload();
        }
      },
      error: function(res){
        console.log(res);
      }
    });
  }
  function show_more(){
    var row_nums = 25;
    $.ajax({
      type: 'POST',
      url: 'php/gen_table_adm_shelter.php?gen_table&show_more',
      dataType: 'json',
      data: {
        start_from,
        row_nums
      },
      beforeSend: function(){
        //создаем div
        var loading = $("<div>", {
          "class" : "ds-loading"
        });
        $('.ds-loading').fadeIn(1000, function(){
          setTimeout(function(){
            $('.ds-loading').fadeIn(500);
          }, 5000);
        });
        //выравним div по центру страницы
        $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
        //добавляем созданный div в конец документа
        $("body").append(loading);
      },
      success: function(res){
        if (res.result == "success"){
          $('#table-zayavka').append(res.html);
          start_from = Number(start_from) + 25;
          if (parseInt(res.row_count) < parseInt($('#row_limit').val()))
            $('#show_more').fadeOut();
          else
            $('#show_more').fadeIn();
            //уничтожаем div
          $('.ds-loading').fadeOut(1000, function(){
            setTimeout(function(){
              $('.ds-loading').fadeOut(500);
            }, 5000);
          });
          $(".ds-loading").detach();
        }
      },
      error: function(res){
        console.log(res);
      }
    });
  }
</script>