<?php
  require_once("php/config.php");
  require_once("php/timezone.php");
  require_once("php/functions.php");
  if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
  require_once("template/head.html");
  require_once("template/header.php");
  
  // Часовой пояс Якутск
  $today_day_time = date("Y-m-d\TH:i:s");
  
  $vet_card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_card WHERE id_application=".$_GET['application']));
  $vet_degilmintizaciya = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_degilmintizaciya WHERE id_application=".$_GET['application']));
  $vet_operation = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_operation WHERE id_application=".$_GET['application']));
  $vet_vaccine = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_vaccine WHERE id_application=".$_GET['application']));;
  $card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".$_GET['application']));
?>
<script type="text/javascript" src="js/veterinar.js"></script>
<section class="content">
	<div class="container">
		<div id="add_application" class="content-form"> 
			<div class="row">
				<h1>Карточка ветеринара</h1>
				
				<div class="col-12 col-sm-4">
                  <div class="form-group">
                    <label for="date_vet">Дата проведения лечебных мероприятий</label>
                    <input class="field width-add field-date" type="datetime-local" name="date_vet" id="date_vet" autocomplete="on" value="<?php echo $vet_card['data']; ?>" required />  
                  </div>
                </div>

		        <div class="col-12 col-sm-4">
		          <div class="form-group">
		            <label for="num_birka">№ неснимаемой/несмываемой метки</label>
		            <input class="field width-add" type="number" name="num_birka" id="num_birka" value="<?php echo $vet_card['num_birka']; ?>"/>
		          </div>
		        </div>
		        
		        <div class="col-12 col-sm-4">
		          <div class="form-group">
		            <label for="num_chip">№ микрочипа</label>
		            <input class="field width-add" type="number" name="num_chip" id="num_chip" value="<?php echo $vet_card['num_chip']; ?>"/>
		          </div>
		        </div>
		    </div>
		    <div class="row">
		        <h3>Дегельминтизация</h3>
		        
		        <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label for="date_degel">Дата дегельминтизации</label>
                    <input class="field width-add field-date" type="datetime-local" name="date_degel" id="date_degel" autocomplete="on" value="<?php echo $vet_degilmintizaciya['data']; ?>" required />  
                  </div>
                </div>
                
		        <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label for="degel_preparat">Препарат</label>
                    <input class="field width-add" type="text" name="degel_preparat" id="degel_preparat" value="<?php echo $vet_degilmintizaciya['preparat']; ?>"/>
                  </div>
                </div> 
            </div> 
            
            <div class="row">
                <h3>Вакцинация против бешенства и лептоспироза</h3>
                
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label for="date_vac">Дата вакцинации</label>
                    <input class="field width-add field-date" type="datetime-local" name="date_vac" id="date_vac" autocomplete="on" value="<?php echo $vet_vaccine['data']; ?>" required />  
                  </div>
                </div>
                
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label for="vac_preparat">Препарат</label>
                    <input class="field width-add" type="text" name="vac_preparat" id="vac_preparat" value="<?php echo $vet_vaccine['preparat']; ?>"/>
                  </div>
                </div> 
            </div>
            
            <div class="row">
                <h3>Стерилизация / кастрация</h3>
                
                <div class="col-12 col-sm-4">
                  <div class="form-group">
                    <label for="date_operation">Дата операции</label>
                    <input class="field width-add field-date" type="datetime-local" name="date_operation" id="date_operation" autocomplete="on" value="<?php echo $vet_operation['data']; ?>" required />  
                  </div>
                </div>
                
                <div class="col-12 col-sm-4">
                  <div class="form-group">
                    <label for="operation_vet">Ответственный специалист</label>
                    <select class="field width-add" id="operation_vet" name="operation_vet" required>
                      <?php
                        $sql = "SELECT * FROM `users` WHERE `id_users_group` = 3;";
                        $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                        echo '<option value='.$_SESSION['id_user'].' selected readonly hidden>'.$_SESSION['sourname_user']. " " .$_SESSION['name_user'].'</option>';
        
                        while($row = mysqli_fetch_array($res))
                        {
                          echo '<option value="'. $row['id'] .'">'. $row['sourname'] . " " . $row['name']. '</option>';
                        }
                      ?>
                    </select>
                  </div>
                </div>
                
                <div class="col-12 col-sm-4">
                  <div class="form-group">
                    <label for="date_operation">№ вольера животного</label>
                    <input class="field width-add" type="text" name="num_aviary" id="num_aviary" value="<?php echo $card['num_aviary']; ?>"/>  
                  </div>
                </div>
                
                <h3>Медиафайлы</h3>
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <div name="mediafiles">
                      <label for="mediafiles">Фото и видео</label>
                      <?php
                        if ($_SESSION['id_users_group'] == 3)
                          $query = "SELECT * FROM `upload_files` WHERE id_user IN (SELECT id FROM `users` WHERE id_users_group = '3') AND id_application = '".$_GET['application']."' ORDER BY id";
                            $res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
                            while ($res = mysqli_fetch_array($res_query)) {
                              
                              if ($res['type'] == "img") {
                                $img = $res['path_thumb'].$res['name_thumb'];
                                echo
                                  "<div class='media-item'>
                                    <img src='{$img}' width='100px'>
                                    <a class='del_file_on_vet_card' href='#' onclick='event.preventDefault()' data='{$res['id']}'></a>
                                  </div>";
                              }
                              else{
                                $vid = $res['path_file'].$res['name_file'];
                                echo 
                                  "<div class='media-item'>
                                    <video src='{$vid}' width='100px'></video>
                                    <a class='del_file_on_vet_card' href='#' onclick='event.preventDefault()' data='{$res['id']}'>
                                    </a>
                                  </div>";
                              }
                            }
                            echo
                              '<input  type="file" class="form-control" name="media-on-edit" id="media-on-edit" accept="image/jpeg, image/png, image/jpg, video/mp4, video/avi, video/3gp, video/mov" multiple>';
                        ?>
                    </div>
                  </div>
                </div>
		        <div class="col-12 col-sm-12">
		          <div class="form-group">
		            <button id="update_vet_card" class="btn field-submit width-submit" data="<?php echo $_GET['application'];?>">Сохранить изменения</button>
		          </div>
		        </div>

			</div>
		</div>
    </div>
</section>
<?php
  require_once("template/footer.html");
?>