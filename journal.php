<?php
    require_once("php/config.php");
    if (!isset($_SESSION['timezone'])){
        echo "Не определён часовой пояс пользователя. Авторизируйтесь заново <a href='auth/login.php'>ЗДЕСЬ</a>";
        exit();
    }
    require_once("php/timezone.php");
    if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false)) {
        header('location: auth/login.php' );
        exit();
    }
    if (($_SESSION['id_users_group'] != "1") && (($_SESSION['id_users_group'] != "4"))) {
        header('location: index.php' );
        exit();
    }
    
    require_once("template/head.html");
    require_once("template/header.php");
    require_once("php/functions.php");
    $_SESSION['num_tr'] = 1; 
    $last_query = " ";
    
    function format_interval(DateInterval $interval) {
        $result = "";
        if ($interval->d) { $result .= $interval->format("%d д. "); }
        if ($interval->h) { $result .= $interval->format("%h ч. "); }
        if ($interval->i) { $result .= $interval->format("%i мин. "); }
        return $result;
    };
    if (($_SESSION['journal_filter_first_date'] == "first") || ($_SESSION['journal_filter_first_date'] == "")) {
        $sql = "SELECT MIN(`date`) AS journal_filter_first_date FROM `journal`";
        $res = mysqli_fetch_array(mysqli_query($SERVER, $sql));
        $journal_filter_first_date = date("Y-m-d\TH:i:s", $res['journal_filter_first_date']);
    }
    else {
        $journal_filter_first_date = $_SESSION['journal_filter_first_date'];
    }
      
    if (($_SESSION['journal_filter_last_date'] == "last") || (($_SESSION['journal_filter_last_date'] == ""))){
        $sql = "SELECT MAX(`date`) AS journal_filter_last_date FROM `journal`";
        $res = mysqli_fetch_array(mysqli_query($SERVER, $sql));
        $journal_filter_last_date = date("Y-m-d\TH:i:s", $res['journal_filter_last_date']);

    }
    else {
        $journal_filter_last_date = $_SESSION['journal_filter_last_date'];
    }
    $today_day_time = date("Y-m-d\TH:i:s");
?>
<script>
    if (screen.width < 480) location.href='../journal-mobile.php';//проверка размера дисплея
</script>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="head-table">
                    <div class="table-caption">Журнал заявок
                        <div class="rule-answer">
                            <a href="#">?</a>
                        </div>
                    </div>
                </div>
                <div class="fixed-filters">
                    <div class="menu-table1"><!--ПЕРВЫЙ БЛОК ФИЛЬТРОВ -->
                        <div class="menu-table-filter">
                            <label for="journal_filter_first_date">Показывать с: </label>
                            <input class="journal-input field-date" id="journal_filter_first_date" name="journal_filter_first_date" type="datetime-local" autocomplete="on" value="<?echo $journal_filter_first_date?>">
                            <label for="journal_filter_last_date">Показывать по: </label>
                            <input class="journal-input field-date" id="journal_filter_last_date" name="journal_filter_last_date" type="datetime-local" autocomplete="on" value="<?echo $journal_filter_last_date?>">
                            <label for="journal_order_by">Сортировать по: </label>
                            <select class="journal-input" id="journal_order_by">
                                <option value="number" <? if ($_SESSION['journal_order_by'] == 'number') echo 'selected';?>>Номер обращения</option>
                                <option value="date" <? if ($_SESSION['journal_order_by'] == 'date') echo 'selected';?>>Дата обращения</option>
                                <option value="name" <? if ($_SESSION['journal_order_by'] == 'name') echo 'selected';?>>ФИО заявителя</option>
                                <option value="phone" <? if ($_SESSION['journal_order_by'] == 'phone') echo 'selected';?>>Телефон заявителя</option>
                                <option value="count" <? if ($_SESSION['journal_order_by'] == 'count') echo 'selected';?>>Количество животных</option>
                                <option value="creator" <? if ($_SESSION['journal_order_by'] == 'creator') echo 'selected';?>>Зарегистрировал</option>
                                <option value="status" <? if ($_SESSION['journal_status'] == 'status') echo 'selected';?>>Статус</option>
                                <option value="id_kinolog" <? if ($_SESSION['journal_id_executor'] == 'id_executor') echo 'selected';?>>Ответственный</option>
                            </select>
                            <label for="journal_sort_by">Направление: </label>
                            <select class="journal-input" id="journal_sort_by">
                                <option value="ASC" <? if ($_SESSION['journal_sort_by'] == 'ASC') echo 'selected'; ?>>по возрастанию</option>
                                <option value="DESC" <? if ($_SESSION['journal_sort_by'] == 'DESC') echo 'selected'; ?>>по убыванию</option>
                            </select>
                            <label for="journal_row_limit">Показывать по: </label>
                            <select class="journal-input" id="journal_row_limit">
                                <option value="50" <? if ($_SESSION['journal_row_limit'] == '50') echo 'selected'; ?>>50</option>
                                <option value="100" <? if ($_SESSION['journal_row_limit'] == '100') echo 'selected'; ?>>100</option>
                                <option value="150" <? if ($_SESSION['journal_row_limit'] == '150') echo 'selected'; ?>>150</option>
                                <option value="200" <? if ($_SESSION['journal_row_limit'] == '200') echo 'selected'; ?>>200</option>
                            </select> 
                        </div>
                    </div>
                    <div class="menu-table1"> 
                        <? if ($_SESSION['id_users_group'] == '1') {?>
                            <label for="journal_id_executor">Исполнитель: </label>
                            <select class="journal-input" id="journal_id_executor">
                                <option value="without" <?if ($_SESSION['journal_id_executor'] == "without") echo "selected"?>>Без исполнителя</option>
                                <option value="all" <?if (($_SESSION['journal_id_executor'] == "all") || ($_SESSION['journal_id_executor'] == "")) echo "selected"?>>Все кинологи</option>
                                <?php
                                    if (($_SESSION['id_region'] == "") || (($_SESSION['id_region'] == "all"))) {
                                        $kinologs_query = mysqli_query($SERVER, "SELECT * FROM `users` WHERE `id_users_group`='4'");
                                    }
                                    else    
                                        $kinologs_query = mysqli_query($SERVER, "SELECT * FROM `users` WHERE `id_users_group`='4' AND `code_region`=".$_SESSION['id_region']);
                                    while ($res = mysqli_fetch_array($kinologs_query)) {
                                        if ($res['id'] == $_SESSION['journal_id_executor'])
                                            $selected = "selected";
                                        else
                                            $selected = "";
                                        echo '<option '.$selected.' value="'.$res['id'].'">'.$res['sourname'].' '.$res['name'].' '.$res['patronymic'].'</option>';
                                    }
                                ?>
                            </select>
                        <? } ?>
                        <? if ($_SESSION['id_users_group'] == '4') {?>
                            <label for="journal_id_executor">Исполнитель: </label>
                            <select class="journal-input" id="journal_id_executor">
                                <option value="without" <?if ($_SESSION['journal_id_executor'] == "without") echo "selected"?>>Без исполнителя</option>
                                <option value="<?echo $_SESSION['id_user']?>" <?if ($_SESSION['journal_id_executor'] == $_SESSION['id_user'])
                                    echo "selected"?>><?$_SESSION['id_user']?><?echo $_SESSION['sourname_user']." ".$_SESSION['name_user']." ".$_SESSION['patronymic_user']?></option>
                            </select>
                        <? } ?>
                        <label for="journal_status">Статус: </label>
                        <select class="journal-input" id="journal_status">
                            <option value="all" <? if ($_SESSION['journal_status'] == 'all') echo 'selected'; ?>>Все статусы</option>
                            <option value="0" <? if ($_SESSION['journal_status'] == '0') echo 'selected'; ?>>Не выполнена</option>
                            <option value="1" <? if ($_SESSION['journal_status'] == '1') echo 'selected'; ?>>В работе</option>
                            <option value="2" <? if ($_SESSION['journal_status'] == '2') echo 'selected'; ?>>Завершено</option>
                        </select>
                        <label for="journal_search_request">Искать: </label>
                        <input class="journal-input" id="journal_search_request" name="journal_search_request" type="text" autocomplete="off" value="<?echo $_SESSION['journal_search_request']?>">
                        <button class="journal-input search_click"> 
                            <span class="glyphicon glyphicon-search"></span>
                        </button> 
                        <button class="journal-input" id="journal_filter_apply" onclick="journal_filter_apply()">Применить фильтры</button>
                        <button class="journal-input" id="journal_filter_reset" onclick="journal_filter_reset()">Сбросить фильтры</button>      
                    </div> 
                </div>
                <?
                    if ($_SESSION['id_users_group'] == '1') {?>
                    <div class="js-form-address add-row-class">
                        <div class="row">
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group">
                                    <label for="journal_date">Дата: </label>
                                    <input class="journal-input" type="datetime-local" id="journal_date" name="journal_date" type="text" autocomplete="off" placeholder="Дата заявки" value="<? echo $today_day_time?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group">
                                    <label for="journal_locality">Населенный пункт: </label>
                                    <input class="journal-input" data-kladr-type="city" id="journal_locality" name="journal_locality" type="text" autocomplete="off" placeholder="Выберите населенный пункт">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group">
                                    <label for="journal_street">Улица: </label>
                                    <input class="journal-input" id="journal_street" name="journal_street" type="text" autocomplete="off" placeholder="Введите улицу">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group">
                                    <label for="journal_applicant_name">Имя заявителя: </label>
                                    <input class="journal-input" id="journal_applicant_name" name="journal_applicant_name" type="text" autocomplete="off" placeholder="ФИО заявителя">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <div class="form-group">
                                    <label for="journal_applicant_phone">Телефон: </label>
                                    <input class="journal-input" id="journal_applicant_phone" name="journal_applicant_phone" type="text" autocomplete="off" placeholder="Номер телефона заявителя">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                <div class="form-group">
                                    <label for="journal_animals_count">Количество животных: </label>
                                    <input class="journal-input" id="journal_animals_count" name="journal_animals_count" type="number" autocomplete="off" placeholder="Количество животных" value="1">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                <div class="form-group">
                                    <label for="journal_description">Описание: </label>
                                    <textarea class="journal-input" id="journal_description" name="journal_description" type="text" autocomplete="off" placeholder="Введите дополнительное описание"></textarea>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                <div class="form-group">
                                    <label for="label-btn">&nbsp</label>
                                    <button id="label-btn" style="background-color: #5cb85c; color: #FFFFFF" class="journal-input btn btn-success" onclick="add_row()">
                                        Зарегистрировать заявление
                                        <span class="glyphicon glyphicon-plus-sign"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?}
                ?>
                <div id="parent-table">
                    <div id="content-right" class="content-right" data-mode="view">
                        <div id="content-right-table" class="content-right-table table-responsive sttab">
                            <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab tab-vet">
                                <tr class="tab-col1">
                                    <th>№</th>
                                    <th>Дата обращения</th>
                                    <th>Адрес обращения</th>
                                    <th>ФИО Заявителя</th>
                                    <th>Телефон заявителя</th>
                                    <th>Количество животных</th>
                                    <th>Примечание</th>
                                    <th>Зарегистрировал</th>
                                    <th>Статус</th>
                                    <? 
                                    if ($_SESSION['id_users_group'] == '1') 
                                        echo '<th>Исполнитель</th><th>Результат</th>'; 
                                    if ($_SESSION['id_users_group'] == '4') 
                                        echo '<th>Результат</th>'; 
                                    ?>
                                    <th>Действие</th>
                                </tr>
                            </table>
                        </div>  
                    </div>
                </div>
                <div class="pagination"></div>
            </div> 
        </div> 
    </div>
</section>
<div id="linked_card_form" class="linked_card_form hideform fancy-animate">
    <h3>Выберите карточку животного, отловленного по этой заявке</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>ID</th>
                <th>Дата</th>
                <th>Расположение</th>
                <th>Вольер</th>
                <th>Животное</th>
                <th>Порода</th>
                <th>Вес</th>
                <th>Высота</th>
                <th>Цвет</th>
                <!--<th>Просмотр</th>-->
                <th>Выбрать</th>
            </thead> 
            <tbody id="cards_list">

            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <button class="btn btn-success btn-lg" id="complete">Выбрать</button>
        </div>
    </div>
</div>
<div id="request_info" class="request_info hideform fancy-animate">
    <h3>Заявка на отлов животного</h3>
    <div class="row" id="request_list" >
        
    </div>
</div>
<?php
    require_once("template/footer.html");
?>
<script type="text/javascript" charset="utf-8" src="js/journal.js"></script>