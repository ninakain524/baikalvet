<?php 
    require_once("config.php");
    require_once("timezone.php");
    require_once("functions.php");
    $today = date("Y-m-d\TH:i:s");
    $query = "SELECT * FROM `application` WHERE `id_sostoyanie`='1' AND `show`='1'";
    $res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
    while ($res = mysqli_fetch_array($res_query)) {

        $end = date('Y-m-d H:i', $res['date_start_karantin']+777600);
        $first_date = new DateTime($today);
        $second_date = new DateTime($end); 
          
        if($res['id_status'] == 2 && $first_date > $second_date)
        { 
            mysqli_query($SERVER, "UPDATE `application` SET `id_status`='10' WHERE id=".$res['id']);
            $queryCard = mysqli_fetch_array(mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM vet_card WHERE id_application=".$res['id']));
            if (!$queryCard[0] > 0) {
            
                $result_id = $SERVER -> query("SELECT `id` FROM `vet_card` ORDER BY `id` DESC LIMIT 1");
                $row_id = mysqli_fetch_array($result_id);
                $id = $row_id['id']+1;
                
                $result_id_d = $SERVER -> query("SELECT `id` FROM `vet_degilmintizaciya` ORDER BY `id` DESC LIMIT 1");
                $row_id_d = mysqli_fetch_array($result_id_d);
                $id_d = $row_id_d['id']+1;
                
                $result_id_v = $SERVER -> query("SELECT `id` FROM `vet_vaccine` ORDER BY `id` DESC LIMIT 1");
                $row_id_v = mysqli_fetch_array($result_id_v);
                $id_v = $row_id_v['id']+1;
                
                $result_id_o = $SERVER -> query("SELECT `id` FROM `vet_operation` ORDER BY `id` DESC LIMIT 1");
                $row_id_o = mysqli_fetch_array($result_id_o);
                $id_o = $row_id_o['id']+1;
            
                mysqli_query($SERVER, "INSERT INTO `vet_degilmintizaciya` (
                    `id`, `preparat`, `id_veterinar`, `id_application`) VALUES (
                    '".$id_d."',
                    'Ивермек',
                    '0',
                    '".$res['id']."')");
                    
                mysqli_query($SERVER, "INSERT INTO `vet_vaccine` (
                    `id`, `preparat`, `id_veterinar`, `id_application`) VALUES (
                    '".$id_v."',
                    'Рабикс',
                    '0',
                    '".$res['id']."')");
                    
                mysqli_query($SERVER, "INSERT INTO `vet_operation` (
                    `id`, `id_veterinar`, `id_application`) VALUES (
                    '".$id_o."',
                    '0',
                    '".$res['id']."')");
                    
                mysqli_query($SERVER, "INSERT INTO `vet_card` (
                    `id`, `id_veterinar`, `id_degelmintizaciya`, `id_vaccine`, `id_operation`, `id_application`) VALUES (
                    '".$id."',
                    '0',
                    '".$id_d."',
                    '".$id_v."',
                    '".$id_o."',
                    '".$res['id']."')");
                
                mysqli_query($SERVER, "UPDATE `application` SET
                    `id_vet_card`='".$id."'
                    WHERE `id`='".$res['id']."';");
            }
        }
        
        $end1 = date('Y-m-d H:i', $res['date_start_vosstanovlenie']+777600);
        $first_date1 = new DateTime($today);
        $second_date1 = new DateTime($end1);
          
        if($res['id_status'] == 3 && $first_date1 > $second_date1)
        { 
          mysqli_query($SERVER, "UPDATE `application` SET `id_status`='11' WHERE id=".$res['id']); 
          
        }
        
    }
?>