<?php
	require_once('config.php');
    require_once("timezone.php");
    require_once("functions.php");
	setlocale(LC_ALL, 'ru_RU.utf8');
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	mb_http_output('UTF-8');
	mb_language('uni');
	
	if (isset($_GET['update-foto-vipusk'])) {
        $time_now = time();
        
        $id = prepare($_GET['id']);
        
        $files_to_delete = explode(',', $_GET['files_to_delete']);
        
        $allow = array('video/mp4', 'video/avi', 'video/3gp', 'video/mov', 'image/png', 'image/jpeg', 'image/jpg', 'image/bmp');
        $imagetype = array('image/png', 'image/jpeg', 'image/jpg', 'image/bmp'); 
        // Полный путь до upload директории.
        $full_path = $_SERVER['DOCUMENT_ROOT'] . '/assets/files/uploads/';
        $thumbs_path = $_SERVER['DOCUMENT_ROOT'] . '/assets/files/thumbs/';
        if (!is_dir($full_path)) {
            mkdir($full_path, 0777, true);
        }
        foreach($_FILES as $file){
            $new_file_name = generate_random_string(11).'.'.pathinfo($file['name'], PATHINFO_EXTENSION);
            if (in_array($file['type'], $allow))
                if (move_uploaded_file($file['tmp_name'], $full_path . $new_file_name))
                    if (in_array($file['type'], $imagetype)){
                        $filetype = "img";
                        mysqli_query($SERVER, "INSERT INTO `upload_files` (type, path_file, path_thumb, name_file, name_thumb, data, actual, id_user, id_application, num_str, vipusk) VALUES ('".$filetype."', '/assets/files/uploads/', '/assets/files/thumbs/', '".$new_file_name."', '".$new_file_name."', '".$time_now."', '1', '".$_SESSION['id_user']."', '".$id."', '1', '1')");
                        ResizeImage($full_path.$new_file_name, 200, 100, $thumbs_path, $new_file_name);
                    }
                    
            }              
        $files_to_delete = array_unique($files_to_delete);
        if ($files_to_delete[0] > 0) {
            for ($i = 0; $i < count($files_to_delete); $i++) { 
                $delete_file = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM upload_files WHERE id=".$files_to_delete[$i]));
                if (mysqli_query($SERVER, "DELETE FROM `upload_files` WHERE id=".$delete_file['id'])) {
                    unlink($full_path.$delete_file['name_file']);
                    unlink($thumbs_path.$delete_file['name_file']);
                }
            }
        }
    }
    
    if (isset($_GET['update-video-vipusk'])) {
        $time_now = time();
        
        $id = prepare($_GET['id']);
        
        $files_to_delete = explode(',', $_GET['files_to_delete']);
        
        $allow = array('video/mp4', 'video/avi', 'video/3gp', 'video/mov', 'image/png', 'image/jpeg', 'image/jpg', 'image/bmp');
        $videotype = array('video/mp4', 'video/avi', 'video/3gp', 'video/mov');
        // Полный путь до upload директории.
        $full_path = $_SERVER['DOCUMENT_ROOT'] . '/assets/files/uploads/';
        $thumbs_path = $_SERVER['DOCUMENT_ROOT'] . '/assets/files/thumbs/';
        if (!is_dir($full_path)) {
            mkdir($full_path, 0777, true);
        }
        foreach($_FILES as $file){
            $new_file_name = generate_random_string(11).'.'.pathinfo($file['name'], PATHINFO_EXTENSION);
            if (in_array($file['type'], $allow))
                if (move_uploaded_file($file['tmp_name'], $full_path . $new_file_name))
                    if (in_array($file['type'], $videotype)){
                        $filetype = "vid";
                        mysqli_query($SERVER, "INSERT INTO `upload_files` (type, path_file, path_thumb, name_file, name_thumb, data, actual, id_user, id_application, num_str, vipusk) VALUES ('".$filetype."', '/assets/files/uploads/', '', '".$new_file_name."', '".$new_file_name."', '".$time_now."', '1', '".$_SESSION['id_user']."', '".$id."', '1', '1')");
                    }
                    
            }              
        $files_to_delete = array_unique($files_to_delete);
        if ($files_to_delete[0] > 0) {
            for ($i = 0; $i < count($files_to_delete); $i++) { 
                $delete_file = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM upload_files WHERE id=".$files_to_delete[$i]));
                if (mysqli_query($SERVER, "DELETE FROM `upload_files` WHERE id=".$delete_file['id'])) {
                    unlink($full_path.$delete_file['name_file']);
                    unlink($thumbs_path.$delete_file['name_file']);
                }
            }
        }
    }

?>