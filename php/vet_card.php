<?php
	require_once('config.php');
    require_once("timezone.php");
    require_once("functions.php");
	setlocale(LC_ALL, 'ru_RU.utf8');
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	mb_http_output('UTF-8');
	mb_language('uni');
    $time_now = time();
    if (isset($_GET['add_vet_card'])) {
        $time_now = time();
        $id_application = $_GET['add_vet_card'];        
        $operation_vet_add = $_POST['operation_vet'];
        // var_dump($operation_vet_add);
        if ($operation_vet_add == 'selected' || $operation_vet_add == '') $operation_vet_add = 0;
        $degel_vet = $vac_vet = $operation_vet = $operation_vet_add;
        
        $num_birka = $_POST['num_birka_add'];
        $num_chip = $_POST['num_chip_add'];
        $degel_preparat = $_POST['degel_preparat'];
        $vac_preparat = $_POST['vac_preparat'];
        
        $num_aviary = $_POST['num_aviary_vet'];
        
        $gender = $_POST['gender_vet'];
        if ($gender == 'selected') $gender = 0;
        
        $comment = $_POST['comment_vet'];

        $result_id = $SERVER -> query("SELECT `id` FROM `vet_card` ORDER BY `id` DESC LIMIT 1");
        $row_id = mysqli_fetch_array($result_id);
        $id = $row_id['id']+1;

        $rs = mysqli_query($SERVER, "SELECT COUNT(*) AS id FROM vet_card WHERE id_application=".$id_application);
        $result = mysqli_fetch_array($rs);
        if (!$result[0] > 0) {

            $result_id_d = $SERVER -> query("SELECT `id` FROM `vet_degilmintizaciya` ORDER BY `id` DESC LIMIT 1");
            $row_id_d = mysqli_fetch_array($result_id_d);
            $id_d = $row_id_d['id']+1;
            
            $result_id_v = $SERVER -> query("SELECT `id` FROM `vet_vaccine` ORDER BY `id` DESC LIMIT 1");
            $row_id_v = mysqli_fetch_array($result_id_v);
            $id_v = $row_id_v['id']+1;
            
            $result_id_o = $SERVER -> query("SELECT `id` FROM `vet_operation` ORDER BY `id` DESC LIMIT 1");
            $row_id_o = mysqli_fetch_array($result_id_o);
            $id_o = $row_id_o['id']+1;
            
            $queryLog = "INSERT INTO `vet_degilmintizaciya` (
                `id`, `preparat`, `id_veterinar`, `id_application`) VALUES (
                '".$id_d."', '".$degel_preparat."', '".$degel_vet."', '".$id_application."')";
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Добавил данные в вет. карточку - " . $queryLog);
            }

            $queryLog = "INSERT INTO `vet_vaccine` (
                `id`, `preparat`, `id_veterinar`, `id_application`) VALUES (
                '".$id_v."', '".$vac_preparat."', '".$degel_vet."', '".$id_application."')";
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Добавил данные в вет. карточку - " . $queryLog);
            }

            $queryLog = "INSERT INTO `vet_operation` (
                `id`, `id_veterinar`, `id_application`) VALUES (
                '".$id_o."', '".$degel_vet."', '".$id_application."')";
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Добавил данные в вет. карточку - " . $queryLog);
            }

            $queryLog = "INSERT INTO `vet_card` (
                `id`, `num_birka`, `num_chip`, `id_veterinar`, `id_degelmintizaciya`, `id_vaccine`, `id_operation`, 
                `id_application`, `comment`) VALUES (
                '".$id."', '".$num_birka."', '".$num_chip."', '".$degel_vet."', '".$id_d."', '".$id_v."', '".$id_o."',
                '".$id_application."', '".$comment."')";
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Добавил данные в вет. карточку - " . $queryLog);
            }

            $queryLog = "UPDATE `application` SET
                `id_vet_card`='".$id."', `num_aviary`='".$num_aviary."', `id_gender`='".$gender."'
                WHERE `id`='".$id_application."';";
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Обновил в карточке животного id вет. карточки - " . $queryLog);
            }

            $result = mysqli_fetch_array(mysqli_query($SERVER, "SELECT date_start_vosstanovlenie FROM application WHERE id=".$id_application));
            if($result[0] == "" && $_POST['check_vet'] == 1) {
                $vet_card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `vet_card` WHERE `id_application`=".$id_application));
                $queryCard = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `application` WHERE `id`=".$id_application));
                $startVosst = $queryCard['data'] + 950400;
                $queryLog = "UPDATE `vet_card` SET `data`='".date("Y-m-d\TH:i:s", $startVosst)."' WHERE `id`=".$vet_card['id'];
                if (mysqli_query($SERVER, $queryLog)) {
                    addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], $queryLog);
                }
                
                $queryLog = "UPDATE `vet_degilmintizaciya` SET `data`='".date("Y-m-d\TH:i:s", $startVosst)."' WHERE `id`=".$vet_card['id_degelmintizaciya'];
                if (mysqli_query($SERVER, $queryLog)) {
                    addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], $queryLog);
                }
                
                $queryLog = "UPDATE `vet_operation` SET `data`='".date("Y-m-d\TH:i:s", $startVosst)."' WHERE `id`=".$vet_card['id_operation'];
                if (mysqli_query($SERVER, $queryLog)) {
                    addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], $queryLog);
                }

                $queryLog = "UPDATE `vet_vaccine` SET `data`='".date("Y-m-d\TH:i:s", $startVosst)."' WHERE `id`=".$vet_card['id_vaccine'];
                if (mysqli_query($SERVER, $queryLog)) {
                    addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], $queryLog);
                }

                $queryLog = "UPDATE `application` SET `id_status`=3, `date_start_vosstanovlenie`='".$startVosst."' WHERE `id`='".$id_application."';";
                if (mysqli_query($SERVER, $queryLog)) {
                    addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], $queryLog);
                }
            }
        }

        $SERVER -> close();
        header("location: ../animal_card.php?application={$_GET['add_vet_card']}"); 
    }
    
    if (isset($_GET['update_vet_card'])) {
        $time_now = time();
        $id_application =$_GET['update_vet_card'];
        // echo($id_application);
        $vet_card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `vet_card` WHERE `id_application`=".$id_application));
        $vet_degilmintizaciya = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `vet_degilmintizaciya` WHERE id_application=".$id_application));
        $vet_operation = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `vet_operation` WHERE id_application=".$id_application));
        $vet_vaccine = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `vet_vaccine` WHERE id_application=".$id_application));
        // $date_vet = $date_degel = $date_vac = $date_operation = $_POST['date_vet_edit'];
        $num_birka = $_POST['num_birka_edit'];
        $num_chip = $_POST['num_chip-edit'];
        $degel_preparat = $_POST['degel_preparat_edit'];
        $operation_vet_edit = $_POST['operation_vet_edit'];
        if ($operation_vet_edit == 'selected' || $operation_vet_edit == '') $operation_vet_edit = 0;
        $degel_vet = $vac_vet = $operation_vet = $operation_vet_edit;
        $vac_preparat = $_POST['vac_preparat_edit'];
        $num_aviary = $_POST['num_aviary_edit'];
        $gender = $_POST['gender_vet_edit'];
        if ($gender == 'selected') $gender = 0;
        $comment = $_POST['comment_vet_edit'];

        $pieceQuery = "";
        $card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".$id_application));
        
        $queryLog = "UPDATE `vet_card` SET `num_birka`='".$num_birka."', 
        `num_chip`='".$num_chip."', `id_veterinar`='".$operation_vet."', 
        `id_degelmintizaciya`='".$vet_degilmintizaciya['id']."', `id_vaccine`='".$vet_vaccine['id']."', 
        `id_operation`='".$vet_operation['id']."', `comment`='".$comment."' WHERE `id`=".$vet_card['id'];
        if (mysqli_query($SERVER, $queryLog)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал вет карточку - " . $queryLog);
        }

        $queryLog = "UPDATE `vet_degilmintizaciya` SET `preparat`='".$degel_preparat."', `id_veterinar`='".$degel_vet."' WHERE `id`=".$vet_card['id_degelmintizaciya'];
        if (mysqli_query($SERVER, $queryLog)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал дополнения вет карточки - " . $queryLog);
        }

        $queryLog = "UPDATE `vet_operation` SET `id_veterinar`='".$operation_vet."' WHERE `id`=".$vet_card['id_operation'];
        if (mysqli_query($SERVER, $queryLog)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал дополнения вет карточки - " . $queryLog);
        }

        $queryLog = "UPDATE `vet_vaccine` SET `preparat`='".$vac_preparat."', `id_veterinar`='".$vac_vet."' WHERE `id`=".$vet_card['id_vaccine'];
        if (mysqli_query($SERVER, $queryLog)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал дополнения вет карточки - " . $queryLog);
        }

        $queryLog = "UPDATE `application` SET `num_aviary`='".$num_aviary."', `id_gender`='".$gender."' WHERE `id`='".$id_application."';";
        if (mysqli_query($SERVER, $queryLog)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал карточку животного (вольер, пол) - " . $queryLog);
        }

        $result = mysqli_fetch_array(mysqli_query($SERVER, "SELECT date_start_vosstanovlenie FROM application WHERE id=".$id_application));
        if($result[0] == "" && $_POST['check_vet_edit'] == 1) {
            $vet_card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `vet_card` WHERE `id_application`=".$id_application));
            $queryCard = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `application` WHERE `id`=".$id_application));
            $startVosst = $queryCard['data'] + 950400;
            $queryLog = "UPDATE `vet_card` SET `data`='".date("Y-m-d\TH:i:s", $startVosst)."' WHERE `id`=".$vet_card['id'];
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал дополнениявет карточку - " . $queryLog);
            }

            $queryLog = "UPDATE `vet_degilmintizaciya` SET `data`='".date("Y-m-d\TH:i:s", $startVosst)."' WHERE `id`=".$vet_card['id_degelmintizaciya'];
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал дополнения вет карточки - " . $queryLog);
            }

            $queryLog = "UPDATE `vet_operation` SET `data`='".date("Y-m-d\TH:i:s", $startVosst)."' WHERE `id`=".$vet_card['id_operation'];
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал дополнения вет карточки - " . $queryLog);
            }

            $queryLog = "UPDATE `vet_vaccine` SET `data`='".date("Y-m-d\TH:i:s", $startVosst)."' WHERE `id`=".$vet_card['id_vaccine'];
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал дополнения вет карточки - " . $queryLog);
            }

            $queryLog = "UPDATE `application` SET `id_status`=3, `date_start_vosstanovlenie`='".$startVosst."' WHERE `id`='".$id_application."';";
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал карточку животного (старт восстановления) - " . $queryLog);
            }
        }

        $SERVER -> close();
        header("location: ../animal_card.php?application={$_GET['update_vet_card']}"); 
    }
    
    if (isset($_GET['update-foto-vet'])) {
        $time_now = time();
        $id = prepare($_GET['id']);
        $files_to_delete = explode(',', $_GET['files_to_delete']);
        
        $allow = array('video/mp4', 'video/avi', 'video/3gp', 'video/mov', 'image/png', 'image/jpeg', 'image/jpg', 'image/bmp');
        $imagetype = array('image/png', 'image/jpeg', 'image/jpg', 'image/bmp'); 
        // Полный путь до upload директории.
        $full_path = $_SERVER['DOCUMENT_ROOT'] . '/assets/files/uploads/';
        $thumbs_path = $_SERVER['DOCUMENT_ROOT'] . '/assets/files/thumbs/';

        $vet_card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `vet_card` WHERE `id_application`=".$id));
        $queryCard = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `application` WHERE `id`=".$id));
        $startVosst = $queryCard['data'] + 950400;
        $queryLog = "UPDATE `vet_card` SET `data`='".date("Y-m-d\TH:i:s", $startVosst)."' WHERE `id`=".$vet_card['id'];
        if (mysqli_query($SERVER, $queryLog)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], $queryLog);
        }
        mysqli_query($SERVER, "UPDATE `vet_degilmintizaciya` SET `data`='".date("Y-m-d\TH:i:s", $startVosst)."' WHERE `id`=".$vet_card['id_degelmintizaciya']);
        mysqli_query($SERVER, "UPDATE `vet_operation` SET `data`='".date("Y-m-d\TH:i:s", $startVosst)."' WHERE `id`=".$vet_card['id_operation']);
        mysqli_query($SERVER, "UPDATE `vet_vaccine` SET `data`='".date("Y-m-d\TH:i:s", $startVosst)."' WHERE `id`=".$vet_card['id_vaccine']);
        
        $result = mysqli_fetch_array(mysqli_query($SERVER, "SELECT date_start_vosstanovlenie FROM application WHERE id=".$id));
        if($result[0] == "") {
            $queryLog = "UPDATE `application` SET `id_status`=3, `date_start_vosstanovlenie`='".$startVosst."' WHERE `id`='".$id."';";
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], $queryLog);
            }
        }
        
        if (!is_dir($full_path)) {
            mkdir($full_path, 0777, true);
        }
        foreach($_FILES as $file){
            $new_file_name = generate_random_string(11).'.'.pathinfo($file['name'], PATHINFO_EXTENSION);
            if (in_array($file['type'], $allow))
                if (move_uploaded_file($file['tmp_name'], $full_path . $new_file_name))
                    if (in_array($file['type'], $imagetype)){
                        $filetype = "img";
                        mysqli_query($SERVER, "INSERT INTO `upload_files` (type, path_file, path_thumb, name_file, name_thumb, data, actual, id_user, id_application, num_str, id_user_group) VALUES ('".$filetype."', '/assets/files/uploads/', '/assets/files/thumbs/', '".$new_file_name."', '".$new_file_name."', '".$time_now."', '1', '".$_SESSION['id_user']."', '".$id."', '1', '3')");
                        ResizeImage($full_path.$new_file_name, 200, 100, $thumbs_path, $new_file_name);
                    }
                    
            }              
        $files_to_delete = array_unique($files_to_delete);
        if ($files_to_delete[0] > 0) {
            for ($i = 0; $i < count($files_to_delete); $i++) { 
                $delete_file = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM upload_files WHERE id=".$files_to_delete[$i]));
                if (mysqli_query($SERVER, "DELETE FROM `upload_files` WHERE id=".$delete_file['id'])) {
                    unlink($full_path.$delete_file['name_file']);
                    unlink($thumbs_path.$delete_file['name_file']);
                }
            }
        }
    }
    
    if (isset($_GET['update-video-vet'])) {
        $time_now = time();
        $id = prepare($_GET['id']);
        $files_to_delete = explode(',', $_GET['files_to_delete']);
        
        $allow = array('video/mp4', 'video/avi', 'video/3gp', 'video/mov', 'image/png', 'image/jpeg', 'image/jpg', 'image/bmp');
        $videotype = array('video/mp4', 'video/avi', 'video/3gp', 'video/mov');
        // Полный путь до upload директории.
        $full_path = $_SERVER['DOCUMENT_ROOT'] . '/assets/files/uploads/';
        $thumbs_path = $_SERVER['DOCUMENT_ROOT'] . '/assets/files/thumbs/';

        $vet_card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `vet_card` WHERE `id_application`=".$id));
        $queryCard = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `application` WHERE `id`=".$id));
        $startVosst = $queryCard['data'] + 950400;
        $queryLog = "UPDATE `vet_card` SET `data`='".date("Y-m-d\TH:i:s", $startVosst)."' WHERE `id`=".$vet_card['id'];
        if (mysqli_query($SERVER, $queryLog)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал вет карточку - " . $queryLog);
        }
        mysqli_query($SERVER, "UPDATE `vet_degilmintizaciya` SET `data`='".date("Y-m-d\TH:i:s", $startVosst)."' WHERE `id`=".$vet_card['id_degelmintizaciya']);
        mysqli_query($SERVER, "UPDATE `vet_operation` SET `data`='".date("Y-m-d\TH:i:s", $startVosst)."' WHERE `id`=".$vet_card['id_operation']);
        mysqli_query($SERVER, "UPDATE `vet_vaccine` SET `data`='".date("Y-m-d\TH:i:s", $startVosst)."' WHERE `id`=".$vet_card['id_vaccine']);
        
        $result = mysqli_fetch_array(mysqli_query($SERVER, "SELECT date_start_vosstanovlenie FROM application WHERE id=".$id));
        if($result[0] == "") {
            $queryLog = "UPDATE `application` SET `id_status`=3, `date_start_vosstanovlenie`='".$startVosst."' WHERE `id`='".$id."';";
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал карточку животного ( поместил на восстановление) - " . $queryLog);
            }
        }

        if (!is_dir($full_path)) {
            mkdir($full_path, 0777, true);
        }
        foreach($_FILES as $file){
            $new_file_name = generate_random_string(11).'.'.pathinfo($file['name'], PATHINFO_EXTENSION);
            if (in_array($file['type'], $allow))
                if (move_uploaded_file($file['tmp_name'], $full_path . $new_file_name))
                    if (in_array($file['type'], $videotype)){
                        $filetype = "vid";
                        mysqli_query($SERVER, "INSERT INTO `upload_files` (type, path_file, path_thumb, name_file, name_thumb, data, actual, id_user, id_application, num_str, id_user_group) VALUES ('".$filetype."', '/assets/files/uploads/', '', '".$new_file_name."', '".$new_file_name."', '".$time_now."', '1', '".$_SESSION['id_user']."', '".$id."', '1', '3')");
                    }
                    
            }              
        $files_to_delete = array_unique($files_to_delete);
        if ($files_to_delete[0] > 0) {
            for ($i = 0; $i < count($files_to_delete); $i++) { 
                $delete_file = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM upload_files WHERE id=".$files_to_delete[$i]));
                if (mysqli_query($SERVER, "DELETE FROM `upload_files` WHERE id=".$delete_file['id'])) {
                    unlink($full_path.$delete_file['name_file']);
                    unlink($thumbs_path.$delete_file['name_file']);
                }
            }
        }
    }

?>