<?php
	require_once("config.php");
    require_once("timezone.php");
    require_once("functions.php");
    $time_now = time();
    $today = date("Y-m-d\TH:i");
    $rowsData = array();
    function format_interval(DateInterval $interval) {
        $result = "";
        if ($interval->d) { $result .= $interval->format("%d д. "); }
        if ($interval->h) { $result .= $interval->format("%h ч. "); }
        if ($interval->i) { $result .= $interval->format("%i мин. "); }
    
        return $result;
    };
	if (isset($_GET['gen_table'])) {
		if (isset($_GET['show_more'])) {
			$limit_constr = $_POST['start_from'].', '.$_POST['row_nums'];
		}
		else
			$limit_constr = $_SESSION['row_limit'];
		
		$shelter_constr = $_SESSION['filter_id_shelter'] == "all" ? "" : "AND id_shelter=".$_SESSION['filter_id_shelter'];
		$kinolog_constr = $_SESSION['filter_kinolog'] == "all" ? "" : "AND id_user=".$_SESSION['filter_kinolog'];
		$show_status_constr = $_SESSION['show_status'] == "all" ? "" : "AND id_status=".$_SESSION['show_status'];
		$show_contract = $_SESSION['show_contract'] == "all" ? "" : "AND id_contract=".$_SESSION['show_contract'];

		$locality_construct = $_SESSION['filter_locality'] == "all" ? "" : "AND locality='".$_SESSION['filter_locality']."'";
		$filter_first_date = $_SESSION['filter_first_date'] == "first" ? "" : strtotime($_SESSION['filter_first_date']);
		$filter_last_date = $_SESSION['filter_last_date'] == "last" ? "" : strtotime($_SESSION['filter_last_date']);
		$region_constr = $_SESSION['id_region'] == "all" ? "" : "AND application.id_region='".$_SESSION['id_region']."'"; 
		$filter_aviarys = $_SESSION['filter_aviarys'] == "" ? "" : "AND `num_aviary`='".$_SESSION['filter_aviarys']."'"; //
		$search_request = $_SESSION['search_request'] == "" ? "" : " AND (application.id =".$_SESSION['search_request']." OR application.id=(SELECT vet_card.id_application FROM vet_card WHERE vet_card.num_birka=".$_SESSION['search_request']."))";
		$query = "SELECT 
                application.id AS id,
                application.id_user AS id_user,
                application.data AS data,
                application.street AS street,
                application.loc_type AS loc_type,
                application.locality AS locality,
                application.id_status AS id_status,
				application.num_aviary AS num_aviary,
                application.breed AS breed, 
                application.age AS age, 
                application.weight AS weight, 
                application.height AS height, 
                application.color AS color,
				application.comment AS comment,
                application.date_start_vosstanovlenie AS date_start_vosstanovlenie,
                application.date_start_karantin AS date_start_karantin,
                application.id_shelter AS id_shelter,
                animal_kind.name_kind AS kind,
                animal_gender.name_gender AS gender,
                application_status.id AS status_id,
                application_status.status_name AS status,
                vet_card.id_veterinar AS id_veterinar,
                vet_card.num_birka AS num_birka,
                vet_card.num_chip AS num_chip,
                DATE_FORMAT(vet_card.data,'%d.%m.%Y') AS vet_card_data,
                animal_shelters.name_shelter AS name_shelter,
                users.sourname AS user_sourname,
                users.name AS user_name,
                users.patronymic AS user_patronymic,
                contract.number AS contract_number,
                contract.customer AS contract_customer,
                contract.name_contract AS name_contract,
                DATE_FORMAT(contract.date_contract,'%d.%m.%Y') AS date_contract,
                animal_color.name_color AS name_color
                FROM application 
                LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
                LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
                LEFT JOIN application_status ON application.id_status = application_status.id
                LEFT JOIN vet_card ON application.id = vet_card.id_application 
                LEFT JOIN animal_shelters ON application.id_shelter = animal_shelters.id
                LEFT JOIN users ON application.id_user = users.id
                LEFT JOIN contract ON application.id_contract = contract.id
                LEFT JOIN animal_color ON application.color = animal_color.id
                WHERE application.data BETWEEN ".$filter_first_date." AND ".$filter_last_date." AND application.show='1' ".$search_request." ".$shelter_constr." ".$filter_aviarys." ".$region_constr." ".$locality_construct." ".$show_status_constr." ".$show_contract." ".$kinolog_constr." ORDER BY `".$_SESSION['order_by']."` ".$_SESSION['sort_by']." LIMIT ".$limit_constr;        
			//file_put_contents('qwe.txt', $query);
			$res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
			$row_count = 0;

		while ($res = mysqli_fetch_array($res_query)) {
			$row_count++;
			$res_image = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `upload_files` WHERE `id_user`='".$res['id_user']."' AND `id_application`='".$res['id']."' AND `type`='img'"));
			$end = date('Y-m-d H:i', $res['date_start_vosstanovlenie']+777600);
            $first_date = new DateTime($today);
            $second_date = new DateTime($end);
            $difference = $first_date->diff($second_date);
            $interval = format_interval($difference);
            
            $end1 = date('Y-m-d H:i', $res['date_start_karantin']+777600);
            $first_date1 = new DateTime($today);
            $second_date1 = new DateTime($end1);
            $difference1 = $first_date1->diff($second_date1);
            $interval1 = format_interval($difference1);

			$name_string = ""; 
				
			// Ответственный
			if ($res['id_status'] == 1 || $res['id_status'] == 2 || $res['id_status'] == 4 || $res['id_status'] == 7 || $res['id_status'] == 8 || $res['id_status'] == 9 || $res['id_status'] == 11 ) {
				$sourname_user = $res['user_sourname'];
				$name_user = mb_substr($res['user_name'], 0, 1);
				$patronymic_user = mb_substr($res['user_patronymic'], 0, 1); 
				$name_string = $sourname_user." ".$name_user.". ".$patronymic_user.".";
			}
			else if ($res['id_status'] == 3 || $res['id_status'] == 10) { // ветеринар
				// $vet = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$res['id_veterinar']));
				// if (!empty($vet['id'])) {
				//     $sourname_user = $vet['sourname'];
				//     $name_user = mb_substr($vet['name'], 0, 1);
				//     $patronymic_user = mb_substr($vet['patronymic'], 0, 1);
				//     $name_string = $sourname_user." ".$name_user.". ".$patronymic_user."."; 
				// }
			}
			else if ($res['id_status'] == 5 || $res['id_status'] == 6 || $res['id_status'] == 12) { // приют
				$adm_shelter = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE shelter_id=".$res['id_shelter'])); 
				if (!empty($adm_shelter['id']) && $adm_shelter['shelter_id'] != 0 ) {
					$sourname_user = $adm_shelter['sourname'];
					$name_user = mb_substr($adm_shelter['name'], 0, 1);
					$patronymic_user = mb_substr($adm_shelter['patronymic'], 0, 1);
					$name_string = $sourname_user." ".$name_user.". ".$patronymic_user."."; 
				}
			}
            
            if(!empty($res_image)) {
				$path = $res_image['path_thumb'].$res_image['name_thumb'];
				$url = "https://baikalvet.ru".$path;
                $urlHeaders = @get_headers($url);
                // проверяем ответ сервера на наличие кода: 200 - ОК
                if(strpos($urlHeaders[0], '200')) {
                    $img = $path;
				}
				else {
				    $img = $res_image['path_thumb'].$res_image['name_thumb'];
				}
		    }   
			else
				$img = "assets/images/no-image.png";
        	
			$start_kar = "";
			if ($res['date_start_karantin'] != 0) $start_kar = date('d.m.Y H:i', $res['date_start_karantin']);

			$rowsData[] = "
				<tr data-href='/animal_card.php?application={$res['id']}' class='application-mobile'>
					<td>
						<div class='block-head'>
							<div class='id-color block-head-num'>{$_SESSION['num_tr']}</div>
							<div class='block-head-date'>".date('d.m.Y H:i', $res['data'])."</div>
							<div class='block-head-adress' style='font-size: 10px;'>{$res['id']}</div>";
							if($res['status_id'] == 7 || $res['status_id'] == 8)
								$rowsData[] = "<div class='block-head-status die-color'>";
							else
								$rowsData[] = "<div class='block-head-status'>";
								if($res['status_id'] == 2)
									$rowsData[] .= "{$res['status']}<br> <div class='status-time'>до ".date("d.m.Y", ($res['date_start_karantin']+777600))."</div>";
								else if($res['status_id'] == 3)
									$rowsData[] .= "{$res['status']}<br> <div class='status-time'>до ".date("d.m.Y", ($res['date_start_vosstanovlenie']+777600))."</div>";
								else
									$rowsData[] .= "{$res['status']}";
			$rowsData[] = "         
						</div>
					</div>
					<div class='block-content'>
						<div class='block-content-img'><img src='{$img}' class='table-img'></div>
						<div class='block-content1' style='display: block;'>
							<div class='block-content-info'>
								<ul>
									<li><b>Дата начала карантина:</b> ".$start_kar."</li>
									<li><b>Место отлова:</b> {$res['loc_type']} {$res['locality']}, {$res['street']}</li>
									<li><b>Контракт:</b> {$res['name_contract']}</li>
									<li><b>Ответственный:</b> {$name_string}</li>
								</ul>
							</div>
							<div class='block-content-info' style='display:flex;'>
								<ul style='width: 80px;'>
									<li>{$res['kind']}</li>
									<li>{$res['gender']}</li>
									<li>{$res['breed']}</li>
									<li>возраст: {$res['age']}</li>
									<li>окрас: {$res['color']}</li>
									<li>вес: {$res['weight']}</li>
								</ul>
								<ul>
									<li>приют: {$res['name_shelter']}</li>
									<li>вольер: {$res['num_aviary']}</li>
									<li>№ бирки: {$res['num_birka']}</li>
									<li>№ чипа: {$res['num_chip']}</li>
									<li>дата мероприятий: {$res['vet_card_data']}</li>
								</ul>
							</div>
						</div>
					</div>
					<div class='block-comment'>{$res['comment']}</div>
				</td>
			</tr>"; 
            
               				
        	$_SESSION['num_tr'] = $_SESSION['num_tr'] + 1;        
		}
		$html = "";
	    foreach($rowsData as $row){
	        $html .= $row;
	    }
	    echo json_encode(array(
	        'result'    => 'success',
	        'html'      => $html,
	        'row_count' => $row_count
	    ));
	}
	if (isset($_GET['update_table_config'])) {
	    $_SESSION['num_tr'] = 1;
		$_SESSION['filter_id_shelter'] = intval($_POST['id_shelter']);
		switch ($_POST['show_status']) {
			case '0':
				$_SESSION['show_status'] = 'all';
				break;
			case '1':
				$_SESSION['show_status'] = '1';
				break;
			case '2':
				$_SESSION['show_status'] = '2';
				break;
			case '3':
				$_SESSION['show_status'] = '3';
				break;
			case '4':
				$_SESSION['show_status'] = '4';
				break;
			case '5':
				$_SESSION['show_status'] = '5';
				break;
			case '6':
				$_SESSION['show_status'] = '6';
				break;
			case '7':
				$_SESSION['show_status'] = '7';
				break;
			case '8':
				$_SESSION['show_status'] = '8';
				break;
			case '9':
				$_SESSION['show_status'] = '9';
				break;
			case '10':
				$_SESSION['show_status'] = '10';
				break;
			case '11':
				$_SESSION['show_status'] = '11';
				break;
			case '12':
				$_SESSION['show_status'] = '12';
				break;
			default:
				$_SESSION['show_status'] = 'all';
				break;
		}
		
		if ($_POST['show_contract'] == 0) {
            $_SESSION['show_contract'] = 'all';
        }
        $res_query = mysqli_query($SERVER, "SELECT id AS id FROM contract WHERE show_contract=1") or die("Ошибка " . mysqli_error($SERVER));
		while ($res = mysqli_fetch_array($res_query)) {
            if ($_POST['show_contract'] == $res['id']) {
                $_SESSION['show_contract'] = $res['id'];
            } 
		}

		if ($_POST['kinolog'] == 0) {
            $_SESSION['filter_kinolog'] = 'all';
        }
        $res_query = mysqli_query($SERVER, "SELECT id AS id FROM `users` WHERE `show` = 1 AND `podtverzhdeno` = 1 AND `id_users_group` = 4") or die("Ошибка " . mysqli_error($SERVER));
		while ($res = mysqli_fetch_array($res_query)) {
            if ($_POST['kinolog'] == $res['id']) {
                $_SESSION['filter_kinolog'] = $res['id'];
            } 
		}

		switch ($_POST['order_by']) {
			case '1':
				$_SESSION['order_by'] = 'data';
				break;
			case '2':
				$_SESSION['order_by'] = 'locality';
				break;
			case '3':
				$_SESSION['order_by'] = 'street';
				break;
			case '4':
				$_SESSION['order_by'] = 'id_kind';
				break;
			case '5':
				$_SESSION['order_by'] = 'breed';
				break;			
			case '6':
				$_SESSION['order_by'] = 'age';
				break;
			case '7':
				$_SESSION['order_by'] = 'color';
				break;
			case '8':
				$_SESSION['order_by'] = 'id';
				break;
			case '9':
				$_SESSION['order_by'] = 'num_aviary';
				break;
			default:
				$_SESSION['order_by'] = 'data';
				break;
		}
		switch ($_POST['sort_by']) {
			case 'DESC':
				$_SESSION['sort_by'] = 'DESC';
				break;
			case 'ASC':
				$_SESSION['sort_by'] = 'ASC';
				break;
			default:
				$_SESSION['sort_by'] = 'ASC';
				break;
		}
		switch ($_POST['row_limit']) {
			case '25':
				$_SESSION['row_limit'] = '25';
				break;
			case '50':
				$_SESSION['row_limit'] = '50';
				break;
			case '75':
				$_SESSION['row_limit'] = '75';
				break;
			case '100':
				$_SESSION['row_limit'] = '100';
				break;
			default:
				$_SESSION['row_limit'] = '25';
				break;
		}
		switch ($_POST['filter_locality']) {
			case 'all':
				$_SESSION['filter_locality'] = "all";
				break;
			default:
				$_SESSION['filter_locality'] = $_POST['filter_locality'];
				break;
		}
		if ($_POST['filter_first_date'] == "first") {
			$_SESSION['filter_first_date'] = $sql = mysqli_fetch_array(mysqli_query($SERVER, "SELECT MIN(`data`) AS filter_first_date FROM `application`"));
		}
		else {
			$_SESSION['filter_first_date'] = $_POST['filter_first_date'];
			
		}
		if ($_POST['filter_last_date'] == "last") {
			$_SESSION['filter_last_date'] = $sql = mysqli_fetch_array(mysqli_query($SERVER, "SELECT MAX(`data`) AS filter_last_date FROM `application`"));
		}
		else {
			$_SESSION['filter_last_date'] = $_POST['filter_last_date'];
			
		}
		if ($_POST['filter_aviarys'] == "")
			$_SESSION['filter_aviarys'] = "";
		else
			$_SESSION['filter_aviarys'] = $_POST['filter_aviarys'];
		echo json_encode(array('res'    => 'success'));
	}
?>