<?php
    require_once("config.php");
    require_once("timezone.php");
    require_once("functions.php");
    $time_now = time();

    if(isset($_GET['edit_status'])) {
        
        $query = "UPDATE `application` SET `id_status`='3', `date_start_vosstanovlenie`='".$_GET['start_vet_date']."' WHERE id=".$_GET['edit_status'];
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Изменил статус на 'На восстановлении' - " . $query);
        }
        $SERVER -> close();
        header("location: ../animal_card.php?application={$_GET['edit_status']}"); 
    }
    else if(isset($_GET['edit_status_die'])) {
        
        $query = "UPDATE `application` SET `id_status`='7', `id_sostoyanie`='2' WHERE id=".$_GET['edit_status_die'];
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Изменил статус на 'Падеж' - " . $query);
        }
        $SERVER -> close();
        header("location: ../animal_card.php?application={$_GET['edit_status_die']}"); 
    }
    else if(isset($_GET['edit_status_die1'])) {
        
        $query = "UPDATE `application` SET `id_status`='8', `id_sostoyanie`='2' WHERE id=".$_GET['edit_status_die1'];
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Изменил статус на 'Умерщвлено' - " . $query);
        }
        $SERVER -> close();
        header("location: ../animal_card.php?application={$_GET['edit_status_die1']}"); 
    }
    else if(isset($_GET['num_aviary'])) {
        
        $query = "UPDATE `application` SET `num_aviary`='".$_POST['num_aviary']."' WHERE id=".$_GET['num_aviary'];
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Изменил вольер' - " . $query);
        }
        $SERVER -> close();
        header("location: ../animal_card.php?application={$_GET['num_aviary']}"); 
    }
    else if(isset($_GET['perezaseleno'])) {
        
        $query = "UPDATE `application` SET `num_aviary`='".$_POST['num_aviary']."', `id_status`='2', `date_start_karantin`='".$time_now."', `perezaseleno`='1' WHERE id=".$_GET['perezaseleno'];
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Перезаселил животное' - " . $query);
        }
        $SERVER -> close();
        header("location: ../animal_card.php?application={$_GET['perezaseleno']}"); 
    }
    else if(isset($_GET['edit_status_vladelets'])) {
        $seriya = $num = 0;
        if ($_POST['seriya'] != "") $seriya = $_POST['seriya'];
        if ($_POST['num'] != "") $num = $_POST['num'];
        
        $query = "UPDATE `application` SET `id_status`='5', `id_sostoyanie`='2' WHERE id=".$_GET['edit_status_vladelets'];
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Изменил статус на 'Возвращено владельцу' - " . $query);
        }
        $query = "INSERT INTO `animal_owners` (id_application, data, sourname_owner, name_owner, patronymic_owner, phone, passport_seriya, passport_num, passport_vidan) 
                                VALUES ('".$_GET['edit_status_vladelets']."', '".$_POST['date']."', '".$_POST['sourname']."', '".$_POST['name']."', '".$_POST['patronymic']."', '".$_POST['phone']."', '".$seriya."', '".$num."', '".$_POST['vidan']."')";
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Добавил карточку владельца - " . $query);
        }
        $SERVER -> close();
        header("location: ../animal_card.php?application={$_GET['edit_status_vladelets']}"); 
    }
    else if(isset($_GET['edit_owner'])) {
        $seriya = $num = 0;
        if ($_POST['eo_seriya'] != "") $seriya = $_POST['eo_seriya'];
        if ($_POST['eo_num'] != "") $num = $_POST['eo_num'];
        
        $query = "UPDATE `animal_owners` SET 
        `data`='".$_POST['eo_date']."', 
        `sourname_owner` = '".$_POST['eo_sourname']."', 
        `name_owner`= '".$_POST['eo_name']."', 
        `patronymic_owner`= '".$_POST['eo_patronymic']."', 
        `phone` = '".$_POST['eo_phone']."', 
        `passport_seriya` = '".$seriya."', 
        `passport_num` = '".$num."', 
        `passport_vidan` = '".$_POST['eo_vidan']."' 
        WHERE `id_application`=".$_GET['edit_owner'];
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал карточку владельца - " . $query);
        }
        $SERVER -> close();
        header("location: ../animal_card.php?application={$_GET['edit_owner']}"); 
    }
    else if(isset($_GET['edit_status_shelter'])) {
        $query = "UPDATE `application` SET `id_status`='12', `id_sostoyanie`='2', `id_shelter`='".$_POST['shelter']."', `num_aviary`='".$_POST['num']."', `date_vipusk`='".$_POST['date']."' WHERE id=".$_GET['edit_status_shelter'];
        //$query = "UPDATE `application` SET `id_status`='12', `id_sostoyanie`='2', `id_shelter`='".$_POST['shelter']."', `num_aviary`='".$_POST['num']."' WHERE id=".$_GET['edit_status_shelter'];//правка от 15.02, смена даты выбытия из вольера
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Изменил статус на 'Передано в приют' - " . $query);
        }
        //15.02 правка с именем приюта при передаче в новый приют
        $shelter = mysqli_fetch_array(mysqli_query($SERVER, "SELECT `name_shelter` FROM `animal_shelters` WHERE `id`=".$_POST['shelter']));
        $query = "INSERT INTO `animal_owners` (id_application, data, name_shelter, phone, adress, num_shelter) 
                                VALUES ('".$_GET['edit_status_shelter']."', '".$_POST['date']."', '".$shelter['name_shelter']."', '".$_POST['phone']."', '".$_POST['adress']."', '".$_POST['num']."')";
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], $query);
        }
        $SERVER -> close();
        header("location: ../animal_card.php?application={$_GET['edit_status_shelter']}"); 
    }
    else{
        header("location: ../animal_card.php"); 
    }

?>