<?php
	require_once("config.php");
    require_once("timezone.php");
    require_once("functions.php");
    $time_now = time();
    $rowsData = array();
	if (isset($_GET['get_notifications'])) {
		$get_notification = mysqli_query($SERVER, "SELECT * FROM `notifications` WHERE `id_user`='".$_SESSION['id_user']."' AND `show`=1");
		$html = "";
        while ($notification = mysqli_fetch_array($get_notification)) {
            if ($notification['type'] == "kinolog_new_task"){
                $rowsData[] .= 
                    "<div data-id='".$notification['id']."'>
                        <span>".date("d-m-Y\ H:i", $notification['date'])."</span>
                        &nbsp&nbsp&nbsp
                        <span>".$notification['text']."</span>
                        &nbsp&nbsp&nbsp
                        <a href='#' class='hide_notification'> Скрыть уведомление</a>
                        <br><br>
                    </div>";
            }
        }
	    foreach($rowsData as $row){
	        $html .= $row;
	    }
        if ($html == "")
            $html .= "<div class='row'>Уведомлений нет</div>";
	    echo json_encode(array(
	        'result'    => 'success',
	        'html'      => $html
	    ));

	}
    if (isset($_GET['hide_notification'])) {
        $query = "UPDATE `notifications` SET `show`='0' WHERE `id`=".$_POST['id_row'];
        if (!mysqli_query($SERVER, $query))
            file_put_contents("logs/notifications_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$query." - Ошибка"."\n", FILE_APPEND);
        else {
            file_put_contents("logs/notifications_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$query." - Успех"."\n", FILE_APPEND);
            echo json_encode(array(
                'result'    => 'success'));
        }
    }
    if (isset($_GET['get_notifications_count'])) {
        $cn = mysqli_fetch_array(mysqli_query($SERVER, "SELECT COUNT(`id`) AS `count` FROM `notifications` WHERE `id_user`='".$_SESSION['id_user']."' AND `show`=1"));
        echo json_encode(array(
            'result'    => 'success',
            'counter_notifications'      => $cn['count']
        ));
    }
?>