<?php
	require_once('config.php');
    require_once("timezone.php");
    require_once("functions.php");
	setlocale(LC_ALL, 'ru_RU.utf8');	
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	mb_http_output('UTF-8');
	mb_language('uni');
	
	if (isset($_GET['add_vet_card'])) { 

        $result_id = $SERVER -> query("SELECT `id` FROM `vet_card` ORDER BY `id` DESC LIMIT 1");
        $row_id = mysqli_fetch_array($result_id);
        $id = $row_id['id']+1;
        
        $result_id_d = $SERVER -> query("SELECT `id` FROM `vet_degilmintizaciya` ORDER BY `id` DESC LIMIT 1");
        $row_id_d = mysqli_fetch_array($result_id_d);
        $id_d = $row_id_d['id']+1;
        
        $result_id_v = $SERVER -> query("SELECT `id` FROM `vet_vaccine` ORDER BY `id` DESC LIMIT 1");
        $row_id_v = mysqli_fetch_array($result_id_v);
        $id_v = $row_id_v['id']+1;
        
        $result_id_o = $SERVER -> query("SELECT `id` FROM `vet_operation` ORDER BY `id` DESC LIMIT 1");
        $row_id_o = mysqli_fetch_array($result_id_o);
        $id_o = $row_id_o['id']+1;
        
        $query = "INSERT INTO `vet_degilmintizaciya` (
            `id`, `preparat`, `data`, `id_veterinar`, `id_application`) VALUES (
            '".$id_d."',
            '".$_POST['degel_preparat']."',
            '".$_POST['date_degel']."',
            '".$_POST['degel_vet']."',
            '".$_GET['add_vet_card']."')";
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Создал дополнение карточки ветеринара - " . $query);
        }
            
        $query = "INSERT INTO `vet_vaccine` (
            `id`, `preparat`, `data`, `id_veterinar`, `id_application`) VALUES (
            '".$id_v."',
            '".$_POST['vac_preparat']."',
            '".$_POST['date_vac']."',
            '".$_POST['vac_vet']."',
            '".$_GET['add_vet_card']."')";
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Создал дополнение карточки ветеринара - " . $query);
        }
            
        $query = "INSERT INTO `vet_operation` (
            `id`, `data`, `id_veterinar`, `id_application`) VALUES (
            '".$id_o."',
            '".$_POST['date_operation']."',
            '".$_POST['operation_vet']."',
            '".$_GET['add_vet_card']."')";
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Создал дополнение карточки ветеринара - " . $query);
        }
            
        $query = "INSERT INTO `vet_card` (
            `id`, `data`, `num_birka`, `num_chip`, `id_veterinar`, `id_degelmintizaciya`, `id_vaccine`, `id_operation`, `id_application`) VALUES (
            '".$id."',
            '".$_POST['date_vet']."',
            '".$_POST['num_birka']."',
            '".$_POST['num_chip']."',
            '".$_SESSION['id_user']."',
            '".$id_d."',
            '".$id_v."',
            '".$id_o."',
            '".$_GET['add_vet_card']."')";
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Создал карточку ветеринара - " . $query);
        }
        
        $query = "UPDATE `application` SET
            `id_vet_card`='".$id."'
            WHERE `id`='".$_GET['add_vet_card']."';";
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Обновил id карточки ветеринара в карточке животного - " . $query);
        }

        $SERVER -> close();
        header('location: ../' );
    }
    else   
        echo 'error';

?>