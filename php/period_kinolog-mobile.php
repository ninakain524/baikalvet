<?php
    require_once("../php/config.php");
    require_once("timezone.php");
    require_once("functions.php");
    if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
    
    $data_first = $data_second = "";
    
    $today = date("Y-m-d\TH:i:s");
    $today1 = date("Y-m-d");
    
    $_SESSION['num_tr'] = 1;

    $last_query = " AND (`id_status`=1 OR `id_status`=2 OR `id_status`=11 OR `id_status`=7 OR `id_status`=8) ";
    
    function format_interval(DateInterval $interval) {
        $result = "";
        if ($interval->d) { $result .= $interval->format("%d д. "); }
        if ($interval->h) { $result .= $interval->format("%h ч. "); }
        if ($interval->i) { $result .= $interval->format("%i мин. "); }
    
        return $result;
    };
    
    if (isset($_POST['data_first1'])) {
    
    $data_def = $_POST['data_def1'];
    $data_first = $_POST['data_first1'];
    $data_second = $_POST['data_second1'];
    
    $query = "SELECT * FROM `application` WHERE `data` >= '".$data_first."' AND `data` <= '".$data_second."' AND `id_sostoyanie`='1' AND `show`='1' AND `id_user`='".$_SESSION['id_user']."' AND (`id_status`=1 OR `id_status`=2 OR `id_status`=11 OR `id_status`=7 OR `id_status`=8) ORDER BY `id` DESC";
    $res = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
    $adress = "";
    $item_adress = "";
    $street = "";
    while($row = mysqli_fetch_array($res))
    {   
        if ($street == $row['street']){
            $item_adress = "";
            $street = "";
        }
        else{
            $street = $row['street'];
            $item_adress = $row['locality'] . ", " . $street . "; "; 
        }
           
        $adress = $adress . $item_adress; 
    }
    
    $card = mysqli_fetch_array(mysqli_query($SERVER, $query));
    $count = mysqli_fetch_array(mysqli_query($SERVER, "SELECT COUNT(1) FROM `application` WHERE `data` >= '".$data_first."' AND `data` <= '".$data_second."' AND `id_sostoyanie`='1' AND `show`='1' AND `id_user`='".$_SESSION['id_user']."' AND (`id_status`=1 OR `id_status`=2 OR `id_status`=11 OR `id_status`=7 OR `id_status`=8) ORDER BY `id` DESC"));
    
    
                    
    if(!empty($card)){
    $user = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$card['id_user']));
        
    $name = $user['name'];
    $name1 = mb_substr($name, 0, 1);
    $patronymic = $user['patronymic'];
    $patronymic1 = mb_substr($patronymic, 0, 1);
    
?>
    <!--Формирование-->
      <div id="hide-form-popup-num_aviary" class="hide-form-popup-num_aviary hideform fancy-animate">
        <form id="popup-num_aviary" method="post" action="../libs/phpword/generate_word.php?data_first=<?php echo ($data_first) ?>&data_second=<?php echo ($data_second) ?>" enctype="multipart/form-data">
          <h3>Формирование акта отлова за период</h3>
          <div class="form-group">
            <p>Дата</p>
            <input class="field width-add field-date" type="date" name="data" autocomplete="on" value="<?php echo $data_def; ?>" />  
          </div>
          <div class="form-group">
             <p>Место отлова</p>
            <input class="field width-add" type="text" name="mesto" value="<? echo ($adress) ?>" />  
          </div>
          <div class="form-group">
              <p>Кинолог</p>
                <input class="field width-add" type="text" name="kinolog" id="kinolog" value="<? echo ($user['sourname']. " " .$name1. ". " .$patronymic1. ".") ?>" />  
          </div>
          <div class="form-group">
              <p>Отловлено животных без владельцев</p>
                <input class="field width-add" type="number" name="count_animals_get" value="<? echo ($count[0]) ?>" />  
          </div>
          <div class="form-group">
              <p>Передано в приют для животных</p>
                <input class="field width-add" type="number" name="count_animals_shelter" value="<? echo ($count[0]) ?>" />  
          </div>
          <div class="form-group">
              <p>Aдминистратор приюта </p>
                <select class="field width-add" id="admin_shelter" name="admin_shelter">
                  <?php
                    $sql = "SELECT * FROM `users` WHERE id_users_group = 2;";
                    $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                    echo '<option value="" selected readonly hidden>Выберите администратора приюта</option>';
    
                    while($row = mysqli_fetch_array($res))
                    {
                        $nameA = $row['name'];
                        $nameA1 = mb_substr($nameA, 0, 1);
                        $patronymicA = $row['patronymic'];
                        $patronymicA1 = mb_substr($patronymicA, 0, 1);
                        echo '<option value="'. $row['sourname'] . ' ' .$nameA1. '. ' .$patronymicA1 .'">'. $row['sourname'] . ' ' .$nameA1. '. ' .$patronymicA1. '.</option>';
                    }
                  ?>
                </select>
          </div>
          <div class="form-group">
              <p>Количество погибших при отлове животных (если есть)</p>
                <input class="field width-add" type="number" name="count_animals_die" value="0" />  
          </div>
          <div class="form-group">
              <p>Причина гибели животных без владельцев (если есть)</p>
                <input class="field width-add" type="textarea" name="comment" value=""/>  
          </div>
          <div class="form-group">
              <p>Заказчик</p>
                <input class="field width-add" type="text" name="zakazchik_name" />  
          </div>
          <div class="form-group">
              <p>Должность заказчика</p>
                <input class="field width-add" type="text" name="zakazchik_role" />  
          </div>
        
          <input class="btn button-auth field-submit field-width" type="submit" value="Сформировать" autofocus />
        </form>
      </div>
    <!--/Формирование-->
    
    <input type="date-time" name="data_first" id="data_first" class="hide_all" value="<?php echo $data_first; ?>" autocomplete="off" style="display: none;"/>
    <input type="date-time" name="data_second" id="data_second" class="hide_all" value="<?php echo $data_second; ?>" autocomplete="off" style="display: none;"/>

    <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab tab-mobile"><?
      $query = "SELECT * FROM `application` WHERE `data` >= '".$data_first."' AND `data` <= '".$data_second."' AND `id_sostoyanie`='1' AND `show`='1' AND `id_user`='".$_SESSION['id_user']."'".$last_query."ORDER BY `id` DESC";

        $show_status_constr = $_SESSION['show_status'] == "all" ? "AND (`id_status`=1 OR `id_status`=2 OR `id_status`=11 OR `id_status`=7 OR `id_status`=8)" : "AND id_status=".$_SESSION['show_status'];
		$query = "SELECT * FROM `application` WHERE `data` >= '".$data_first."' AND `data` <= '".$data_second."' AND `id_sostoyanie`='1' AND `show`='1' AND `id_user`='".$_SESSION['id_user']."'".$last_query."ORDER BY `id` DESC";
		$res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
		$row_count = 0;
        while ($res = mysqli_fetch_array($res_query)) {
            $row_count++;
    		$res_image = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `upload_files` WHERE `id_user`='".$_SESSION['id_user']."' AND `id_application`='".$res['id']."' AND `type`='img'"));
    		$res_kind = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `animal_kind` WHERE `id`=".$res['id_kind']));
    		$res_status= mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `application_status` WHERE `id`=".$res['id_status']));
    		$end = date('Y-m-d H:i', $res['date_start_karantin']+777600);
    		$first_date = new DateTime($today);
    		$second_date = new DateTime($end);
    		$difference = $first_date->diff($second_date);
    		$interval = format_interval($difference);
    		
    		if(!empty($res_image))
    			$img = $res_image['path_thumb'].$res_image['name_thumb'];
    		else
    			$img = "assets/images/no-image.png";
    		    
    		echo "<tr data-href='/animal_card.php?application={$res['id']}' class='application-mobile'>
                			<td>
                			    <div class='block-head'>
                			        <div class='id-color block-head-num'>{$_SESSION['num_tr']}</div>
                			        <div class='block-head-date'>".date('d.m.Y', $res['data'])."</div>
                			        <div class='block-head-adress'>{$res['locality']}, {$res['street']}</div>";
                                    if($res_status['id'] == 7 || $res_status['id'] == 8)
                                        $rowsData[] = "<div class='block-head-status die-color'>";
                                    else
                                        $rowsData[] = "<div class='block-head-status'>";
                    			        if($res_status['id'] == 2)
                            				$rowsData[] .= "{$res_status['status_name']}<br> <div class='status-time'>до ".date("d.m.Y", ($res['date_start_karantin']+777600))."</div>";
                            			else
                            				$rowsData[] .= "{$res_status['status_name']}";
            echo "         </div>
                                </div>
                                <div class='block-content'>
                                    <div class='block-content-img'><img src='{$img}' class='table-img'></div>
                                    <div class='block-content-info'>
                                        <ul>
                                            <li>{$res_kind['name_kind']}</li>
                                            <li>{$res['breed']}</li>
                                            <li>возраст: {$res['age']}</li>
                                            <li>окрас: {$res['color']}</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class='block-comment'>{$res['comment']}</div>
                            </td>
                        </tr>";
                        $_SESSION['num_tr'] = $_SESSION['num_tr'] + 1;
        }
      ?>
    </table>
    <input id="get_akt" name="display-set" type="submit" class="btn button-auth field-submit btn-card" value="Сформировать акт отлова" autofocus />
    <!--<a href="libs/phpword/generate_word.php?application='.$_GET['application'].'" class="btn button-auth field-submit btn-card">Акт отлова</a>-->


<?
}}
?>
<script type="text/javascript">
    
    $("#get_akt").click(function() {
        $.fancybox.open({
          src  : '#hide-form-popup-num_aviary',
          type : 'inline'
        });
    });
    
</script>