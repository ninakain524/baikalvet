<? 
    require_once("config.php");
    require_once("timezone.php");
    require_once("functions.php");
    if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }

    if (isset($_POST['contract'])) {
        $contract = $_POST['contract'];
        $rowsData = [];
        $adressArr = [];
        $sql_adress = "SELECT application.id AS id, application.loc_type AS loc_type, application.locality AS locality, 
            application.street AS street FROM application WHERE application.show = 1 AND application.id_contract = ".$contract." 
            GROUP BY application.id, application.loc_type, application.locality, application.street 
            ORDER BY application.data ASC;";
    
        $res_adress = mysqli_query($SERVER, $sql_adress) or die("Ошибка " . mysqli_error($SERVER));
        array_push($rowsData,'<option value="all" selected>Все места отлова</option>');
        
        while($row_adress = mysqli_fetch_array($res_adress))
        {
            $adressArr[$row_adress['id']] = $row_adress['loc_type']." ".$row_adress['locality'].", ".$row_adress['street'];
        } 
        $no_dubs = array_unique($adressArr);

        foreach($no_dubs as $key=>$value) {
            array_push($rowsData, '<option value="'.$key.'">'.$value.'</option>');
            // next($adressArr);
        }

        $html = "";
	    foreach($rowsData as $row){
	        $html .= $row;
	    }
	    echo json_encode(array(
	        'result'    => 'success',
	        'html'      => $html
	    ));
    }
?>