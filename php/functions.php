<?php
    // function getNumberCard ($id, $id_contract, $SERVER) {
    //     $num1 = mysqli_query($SERVER, "SET @row_number = 0;");
    //     $num = mysqli_fetch_array(mysqli_query($SERVER, 
    //         "SELECT RowNums.num AS numCard FROM 
    //         (SELECT (@row_number:=@row_number + 1) AS num, id FROM
    //             application
    //         WHERE application.id_contract = ".$id_contract."
    //         ORDER BY application.data ASC) AS RowNums WHERE RowNums.id = ".$id));
    //     return $num['numCard'];
    // }

    // function wordQuery ($id, $SERVER) {

    // }
    require_once("config.php");
    require_once("timezone.php");
    function addLogs($name, $query) {
        file_put_contents($_SERVER['DOCUMENT_ROOT']."/"."php/logs/logsForAdmin.txt", 
        date("d.m.Y H:i:s", time()) . " ; " . $_SERVER['REMOTE_ADDR']. " ; " . $name . " ; " . str_replace(array("\r\n", "\r", "\n", "  ", "    ", "    "), '', $query)."\n", FILE_APPEND | LOCK_EX);    
        return true;   
    }
    if (isset($_GET['reset_filter'])) {
        $_SESSION['shelter_id'] = $user['shelter_id']; // id приюта
        $_SESSION['order_by'] = 'data'; // сортировка по столбцам, по дефолту - по дате
        $_SESSION['sort_by'] = 'ASC'; // сортировка по > или <, по дефолту - по возрастанию
        $_SESSION['show_status'] = 'all'; // отображение по статусам, по дефолту - все статусы
        $_SESSION['row_limit'] = '25'; // количество отображаемых записей, по дефолту - 25
        $_SESSION['default_shelter'] = 'all'; // приют по умолчанию для админа, по дефолту - все
        $_SESSION['filter_id_shelter'] = 'all';// приют по умолчанию для админа, по дефолту - все
        $_SESSION['show_contract'] = 'all';
        $_SESSION['filter_kinolog'] = 'all';
        $_SESSION['show_contract_otchet'] = 'all';
        $_SESSION['num_tr'] = 0;
        $_SESSION['filter_locality'] = 'all';//населенный пункт
        $_SESSION['filter_first_date'] = 'first';
        $_SESSION['filter_last_date'] = 'last';
        $_SESSION['filter_aviarys'] = '';
        echo json_encode(array(
	        'res'    => 'success'
	    ));
    }
?>