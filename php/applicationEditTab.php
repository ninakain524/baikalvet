<?php
	require_once('config.php');
	require_once("timezone.php");
	require_once("functions.php");
	setlocale(LC_ALL, 'ru_RU.utf8');
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	mb_http_output('UTF-8');
	mb_language('uni');

    if (isset($_GET['update'])) {
        
        $idApp = $_GET['update'];
        
        $data = strtotime(prepare($_GET['data']));
		$name_region = prepare($_GET['name_region']);
		$locality = prepare($_GET['locality']);
		$street = prepare($_GET['street']);
		$name_shelter = prepare($_GET['name_shelter']);
		$name_kind = prepare($_GET['name_kind']);
		$breed = prepare($_GET['breed']);
		$name_gender = prepare($_GET['name_gender']);
		$name_color = prepare($_GET['name_color']);
		$age = $_GET['age'];
		$weight = prepare($_GET['weight']);
		$height = prepare($_GET['height']);
		$customer = prepare($_GET['customer']);
		$comment = prepare($_GET['comment']);
		$status_name = prepare($_GET['status_name']);
		$part_query = "";
		if(prepare($_GET['date_start_karantin']) != "") {
			$date_start_karantin = strtotime(prepare($_GET['date_start_karantin']));
			$part_query = "`date_start_karantin`='".$date_start_karantin."',";
		}
		$kinolog = prepare($_GET['kinolog']);
		$vet_card_data = prepare($_GET['vet_card_data']);
		$sourname = prepare($_GET['sourname']);
		$num_birka = prepare($_GET['num_birka']);
		$num_chip = prepare($_GET['num_chip']);
		$degel_date = prepare($_GET['degel_date']);
		$degel_preparat = prepare($_GET['degel_preparat']);
		$vac_date = prepare($_GET['vac_date']);
		$vac_preparat = prepare($_GET['vac_preparat']);
		$o_date = prepare($_GET['o_date']);
		$show_app = 0;
		if (prepare($_GET['show_app']) == 1) $show_app = 1;
		
		$queryLog = "UPDATE `application` SET 
    		`data`='".$data."', 
    		`id_region`='".$name_region."', 
    		`locality`='".$locality."', 
			`id_user`='".$kinolog."',
    		`street`='".$street."', 
    		`id_shelter`='".$name_shelter."', 
    		`id_kind`='".$name_kind."', 
    		`breed`='".$breed."', 
    		`id_gender`='".$name_gender."', 
    		`color`='".$name_color."', 
    		`age`='".$age."', 
    		`weight`='".$weight."', 
    		`height`='".$height."', 
    		`id_contract`='".$customer."', 
    		`comment`='".$comment."', 
    		`id_status`='".$status_name."', 
    		".$part_query."
			`show`='".$show_app."'
    		WHERE id=".$idApp;
		if (mysqli_query($SERVER, $queryLog)) {
			addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал карточку животного на главной - " . $queryLog);
		}
    	
    	$rs = mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM vet_card WHERE id_application=".$idApp);
        $result = mysqli_fetch_array($rs);
        if ($result[0] > 0) {	
        	$queryLog = "UPDATE `vet_card` SET 
        		`data`='".$vet_card_data."',
        		`id_veterinar`='".$sourname."',
        		`num_birka`='".$num_birka."',
        		`num_chip`='".$num_chip."'
        		WHERE id_application=".$idApp;
			if (mysqli_query($SERVER, $queryLog)) {
				addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал карточку ветеринара на главной - " . $queryLog);
			}
            
            mysqli_query($SERVER, "UPDATE `vet_degilmintizaciya` SET 
        		`data`='".$degel_date."',
        		`preparat`='".$degel_preparat."'
        		WHERE id_application=".$idApp);
        	
        	mysqli_query($SERVER, "UPDATE `vet_vaccine` SET 
        		`data`='".$vac_date."',
        		`preparat`='".$vac_preparat."'
        		WHERE id_application=".$idApp);
        		
        	mysqli_query($SERVER, "UPDATE `vet_operation` SET 
        		`data`='".$o_date."'
        		WHERE id_application=".$idApp);
        }
        else {
            $result_id = $SERVER -> query("SELECT `id` FROM `vet_card` ORDER BY `id` DESC LIMIT 1");
            $row_id = mysqli_fetch_array($result_id);
            $id = $row_id['id']+1;
            
            $result_id_d = $SERVER -> query("SELECT `id` FROM `vet_degilmintizaciya` ORDER BY `id` DESC LIMIT 1");
            $row_id_d = mysqli_fetch_array($result_id_d);
            $id_d = $row_id_d['id']+1;
            
            $result_id_v = $SERVER -> query("SELECT `id` FROM `vet_vaccine` ORDER BY `id` DESC LIMIT 1");
            $row_id_v = mysqli_fetch_array($result_id_v);
            $id_v = $row_id_v['id']+1;
            
            $result_id_o = $SERVER -> query("SELECT `id` FROM `vet_operation` ORDER BY `id` DESC LIMIT 1");
            $row_id_o = mysqli_fetch_array($result_id_o);
            $id_o = $row_id_o['id']+1;
            
            $query = "INSERT INTO `vet_degilmintizaciya` (
                `id`, `preparat`, `data`, `id_veterinar`, `id_application`) VALUES (
                '".$id_d."',
                '".$degel_preparat."',
                '".$degel_date."',
                '".$sourname."',
                '".$idApp."')";
			if (mysqli_query($SERVER, $query)) {
				addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Добавил дополнения карточки ветеринара на главной - " . $query);
			}
                
            $query = "INSERT INTO `vet_vaccine` (
                `id`, `preparat`, `data`, `id_veterinar`, `id_application`) VALUES (
                '".$id_v."',
                '".$vac_preparat."',
                '".$vac_date."',
                '".$sourname."',
                '".$idApp."')";
			if (mysqli_query($SERVER, $query)) {
				addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Добавил дополнения карточки ветеринара на главной - " . $query);
			}  

            $query = "INSERT INTO `vet_operation` (
                `id`, `data`, `id_veterinar`, `id_application`) VALUES (
                '".$id_o."',
                '".$o_date."',
                '".$sourname."',
                '".$idApp."')";
			if (mysqli_query($SERVER, $query)) {
				addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Добавил дополнения карточки ветеринара на главной - " . $query);
			}
                
            $query = "INSERT INTO `vet_card` (
                `id`, `data`, `num_birka`, `num_chip`, `id_veterinar`, `id_degelmintizaciya`, `id_vaccine`, `id_operation`, `id_application`) VALUES (
                '".$id."',
                '".$vet_card_data."',
                '".$num_birka."',
                '".$num_chip."',
                '".$sourname."',
                '".$id_d."',
                '".$id_v."',
                '".$id_o."',
                '".$idApp."')";
			if (mysqli_query($SERVER, $query)) {
				addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Добавил карточку ветеринара на главной - " . $query);
			}

            $query = "UPDATE `application` SET
                `id_vet_card`='".$id."'
                WHERE `id`='".$idApp."';";
			if (mysqli_query($SERVER, $query)) {
				addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Обновил id карточки ветеринара в карточке животного - на главной " . $query);
			}
        }
    		
		$SERVER -> close();
    }