<?php 
    require_once("config.php");
    require_once("timezone.php");
    require_once("functions.php");
    if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
    require_once("../template/head.html");
    require_once("../php/ExportToWord.inc.php");
    setlocale(LC_ALL, 'ru_RU.utf8');
    mb_internal_encoding('UTF-8');
    mb_regex_encoding('UTF-8');
    mb_http_output('UTF-8');
    mb_language('uni');

    $show_contract = $_GET['show_contract'] == "all" ? "" : "AND application.id_contract=".$_GET['show_contract'];
    $id_shelter = $_GET['id_shelter'] == "all" ? "" : "AND application.id_shelter=".$_GET['id_shelter'];
    // if (isset($_GET['without']))
    ?>
    
    <!-- Content -->
    <section class="content" id="export-content">
      <div class="row">
        <div class="col-md-12" style="width: 100%">
          <table id="table2excel" class="table-otchet-r">
              <tr>
                  <td colspan="22">
                      <div class="button-card">
                        <h4><center>Реестр животных без владельцев</center></h4>
                      </div>
                  </td>
              </tr> 
              <tr>
                  <th width="20" height="200" style="word-wrap:break-word;" white-space: "pre-line" rowspan="2">№ п/п</th>
                  <th rowspan="2">Дата отлова, поступления<br>в приют для животных,<br>заключения о клиническом<br>состоянии животного</th>
                  <th rowspan="2">Место отлова, адрес</th>
                  <? if (isset($_GET['without'])) { 
                    echo '<th colspan="1" rowspan="2">Видео отлова</th>';
                    } else { 
                    echo '<th colspan="2">Видео отлова (название файла, ссылка для скачивания)</th>';
                    } 
                  ?>
                  <th rowspan="2">Приют</th>
                  <th rowspan="2">№ вольера</th>
                  <th colspan="6">Вид, порода, окрас, вес, приблизительный возраст, визуальная характеристика состояния животного, а также иные данные, позволяющие его идентифицировать</th>
                  <th rowspan="2">Идентификационный индивидуальный<br>номер животного, наносимый на бирку</th>
                  <th rowspan="2">Номер чипа</th>
                  <th colspan="3">Дата проведения лечебно-профилактических мероприятий</th>
                  <th colspan="2">Дата передачи животного его владельцу,<br>новому владельцу, в другой приют,<br>данные о владельце (ФИО, адрес, телефон),<br>приюте (название, адрес)</th>
                  <th rowspan="2">Дата возврата животного на место прежнего обитания</th>
                  <? if (isset($_GET['without'])) { 
                    echo '<th colspan="1" rowspan="2">Видео возврата</th>';
                    } else { 
                    echo '<th colspan="2">Видео возврата (название файла, ссылка для скачивания)</th>';
                    } 
                  ?>
                  <th rowspan="2">Дата умерщвления, естественной смерти/причины</th>
              </tr>
              <tr>
                <? if (!isset($_GET['without'])) { 
                  echo '<th>Название файла</th>
                    <th>Ссылка для скачивания</th>';
                  } 
                ?>
                <th>Вид</th>
                <th>Порода</th>
                <th>Пол</th>
                <th>Окрас</th>
                <th>Возраст ≈</th>
                <th>Вес ≈</th>
                <th>Дегильминтизации</th>
                <th>Вакцинации от бешенства</th>
                <th>Операции</th>
                <th>Дата передачи</th>
                <th>Данные о владельце, приюте</th>
                <? if (!isset($_GET['without'])) { 
                  echo '<th>Название файла</th>
                    <th>Ссылка для скачивания</th>';
                  } 
                ?>
              </tr>
  
              <?php  
              $query = "SELECT 
              application.id AS id,
              application.data AS data,
              application.street AS street,
              application.loc_type AS loc_type,
              application.locality AS locality,
              application.breed AS breed, 
              application.age AS age, 
              application.num_aviary AS num_aviary, 
              application.weight AS weight, 
              application.color AS color,
              application.date_start_vosstanovlenie AS date_start_vosstanovlenie,
              application.date_start_karantin AS date_start_karantin,
              DATE_FORMAT(application.date_vipusk,'%d.%m.%Y') AS date_vipusk,
              DATE_FORMAT(application.date_die,'%d.%m.%Y') AS date_die,
              animal_kind.name_kind AS kind,
              animal_gender.name_gender AS gender,
              application_status.id AS status_id,
              application_status.status_name AS status,
              vet_card.num_birka AS num_birka,
              vet_card.num_chip AS num_chip,
              DATE_FORMAT(vet_card.data,'%d.%m.%Y') AS vet_card_data,
              animal_shelters.name_shelter AS name_shelter,
              DATE_FORMAT(vet_degilmintizaciya.data,'%d.%m.%Y') AS vet_degilmintizaciya_data,
              DATE_FORMAT(vet_operation.data,'%d.%m.%Y') AS vet_operation_data,
              DATE_FORMAT(vet_vaccine.data,'%d.%m.%Y') AS vet_vaccine_data,
              DATE_FORMAT(animal_owners.data,'%d.%m.%Y') AS animal_owners_data,
              animal_owners.name_shelter AS a_name_shelter,
              animal_owners.adress AS a_adress,
              animal_owners.sourname_owner AS sourname_owner,
              animal_owners.name_owner AS name_owner,
              animal_owners.patronymic_owner AS patronymic_owner,
              animal_owners.phone AS phone
              FROM application 
              LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
              LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
              LEFT JOIN application_status ON application.id_status = application_status.id
              LEFT JOIN vet_card ON application.id = vet_card.id_application
              LEFT JOIN animal_shelters ON application.id_shelter = animal_shelters.id
              LEFT JOIN vet_degilmintizaciya ON vet_card.id_degelmintizaciya = vet_degilmintizaciya.id
              LEFT JOIN vet_operation ON vet_card.id_vaccine = vet_operation.id
              LEFT JOIN vet_vaccine ON vet_card.id_operation = vet_vaccine.id
              LEFT JOIN animal_owners ON application.id = animal_owners.id_application
              LEFT JOIN contract ON application.id_contract = contract.id
              WHERE application.data >= '".$_GET['data_first']."' AND application.data <= '".$_GET['data_second']."' AND application.show='1' ".$show_contract." ".$id_shelter." ORDER BY application.data ASC";

              $res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
              $i = 1;

              $count_q = mysqli_fetch_array(mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM application WHERE application.data >= '".$_GET['data_first']."' AND application.data <= '".$_GET['data_second']."' AND application.show='1' ".$show_contract." ".$id_shelter." ORDER BY application.data ASC"));
              $count_r = $count_q[0];

              $row1 = 0;
              $row2 = 0;

              while ($res = mysqli_fetch_array($res_query)) {
                  
                  if (ctype_digit($res['color'])) {
                      $animal_color1 = mysqli_fetch_array(mysqli_query($SERVER, "SELECT name_color FROM animal_color WHERE id=".$res['color']));
                      $animal_color = $animal_color1['name_color'];
                  }
                else
                    $animal_color = $res['color']; 
                  
                  $query_file = mysqli_query($SERVER, "SELECT * FROM upload_files WHERE id_application = '".$res['id']."' AND type = 'vid' LIMIT 1");
                  $name_file = "";
                  $path_file = "";
                  while ($res_file = mysqli_fetch_array($query_file)) {
                      $name_file = $res_file['name_file'];
                      $path_file = $res_file['path_file'];
                  }   
                  
                  $query_file_vipusk = mysqli_query($SERVER, "SELECT * FROM upload_files WHERE id_application = '".$res['id']."' AND type = 'vid' AND vipusk = 1 LIMIT 1");
                  $name_file_vipusk = "";
                  $path_file_vipusk = "";
                  while ($res_file_vipusk = mysqli_fetch_array($query_file_vipusk)) {
                      $name_file_vipusk = $res_file_vipusk['name_file'];
                      $path_file_vipusk = $res_file_vipusk['path_file'];
                  } 
                      
                  $name = $res['name_owner'];
                  $name1 = mb_substr($name, 0, 1);
                  $patronymic = $res['patronymic_owner'];
                  $patronymic1 = mb_substr($patronymic, 0, 1);
  
                  echo "<tr>
                  <td><center>{$i}<center></td>
                  <td>".date('d.m.Y', $res['data'])."</td>
                  <td>{$res['loc_type']} {$res['locality']}, {$res['street']};</td>";
                  
                  if (isset($_GET['without'])) {
                    if ($row1 == 0) { 
                      echo "<td rowspan='".$count_r."' class='vertical-text'>Видео предоставляется по запросу службы ветеринарии или заказчика</td>";
                      $row1 = 1;
                    }
                  } else { 
                    echo "<td>{$name_file}</td>
                    <td>
                      <a href='https://baikalvet.ru{$path_file}{$name_file}' target='_blank'>
                        https://baikalvet.ru{$path_file}{$name_file}
                      </a>
                    </td>";
                  } 
                  echo "
                  <td>{$res['name_shelter']}</td>
                  <td>{$res['num_aviary']}</td>
                  <td>{$res['kind']}</td>
                  <td>{$res['breed']}</td>
                  <td>{$res['gender']}</td>
                  <td>{$animal_color}</td>
                  <td>{$res['age']}</td>
                  <td>{$res['weight']}</td>
                  <td>{$res['num_birka']}</td>
                  <td>{$res['num_chip']}</td>
                  <td>{$res['vet_degilmintizaciya_data']}</td>
                  <td>{$res['vet_vaccine_data']}</td>
                  <td>{$res['vet_operation_data']}</td>
                  <td>{$res['animal_owners_data']}</td>
                  <td>{$res['sourname_owner']} {$name1}{$res['a_name_shelter']}. {$res['a_adress']}{$patronymic1}. {$res['phone']}</td>
                  <td>{$res['date_vipusk']}</td>";
                  if (isset($_GET['without'])) { 
                    if ($row2 == 0) { 
                      echo "<td rowspan='".$count_r."' class='vertical-text'>Видео предоставляется по запросу службы ветеринарии или заказчика</td>";
                      $row2 = 1;
                    }
                  } else { 
                    echo "<td>{$name_file_vipusk}</td>
                    <td>
                      <a href='https://baikalvet.ru{$path_file_vipusk}{$name_file_vipusk}' target='_blank'>
                        https://baikalvet.ru{$path_file_vipusk}{$name_file_vipusk}
                      </a>
                    </td>";
                  } 
                  echo "
                  <td>{$res['date_die']}</td>
                  </tr>";

                  $i = $i + 1;
              }
            ?>
          </table>
        </div>  
        <div class='col-md-12'>
          <div class='button-card but-vet'>
            <input id="zakluchenie_print" name="submit" type="submit" class="btn button-auth field-submit1 btn-card hide-from-printer" onClick="window.print()" value="Распечатать"/>
            <?
              $link = "";
              if (isset($_GET['without'])) 
                $link = "without";
              echo "<a href='../php/to_excel.php?{$link}&data_first={$_GET['data_first']}&data_second={$_GET['data_second']}&show_contract={$_GET['show_contract']}&id_shelter={$_GET['id_shelter']}' class='btn button-auth field-submit1 btn-card hide-from-printer'>Экспорт в Excel</a>"; 
            ?>
          </div>
        </div>
      </div>
    </section>

<script src="../ExportToWord/FileSaver.js"></script>
<script src="../ExportToWord/jquery.wordexport.js"></script>
<script src="/js/jquery.table2excel.js"></script>
<script>
    jQuery(document).ready(function($) {
      $("a.word-export").click(function(event) {
        $("#export-content").wordExport();
      });
    });
</script>

<? require_once("../template/footer.html"); ?>