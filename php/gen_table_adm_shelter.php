<?php
	require_once("config.php");
    require_once("timezone.php");
    require_once("functions.php");
    $time_now = time();
    $today = date("Y-m-d\TH:i:s");
    $rowsData = array();
    function format_interval(DateInterval $interval) {
        $result = "";
        if ($interval->d) { $result .= $interval->format("%d д. "); }
        if ($interval->h) { $result .= $interval->format("%h ч. "); }
        if ($interval->i) { $result .= $interval->format("%i мин. "); }
    
        return $result;
    };
	if (isset($_GET['gen_table'])) {
		if (isset($_GET['show_more']))
			$limit_constr = $_POST['start_from'].', '.$_POST['row_nums'];
		else
			$limit_constr = $_SESSION['row_limit'];
		$show_status_constr = $_SESSION['show_status'] == "all" ? "" : "AND id_status=".$_SESSION['show_status'];
        $query = "SELECT 
                application.id AS id,
                application.data AS data,
                application.street AS street,
				application.loc_type AS loc_type,
                application.locality AS locality,
                application.breed AS breed,
                application.num_aviary AS num_aviary, 
                application.age AS age, 
                application.weight AS weight, 
                application.color AS color,
                application.date_start_vosstanovlenie AS date_start_vosstanovlenie,
                application.date_start_karantin AS date_start_karantin,
                animal_kind.name_kind AS kind,
                animal_gender.name_gender AS gender,
                application_status.id AS status_id,
                application_status.status_name AS status,
                vet_card.num_birka AS num_birka,
                vet_card.num_chip AS num_chip,
                DATE_FORMAT(vet_card.data,'%d.%m.%Y') AS vet_card_data
                FROM application 
                LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
                LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
                LEFT JOIN application_status ON application.id_status = application_status.id
                LEFT JOIN vet_card ON application.id = vet_card.id_application 
                WHERE application.show='1' AND application.show='1' ".$show_status_constr." ORDER BY `".$_SESSION['order_by']."` ".$_SESSION['sort_by']." LIMIT ".$limit_constr;
		$res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
		$row_count=0;
		while ($res = mysqli_fetch_array($res_query)) {
			$row_count++;
			$end = date('Y-m-d H:i', $res['date_start_vosstanovlenie']+777600);
            $first_date = new DateTime($today);
            $second_date = new DateTime($end);
            $difference = $first_date->diff($second_date);
            $interval = format_interval($difference);
            
            $end1 = date('Y-m-d H:i', $res['date_start_karantin']+777600);
            $first_date1 = new DateTime($today);
            $second_date1 = new DateTime($end1);
            $difference1 = $first_date1->diff($second_date1);
            $interval1 = format_interval($difference1);
			$rowsData[] = "<tr data-href='/animal_card.php?application={$res['id']}' class='application'>
                    <td><div class='id-color'>{$_SESSION['num_tr']}</div></td>
                    <td>".date('d.m.Y', $res['data'])."</td>
                    <td>{$res['num_aviary']}</td>
                    <td>{$res['loc_type']} {$res['locality']}</td>
                    <td>{$res['street']}</td>
                    <td>{$res['kind']}</td>
                    <td>{$res['breed']}</td>
                    <td>{$res['gender']}</td>
                    <td>{$res['color']}</td>
                    <td>{$res['age']}</td>
                    <td>{$res['weight']}</td>";
                    
                    if($res['status_id'] == 3) //на восстановлении
                    { $rowsData[] .= "<td>{$res['status']}<br> <div class='status-time'>Осталось: {$interval}</div></td>"; }
                    else if($res['status_id'] == 2) //на карантине
                    { $rowsData[] .= "<td>{$res['status']}<br> <div class='status-time'>Осталось: {$interval1}</div></td>"; }
                    else
                    { $rowsData[] .= "<td>{$res['status']}</td>"; }
                    
                    $rowsData[] .= "</tr>";
            $_SESSION['num_tr'] = $_SESSION['num_tr'] + 1;
		}
		$html = "";
	    foreach($rowsData as $row){
	        $html .= $row;
	    }
	    echo json_encode(array(
	        'result'    => 'success',
	        'html'      => $html,
	        'row_count' => $row_count
	    ));
	}
	if (isset($_GET['update_table_config'])) {
	    $_SESSION['num_tr'] = 1;
		switch ($_POST['show_status']) {
			case '0':
				$_SESSION['show_status'] = 'all';
				break;
			case '1':
				$_SESSION['show_status'] = '1';
				break;
			case '2':
				$_SESSION['show_status'] = '2';
				break;
			case '3':
				$_SESSION['show_status'] = '3';
				break;
			case '4':
				$_SESSION['show_status'] = '4';
				break;
			case '5':
				$_SESSION['show_status'] = '5';
				break;
			case '6':
				$_SESSION['show_status'] = '6';
				break;
			case '7':
				$_SESSION['show_status'] = '7';
				break;
			case '8':
				$_SESSION['show_status'] = '8';
				break;
			case '9':
				$_SESSION['show_status'] = '9';
				break;
			case '10':
				$_SESSION['show_status'] = '10';
				break;
			case '11':
				$_SESSION['show_status'] = '11';
				break;
			case '12':
				$_SESSION['show_status'] = '12';
				break;
			default:
				$_SESSION['show_status'] = 'all';
				break;
		}
		switch ($_POST['order_by']) {
			case '1':
				$_SESSION['order_by'] = 'data';
				break;
			case '2':
				$_SESSION['order_by'] = 'locality';
				break;
			case '3':
				$_SESSION['order_by'] = 'street';
				break;
			case '4':
				$_SESSION['order_by'] = 'id_kind';
				break;
			case '5':
				$_SESSION['order_by'] = 'breed';
				break;			
			case '6':
				$_SESSION['order_by'] = 'age';
				break;
			case '7':
				$_SESSION['order_by'] = 'color';
				break;
			case '9':
				$_SESSION['order_by'] = 'num_aviary';
				break;
			default:
				$_SESSION['order_by'] = 'data';
				break;
		}
		switch ($_POST['sort_by']) {
			case 'DESC':
				$_SESSION['sort_by'] = 'DESC';
				break;
			case 'ASC':
				$_SESSION['sort_by'] = 'ASC';
				break;
			default:
				$_SESSION['sort_by'] = 'DESC';
				break;
		}
		switch ($_POST['row_limit']) {
			case '25':
				$_SESSION['row_limit'] = '25';
				break;
			case '50':
				$_SESSION['row_limit'] = '50';
				break;
			case '75':
				$_SESSION['row_limit'] = '75';
				break;
			case '100':
				$_SESSION['row_limit'] = '100';
				break;
			default:
				$_SESSION['row_limit'] = '25';
				break;
		}
		echo json_encode(array('res'    => 'success'));
	}
?>