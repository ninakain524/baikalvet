<?php
	require_once('config.php');
    require_once("timezone.php");
    require_once("functions.php");
	setlocale(LC_ALL, 'ru_RU.utf8');
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	mb_http_output('UTF-8');
	mb_language('uni');

    if (isset($_GET['add_application'])) {
    	$time_now = time();
        $today_day_time = date("Y-m-d\TH:i:s");
        $date_aviary = $date_start = NULL;
        $city = prepare($_GET['city']);
        
        if ($city == "Бесплатная версия kladr-api.ru")
            $city = "";
		$street = prepare($_GET['street']);
		if ($street == "Бесплатная версия kladr-api.ru")
            $street = "";
        $reason = prepare($_GET['reason']) == "wo_reason" ? 'NULL' : '"'.prepare($_GET['reason']).'"';  
        $loc_type = prepare($_GET['loc_type']).".";   
        if ($loc_type == "undefined.") 
            $loc_type = "";
		$id_kind = prepare($_GET['id_kind']);
		$id_contract = prepare($_GET['id_contract']);
		$breed = prepare($_GET['breed']);
		$id_gender = prepare($_GET['gender']);
		$color = prepare($_GET['color']);
		$age = prepare($_GET['age']);
		$weight = prepare($_GET['weight']);
		$height = prepare($_GET['height']);
		$id_shelter = prepare($_GET['shelter']);
		$num_aviary = $_GET['num_aviary'];
        $data = prepare($_GET['data']);
        $comment = prepare($_GET['comment']);
        if ($num_aviary != 0) {
            //Проверка времени на 22 и 23 часа
            
            if ((date("H", $data) == "22") || (date("H", $data) == "23")) {
                $date_aviary = date("Y-m-d\TH:i:s", $data);
                $date_start = $data;
            }
            else {
                $date_aviary = date("Y-m-d\TH:i:s", $time_now + 7200);
                $date_start = strtotime('+2 hours', $data);
            }
            
        }

		if ( $_SESSION['id_users_group'] == "1" ) {
		    $kinolog = prepare($_GET['kinolog']);
		    $region = prepare($_GET['region']);
		}
		else{
		    $kinolog = $_SESSION['id_user'];
		    $region = $_SESSION['id_region'];
		}
// 		$d = DateTime::createFromFormat('d-m-Y H:i:s', $data);
		
		$result_id = $SERVER -> query("SELECT `id` FROM `application` ORDER BY `id` DESC LIMIT 1");
        $row_id = mysqli_fetch_array($result_id);
        $id_card = $row_id['id']+1;
        if ($num_aviary != 0) {
            $query = "INSERT INTO `application` (`id`, `id_journal_application`, `data`, `id_user`, `loc_type`, `locality`, `id_region`, `street`, `id_kind`, `breed`, `id_gender`, `color`, `age`, `weight`, `height`, `id_shelter`, `num_aviary`, `id_status`, `id_sostoyanie`, `comment`, `id_contract`, `date_zaseleno`, `date_start_karantin`) VALUES ('".$id_card."', ".$reason.", '".$data."', '".$kinolog."', '".$loc_type."','".$city."', '".$region."', '".$street."', '".$id_kind."', '".$breed."', '".$id_gender."', '".$color."', '".$age."', '".$weight."', '".$height."', '".$id_shelter."', '".$num_aviary."', '1', '1', '".$comment."', '".$id_contract."', '".$date_aviary."', '".$date_start."')";
        if (mysqli_query($SERVER, $query)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Добавил карточку животного - " . $query);
            }
        }
        else {
            $query = "INSERT INTO `application` (`id`, `id_journal_application`, `data`, `id_user`, `loc_type`, `locality`, `id_region`, `street`, `id_kind`, `breed`, `id_gender`, `color`, `age`, `weight`, `height`, `id_shelter`, `num_aviary`, `id_status`, `id_sostoyanie`, `comment`, `id_contract`) VALUES ('".$id_card."', ".$reason.", '".$data."', '".$kinolog."', '".$loc_type."','".$city."', '".$region."', '".$street."', '".$id_kind."', '".$breed."', '".$id_gender."', '".$color."', '".$age."', '".$weight."', '".$height."', '".$id_shelter."', '".$num_aviary."', '1', '1', '".$comment."', '".$id_contract."')";
            if (mysqli_query($SERVER, $query)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Добавил карточку животного - " . $query);
            }
        }
        
            $inserted_id = mysqli_insert_id($SERVER);
        // Разрешенные расширения файлов.
        $allow = array('video/mp4', 'video/avi', 'video/3gp', 'video/mov', 'image/png', 'image/jpeg', 'image/jpg', 'image/bmp'); 
        $videotype = array('video/mp4', 'video/avi', 'video/3gp', 'video/mov');
        $imagetype = array('image/png', 'image/jpeg', 'image/jpg', 'image/bmp'); 
        // Полный путь до upload директории.
        $full_path = $_SERVER['DOCUMENT_ROOT'] . '/assets/files/uploads/';
        $thumbs_path = $_SERVER['DOCUMENT_ROOT'] . '/assets/files/thumbs/';
        if (!is_dir($full_path)) {
            mkdir($full_path, 0777, true);
        }
        $err = 0;
        foreach($_FILES as $file){
            $new_file_name = generate_random_string(11).'.'.pathinfo($file['name'], PATHINFO_EXTENSION);
            if (in_array($file['type'], $allow))
                if (move_uploaded_file($file['tmp_name'], $full_path . $new_file_name))
                    if (in_array($file['type'], $videotype)){
                        $filetype = "vid";
                        $query = "INSERT INTO `upload_files` (type, path_file, path_thumb, name_file, name_thumb, data, actual, id_user, id_application, num_str, id_user_group) VALUES ('".$filetype."', '/assets/files/uploads/', '', '".$new_file_name."', '".$new_file_name."', '".$time_now."', '1', '".$_SESSION['id_user']."', '".$inserted_id."', '1', '4')";
                        if (mysqli_query($SERVER, $query)) {
                            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Добавил видео в карточку животного - " . $query);
                        }
                    }
                    elseif (in_array($file['type'], $imagetype)){
                        $filetype = "img";
                        $query = "INSERT INTO `upload_files` (type, path_file, path_thumb, name_file, name_thumb, data, actual, id_user, id_application, num_str, id_user_group) VALUES ('".$filetype."', '/assets/files/uploads/', '/assets/files/thumbs/', '".$new_file_name."', '".$new_file_name."', '".$time_now."', '1', '".$_SESSION['id_user']."', '".$inserted_id."', '1', '4')";
                        if (mysqli_query($SERVER, $query)) {
                            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Добавил фото в карточку животного - " . $query);
                        }
                        ResizeImage($full_path.$new_file_name, 200, 100, $thumbs_path, $new_file_name);
                    }
                    
            }  
        $result_add = mysqli_fetch_array(mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM `application` WHERE `id`=".$id_card));
        if ($result_add[0] > 0) {
            echo("1");
        }
        else{
            echo("0");
        }
    }


    if (isset($_GET['update'])) {
      	if (isset($_POST['files_to_delete'])){
            $files_to_delete = $_POST['files_to_delete'];
        }
      	$id = $_POST['id'];
        $street = $_POST['street'];
        $kind = $_POST['kind'];
        $breed = $_POST['breed'];
        $gender = $_POST['gender'];
        $color = $_POST['color'];
        $age = $_POST['age'];
        $shelter = $_POST['shelter'];
        $num_aviary = $_POST['num_aviary'];
        $queryLog = "UPDATE `application` SET `street`='".$street."', `id_kind`='".$kind."', `breed`='".$breed."', `id_gender`='".$gender."', `color`='".$color."', `age`='".$age."', `id_shelter`='".$shelter."', `num_aviary`='".$num_aviary."' WHERE `id`=".$id;
        if (mysqli_query($SERVER, $queryLog)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал карточку животного - " . $queryLog);
        }
	}

	if (isset($_GET['to_karantin'])){
        if (isset($_POST['id_card'])){
            $queryLog = "UPDATE `application` SET `id_status`='2', `date_start_karantin`='".$today_day_time."' WHERE id=".$_POST['id_card'];
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Поместил животное на карантин - " . $queryLog);
            }
            header("Refresh:0");
        }
        else   
            echo 'error';
    }

    if (isset($_GET['free'])){
        if (isset($_POST['id_card'])){
            $queryLog = "UPDATE `application` SET `id_status`='4' WHERE id=".$_POST['id_card'];
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Выпустил животное - " . $queryLog);
            }
            header("Refresh:0");
        }
        else   
            echo 'error';
    }

    if (isset($_GET['buried'])){
        if (isset($_POST['id_card'])){
            $queryLog = "UPDATE `application` SET `id_status`='9' WHERE id=".$_POST['id_card'];
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Утилизировал животное - " . $queryLog);
            }
            header("Refresh:0");
        }
        else   
            echo 'error';
    }
    if (isset($_GET['edit_owner'])) {
        $eo_sourname = prepare($_POST['eo_sourname']);
        $eo_name = prepare($_POST['eo_name']);
        $eo_patronymic = prepare($_POST['eo_patronymic']);
        $eo_phone = prepare($_POST['eo_phone']);
        $eo_seriya = prepare($_POST['eo_seriya']);
        $eo_num = prepare($_POST['eo_num']);
        $ed_vidan = prepare($_POST['eo_vidan']);
        $owner_id = prepare($_POST['owner_id']);
        $edit_owner_query = "UPDATE `animal_owners` SET `sourname_owner`='".$eo_sourname."', `name_owner`='".$eo_name."', `patronymic_owner`='".$eo_patronymic."', `phone`='".$eo_phone."', `passport_seriya`='".$eo_seriya."', `passport_num`='".$eo_num."', `passport_vidan`='".$ed_vidan."' WHERE `id`=".$owner_id;
        //file_put_contents("ins.txt", $edit_owner_query);
        if (mysqli_query($SERVER, $edit_owner_query)){
            echo json_encode(array(
                'result'    =>  'success',
                'id'    => $_POST['owner_id']
            ));
        }

    }
?>