<?php
    require_once("../php/config.php");
    require_once("timezone.php");
    require_once("functions.php");
    if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
    
    $data_first = $data_second = "";
    
    $today = date("Y-m-d\TH:i:s");
    $today1 = date("Y-m-d");
    
    function format_interval(DateInterval $interval) {
        $result = "";
        if ($interval->d) { $result .= $interval->format("%d д. "); }
        if ($interval->h) { $result .= $interval->format("%h ч. "); }
        if ($interval->i) { $result .= $interval->format("%i мин. "); }
    
        return $result;
    };
    
    if (isset($_POST['data_first1'])) {
    
    $data_def = $_POST['data_def1'];
    $data_first = $_POST['data_first1'];
    $data_second = $_POST['data_second1'];
    
    if ($_POST['show_contract'] == 0) {
        $_SESSION['show_contract_otchet'] = 'all';
    }
    $res_query = mysqli_query($SERVER, "SELECT id AS id FROM contract WHERE show_contract=1") or die("Ошибка " . mysqli_error($SERVER));
	while ($res = mysqli_fetch_array($res_query)) {
        if ($_POST['show_contract'] == $res['id']) {
            $_SESSION['show_contract_otchet'] = $res['id'];
        } 
	}
		
    $show_contract = $_SESSION['show_contract_otchet'] == "all" ? "" : "AND application.id_contract=".$_SESSION['show_contract_otchet'];
    
    if ($_POST['id_shelter'] == 0) {
        $_SESSION['id_shelter_show'] = 'all';
    }
    $res_query = mysqli_query($SERVER, "SELECT id AS id FROM animal_shelters WHERE actual=1") or die("Ошибка " . mysqli_error($SERVER));
	while ($res = mysqli_fetch_array($res_query)) {
        if ($_POST['id_shelter'] == $res['id']) {
            $_SESSION['id_shelter_show'] = $res['id'];
        } 
	}
		
    $id_shelter = $_SESSION['id_shelter_show'] == "all" ? "" : "AND application.id_shelter=".$_SESSION['id_shelter_show'];
    
    $query = "SELECT * FROM `application` WHERE `data` >= '".$data_first."' AND `data` <= '".$data_second."' AND `show`='1' ".$show_contract." ".$id_shelter." ORDER BY `data` ASC";
    $res = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
    $adress = "";
    $item_adress = "";
    $street = "";
    while($row = mysqli_fetch_array($res))
    {   
        if ($street == $row['street']){
            $item_adress = "";
            $street = "";
        }
        else{
            $street = $row['street'];
            $item_adress = $row['locality'] . ", " . $street . "; "; 
        }
           
        $adress = $adress . $item_adress; 
    }
    
    $card = mysqli_fetch_array(mysqli_query($SERVER, $query));
    $count = mysqli_fetch_array(mysqli_query($SERVER, "SELECT COUNT(1) FROM `application` WHERE `data` >= '".$data_first."' AND `data` <= '".$data_second."' AND `show`='1' ".$show_contract." ".$id_shelter." ORDER BY `data` ASC"));
    
    
                    
    if(!empty($card)){
    $user = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$card['id_user']));
        
    $name = $user['name'];
    $name1 = mb_substr($name, 0, 1);
    $patronymic = $user['patronymic'];
    $patronymic1 = mb_substr($patronymic, 0, 1);
    
?>
    <!--Выбор ссылки-->
    <!-- <div id="hide-form-popup-num_aviary" class="hide-form-popup-num_aviary hideform fancy-animate">
        <form id="popup-num_aviary" method="post" action="/php/edit_status.php?num_aviary=<?php echo ($_GET['application']) ?>" enctype="multipart/form-data">
        <h3>Формировать со ссылками на видео?</h3>
        <input class="btn button-auth field-submit field-width" type="submit" value="Со ссылками" autofocus />
        <input class="btn button-auth field-submit field-width" type="submit" value="Без ссылок" autofocus />
        </form>
    </div> -->
    <!--/Выбор ссылки-->

    <input type="date-time" name="data_first" id="data_first" class="hide_all" value="<?php echo $data_first; ?>" autocomplete="off" style="display: none;"/>
    <input type="date-time" name="data_second" id="data_second" class="hide_all" value="<?php echo $data_second; ?>" autocomplete="off" style="display: none;"/>

    <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab">
      <tr class="tab-col1">
        <th rowspan="2">№</th>
        <th rowspan="2">Дата отлова</th>
        <th rowspan="2">Контракт</th>
        <th rowspan="2">Место отлова</th>
        <th rowspan="2">Приют</th>
        <th rowspan="2">№ вольера</th>
        <th colspan="6">Описание животного</th>
        <th rowspan="2">Статус</th>
      </tr>
      <tr class="tab-col1">
        <th>Вид</th>
        <th>Порода</th>
        <th>Пол</th>
        <th>Окрас</th>
        <th>Возраст ≈</th>
        <th>Вес ≈</th>
      </tr>
 
      <?
      // AND 
      switch ($_SESSION['id_region']) {
        case 'all':
            $region = '';
            break;
        case '':
            $region = '';
            break;
        default:
            $region = 'AND application.id_region='.$_SESSION['id_region'];
            break;
      }
      
      $query = "SELECT 
                application.id AS id,
                application.data AS data,
                application.street AS street,
                application.loc_type AS loc_type,
                application.locality AS locality,
                application.breed AS breed, 
                application.age AS age, 
                application.weight AS weight, 
                application.num_aviary AS num_aviary, 
                application.color AS color,
                application.date_start_vosstanovlenie AS date_start_vosstanovlenie,
                application.date_start_karantin AS date_start_karantin,
                animal_kind.name_kind AS kind,
                animal_gender.name_gender AS gender,
                application_status.id AS status_id,
                application_status.status_name AS status,
                vet_card.num_birka AS num_birka,
                vet_card.num_chip AS num_chip,
                DATE_FORMAT(vet_card.data,'%d.%m.%Y') AS vet_card_data,
                animal_shelters.name_shelter AS name_shelter,
                contract.name_contract AS name_contract
                FROM application 
                LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
                LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
                LEFT JOIN application_status ON application.id_status = application_status.id
                LEFT JOIN vet_card ON application.id = vet_card.id_application
                LEFT JOIN animal_shelters ON application.id_shelter = animal_shelters.id
                LEFT JOIN contract ON application.id_contract = contract.id
                WHERE application.data >= '".$data_first."' AND application.data <= '".$data_second."' AND application.show='1' ".$show_contract." ".$region." ".$id_shelter." ORDER BY application.data ASC";

        $res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
        $i = 1;
        while ($res = mysqli_fetch_array($res_query)) {
            
            if (ctype_digit($res['color'])) {
                $animal_color1 = mysqli_fetch_array(mysqli_query($SERVER, "SELECT name_color FROM animal_color WHERE id=".$res['color']));
                $animal_color = $animal_color1['name_color'];
            }
        	else
        	    $animal_color = $res['color']; 
        	    
            $end = date('Y-m-d H:i', $res['date_start_vosstanovlenie']+777600);
            $first_date = new DateTime($today);
            $second_date = new DateTime($end);
            $difference = $first_date->diff($second_date);
            $interval = format_interval($difference);
            
            $end1 = date('Y-m-d H:i', $res['date_start_karantin']+777600);
            $first_date1 = new DateTime($today);
            $second_date1 = new DateTime($end1);
            $difference1 = $first_date1->diff($second_date1);
            $interval1 = format_interval($difference1);

          echo "<tr data-href='/animal_card.php?application={$res['id']}' class='application'>
            <td><div class='id-color'>{$i}</div></td>
            <td>".date('d.m.Y', $res['data'])."</td>
            <td>{$res['name_contract']}</td>
            <td>{$res['loc_type']} {$res['locality']}, {$res['street']}</td>
            <td>{$res['name_shelter']}</td>
            <td>{$res['num_aviary']}</td>
            <td>{$res['kind']}</td>
            <td>{$res['breed']}</td>
            <td>{$res['gender']}</td>
            <td>{$animal_color}</td>
            <td>{$res['age']}</td>
            <td>{$res['weight']}</td>";
            
            if($res['status_id'] == 3) //на восстановлении
            { echo "<td>{$res['status']}<br> <div class='status-time'>Осталось: {$interval}</div></td>"; }
            else if($res['status_id'] == 2) //на карантине
            { echo "<td>{$res['status']}<br> <div class='status-time'>Осталось: {$interval1}</div></td>"; }
            else
            { echo "<td>{$res['status']}</td>"; }
            echo "</tr>";
            $i = $i + 1;
        }
      ?>
    </table> 
    <input id="get_reestr" type="submit" class="btn button-auth field-submit btn-card" value="Сформировать реестр со ссылками" autofocus />
    <input id="get_reestr_without" type="submit" class="btn button-auth field-submit btn-card" value="Сформировать реестр без ссылок" autofocus />
    <!-- <a href="?select" class="btn button-auth field-submit1 btn-card">Сформировать реестр животных</a> -->
    <!-- <input id="get_reestr" type="submit" class="btn button-auth field-submit btn-card" value="Сформировать реестр животных" autofocus /> -->
    <? }} ?>
<script type="text/javascript">

    $('body').on('click', '#get_reestr', function(){
        var data_first1 = "";
        var data_first = "";
        var data_second1 = "";
        var data_second = "";
        let show_contract = "";
        let id_shelter = "";
        
        data_first1 = document.getElementById('data_first').value + "T00:00:00";
        data_first = Date.parse(data_first1)/1000 - 21600;
        data_second1 = document.getElementById('data_second').value + "T23:59:59";
        data_second =  Date.parse(data_second1)/1000 - 21600;
        show_contract =  document.getElementById('show_contract').value;
        id_shelter = document.getElementById('id_shelter').value;
        // alert(data_first);
        console.log("111");
        window.open('php/otchet_reestr.php?data_first=' + data_first + '&data_second=' + data_second + '&show_contract=' + show_contract + '&id_shelter=' + id_shelter);
    });

    $('body').on('click', '#get_reestr_without', function(){
        var data_first1 = "";
        var data_first = "";
        var data_second1 = "";
        var data_second = "";
        let show_contract = "";
        let id_shelter = "";
        
        data_first1 = document.getElementById('data_first').value + "T00:00:00";
        data_first = Date.parse(data_first1)/1000 - 21600;
        data_second1 = document.getElementById('data_second').value + "T23:59:59";
        data_second =  Date.parse(data_second1)/1000 - 21600;
        show_contract =  document.getElementById('show_contract').value;
        id_shelter = document.getElementById('id_shelter').value;
        console.log("222");
        // alert(data_first);
        window.open('php/otchet_reestr.php?data_first=' + data_first + '&data_second=' + data_second + '&show_contract=' + show_contract + '&id_shelter=' + id_shelter + '&without');
    });
    
</script>