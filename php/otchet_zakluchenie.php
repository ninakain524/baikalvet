<?php 
    require_once("config.php");
    require_once("timezone.php");
    require_once("functions.php");
    if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
    require_once("../template/head.html");
    require_once("../php/ExportToWord.inc.php");
    setlocale(LC_ALL, 'ru_RU.utf8');
    mb_internal_encoding('UTF-8');
    mb_regex_encoding('UTF-8');
    mb_http_output('UTF-8');
    mb_language('uni');
 
    $application = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".$_GET['zakluchenie']));
    $shelter = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_shelters WHERE id=".$application['id_shelter']));
    $user_shelter = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE shelter_id=".$shelter['id']));
    
    $name = $_SESSION['name_user'];
    $name1 = mb_substr($name, 0, 1);
    $patronymic = $_SESSION['patronymic_user'];
    $patronymic1 = mb_substr($patronymic, 0, 1);
    
    $name_shelter = $user_shelter['name'];
    $name_shelter1 = mb_substr($name_shelter, 0, 1);
    $patronymic_shelter = $user_shelter['patronymic'];
    $patronymic_shelter1 = mb_substr($patronymic_shelter, 0, 1);
    
    ?>
    
    <!-- Content -->
    <section class="content" id="export-content">
      <div class="container">
        <div class="row">
          <div class="col-md-12" style="width: 100%">
              <table id="table2excel" class="table-otchet">
                <tr>
                    <td colspan="8">
                        <div class="button-card">
                          <h4><center>Заключение<br>о клиническом состоянии животного без владельца</center></h4>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                        Я, ветеринарный врач <? echo($_SESSION['sourname_user']." ".$name1.". ".$patronymic1) ?>., в присутствии работника приюта для животных ООО "Пять Звезд" по имени <? echo($user_shelter['sourname']." ".$name_shelter1.". ".$patronymic_shelter1) ?>., проведен клинический осмотр животного без владельца.
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                        В результате осмотра установлено*:
                    </td>
                </tr> 
                <tr>
                    <th width="20" height="200" style="word-wrap:break-word;" white-space: "pre-line">№ заключения о клиническом состоянии</th>
                    <th>Дата заключения о клиническом состоянии</th>
                    <th>Вид</th>
                    <th>Порода</th>
                    <th>Пол</th>
                    <th>Окрас</th>
                    <th>Возраст (≈лет)</th>
                    <th>Вес (≈кг)</th>
                </tr>
    
              <?php  
              $query = "SELECT 
                application.id AS id,
                application.data AS data,
                application.street AS street,
                application.locality AS locality,
                application.breed AS breed, 
                application.age AS age, 
                application.weight AS weight, 
                application.color AS color,
                application.date_start_vosstanovlenie AS date_start_vosstanovlenie,
                animal_kind.name_kind AS kind,
                animal_gender.name_gender AS gender,
                application_status.id AS status_id,
                application_status.status_name AS status,
                DATE_FORMAT(vet_card.data,'%d.%m.%Y %H:%i:%s') AS vet_card_data,
                vet_card.num_birka AS num_birka,
                vet_card.num_chip AS num_chip,
                DATE_FORMAT(vet_card.data,'%d.%m.%Y %H:%i:%s') AS vet_card_data
                FROM application 
                LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
                LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
                LEFT JOIN application_status ON application.id_status = application_status.id
                LEFT JOIN vet_card ON application.id = vet_card.id_application
                WHERE application.id='".$_GET['zakluchenie']."'";

                $res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
                
                while ($res = mysqli_fetch_array($res_query)) {
                    
                    echo "<tr>
                    <td><center>{$res['id']}<center></td>
                    <td>".date('d.m.Y H:i', $res['data']+777600)."</td>
                    <td>{$res['kind']}</td>
                    <td>{$res['breed']}</td>
                    <td>{$res['gender']}</td>
                    <td>{$res['color']}</td>
                    <td>{$res['age']}</td>
                    <td>{$res['weight']}</td></tr>";
                }
              ?>
              <tr><td></td></tr>
              <tr>
                  <td colspan="8"><p style="padding-top: 50px;">Иные данные:_____________________________________________________________</p></td>
              </tr>
              <tr><td></td></tr>
              <tr>
                  <td colspan="8"><p>Безнадзорное животное клинически здорово. Признаки заразных и иных заболеваний отсутствуют.</p></td>
              </tr>
              <tr><td></td></tr>
              <tr><td></td></tr>
              <tr><td></td></tr>
              <tr><td></td></tr>
              <tr>
                  <td colspan="4"></td>
                  <td colspan="4" style="display:none">______________________________________________________________________</td>
              </tr>
              <tr>
                  <td colspan="8"><p style="border-top: 1px solid #000; margin-top: 60px; font-size: 12px; line-height: 1.2; width: 25%; text-align: right; float: right;">(подпись специалиста в области ветеринарии)</p></td>
              </tr>
              <tr><td></td></tr>
              <tr><td></td></tr>
              <tr><td></td></tr>
              <tr><td></td></tr>
              <tr><td></td></tr>
              <tr><td></td></tr>
              <tr><td></td></tr>
              <tr><td></td></tr>
              <tr>
                  <td colspan="8" style="display:none">____________________________________________________________________________________________________________________________________________________________________________</td>
                  </tr>
              <tr>
                  <td colspan="8">
                      <div style="border-top: 1px solid #000; font-size: 12px; line-height: 1.2; margin-top: 14%;">
                        <p>*В результате осмотра отражается характеристика животного без владельца (вид, порода, окрас, вес, приблизительный возраст, визуальная характеристика состояния животного, а также иные данные, позволяющие его идентифицировать), информация о состоянии здоровья животного, о наличии у животного признаков заразных и иных заболеваний, о необходимости его умерщвления.</p>
                      </div> 
                  </td>
              </tr>
            </table>
            
            
            
          </div>  

          <div class='col-md-12'>
	   	        <div class='button-card but-vet'>
    	            <input id="zakluchenie_print" name="submit" type="submit" class="btn button-auth field-submit1 btn-card hide-from-printer" onClick="window.print()" value="Распечатать"/>
    	            <!--<a class="word-export" href="javascript:void(0)">Экспорт в Word</a>-->
    	            <input id="generate-excel_zakluchenie" name="submit" type="submit" class="btn button-auth field-submit1 btn-card hide-from-printer" value="Экспорт в Excel"/>
    			</div>
    		</div>
        </div>
      </div> 
    </div> 
  </div>
</section>

<script src="../ExportToWord/FileSaver.js"></script>
<script src="../ExportToWord/jquery.wordexport.js"></script>

<script>
    
    jQuery(document).ready(function($) {
      $("a.word-export").click(function(event) {
        $("#export-content").wordExport();
      });
    });

    $(document).ready(function() {
        
        $("#generate-excel_zakluchenie").click(function(){
            // alert("export");
            $("#table2excel").table2excel({
              // CSS-класс строк таблицы, которые не экспортируются
                        exclude: ".noExl",
                        // Имя экспортируемого документа Excel
                        name: "Excel Document Name",
                        // Имя файла Excel
                        filename: "Заключение_о_клиническом_состоянии_животного",
                        //Расширение файла
                        fileext: ".xls",
                        // Следует ли исключать экспортированные изображения
                        exclude_img: false,
                        // Следует ли исключать экспортные гиперссылки
                        exclude_links: false,
                        // Следует ли исключить содержимое поля ввода экспорта
                        exclude_inputs: false,
                        preserveColors: false
            }); 
        });
    });
    
</script>
<script src="/js/jquery.table2excel.js"></script>
<? require_once("../template/footer.html"); ?>