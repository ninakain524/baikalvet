<?php 
    require_once("config.php");
    require_once("timezone.php");
    require_once("functions.php");
    setlocale(LC_ALL, 'ru_RU.utf8');
    mb_internal_encoding('UTF-8');
    mb_regex_encoding('UTF-8');
    mb_http_output('UTF-8');
    mb_language('uni');
    
    $today = date("Y-m-d\TH:i:s");
    $str_status = "";
    
    if (isset($_GET['edit_application'])) { 

        $status = $_POST['id_status'];
        
        if ( $_SESSION['id_users_group'] == "1" ) {
            $kin = $_POST['kin'];
            if ($kin == "selected")
                $kin = 0;

            $last_query = "`id_user`='".$kin."', `id_region`='".$_POST['id_region']."', `id_status`='".$_POST['id_status']."',";
            
            
            $last = "";
            if ($status == "2") {
                $time_now = time();
                $last = " ";
            }
            if($status == "1" || $status == "2" || $status == "3" || $status == "6" || $status == "7" || $status == "8" || $status == "10" || $status == "11" || $status == "12")
                $str_status = "`id_sostoyanie`=1,";
            else
                $str_status = "`id_sostoyanie`=2,";
        }
        else {
            $kin = NULL; 
            $last_query = "`id_status`='".$_POST['id_status']."',";  
            $str_status = "";
            $last = "";
        }
        $date_zaseleno_q = $date_k = $date_vosst = $date_vipusk_q = "";

        if ($_POST['date_z'] != "" && (trim($_POST['aviary']) != 0 && trim($_POST['aviary']) != ""))
            $date_zaseleno_q = "`date_zaseleno`='".trim($_POST['date_z'])."', ";
        
        if (($_SESSION['id_users_group'] != "4" && $_POST['date-karantin'] != "" && (trim($_POST['aviary']) != 0 && trim($_POST['aviary']) != "")) || 
            ($_SESSION['id_users_group'] == "4" && $status == "2" && $_POST['date-karantin'] != "" && (trim($_POST['aviary']) != 0 && trim($_POST['aviary']) != "")))
            $date_k = "`date_start_karantin`='".strtotime($_POST['date-karantin'])."', ";
        
        // if ($_POST['date-vosst'] != "") {
        //     $date_vosst = "`date_start_vosstanovlenie`='".strtotime($_POST['date-vosst'])."', ";
        // }
        
        if ($_POST['date_v'] != "")
            $date_vipusk_q = "`date_vipusk`='".trim($_POST['date_v'])."',";
        
            // date-vosst

        $aviary = trim($_POST['aviary']);
        if ($aviary == "") $aviary = 0;
        
        $query = "UPDATE `application` SET 
            `locality`='".trim($_POST['city'])."', 
            `street`='".trim($_POST['street'])."', 
            `data`='".strtotime($_POST['date'])."', 
            ".$date_k . $date_vosst . $date_zaseleno_q . $date_vipusk_q."
            `id_kind`='".trim($_POST['id_kind'])."',
            `breed`='".trim($_POST['breed'])."', 
            `id_gender`='".trim($_POST['gender'])."', 
            `color`='".trim($_POST['color'])."', 
            `age`='".trim($_POST['age'])."', 
            ".$last_query.$str_status."
            `weight`='".trim($_POST['weight'])."', 
            `height`='".trim($_POST['height'])."',
            `id_shelter`='".trim($_POST['shelter'])."', 
            `id_contract`='".trim($_POST['id_contract'])."', 
            `num_aviary`='".$aviary."',
            `comment`='".trim($_POST['comment'])."'".$last.
            "WHERE `id`=".$_GET['edit_application'];
        
        $rs = mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM vet_card WHERE id_application=".$_GET['edit_application']);
        $result = mysqli_fetch_array($rs);
        if ($result[0] > 0 && $_POST['date-vosst'] != "") {
            $date_vosst = $_POST['date-vosst'];
            $vet_card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `vet_card` WHERE `id_application`=".$_GET['edit_application']));
            $queryLog = "UPDATE `vet_card` SET `data`='".$date_vosst."' WHERE `id`=".$vet_card['id'];
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал вет карточку - " . $queryLog);
            }
            $queryLog = "UPDATE `vet_degilmintizaciya` SET `data`='".$date_vosst."' WHERE `id`=".$vet_card['id_degelmintizaciya'];
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал дополнения вет карточки - " . $queryLog);
            }
            $queryLog = "UPDATE `vet_operation` SET `data`='".$date_vosst."' WHERE `id`=".$vet_card['id_operation'];
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал дополнения вет карточки - " . $queryLog);
            }
            $queryLog = "UPDATE `vet_vaccine` SET `data`='".$date_vosst."' WHERE `id`=".$vet_card['id_vaccine'];
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал дополнения вет карточки - " . $queryLog);
            }
            $queryLog = "UPDATE `application` SET `date_start_vosstanovlenie`='".strtotime($date_vosst)."' WHERE `id`='".$_GET['edit_application']."';";
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал старт восстановления - " . $queryLog);
            }
        }    
        
        // mysqli_query($SERVER, $query);
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал карточку животного - " . $query);
        }
        $SERVER -> close();
        header("location: ../animal_card.php?application=".$_GET['edit_application']);
        
    }
    
    if (isset($_GET['update-foto'])) {
        $time_now = time();
        
        $id = prepare($_GET['id']);
        
        $files_to_delete = explode(',', $_GET['files_to_delete']);
        
        $allow = array('video/mp4', 'video/avi', 'video/3gp', 'video/mov', 'image/png', 'image/jpeg', 'image/jpg', 'image/bmp');
        $imagetype = array('image/png', 'image/jpeg', 'image/jpg', 'image/bmp'); 
        // Полный путь до upload директории.
        $full_path = $_SERVER['DOCUMENT_ROOT'] . '/assets/files/uploads/';
        $thumbs_path = $_SERVER['DOCUMENT_ROOT'] . '/assets/files/thumbs/';
        if (!is_dir($full_path)) {
            mkdir($full_path, 0777, true);
        }
        foreach($_FILES as $file){
            $new_file_name = generate_random_string(11).'.'.pathinfo($file['name'], PATHINFO_EXTENSION);
            if (in_array($file['type'], $allow))
                if (move_uploaded_file($file['tmp_name'], $full_path . $new_file_name))
                    if (in_array($file['type'], $imagetype)){
                        $filetype = "img";
                        mysqli_query($SERVER, "INSERT INTO `upload_files` (type, path_file, path_thumb, name_file, name_thumb, data, actual, id_user, id_application, num_str, id_user_group) VALUES ('".$filetype."', '/assets/files/uploads/', '/assets/files/thumbs/', '".$new_file_name."', '".$new_file_name."', '".$time_now."', '1', '".$_SESSION['id_user']."', '".$id."', '1', '4')");
                        ResizeImage($full_path.$new_file_name, 200, 100, $thumbs_path, $new_file_name);
                    }
                    
            }              
        $files_to_delete = array_unique($files_to_delete);
        if ($files_to_delete[0] > 0) {
            for ($i = 0; $i < count($files_to_delete); $i++) { 
                $delete_file = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM upload_files WHERE id=".$files_to_delete[$i]));
                if (mysqli_query($SERVER, "DELETE FROM `upload_files` WHERE id=".$delete_file['id'])) {
                    unlink($full_path.$delete_file['name_file']);
                    unlink($thumbs_path.$delete_file['name_file']);
                }
            }
        }
    }
    
    if (isset($_GET['update-video'])) {
        $time_now = time();
        
        $id = prepare($_GET['id']);
        
        $files_to_delete = explode(',', $_GET['files_to_delete']);
        
        $allow = array('video/mp4', 'video/avi', 'video/3gp', 'video/mov', 'image/png', 'image/jpeg', 'image/jpg', 'image/bmp');
        $videotype = array('video/mp4', 'video/avi', 'video/3gp', 'video/mov');
        // Полный путь до upload директории.
        $full_path = $_SERVER['DOCUMENT_ROOT'] . '/assets/files/uploads/';
        $thumbs_path = $_SERVER['DOCUMENT_ROOT'] . '/assets/files/thumbs/';
        if (!is_dir($full_path)) {
            mkdir($full_path, 0777, true);
        }
        foreach($_FILES as $file){
            $new_file_name = generate_random_string(11).'.'.pathinfo($file['name'], PATHINFO_EXTENSION);
            if (in_array($file['type'], $allow))
                if (move_uploaded_file($file['tmp_name'], $full_path . $new_file_name))
                    if (in_array($file['type'], $videotype)){
                        $filetype = "vid";
                        mysqli_query($SERVER, "INSERT INTO `upload_files` (type, path_file, path_thumb, name_file, name_thumb, data, actual, id_user, id_application, num_str, id_user_group) VALUES ('".$filetype."', '/assets/files/uploads/', '', '".$new_file_name."', '".$new_file_name."', '".$time_now."', '1', '".$_SESSION['id_user']."', '".$id."', '1', '4')");
                    }
                    
            }              
        $files_to_delete = array_unique($files_to_delete);
        if ($files_to_delete[0] > 0) {
            for ($i = 0; $i < count($files_to_delete); $i++) { 
                $delete_file = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM upload_files WHERE id=".$files_to_delete[$i]));
                if (mysqli_query($SERVER, "DELETE FROM `upload_files` WHERE id=".$delete_file['id'])) {
                    unlink($full_path.$delete_file['name_file']);
                    unlink($thumbs_path.$delete_file['name_file']);
                }
            }
        }
    }
 
    if (isset($_GET['update'])) {
        $time_now = time();
        $id = prepare($_GET['id']);
        $city = prepare($_GET['city']);
        $street = prepare($_GET['street']);
        $id_kind = prepare($_GET['id_kind']);
        $breed = prepare($_GET['breed']);
        $id_gender = prepare($_GET['gender']);
        $color = prepare($_GET['color']);
        $age = prepare($_GET['age']);
        $weight = prepare($_GET['weight']);
        $id_shelter = prepare($_GET['shelter']);
        $num_aviary = $_GET['num_aviary'];
        $comment = prepare($_GET['comment']);
        
        if ( $_SESSION['id_users_group'] == "1" ) {
            $id_status = prepare($_GET['id_status']);
            $id_sostoyanie = prepare($_GET['id_sostoyanie']);
        }
        
        
        $files_to_delete = explode(',', $_GET['files_to_delete']);
        //echo $files_to_delete;
        if ( $_SESSION['id_users_group'] == "1" ) {
            $query = "UPDATE `application` SET `locality`='".$city."', `street`='".$street."', `id_kind`='".$id_kind."', `breed`='".$breed."', `id_gender`='".$id_gender."', `color`='".$color."', `age`='".$age."', `weight`='".$weight."', `id_shelter`='".$id_shelter."', `comment`='".$comment."', `num_aviary`='".$num_aviary."', `id_status`='".$id_status."', `id_sostoyanie`='".$id_sostoyanie."' WHERE `id`=".$id;
        }
        else{
            $query = "UPDATE `application` SET `locality`='".$city."', `street`='".$street."', `id_kind`='".$id_kind."', `breed`='".$breed."', `id_gender`='".$id_gender."', `color`='".$color."', `age`='".$age."', `weight`='".$weight."', `id_shelter`='".$id_shelter."', `comment`='".$comment."', `num_aviary`='".$num_aviary."' WHERE `id`=".$id;
        
        }
        // mysqli_query($SERVER, $query);
        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал карточку животного - " . $query);
        }
        $allow = array('video/mp4', 'video/avi', 'video/3gp', 'video/mov', 'image/png', 'image/jpeg', 'image/jpg', 'image/bmp'); 
        $videotype = array('video/mp4', 'video/avi', 'video/3gp', 'video/mov');
        $imagetype = array('image/png', 'image/jpeg', 'image/jpg', 'image/bmp'); 
        // Полный путь до upload директории.
        $full_path = $_SERVER['DOCUMENT_ROOT'] . '/assets/files/uploads/';
        $thumbs_path = $_SERVER['DOCUMENT_ROOT'] . '/assets/files/thumbs/';
        if (!is_dir($full_path)) {
            mkdir($full_path, 0777, true);
        }
        foreach($_FILES as $file){
            $new_file_name = generate_random_string(11).'.'.pathinfo($file['name'], PATHINFO_EXTENSION);
            if (in_array($file['type'], $allow))
                if (move_uploaded_file($file['tmp_name'], $full_path . $new_file_name))
                    if (in_array($file['type'], $videotype)){
                        $filetype = "vid";
                        mysqli_query($SERVER, "INSERT INTO `upload_files` (type, path_file, name_file, name_thumb, data, actual, id_user, id_application, num_str) VALUES ('".$filetype."', '/assets/files/uploads/', '".$new_file_name."', '".$new_file_name."', '".$time_now."', '1', '".$_SESSION['id_user']."', '".$id."', '1')");
                    }
                    elseif (in_array($file['type'], $imagetype)){
                        $filetype = "img";
                        mysqli_query($SERVER, "INSERT INTO `upload_files` (type, path_file, path_thumb, name_file, name_thumb, data, actual, id_user, id_application, num_str) VALUES ('".$filetype."', '/assets/files/uploads/', '/assets/files/thumbs/', '".$new_file_name."', '".$new_file_name."', '".$time_now."', '1', '".$_SESSION['id_user']."', '".$id."', '1')");
                        ResizeImage($full_path.$new_file_name, 200, 100, $thumbs_path, $new_file_name);
                    }
                    
            }              
        $files_to_delete = array_unique($files_to_delete);
        if ($files_to_delete[0] > 0) {
            for ($i = 0; $i < count($files_to_delete); $i++) { 
                $delete_file = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM upload_files WHERE id=".$files_to_delete[$i]));
                if (mysqli_query($SERVER, "DELETE FROM `upload_files` WHERE id=".$delete_file['id'])) {
                    unlink($full_path.$delete_file['name_file']);
                    unlink($thumbs_path.$delete_file['name_file']);
                }
            }
        }
    }
    if (isset($_GET['to_karantin'])){
        if (isset($_POST['id_card'])){
            $time_now = time();
            $queryLog = "UPDATE `application` SET `id_status`='2', `date_start_karantin`='".$time_now."' WHERE id=".$_POST['id_card'];
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Поместил на карантин - " . $queryLog);
            }
            header("Refresh:0");
        }
        else   
            echo 'error';
    }
    if (isset($_GET['free'])){
        if (isset($_POST['id_card'])){
            $queryLog = "UPDATE `application` SET `id_status`='4', `id_sostoyanie`='2', date_vipusk = '".$today."' WHERE id=".$_POST['id_card'];
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Выпустил - " . $queryLog);
            }
            header("Refresh:0");
        }
        else   
            echo 'error';
    }
    if (isset($_GET['buried'])){
        if (isset($_POST['id_card'])){
            $queryLog = "UPDATE `application` SET `id_status`='9', `id_sostoyanie`='2', date_die = '".$today."' WHERE id=".$_POST['id_card'];
            if (mysqli_query($SERVER, $queryLog)) {
                addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Захоронил - " . $queryLog);
            }
            header("Refresh:0");
        }
        else   
            echo 'error';
    }
?>