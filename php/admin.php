<?php
	require_once('config.php');
	require_once("timezone.php");
	require_once("functions.php");
	setlocale(LC_ALL, 'ru_RU.utf8');
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	mb_http_output('UTF-8');
	mb_language('uni');


	if (isset($_GET['delete_application'])) {
	    $_SESSION['num_tr'] = 1;
		$errors = 0;
		$checked = array_unique(json_decode(stripslashes($_POST['checked'])));
		for ($i=0; $i < count($checked); $i++) { 
			$queryLog = "UPDATE `application` SET `show`=0 WHERE id=".$checked[$i];
			if (!mysqli_query($SERVER, $queryLog))
				$errors++;
			else {
				addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Скрыл запись через редактор на главной - " . $queryLog);
			}
		}
		if ($errors == 0)
			echo json_encode(array('res' => 'success'));
		else
			echo json_encode(array('res' => 'error'));
	}
	else if (isset($_GET['delete_users'])) {
		$errors = 0;
		$checked = array_unique(json_decode(stripslashes($_POST['checked'])));
		for ($i=0; $i < count($checked); $i++) { 
			$queryLog = "UPDATE `users` SET `show`=0 WHERE id=".$checked[$i];
			if (!mysqli_query($SERVER, $queryLog))
				$errors++;
			else {
				addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Скрыл пользователя - " . $queryLog);
			}
		}
		if ($errors == 0)
			echo json_encode(array('res' => 'success'));
		else
			echo json_encode(array('res' => 'error'));
	}
	else if (isset($_GET['delete_contracts'])) {
		$errors = 0;
		$checked = array_unique(json_decode(stripslashes($_POST['checked'])));
		
		for ($i=0; $i < count($checked); $i++) { 
			$queryLog = "UPDATE `contract` SET `show_contract`=0 WHERE id=".$checked[$i];
			if (!mysqli_query($SERVER, $queryLog))
				$errors++;
			else {
				addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Скрыл контракт - " . $queryLog);
			}
		}
		if ($errors == 0)
			echo json_encode(array('res' => 'success'));
		else
			echo json_encode(array('res' => 'error'));
	}
	if (isset($_GET['change_location'])) {
		$new_id_location = prepare($_POST['new_a_location']);
		if ($new_id_location == "all"){
			$_SESSION['id_region'] = "all";
			$_SESSION['region'] = "Все регионы";
			$_SESSION['code'] = "";
			echo 'success';
		}
		else {
			$query = mysqli_query($SERVER, "SELECT * FROM `region` WHERE id=".$new_id_location);
			if ($query) {
				$new_location = mysqli_fetch_array($query);
				$_SESSION['id_region'] = $new_location['id'];
				$_SESSION['region'] = $new_location['name_region'];
				$_SESSION['code'] = $new_location['code'];
				echo 'success';
			}
			else {
				echo 'error';
			}
		}
	}
	if (isset($_GET['admin_search'])) {
        if (!ctype_space($_POST['search_request']))
            $_SESSION['search_request'] = $_POST['search_request'];

        echo json_encode(array(
	        'result'    => 'success',
            'request'   => $_POST['search_request']
        ));
    }
	if (isset($_GET['reset_filter'])) {
        $_SESSION['shelter_id'] = $user['shelter_id']; // id приюта
        $_SESSION['order_by'] = 'data'; // сортировка по столбцам, по дефолту - по дате
        $_SESSION['sort_by'] = 'ASC'; // сортировка по > или <, по дефолту - по возрастанию
        $_SESSION['show_status'] = 'all'; // отображение по статусам, по дефолту - все статусы
        $_SESSION['row_limit'] = '25'; // количество отображаемых записей, по дефолту - 25
        $_SESSION['default_shelter'] = 'all'; // приют по умолчанию для админа, по дефолту - все
        $_SESSION['filter_id_shelter'] = 'all';// приют по умолчанию для админа, по дефолту - все
        $_SESSION['show_contract'] = 'all';
        $_SESSION['filter_kinolog'] = 'all';
        $_SESSION['show_contract_otchet'] = 'all';
        $_SESSION['num_tr'] = 0;
        $_SESSION['filter_locality'] = 'all';//населенный пункт
        $_SESSION['filter_first_date'] = 'first';
        $_SESSION['filter_last_date'] = 'last';
        $_SESSION['filter_aviarys'] = '';
        $_SESSION['filter_aviarys'] = '';
        $_SESSION['search_request'] = '';
        echo json_encode(array(
	        'res'    => 'success'
	    ));
    }
?>