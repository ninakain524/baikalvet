<?php
	require_once("config.php");
    require_once("timezone.php");
    require_once("functions.php");
    $time_now = time();
    $today = date("Y-m-d\TH:i:s");
    $rowsData = array();
    
	if (isset($_GET['gen_table'])) {
		if (isset($_GET['show_more'])) {
			$limit_constr = $_POST['start_from'].', '.$_POST['row_nums'];
		}
		else
			$limit_constr = $_SESSION['row_limit'];
			
		$shelter_constr = $_SESSION['filter_id_shelter'] == "all" ? "" : "AND id_shelter=".$_SESSION['filter_id_shelter'];
		if ($shelter_constr == "")
		    $show_status_constr = $_SESSION['show_status'] == "all" ? "" : "AND id_status=".$_SESSION['show_status'];
		else
		    $show_status_constr = $_SESSION['show_status'] == "all" ? "" : "AND id_status=".$_SESSION['show_status'];

		$show_contract = $_SESSION['show_contract'] == "all" ? "" : "AND id_contract=".$_SESSION['show_contract'];
		$kinolog_constr = $_SESSION['filter_kinolog'] == "all" ? "" : "AND id_user=".$_SESSION['filter_kinolog'];
		    
		$region_constr = $_SESSION['id_region'] == "all" ? "" : "AND application.id_region='".$_SESSION['id_region']."'"; 
		$locality_construct = $_SESSION['filter_locality'] == "all" ? "" : "AND locality='".$_SESSION['filter_locality']."'";
		$filter_first_date = $_SESSION['filter_first_date'] == "first" ? "" : strtotime($_SESSION['filter_first_date']);
		$filter_last_date = $_SESSION['filter_last_date'] == "last" ? "" : strtotime($_SESSION['filter_last_date']);
		$filter_aviarys = $_SESSION['filter_aviarys'] == "" ? "" : "AND `num_aviary`='".$_SESSION['filter_aviarys']."'"; //
		$search_request = $_SESSION['search_request'] == "" ? "" : " AND (application.id =".$_SESSION['search_request']." OR application.id=(SELECT vet_card.id_application FROM vet_card WHERE vet_card.num_birka=".$_SESSION['search_request']."))";
		$query = "SELECT 
                application.id AS id,
                application.id_user AS id_user,
                application.data AS data,
                application.street AS street,
                application.locality AS locality,
                application.id_region AS id_region,
                application.id_status AS id_status,
                application.breed AS breed, 
                application.age AS age, 
                application.weight AS weight, 
                application.height AS height, 
                application.color AS color,
                application.date_start_vosstanovlenie AS date_start_vosstanovlenie,
                application.date_start_karantin AS date_start_karantin,
                application.id_shelter AS id_shelter,
                application.num_aviary AS num_aviary,
                application.comment AS comment,
                application.show AS show_app,
                application.id_sostoyanie AS id_sostoyanie,
                animal_kind.id AS id_kind,
                animal_kind.name_kind AS kind,
                animal_gender.id AS id_gender,
                animal_gender.name_gender AS gender,
                application_status.id AS status_id,
                application_status.status_name AS status,
                vet_card.id_veterinar AS id_veterinar,
                vet_card.num_birka AS num_birka,
                vet_card.num_chip AS num_chip,
                vet_card.id_degelmintizaciya AS id_degelmintizaciya,
                vet_card.id_vaccine AS id_vaccine,
                vet_card.id_operation AS id_operation,
                DATE_FORMAT(vet_card.data,'%d.%m.%Y') AS vet_card_data,
                animal_shelters.id AS id_shelter,
                animal_shelters.name_shelter AS name_shelter,
                users.sourname AS user_sourname,
                users.name AS user_name,
                users.patronymic AS user_patronymic,
                region.id AS id_region,
                region.name_region AS name_region,
                contract.id AS contract_id,
                contract.number AS contract_number,
                contract.customer AS contract_customer,
                contract.name_contract AS name_contract,
                DATE_FORMAT(contract.date_contract,'%d.%m.%Y') AS date_contract,
				animal_color.id AS id_color,
				animal_color.name_color AS name_color
                FROM application 
                LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
                LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
                LEFT JOIN application_status ON application.id_status = application_status.id
                LEFT JOIN vet_card ON application.id = vet_card.id_application 
                LEFT JOIN animal_shelters ON application.id_shelter = animal_shelters.id
                LEFT JOIN users ON application.id_user = users.id
                LEFT JOIN region ON application.id_region = region.id
                LEFT JOIN contract ON application.id_contract = contract.id
				LEFT JOIN animal_color ON application.color = animal_color.id
                WHERE application.data BETWEEN ".$filter_first_date." AND ".$filter_last_date." ".$search_request." ".$region_constr." ".$filter_aviarys." ".$locality_construct." ".$shelter_constr." ".$show_status_constr." ".$show_contract." ".$kinolog_constr." ORDER BY `".$_SESSION['order_by']."` ".$_SESSION['sort_by']." LIMIT ".$limit_constr;
		$res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
		$row_count = 0;
		while ($res = mysqli_fetch_array($res_query)) {
			$row_count++;
			
			$date_start_karantin = "";
			if ( $res['date_start_karantin'] != 0 && $res['date_start_karantin'] != NULL)
			    $date_start_karantin = date('d.m.Y H:i:s', $res['date_start_karantin']);
            
            // Ответственный ветеринар
            
            $name_string_kin = $name_string_vet = $degel_date = $degel_preparat = $vac_date = $vac_preparat = $o_date = "";
			// if ($res['id_user'] != 0 && $res['id_user'] != "" && $res['id_user'] != NULL) {
				$sourname_kin = $res['user_sourname'];
				$name_kin = mb_substr($res['user_name'], 0, 1);
				$patronymic_kin = mb_substr($res['user_patronymic'], 0, 1);
				$name_string_kin = $sourname_kin." ".$name_kin.". ".$patronymic_kin."."; 
			// }            
            $rs = mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM vet_card WHERE id_application=".$res['id']);
            $result = mysqli_fetch_array($rs);
            if ($result[0] > 0) {
                $vet = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$res['id_veterinar']));
                if (!empty($vet['id'])) {
                    $sourname_vet = $vet['sourname'];
                    $name_vet = mb_substr($vet['name'], 0, 1);
                    $patronymic_vet = mb_substr($vet['patronymic'], 0, 1);
                    $name_string_vet = $sourname_vet." ".$name_vet.". ".$patronymic_vet."."; 
                }
                
                $degel = mysqli_fetch_array(mysqli_query($SERVER, 
                    "SELECT 
                    vet_degilmintizaciya.id AS id,
                    DATE_FORMAT(vet_degilmintizaciya.data,'%d.%m.%Y') AS degel_date,
                    vet_degilmintizaciya.preparat AS degel_preparat
                    FROM vet_degilmintizaciya WHERE id=".$res['id_degelmintizaciya']));
                if (!empty($degel['id'])) {
                    $degel_date = $degel['degel_date'];
                    $degel_preparat = $degel['degel_preparat'];
                }
                
                $vac = mysqli_fetch_array(mysqli_query($SERVER, 
                    "SELECT 
                    vet_vaccine.id AS id,
                    DATE_FORMAT(vet_vaccine.data,'%d.%m.%Y') AS vac_date,
                    vet_vaccine.preparat AS vac_preparat
                    FROM vet_vaccine WHERE id=".$res['id_vaccine']));
                if (!empty($vac['id'])) {
                    $vac_date = $vac['vac_date'];
                    $vac_preparat = $vac['vac_preparat'];
                }
                
                $oper = mysqli_fetch_array(mysqli_query($SERVER, 
                    "SELECT 
                    vet_operation.id AS id,
                    DATE_FORMAT(vet_operation.data,'%d.%m.%Y') AS o_date
                    FROM vet_operation WHERE id=".$res['id_operation']));
                if (!empty($oper['id'])) {
                    $o_date = $oper['o_date'];
                }
                
            }
            
            $res_image = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `upload_files` WHERE `id_application`='".$res['id']."' AND `type`='img'"));
            
            if(!empty($res_image)) {
				$path = $res_image['path_thumb'].$res_image['name_thumb'];
				$url = "https://baikalvet.ru".$path;
                $urlHeaders = @get_headers($url);
                if(strpos($urlHeaders[0], '200')) {
                    $img = $path;
				}
				else {
				    $img = $res_image['path_thumb'].$res_image['name_thumb'];
				}
		    }   
			else
				$img = "assets/images/no-image.png";
				
			$vet_card_data = $res['vet_card_data'];
			if($vet_card_data == "00.00.0000") $vet_card_data = "";
			if($degel_date == "00.00.0000") $degel_date = "";
			if($vac_date == "00.00.0000") $vac_date = "";
			if($o_date == "00.00.0000") $o_date = "";

			$show = "нет";
			if($res['show_app'] == 1) $show = "да";
            
			$rowsData[] = 	"<tr id='".$res['id']."' class='application tr-edit-cell' data='{$res['id']}'>
			                <td class='no'><div class='id-color'>{$_SESSION['num_tr']}</div></td>
			                <td class='no'><img src='{$img}' class='table-img'></td>
			                <td id='data' class='data".$res['id']." dateTedit'>".date('d.m.Y H:i:s', $res['data'])."</td>
			                <td id='date_start_karantin' class='date_start_karantin".$res['id']." dateTedit'>{$date_start_karantin}</td>
			                <td id='name_region' class='name_region".$res['id']." selectTedit' data-table='region' data-field='{$res['id_region']}'>{$res['name_region']}</td>
			                <td id='locality' class='locality".$res['id']." TextTedit'>{$res['locality']}</td>
			                <td id='street' class='street".$res['id']." TextTedit'>{$res['street']}</td>
			                <td id='name_shelter' class='name_shelter".$res['id']." selectTedit' data-table='animal_shelters' data-field='{$res['id_shelter']}'>{$res['name_shelter']}</td>
			                <td id='num_aviary' class='num_aviary".$res['id']." no'>{$res['num_aviary']}</td>
			                <td id='name_kind' class='name_kind".$res['id']." selectTedit' data-table='animal_kind' data-field='{$res['id_kind']}'>{$res['kind']}</td>
			                <td id='breed' class='breed".$res['id']." TextTedit'>{$res['breed']}</td>
			                <td id='name_gender' class='name_gender".$res['id']." selectTedit' data-table='animal_gender' data-field='{$res['id_gender']}'>{$res['gender']}</td>
			                <td id='name_color' class='name_color".$res['id']." selectTedit' data-table='animal_color' data-field='{$res['id_color']}'>{$res['name_color']}</td>
			                <td id='age' class='age".$res['id']." TextTedit'>{$res['age']}</td>
			                <td id='weight' class='weight".$res['id']." TextTedit'>{$res['weight']}</td>
			                <td id='height' class='height".$res['id']." TextTedit'>{$res['height']}</td>
			                <td id='customer' class='customer".$res['id']." selectTedit' data-table='contract' data-field='{$res['contract_id']}'>{$res['name_contract']}</td>
			                <td id='comment' class='comment".$res['id']." TextTedit'>{$res['comment']}</td>
			                <td id='status_name' class='status_name".$res['id']." selectTedit' data-table='application_status' data-field='{$res['status_id']}'>{$res['status']}</td>
			                <td id='kinolog' class='kinolog".$res['id']." selectTedit' data-table='users' data-field='{$res['id_user']}'>{$name_string_kin}</td>
			                <td id='vet_card_data' class='vet_card_data".$res['id']." dateTedit'>{$vet_card_data}</td>
			                <td id='sourname' class='sourname".$res['id']." selectTedit' data-table='users' data-field='{$res['id_veterinar']}'>{$name_string_vet}</td>
			                <td id='num_birka' class='num_birka".$res['id']." TextTedit'>{$res['num_birka']}</td>
			                <td id='num_chip' class='num_chip".$res['id']." TextTedit'>{$res['num_chip']}</td>
			                <td id='degel_date' class='degel_date".$res['id']." dateTedit'>{$degel_date}</td>
			                <td id='degel_preparat' class='degel_preparat".$res['id']." TextTedit'>{$degel_preparat}</td>
			                <td id='vac_date' class='vac_date".$res['id']." dateTedit'>{$vac_date}</td>
			                <td id='vac_preparat' class='vac_preparat".$res['id']." TextTedit'>{$vac_preparat}</td>
			                <td id='o_date' class='o_date".$res['id']." dateTedit'>{$o_date}</td>

							<td id='show_app' class='show_app".$res['id']." selectTedit' data-field='{$res['show_app']}'>{$show}</td>
							<td id='id' class='id".$res['id']." TextTedit'>{$res['id']}</td>
			                ";
			                $rowsData[] .= "</tr>";
			                
			             //   <td id='sostoyanie' class='TextTedit'>{$sostoyanie}</td>
			             //   <td id='show_app' class='TextTedit'>{$res['show_app']}</td>
		
		    $_SESSION['num_tr'] = $_SESSION['num_tr'] + 1;
		}
		$html = "";
	    foreach($rowsData as $row){
	        $html .= $row;
	    }
	    echo json_encode(array(
	        'result'    => 'success',
	        'html'      => $html,
	        'row_count' => $row_count
	    ));
	}
	if (isset($_GET['update_table_config'])) {
	    $_SESSION['num_tr'] = 1;
		$_SESSION['filter_id_shelter'] = intval($_POST['id_shelter']);
		switch ($_POST['show_status']) {
			case '0':
				$_SESSION['show_status'] = 'all';
				break;
			case '1':
				$_SESSION['show_status'] = '1';
				break;
			case '2':
				$_SESSION['show_status'] = '2';
				break;
			case '3':
				$_SESSION['show_status'] = '3';
				break;
			case '4':
				$_SESSION['show_status'] = '4';
				break;
			case '5':
				$_SESSION['show_status'] = '5';
				break;
			case '6':
				$_SESSION['show_status'] = '6';
				break;
			case '7':
				$_SESSION['show_status'] = '7';
				break;
			case '8':
				$_SESSION['show_status'] = '8';
				break;
			case '9':
				$_SESSION['show_status'] = '9';
				break;
			case '10':
				$_SESSION['show_status'] = '10';
				break;
			case '11':
				$_SESSION['show_status'] = '11';
				break;
			case '12':
				$_SESSION['show_status'] = '12';
				break;
			default:
				$_SESSION['show_status'] = 'all';
				break;
		}

		if ($_POST['show_contract'] == 0) {
            $_SESSION['show_contract'] = 'all';
        }
        $res_query = mysqli_query($SERVER, "SELECT id AS id FROM contract WHERE show_contract=1") or die("Ошибка " . mysqli_error($SERVER));
		while ($res = mysqli_fetch_array($res_query)) {
            if ($_POST['show_contract'] == $res['id']) {
                $_SESSION['show_contract'] = $res['id'];
            } 
		}

		if ($_POST['kinolog'] == 0) {
            $_SESSION['filter_kinolog'] = 'all';
        }
        $res_query = mysqli_query($SERVER, "SELECT id AS id FROM `users` WHERE `show` = 1 AND `podtverzhdeno` = 1 AND `id_users_group` = 4") or die("Ошибка " . mysqli_error($SERVER));
		while ($res = mysqli_fetch_array($res_query)) {
            if ($_POST['kinolog'] == $res['id']) {
                $_SESSION['filter_kinolog'] = $res['id'];
            } 
		}
		
		switch ($_POST['order_by']) {
			case '1':
				$_SESSION['order_by'] = 'data';
				break;
			case '2':
				$_SESSION['order_by'] = 'locality';
				break;
			case '3':
				$_SESSION['order_by'] = 'street';
				break;
			case '4':
				$_SESSION['order_by'] = 'id_kind';
				break;
			case '5':
				$_SESSION['order_by'] = 'breed';
				break;			
			case '6':
				$_SESSION['order_by'] = 'age';
				break;
			case '7':
				$_SESSION['order_by'] = 'color';
				break;
			case '8':
				$_SESSION['order_by'] = 'id';
				break;
			case '9':
				$_SESSION['order_by'] = 'num_aviary';
				break;
			default:
				$_SESSION['order_by'] = 'data';
				break;
		}
		switch ($_POST['sort_by']) {
			case 'DESC':
				$_SESSION['sort_by'] = 'DESC';
				break;
			case 'ASC':
				$_SESSION['sort_by'] = 'ASC';
				break;
			default:
				$_SESSION['sort_by'] = 'ASC';
				break;
		}
		switch ($_POST['row_limit']) {
			case '25':
				$_SESSION['row_limit'] = '25';
				break;
			case '50':
				$_SESSION['row_limit'] = '50';
				break;
			case '75':
				$_SESSION['row_limit'] = '75';
				break;
			case '100':
				$_SESSION['row_limit'] = '100';
				break;
			default:
				$_SESSION['row_limit'] = '25';
				break;
		}
		switch ($_POST['filter_locality']) {
			case 'all':
				$_SESSION['filter_locality'] = "all";
				break;
			default:
				$_SESSION['filter_locality'] = $_POST['filter_locality'];
				break;
		}
		if ($_POST['filter_first_date'] == "first") {
			$_SESSION['filter_first_date'] = $sql = mysqli_fetch_array(mysqli_query($SERVER, "SELECT MIN(`data`) AS filter_first_date FROM `application`"));
		}
		else {
			$_SESSION['filter_first_date'] = $_POST['filter_first_date'];
			
		}
		if ($_POST['filter_last_date'] == "last") {
			$_SESSION['filter_last_date'] = $sql = mysqli_fetch_array(mysqli_query($SERVER, "SELECT MAX(`data`) AS filter_last_date FROM `application`"));
		}
		else {
			$_SESSION['filter_last_date'] = $_POST['filter_last_date'];
			
		}
		if ($_POST['filter_aviarys'] == "")
			$_SESSION['filter_aviarys'] = "";
		else
			$_SESSION['filter_aviarys'] = $_POST['filter_aviarys'];
		echo json_encode(array('res'    => 'success'));
	}
?>