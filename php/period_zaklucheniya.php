<?php
    require_once("../php/config.php");
    require_once("timezone.php");
    require_once("functions.php");
    if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false)) 
    {
        header('location: auth/login.php' );
        exit();
    }
    
    $data_first = $data_second = "";
    
    $today = date("Y-m-d\TH:i:s");
    $today1 = date("Y-m-d");
    
    function format_interval(DateInterval $interval) {
        $result = "";
        if ($interval->d) { $result .= $interval->format("%d д. "); }
        if ($interval->h) { $result .= $interval->format("%h ч. "); }
        if ($interval->i) { $result .= $interval->format("%i мин. "); }
    
        return $result;
    };
    
    if (isset($_POST['data_first'])) {
    
    $data_def = $_POST['data_def'];
    $data_first = $_POST['data_first'];
    $data_second = $_POST['data_second'];

    $part_query_kinolog = $part_query_shelter = $part_query_contract = "";

    $contract = $_POST['contract1'];
    if ($contract != "all") 
        $part_query_contract = " AND contract.id='".$contract."' ";

    $adress = $_POST['adress'];
    $adress_text = "";
    if ($adress != "all" && $adress != "" && $adress != "selected") {
        $adressQuery = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".$adress));
        $adress_text = " AND application.loc_type='".$adressQuery['loc_type']."' AND application.locality='".$adressQuery['locality']."' AND application.street='".$adressQuery['street']."' ";
    }

    $kinolog = $_POST['kinolog'];
    if ($kinolog != "all") 
        $part_query_kinolog = " AND application.id_user='".$kinolog."' ";

    $shelter = $_POST['shelter'];
    if ($shelter != "all") 
        $part_query_shelter = " AND animal_shelters.id='".$shelter."' ";

    $type = $_POST['type'];

    if ($type == "akt_otlova") {
        $query = "SELECT 
            application.id AS id,
            application.data AS data,
            application.street AS street,
            application.loc_type AS loc_type,
            application.locality AS locality,
            application.breed AS breed, 
            application.age AS age, 
            application.id_user AS id_user, 
            application.weight AS weight, 
            application.color AS color,
            application.date_start_vosstanovlenie AS date_start_vosstanovlenie,
            application.date_start_karantin AS date_start_karantin,
            animal_kind.name_kind AS kind,
            animal_gender.name_gender AS gender,
            application_status.id AS status_id,
            application_status.status_name AS status,
            vet_card.num_birka AS num_birka,
            vet_card.num_chip AS num_chip,
            DATE_FORMAT(vet_card.data,'%d.%m.%Y') AS vet_card_data,
            animal_shelters.name_shelter AS name_shelter,
            contract.number AS contract_number,
            contract.customer AS contract_customer,
            contract.name_contract AS name_contract,
            DATE_FORMAT(contract.date_contract,'%d.%m.%Y') AS date_contract,
            users.sourname AS sourname,
            users.name AS name,
            users.patronymic AS patronymic
            FROM application 
            LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
            LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
            LEFT JOIN application_status ON application.id_status = application_status.id
            LEFT JOIN vet_card ON application.id = vet_card.id_application
            LEFT JOIN animal_shelters ON application.id_shelter = animal_shelters.id
            LEFT JOIN users ON application.id_user = users.id
            LEFT JOIN contract ON application.id_contract = contract.id
            WHERE application.data >= '".$data_first."' AND application.data <= '".$data_second."' AND application.show='1'".$part_query_kinolog."".$part_query_shelter."".$part_query_contract."".$adress_text."ORDER BY application.data ASC";
        
        $res = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
        $adressArray = $kinArray = $countArray = $countArray1 = [];
        $adress1 = $item_adress = $street = $kin_name = $count = "";

        while($row = mysqli_fetch_array($res))
        {   
            if (($row['street'] != "") && ($street == $row['street'])){
                $item_adress = "";
                $street = "";
            }
            else{
                $street = $row['street'];
                $item_adress = $row['loc_type'] . "." . $row['locality'] . ", " . $street . "; "; 
            }
            array_push($adressArray, $item_adress);

            if ($kinolog == "all") {
                $name = $row['name'];
                $name1 = mb_substr($name, 0, 1);
                $patronymic = $row['patronymic'];
                $patronymic1 = mb_substr($patronymic, 0, 1);
                $kin_name = $row['sourname']. " " .$name1. ". " .$patronymic1. ".";
                array_push($kinArray, $kin_name);
                array_push($countArray, $row['id_user']);
            }
        }
        $adress1 = implode('', array_unique($adressArray)); //удалить дубликаты и в строку
        $kin_name = implode('; ', array_unique($kinArray));
        $usersArray = array_unique($countArray);
        

        $card = mysqli_fetch_array(mysqli_query($SERVER, $query));
        if ($kinolog != "all") {
            $countQuery = mysqli_fetch_array(mysqli_query($SERVER, "SELECT COUNT(1) FROM application 
            LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
            LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
            LEFT JOIN application_status ON application.id_status = application_status.id
            LEFT JOIN vet_card ON application.id = vet_card.id_application
            LEFT JOIN animal_shelters ON application.id_shelter = animal_shelters.id
            LEFT JOIN contract ON application.id_contract = contract.id
            WHERE application.data >= '".$data_first."' AND application.data <= '".$data_second."' AND application.show='1'".$part_query_kinolog."".$part_query_shelter."".$part_query_contract."".$adress_text."ORDER BY application.data ASC"));
            $count = $countQuery[0];  
        }
        else {
            foreach($usersArray as $user) { 
                $countQuery = mysqli_fetch_array(mysqli_query($SERVER, "SELECT COUNT(1) FROM application 
                LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
                LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
                LEFT JOIN application_status ON application.id_status = application_status.id
                LEFT JOIN vet_card ON application.id = vet_card.id_application
                LEFT JOIN animal_shelters ON application.id_shelter = animal_shelters.id
                LEFT JOIN contract ON application.id_contract = contract.id
                WHERE application.id_user = '".intval($user)."' AND application.data >= '".$data_first."' AND application.data <= '".$data_second."' AND application.show='1'".$part_query_kinolog."".$part_query_shelter."".$part_query_contract."".$adress_text."ORDER BY application.data ASC"));
                array_push($countArray1, $countQuery[0]);
            }
            $count = implode('; ', $countArray1);
        }
        if(!empty($card)){
            if ($kinolog != "all") {
                $user = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$card['id_user']));
                    
                $name = $user['name'];
                $name1 = mb_substr($name, 0, 1);
                $patronymic = $user['patronymic'];
                $patronymic1 = mb_substr($patronymic, 0, 1);

                $kin_name = $user['sourname']. " " .$name1. ". " .$patronymic1. ".";
            }
        }
    }
?>
    <!--Формирование акта отлова-->
    <div id="hide-form-popup-num_aviary" class="hide-form-popup-num_aviary hideform fancy-animate">
        <form id="popup-num_aviary" method="post" action="../libs/phpword/generate_word_akt_otlova.php?adress=<? echo ($adress) ?>&kinolog=<? echo ($kinolog) ?>&contract=<? echo ($contract) ?>&shelter=<? echo ($shelter) ?>&data_first=<?php echo ($data_first) ?>&data_second=<?php echo ($data_second) ?>" enctype="multipart/form-data">
          <h3>Формирование акта отлова за период</h3>
          <div class="form-group">
            <p>Дата от</p>
            <input class="field width-add field-date" type="date" name="data" autocomplete="on" value="<? echo $data_def; ?>" />  
          </div>
          <div class="form-group">
             <p>Места отлова</p>
             <textarea class="field width-add" name="mesto" id="mesto" autocomplete="off" style="width: 500px; min-height: 113px;"><? echo ($adress1) ?></textarea>  
          </div>
          <div class="form-group">
              <p>Кинолог</p>
                <input class="field width-add" type="text" name="kinolog" id="kinolog" value="<? echo ($kin_name) ?>" />  
          </div>
          <div class="form-group">
              <p>Отловлено животных без владельцев</p>
                <input class="field width-add" type="text" name="count_animals_get" value="<? echo ($count) ?>" />  
          </div>
          <div class="form-group">
              <p>Передано в приют для животных</p>
                <input class="field width-add" type="text" name="count_animals_shelter" value="<? echo ($count) ?>" />  
          </div>
          <div class="form-group">
              <p>Aдминистратор приюта </p>
                <select class="field width-add" id="admin_shelter" name="admin_shelter">
                  <?php
                    $sql = "SELECT * FROM `users` WHERE id_users_group = 2;";
                    $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                    echo '<option value="" selected readonly hidden>Выберите администратора приюта</option>';
    
                    while($row = mysqli_fetch_array($res))
                    {
                        $nameA = $row['name'];
                        $nameA1 = mb_substr($nameA, 0, 1);
                        $patronymicA = $row['patronymic'];
                        $patronymicA1 = mb_substr($patronymicA, 0, 1);
                        echo '<option value="'. $row['sourname'] . ' ' .$nameA1. '. ' .$patronymicA1 .'">'. $row['sourname'] . ' ' .$nameA1. '. ' .$patronymicA1. '.</option>';
                    }
                  ?>
                </select>
          </div>
          <div class="form-group">
              <p>Количество погибших при отлове животных (если есть)</p>
                <input class="field width-add" type="number" name="count_animals_die" value="0" />  
          </div>
          <div class="form-group">
              <p>Причина гибели животных без владельцев (если есть)</p>
                <input class="field width-add" type="textarea" name="comment" value=""/>  
          </div>
          <div class="form-group">
              <p>Заказчик</p>
                <input class="field width-add" type="text" name="zakazchik_name" />  
          </div>
          <div class="form-group">
              <p>Должность заказчика</p>
                <input class="field width-add" type="text" name="zakazchik_role" />  
          </div>
        
          <input class="btn button-auth field-submit field-width" type="submit" value="Сформировать" autofocus />
        </form>
      </div>
    <!--/Формирование акта отлова-->

    <!--Формирование актов отлова-->
    <div id="hide-form-popup-num_aviary1" class="hide-form-popup-num_aviary hideform fancy-animate"> 
        <form id="popup-num_aviary" method="post" action="../libs/phpword/generate_word_akt_otlova_mass.php?adress=<? echo ($adress) ?>&kinolog=<? echo ($kinolog) ?>&contract=<? echo ($contract) ?>&shelter=<? echo ($shelter) ?>&data_first=<?php echo ($data_first) ?>&data_second=<?php echo ($data_second) ?>" enctype="multipart/form-data">
          <h3>Формирование актов отлова за период</h3>
          <div class="form-group">
            <p>Дата от</p>
            <input class="field width-add field-date" type="date" name="data" autocomplete="on" value="<? echo $data_def; ?>" />  
          </div>
          <div class="form-group">
             <p>Места отлова</p>
             <textarea class="field width-add" name="mesto" id="mesto" autocomplete="off" style="width: 500px; min-height: 113px;"><? echo ($adress1) ?></textarea>  
          </div>
          <div class="form-group">
              <p>Кинологи</p>
                <input class="field width-add" type="text" name="kinolog" id="kinolog" value="<? echo ($kin_name) ?>" />  
          </div>
          <div class="form-group">
              <p>Отловлено животных без владельцев</p>
                <input class="field width-add" type="text" name="count_animals_get" value="<? echo ($count) ?>" />  
          </div>
          <div class="form-group">
              <p>Передано в приют для животных</p>
                <input class="field width-add" type="text" name="count_animals_shelter" value="<? echo ($count) ?>" />  
          </div>
          <div class="form-group">
              <p>Aдминистратор приюта </p>
                <select class="field width-add" id="admin_shelter" name="admin_shelter">
                  <?php
                    $sql = "SELECT * FROM `users` WHERE id_users_group = 2;";
                    $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                    echo '<option value="" selected readonly hidden>Выберите администратора приюта</option>';
    
                    while($row = mysqli_fetch_array($res))
                    {
                        $nameA = $row['name'];
                        $nameA1 = mb_substr($nameA, 0, 1);
                        $patronymicA = $row['patronymic'];
                        $patronymicA1 = mb_substr($patronymicA, 0, 1);
                        echo '<option value="'. $row['sourname'] . ' ' .$nameA1. '. ' .$patronymicA1 .'">'. $row['sourname'] . ' ' .$nameA1. '. ' .$patronymicA1. '.</option>';
                    }
                  ?>
                </select>
          </div>
          <div class="form-group">
              <p>Количество погибших при отлове животных (если есть)</p>
                <input class="field width-add" type="number" name="count_animals_die" value="0" />  
          </div>
          <div class="form-group">
              <p>Причина гибели животных без владельцев (если есть)</p>
                <input class="field width-add" type="textarea" name="comment" value=""/>  
          </div>
          <div class="form-group">
              <p>Заказчик</p>
                <input class="field width-add" type="text" name="zakazchik_name" />  
          </div>
          <div class="form-group">
              <p>Должность заказчика</p>
                <input class="field width-add" type="text" name="zakazchik_role" />  
          </div>
        
          <input class="btn button-auth field-submit field-width" type="submit" value="Сформировать" autofocus />
        </form>
      </div>
    <!--/Формирование акта отлова-->

    <!--Выбрать ветеринара-->
    <div id="hide-form-popup-select-vet" class="hide-form-popup-select-vet hideform fancy-animate">
        <form id="popup-select-vet" enctype="multipart/form-data">
            <h3>Выберите ветеринара для первичных заключений</h3>
            <div class="col-12">
            <div class="form-group">
                <select class="field width-add" id="vet-select" name="vet-select" required>
                <?php
                    $sql_k = "SELECT * FROM `users` WHERE `show` = 1 AND `podtverzhdeno` = 1 AND `id_users_group` = 3;";
                    $res_k = mysqli_query($SERVER, $sql_k) or die("Ошибка " . mysqli_error($SERVER));
                    echo '<option value="" selected readonly hidden>Выберите ветеринара</option>';
                    while($row_k = mysqli_fetch_array($res_k))
                    {
                        echo '<option value="'. $row_k['id'] .'">'.$row_k['sourname']." ".$row_k['name'].'</option>';
                    }
                ?>
                </select>
            </div>
            </div> 
            <input class="btn button-auth field-submit field-width" type="submit" id="get_zak_first1" value="Сформировать" autofocus />
        </form>
    </div>
    <!--/Выбрать ветеринара-->

    <input type="date-time" name="data_first" id="data_first" class="hide_all" value="<?php echo $data_first; ?>" autocomplete="off" style="display: none;"/>
    <input type="date-time" name="data_second" id="data_second" class="hide_all" value="<?php echo $data_second; ?>" autocomplete="off" style="display: none;"/>
    <input type="text" id="contract" class="hide_all" value="<? echo $contract; ?>" style="display: none;"/>
    <input type="text" id="adress" class="hide_all" value="<? echo $adress; ?>" style="display: none;"/>
    <input type="text" id="kinolog" class="hide_all" value="<? echo $kinolog; ?>" style="display: none;"/>
    <input type="text" id="shelter" class="hide_all" value="<? echo $shelter; ?>" style="display: none;"/>
    <input type="text" id="type" class="hide_all" value="<? echo $type; ?>" style="display: none;"/>
    
    <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab">
      <tr class="tab-col1">
        <th rowspan="2">№</th>
        <th rowspan="2">Дата</th>
        <th rowspan="2">Контракт</th>
        <th rowspan="2">Место отлова</th>
        <th colspan="6">Описание животного</th>
        <th rowspan='2'>Приют</th>
        <th rowspan='2'>Кинолог</th>
        <th rowspan="2">Статус</th>
      </tr>
      <tr class="tab-col1">
        <th>Вид</th>
        <th>Порода</th>
        <th>Пол</th>
        <th>Окрас</th>
        <th>Возраст</th>
        <th>Вес</th>
      </tr>
 
      <? 
        $part_query = "LEFT JOIN vet_card ON application.id = vet_card.id_application";
        $part_status = "";
        if ($type == "akt_otlova") {}
        // if ($type == "first_zakluchenie") {}
        if ($type == "second_zakluchenie") {
            $part_query = "RIGHT JOIN vet_card ON application.id = vet_card.id_application";
        }
        // if ($type == "uchetnoe_delo") {}
        if ($type == "akt_vibitiya") {
            $part_status = " AND application_status.id=4 ";
        }

        $query = "SELECT 
            application.id AS id,
            application.data AS data,
            application.street AS street,
            application.loc_type AS loc_type,
            application.locality AS locality,
            application.breed AS breed, 
            application.age AS age, 
            application.weight AS weight, 
            application.color AS color,
            application.date_start_vosstanovlenie AS date_start_vosstanovlenie,
            application.date_start_karantin AS date_start_karantin,
            animal_kind.name_kind AS kind,
            animal_gender.name_gender AS gender,
            application_status.id AS status_id,
            application_status.status_name AS status,
            vet_card.num_birka AS num_birka,
            vet_card.num_chip AS num_chip,
            DATE_FORMAT(vet_card.data,'%d.%m.%Y') AS vet_card_data,
            animal_shelters.name_shelter AS name_shelter,
            contract.number AS contract_number,
            contract.customer AS contract_customer,
            contract.name_contract AS name_contract,
            DATE_FORMAT(contract.date_contract,'%d.%m.%Y') AS date_contract,
            users.sourname AS user_sourname,
            users.name AS user_name,
            users.patronymic AS user_patronymic
            FROM application 
            LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
            LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
            LEFT JOIN application_status ON application.id_status = application_status.id
            ".$part_query."
            LEFT JOIN animal_shelters ON application.id_shelter = animal_shelters.id
            LEFT JOIN users ON application.id_user = users.id
            LEFT JOIN contract ON application.id_contract = contract.id
            WHERE application.data >= '".$data_first."' AND application.data <= '".$data_second."' AND application.show='1'".$part_query_kinolog."".$part_query_shelter."".$part_query_contract."".$part_status."".$adress_text."ORDER BY application.data ASC";

        $res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
        $i = 1;
        while ($res = mysqli_fetch_array($res_query)) {
            
            if (ctype_digit($res['color'])) {
                $animal_color1 = mysqli_fetch_array(mysqli_query($SERVER, "SELECT name_color FROM animal_color WHERE id=".$res['color']));
                $animal_color = $animal_color1['name_color'];
            }
          else
              $animal_color = $res['color']; 

            $end = date('Y-m-d H:i', $res['date_start_vosstanovlenie']+777600);
            $first_date = new DateTime($today);
            $second_date = new DateTime($end);
            $difference = $first_date->diff($second_date);
            $interval = format_interval($difference);
            
            $end1 = date('Y-m-d H:i', $res['date_start_karantin']+777600);
            $first_date1 = new DateTime($today);
            $second_date1 = new DateTime($end1);
            $difference1 = $first_date1->diff($second_date1);
            $interval1 = format_interval($difference1);

            $sourname_user = $res['user_sourname'];
            $name_user = mb_substr($res['user_name'], 0, 1);
            $patronymic_user = mb_substr($res['user_patronymic'], 0, 1); 
            $name_string = $sourname_user." ".$name_user.". ".$patronymic_user.".";

          echo "<tr data-href='/animal_card.php?application={$res['id']}' class='application'>
            <td><div class='id-color'>{$i}</div></td>
            <td>".date('d.m.Y', $res['data'])."</td>
            <td>{$res['name_contract']}</td>
            <td>{$res['loc_type']} {$res['locality']}, {$res['street']}</td>
            <td>{$res['kind']}</td>
            <td>{$res['breed']}</td>
            <td>{$res['gender']}</td>
            <td>{$animal_color}</td>
            <td>{$res['age']}</td>
            <td>{$res['weight']}</td>
            <td>{$res['name_shelter']}</td>
            <td>{$name_string}</td>";
            if($res['status_id'] == 3) //на восстановлении
            { echo "<td>{$res['status']}<br> <div class='status-time'>Осталось: {$interval}</div></td>"; }
            else if($res['status_id'] == 2) //на карантине
            { echo "<td>{$res['status']}<br> <div class='status-time'>Осталось: {$interval1}</div></td>"; }
            else
            { echo "<td>{$res['status']}</td>"; }
            echo "</tr>";
            $i = $i + 1;
        }
      ?>
    </table>
    <?
        if ($type == "akt_otlova") {
            if ($kinolog != "all")
                echo '<input id="get_akt" name="display-set" type="submit" class="btn button-auth field-submit btn-card" value="Сформировать акт отлова" autofocus />';
            else {
            ?>
                <form style="margin-top:20px;" id="popup-num_aviary" method="post" action="../libs/phpword/generate_word_akt_otlova_mass.php?adress=<? echo ($adress) ?>&kinolog=<? echo ($kinolog) ?>&contract=<? echo ($contract) ?>&shelter=<? echo ($shelter) ?>&data_first=<?php echo ($data_first) ?>&data_second=<?php echo ($data_second) ?>" enctype="multipart/form-data">
                    <input class="btn button-auth field-submit field-width" type="submit" value="Сформировать акт отлова" autofocus />
                </form>
            <?
            }
        }
        if ($type == "first_zakluchenie") {
            echo '<input id="get_zak_first" type="submit" class="btn button-auth field-submit btn-card" value="Экспорт первичных заключений" autofocus />';
        }
        if ($type == "second_zakluchenie") {
            echo '<input id="get_zak" type="submit" class="btn button-auth field-submit btn-card" value="Экспорт вторичных заключений" autofocus />';
        }
        if ($type == "uchetnoe_delo") {
            echo '<input id="uchet_del" type="submit" class="btn button-auth field-submit btn-card" value="Экспорт учетных дел" autofocus />';
        }
        if ($type == "akt_vibitiya") {
            echo '<input id="akt" type="submit" class="btn button-auth field-submit btn-card" value="Экспорт актов выбытия" autofocus />';
        }

        // if ($type == "akt_otlova") {
        //     if ($kinolog != "all")
        //         echo '<input id="get_akt" name="display-set" type="submit" class="btn button-auth field-submit btn-card" value="Сформировать акт отлова" autofocus />';
        //     else
        //         echo '<input id="get_akt1" name="display-set" type="submit" class="btn button-auth field-submit btn-card" value="Сформировать акт отлова" autofocus />';
        // }
        // if ($type == "first_zakluchenie") {
        //     echo '<input id="get_zak_first" type="submit" class="btn button-auth field-submit btn-card" value="Экспорт первичных заключений" autofocus />';
        // }
        // if ($type == "second_zakluchenie") {
        //     echo '<input id="get_zak" type="submit" class="btn button-auth field-submit btn-card" value="Экспорт вторичных заключений" autofocus />';
        // }
        // if ($type == "uchetnoe_delo") {
        //     echo '<input id="uchet_del" type="submit" class="btn button-auth field-submit btn-card" value="Экспорт учетных дел" autofocus />';
        // }
        // if ($type == "akt_vibitiya") {
        //     echo '<input id="akt" type="submit" class="btn button-auth field-submit btn-card" value="Экспорт актов выбытия" autofocus />';
        // }
    }
?>
<script type="text/javascript">

    var data_first1 = "";
    var data_first = "";
    var data_second1 = "";
    var data_second = "";
    var contract2 =  "";
    var adress =  "";
    var kinolog =  "";
    var shelter =  "";
    var type =  "";

    $("#get_zak_first").click(function() {
        $.fancybox.open({
          src  : '#hide-form-popup-select-vet',
          type : 'inline'
        });
    });

    $("#get_akt").click(function() {
        $.fancybox.open({
          src  : '#hide-form-popup-num_aviary',
          type : 'inline'
        });
    });

    $("#get_akt1").click(function() {
        $.fancybox.open({
          src  : '#hide-form-popup-num_aviary1',
          type : 'inline'
        });
    });
    
    data_first1 = document.getElementById('data_first').value + "T00:00:00";
    data_first = Date.parse(data_first1)/1000 - 21600;
    data_second1 = document.getElementById('data_second').value + "T23:59:59";
    data_second =  Date.parse(data_second1)/1000 - 21600;

    contract2 =  document.getElementById('contract').value;
    adress =  document.getElementById('adress').value;
    kinolog =  document.getElementById('kinolog').value;
    shelter =  document.getElementById('shelter').value;
    type =  document.getElementById('type').value;

    $('body').on('click', '#get_zak', function(){
        console.log("1111");
        window.open('../libs/phpword/generate_zakluchenie_second.php?adress=' + adress + '&kinolog=' + kinolog + '&contract=' + contract2 + '&shelter=' + shelter + '&data_first=' + data_first + '&data_second=' + data_second);
    });

    $('body').on('click', '#get_zak_first1', function(){
        console.log("1111");
        let vet_select =  document.getElementById('vet-select').value;
        window.open('../libs/phpword/generate_zakluchenie_first.php?adress=' + adress + '&vet_select=' + vet_select + '&kinolog=' + kinolog + '&contract=' + contract2 + '&shelter=' + shelter + '&data_first=' + data_first + '&data_second=' + data_second);
    });

    $('body').on('click', '#uchet_del', function(){
        window.open('../libs/phpword/uchetnoe_delo.php?adress=' + adress + '&kinolog=' + kinolog + '&contract=' + contract2 + '&shelter=' + shelter + '&data_first=' + data_first + '&data_second=' + data_second);
    });

    $('body').on('click', '#akt', function(){
        window.open('../libs/phpword/generate_word_akt_vibitiya.php?adress=' + adress + '&kinolog=' + kinolog + '&contract=' + contract2 + '&shelter=' + shelter + '&data_first=' + data_first + '&data_second=' + data_second);
    });
  
</script>