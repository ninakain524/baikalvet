<?php
    require_once('config.php');
    require_once("timezone.php");
    require_once("functions.php");
	setlocale(LC_ALL, 'ru_RU.utf8');
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	mb_http_output('UTF-8');
	mb_language('uni');
    $rowsData = array();
    $DR = $_SERVER['DOCUMENT_ROOT'].'/';
    $full_path = 'assets/files/uploads/';//базовая директория загружаемых файлов
    $zip_dir = 'assets/files/free/';//директория расположения архива свободных выездов
    if (isset($_GET['add_free_application'])) {
    	$time_now = time();
        $city = prepare($_GET['city']);
        $date = $_GET['date'];
        $loc_type = prepare($_GET['loc_type']).".";  
		$street = prepare($_GET['street']);
        if ($city == "Бесплатная версия kladr-api.ru")
            $city = "";
		if ($street == "Бесплатная версия kladr-api.ru")
            $street = "";
        if ($loc_type == "undefined.") 
            $loc_type = "";
        // Разрешенные расширения файлов.
        $allow = array('video/mp4', 'video/avi', 'video/3gp', 'video/mov', 'image/png', 'image/jpeg', 'image/jpg', 'image/bmp'); 
        //***************** КАЖДЫЙ АДРЕС ДОЛЖЕН ОКАНЧИВАТЬСЯ СЛЭШЕМ */
        
        
        $zip = new ZipArchive();
        $last_id = mysqli_fetch_array(mysqli_query($SERVER, "SELECT MAX(`id`) AS `last_id` FROM `free_application_files`"));
        if (!is_dir($DR.$full_path)) {
            mkdir($DR.$full_path, 0777, true);
        }
        if (!is_dir($DR.$zip_dir)) {
            mkdir($DR.$zip_dir, 0777, true);
        }
        $temp_dir = 'assets/files/temp/';
        if (!is_dir($DR.$temp_dir)) {
            mkdir($DR.$temp_dir, 0777, true);
        }
        $counter = 1;
        $zip_name = $last_id['last_id'].' '.$date.' '.$loc_type.$city.' '.$street.".zip";
        if ($zip->open($DR.$zip_dir.$zip_name, ZipArchive::CREATE)!==TRUE) {
            exit("Невозможно открыть <$DR$zip_dir$zip_name>\n");
        }            
        foreach($_FILES as $file){
            $new_file_name = $last_id['last_id'].$date.' '.$loc_type.$city.' '.$street.'_'.$counter.'.'.pathinfo($file['name'], PATHINFO_EXTENSION);
            if (in_array($file['type'], $allow))
                if (move_uploaded_file($file['tmp_name'], $DR.$temp_dir.$new_file_name))
                    if (in_array($file['type'], $allow)){
                        $zip->addFile($DR.$temp_dir.$new_file_name, $new_file_name);
                    }
            $counter++;
        } 
        mysqli_query($SERVER, "INSERT INTO `free_application_files` (`id_region`, `name_file`, `id_user`, `date`) VALUES ('".$_SESSION['id_region']."', '".$zip_name."', '".$_SESSION['id_user']."', '".$date."')");
        $zip->close();

        echo json_encode(array(
            'result'    => 'success'
        )); 
    }
    if (isset($_GET['get_free_applications'])) {
        $region_constr = $_SESSION['id_region'] == 'all' ? "WHERE 1" : "WHERE `id_region`=".$_SESSION['id_region'];
        $query = mysqli_query($SERVER, "SELECT * FROM `free_application_files`".$region_constr." ORDER BY `id` ASC ");
        while($row = mysqli_fetch_array($query)) {
            $user = mysqli_fetch_array(mysqli_query($SERVER, "SELECT `name`, `sourname`, `patronymic` FROM `users` WHERE `id`=".$row['id_user']));
            $files = mysqli_query($SERVER, "SELECT * FROM `free_application_files` WHERE `id`=".$row['id']);//
            $region = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `region` WHERE `id`=".$row['id_region']));
            $rowsData[] .= 
            "<tr id-file='".$row['id']."'>
                <td>".$row['id']."</td>
                <td>".$region['name_region']."</td>
                <td>".$row['date']."</td>
                <td>".$user['sourname']." ".$user['name']." ".$user['patronymic']."</td>
                <td>";
                while($file = mysqli_fetch_array($files)) {
                    #################################
                    #   Поменять строки местами,    #
                    #   работает в зависимости      #
                    #   от сервера                  #
                    #################################
                    //$rowsData[] .=  "<a href='".$DR.$zip_dir.$file['name_file']."'><span class='glyphicon glyphicon-download-alt' aria-hidden='false'></span></a>"; //Для работы на локальном сервере
                    $rowsData[] .=  "<a href='".$zip_dir.$file['name_file']."'><span class='glyphicon glyphicon-download-alt' aria-hidden='false'></span></a>"; //Для работы на домене
                }
            $rowsData[] .= "<td><button class='btn btn-default'><span class='glyphicon glyphicon-remove delete-files' aria-hidden='true'></span></button></td>";
            $rowsData[] .= "</td>
            </tr>" ;
        }
        $html = "";
	    foreach($rowsData as $row){
	        $html .= $row;
	    }
        
        echo json_encode(array(
	        'result'    => 'success',
	        'html'      => $html
	    ));
    }
    if (isset($_GET['delete_free_application'])) {
        $id_file = $_POST['id_file'];
        $file = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `free_application_files` WHERE `id`=".$id_file));
        if (mysqli_query($SERVER, "DELETE FROM `free_application_files` WHERE `id`=".$id_file)){
            if (unlink($DR.$zip_dir.$file['name_file']))
                echo json_encode(array(
                    'result'    => 'success',
                ));            
        }       
    }
?>