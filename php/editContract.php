<?php
    require_once("config.php");
    require_once("timezone.php");
    require_once("functions.php");
    $time_now = time();
    
    if (isset($_GET['contract'])) {
        $path = "";
        if (count($_FILES['doc_edit']['name']) > 0) {
            if ($_FILES && $_FILES["doc_edit"]["error"]== UPLOAD_ERR_OK)
            {
                $path = '/assets/files/contracts/'.$_FILES["doc_edit"]["name"];
                $name = $_SERVER['DOCUMENT_ROOT'] . $path;
                move_uploaded_file($_FILES["doc_edit"]["tmp_name"], $name);
                // echo $name. " Файл загружен";
            }
        }
        $queryLog = "UPDATE `contract` SET 
        `customer`='".trim($_POST['customer_edit'])."', 
        `deadline`='".trim($_POST['deadline_edit'])."', 
        `term_services`='".trim($_POST['term_services_edit'])."', 
        `number`='".trim($_POST['number_edit'])."', 
        `name_contract`='".trim($_POST['name'])."', 
        `date_contract`='".trim($_POST['date_contract_edit'])."', 
        `comment`='".trim($_POST['comment_edit'])."',
        `id_region`='".trim($_POST['region'])."',
        `full_name`='".trim($_POST['full-name_edit'])."',
        `person_customer`='".trim($_POST['person_customer_edit'])."',
        `index_a`='".trim($_POST['index_a_edit'])."',
        `adress`='".trim($_POST['adress_edit'])."',
        `inn`='".trim($_POST['inn_edit'])."',
        `kpp`='".trim($_POST['kpp_edit'])."',
        `ogrn`='".trim($_POST['ogrn_edit'])."',
        `phone`='".trim($_POST['phone_edit'])."',
        `num_application`='".trim($_POST['num_application_edit'])."', 
        `name_tz`='".trim($_POST['name_tz_edit'])."',
        `name_organization`='".trim($_POST['name_organization_edit'])."',
        `status`='".trim($_POST['status'])."',
        `document`='".$path."'
        WHERE id=".$_GET['contract'];
        if (mysqli_query($SERVER, $queryLog)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал контракт - " . $queryLog);
        }
        
        $SERVER -> close();
        header("location: ../contract.php"); 
    }
    else if (isset($_GET['contract1'])) {
        $path = "";
        if (count($_FILES['doc_edit']['name']) > 0) {
            if ($_FILES && $_FILES["doc_edit"]["error"]== UPLOAD_ERR_OK)
            {
                $path = '/assets/files/contracts/'.$_FILES["doc_edit"]["name"];
                $name = $_SERVER['DOCUMENT_ROOT'] . $path;
                move_uploaded_file($_FILES["doc_edit"]["tmp_name"], $name);
                // echo $name. " Файл загружен";
            }
        }
        $queryLog = "UPDATE `contract` SET 
        `customer`='".trim($_POST['customer_edit'])."', 
        `deadline`='".trim($_POST['deadline_edit'])."', 
        `term_services`='".trim($_POST['term_services_edit'])."', 
        `number`='".trim($_POST['number_edit'])."', 
        `name_contract`='".trim($_POST['name'])."', 
        `date_contract`='".trim($_POST['date_contract_edit'])."', 
        `comment`='".trim($_POST['comment_edit'])."',
        `id_region`='".trim($_POST['region'])."',
        `full_name`='".trim($_POST['full-name_edit'])."',
        `person_customer`='".trim($_POST['person_customer_edit'])."',
        `index_a`='".trim($_POST['index_a_edit'])."',
        `adress`='".trim($_POST['adress_edit'])."',
        `inn`='".trim($_POST['inn_edit'])."',
        `kpp`='".trim($_POST['kpp_edit'])."',
        `ogrn`='".trim($_POST['ogrn_edit'])."',
        `phone`='".trim($_POST['phone_edit'])."',
        `num_application`='".trim($_POST['num_application_edit'])."', 
        `name_tz`='".trim($_POST['name_tz_edit'])."',
        `name_organization`='".trim($_POST['name_organization_edit'])."',
        `status`='".trim($_POST['status'])."',
        `document`='".$path."'
        WHERE id=".$_GET['contract1'];
        if (mysqli_query($SERVER, $queryLog)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал контракт - " . $queryLog);
        }
        
        $SERVER -> close();
        header("location: ../contract_card.php?contract=".$_GET['contract1']); 
    }
    else   
        echo 'error';

?>