<?php
    require_once("config.php");
    date_default_timezone_set($_SESSION['timezone']);
    if (isset($_GET['apply_timezone'])) {
        $_SESSION['timezone'] = $_POST['timezone'];
        if (($_SESSION['timezone'] != "") || ($_SESSION['timezone'] != NULL)) {
            echo json_encode(array(
                'result'    => 'success'
            ));
        }
    }
?>