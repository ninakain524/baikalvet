<?php
	require_once('config.php');
    require_once("timezone.php");
    require_once("functions.php");
	setlocale(LC_ALL, 'ru_RU.utf8');
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	mb_http_output('UTF-8');
	mb_language('uni');
	
	$resCurrent = array();
	$resTab = array();
	
	if (isset($_GET['idApplication'])) {
	    
	    $idApplication = $_GET['idApplication']; 
	    $fieldName = $_GET['fieldName'];
	    $fieldData = $_GET['fieldData'];
	    $tableName = $_GET['tableName'];
    	
    	$query = "SELECT 
                application.id AS id,
                application.id_user AS id_user,
                application.data AS data,
                application.street AS street,
                application.locality AS locality,
                application.id_region AS id_region,
                application.id_status AS id_status,
                application.breed AS breed, 
                application.age AS age, 
                application.weight AS weight, 
                application.height AS height, 
                application.color AS name_color,
                application.date_start_vosstanovlenie AS date_start_vosstanovlenie,
                application.date_start_karantin AS date_start_karantin,
                application.id_shelter AS id_shelter,
                application.num_aviary AS num_aviary,
                application.comment AS comment,
                application.show AS show_app,
                application.id_sostoyanie AS id_sostoyanie,
                animal_kind.id AS id_kind,
                animal_kind.name_kind AS name_kind,
                animal_gender.id AS id_gender,
                animal_gender.name_gender AS name_gender,
                application_status.id AS status_id,
                application_status.status_name AS status_name,
                vet_card.id_veterinar AS id_veterinar,
                vet_card.num_birka AS num_birka,
                vet_card.num_chip AS num_chip,
                vet_card.id_degelmintizaciya AS id_degelmintizaciya,
                vet_card.id_vaccine AS id_vaccine,
                vet_card.id_operation AS id_operation,
                DATE_FORMAT(vet_card.data,'%d.%m.%Y') AS vet_card_data,
                animal_shelters.id AS id_shelter,
                animal_shelters.name_shelter AS name_shelter,
                users.sourname AS sourname,
                users.name AS user_name,
                users.patronymic AS user_patronymic,
                region.id AS id_region,
                region.name_region AS name_region,
                contract.id AS contract_id,
                contract.number AS contract_number,
                contract.customer AS customer,
                contract.name_contract AS name_contract,
                DATE_FORMAT(contract.date_contract,'%d.%m.%Y') AS date_contract,
                animal_color.id AS id_color,
				animal_color.name_color AS name_color
                FROM application 
                LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
                LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
                LEFT JOIN application_status ON application.id_status = application_status.id
                LEFT JOIN vet_card ON application.id = vet_card.id_application 
                LEFT JOIN animal_shelters ON application.id_shelter = animal_shelters.id
                LEFT JOIN users ON application.id_user = users.id
                LEFT JOIN region ON application.id_region = region.id
                LEFT JOIN contract ON application.id_contract = contract.id
                LEFT JOIN animal_color ON application.color = animal_color.id
                WHERE application.id=".$idApplication;
                    
                // ЦВЕТ ПО ЕБАНОМУ ДЕЛАЕМ
                
        $res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
		while ($res = mysqli_fetch_array($res_query)) {
		    if ($fieldName == "customer")
		        $resCurrent += [[0=>$res['name_contract']]];
		    else if ($fieldName == "sourname") {
		        $rs = mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM vet_card WHERE id_application=".$res['id']);
                $result = mysqli_fetch_array($rs);
                if ($result[0] > 0) {
                    $vet = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$res['id_veterinar']));
                    if (!empty($vet['id'])) {
                        $sourname_vet = $vet['sourname'];
                        $name_vet = mb_substr($vet['name'], 0, 1);
                        $patronymic_vet = mb_substr($vet['patronymic'], 0, 1);
                        $name_string_vet = $sourname_vet." ".$name_vet.". ".$patronymic_vet."."; 
                        $resCurrent += [[0=>$name_string_vet]];
                    }
                }
		    }
            else if ($fieldName == "kinolog") {
                $kinolog = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$res['id_user']));
                if (!empty($kinolog['id'])) {
                    $sourname_kin = $kinolog['sourname'];
                    $name_kin = mb_substr($kinolog['name'], 0, 1);
                    $patronymic_kin = mb_substr($kinolog['patronymic'], 0, 1);
                    $name_string_kin = $sourname_kin." ".$name_kin.". ".$patronymic_kin."."; 
                    $resCurrent += [[0=>$name_string_kin]];
                }
            }
            else if ($fieldName == "show_app") {
                if($res['show_app'] == 0)
                    $resCurrent += [[0=>"нет"]];
                else
                    $resCurrent += [[0=>"да"]];
            }
		    else
		        $resCurrent += [[0=>$res[$fieldName]]];
		}
		
		if ($fieldName == "customer")
		    $queryT = "SELECT contract.id AS id, contract.name_contract AS name_contract FROM ". $tableName;
        else if ($fieldName == "kinolog")
		    $queryT = "SELECT * FROM ". $tableName ." WHERE id_users_group=4 AND podtverzhdeno=1";
        else if ($fieldName == "sourname")
		    $queryT = "SELECT * FROM ". $tableName ." WHERE id_users_group=3 AND podtverzhdeno=1";
		else
		    $queryT = "SELECT * FROM ". $tableName;
		
        if ($fieldName != "show_app") {
            $res_queryT = mysqli_query($SERVER, $queryT) or die("Ошибка " . mysqli_error($SERVER));
            while ($resT = mysqli_fetch_array($res_queryT)) {
                if ($fieldName == "customer")
                    array_push($resTab, [$resT['id']=>$resT['name_contract']]);
                else if ($fieldName == "sourname") {
                    $sourname_vet = $resT['sourname'];
                    $name_vet = mb_substr($resT['name'], 0, 1);
                    $patronymic_vet = mb_substr($resT['patronymic'], 0, 1);
                    $name_string_vet = $sourname_vet." ".$name_vet.". ".$patronymic_vet.".";
                    array_push($resTab, [$resT['id']=>$name_string_vet]);
                }
                else if ($fieldName == "kinolog") {
                    $sourname_kin = $resT['sourname'];
                    $name_kin = mb_substr($resT['name'], 0, 1);
                    $patronymic_kin = mb_substr($resT['patronymic'], 0, 1);
                    $name_string_kin = $sourname_kin." ".$name_kin.". ".$patronymic_kin.".";
                    array_push($resTab, [$resT['id']=>$name_string_kin]);
                }
                else
                    array_push($resTab, [$resT['id']=>$resT[$fieldName]]);
            }
        }
        else {
            array_push($resTab, [1=>"да"]);
            array_push($resTab, [2=>"нет"]);
        }
		
		$arrResult = json_encode(array_merge($resCurrent, $resTab));
		print_r ($arrResult);
	}
?>