<?php
    require_once('config.php');
    require_once('functions.php');
	if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
?>