<?php
    require_once("config.php");
    require_once("timezone.php");
    if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
    // require_once("../template/head.html");
    require_once '../libs/PHPExcel-1.8/Classes/PHPExcel.php';
    require_once '../libs/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php';
    mb_internal_encoding('UTF-8');
    mb_regex_encoding('UTF-8');
    mb_http_output('UTF-8');
    mb_language('uni');
    
    $show_contract = $_GET['show_contract'] == "all" ? "" : "AND application.id_contract=".$_GET['show_contract'];
    $id_shelter = $_GET['id_shelter'] == "all" ? "" : "AND application.id_shelter=".$_GET['id_shelter'];
    
    $xls = new PHPExcel();
    
    $xls->getProperties()->setTitle("Реестр животных за период без владельца");
    $xls->setActiveSheetIndex(0);
    $sheet = $xls->getActiveSheet();
    $sheet->setTitle('TSheet');
    $sheet->getPageSetup()->SetPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
 
    // Ориентация
    $sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
     
    // Поля
    $sheet->getPageMargins()->setTop(1);
    $sheet->getPageMargins()->setRight(0.75);
    $sheet->getPageMargins()->setLeft(0.75);
    $sheet->getPageMargins()->setBottom(1);
    
    $border = array(
    	'borders'=>array(
    		'allborders' => array(
    			'style' => PHPExcel_Style_Border::BORDER_THIN,
    			'color' => array('rgb' => '000000')
    		)
    	)
    );
    $sheet->getStyle("A1:X2")->applyFromArray($border);

    
    $sheet->getRowDimension("1")->setRowHeight(100);
    
    $sheet->getStyle("A1:X2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle("A1:X2")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    
    $sheet->getColumnDimension("A")->setWidth(4);
    $sheet->mergeCells("A1:A2");
    $sheet->setCellValue("A1", "№ п/п");
    $sheet->getStyle("A1")->getAlignment()->setWrapText(true);
    
    $sheet->getColumnDimension("B")->setWidth(14);
    $sheet->mergeCells("B1:B2");
    $sheet->setCellValue("B1", "Дата отлова, поступления в приют для животных, заключения о клиническом состоянии животного");
    $sheet->getStyle("B1")->getAlignment()->setWrapText(true);
    
    $sheet->getColumnDimension("C")->setWidth(17);
    $sheet->mergeCells("C1:C2");
    $sheet->setCellValue("C1", "Место отлова, адрес");
    $sheet->getStyle("C1")->getAlignment()->setWrapText(true);

    if (isset($_GET['without'])) { 
        $sheet->getColumnDimension("D")->setWidth(7);
        $sheet->mergeCells("D1:E2");
        $sheet->setCellValue("D1", "Видео отлова");
        $sheet->getStyle("D1")->getAlignment()->setWrapText(true);
    }
    else { 
        $sheet->getColumnDimension("D")->setWidth(20);
        $sheet->mergeCells("D1:E1");
        $sheet->setCellValue("D1", "Видео отлова (название файла, ссылка для скачивания)");
        $sheet->getStyle("D1")->getAlignment()->setWrapText(true);
        $sheet->setCellValue("D2", "Название файла");
        $sheet->getStyle("D2")->getAlignment()->setWrapText(true);
        
        $sheet->getColumnDimension("E")->setWidth(35);
        $sheet->setCellValue("E2", "Ссылка для скачивания");
        $sheet->getStyle("E2")->getAlignment()->setWrapText(true);
    }

    $sheet->mergeCells("F1:K1");
    $sheet->setCellValue("F1", "Вид, порода, окрас, вес, приблизительный возраст, визуальная характеристика состояния животного, а также иные данные, позволяющие его идентифицировать");
    $sheet->getStyle("F1")->getAlignment()->setWrapText(true);
    $sheet->setCellValue("F2", "Вид");
    $sheet->setCellValue("G2", "Порода");
    $sheet->setCellValue("H2", "Пол");
    $sheet->setCellValue("I2", "Окрас");
    $sheet->setCellValue("J2", "Возраст ≈");
    $sheet->setCellValue("K2", "Вес ≈");
    
    $sheet->getColumnDimension("L")->setWidth(21);
    $sheet->mergeCells("L1:L2");
    $sheet->setCellValue("L1", "Идентификационный индивидуальный номер животного, наносимый на бирку");
    $sheet->getStyle("L1")->getAlignment()->setWrapText(true);
    
    $sheet->getColumnDimension("M")->setWidth(12);
    $sheet->mergeCells("M1:M2");
    $sheet->setCellValue("M1", "Номер чипа");
    $sheet->getStyle("M1")->getAlignment()->setWrapText(true);
    
    $sheet->getColumnDimension("N")->setWidth(19);
    $sheet->mergeCells("N1:P1");
    $sheet->setCellValue("N1", "Дата проведения лечебно-профилактических мероприятий");
    $sheet->getStyle("N1")->getAlignment()->setWrapText(true);
    $sheet->setCellValue("N2", "Дегильминтизации");
    $sheet->getColumnDimension("O")->setWidth(15);
    $sheet->setCellValue("O2", "Вакцинации от бешенства");
    $sheet->getStyle("O2")->getAlignment()->setWrapText(true);
    $sheet->getColumnDimension("P")->setWidth(13);
    $sheet->setCellValue("P2", "Операции");
    
    $sheet->getColumnDimension("Q")->setWidth(20);
    $sheet->mergeCells("Q1:R1");
    $sheet->setCellValue("Q1", "Дата передачи животного его владельцу, новому владельцу, в другой приют, данные о владельце (ФИО, адрес, телефон), приюте (название, адрес)");
    $sheet->getStyle("Q1")->getAlignment()->setWrapText(true);
    $sheet->setCellValue("Q2", "Дата передачи");
    $sheet->getColumnDimension("R")->setWidth(27);
    $sheet->setCellValue("R2", "Данные о владельце, приюте");
    $sheet->getStyle("R2")->getAlignment()->setWrapText(true);
    
    $sheet->getColumnDimension("S")->setWidth(17);
    $sheet->mergeCells("S1:S2");
    $sheet->setCellValue("S1", "Дата возврата животного на место прежнего обитания");
    $sheet->getStyle("S1")->getAlignment()->setWrapText(true);

    if (isset($_GET['without'])) { 
        $sheet->getColumnDimension("T")->setWidth(7);
        $sheet->mergeCells("T1:U2");
        $sheet->setCellValue("T1", "Видео возврата");
        $sheet->getStyle("T1")->getAlignment()->setWrapText(true);
    }
    else { 
        $sheet->getColumnDimension("T")->setWidth(20);
        $sheet->mergeCells("T1:U1");
        $sheet->setCellValue("T1", "Видео возврата (название файла, ссылка для скачивания)");
        $sheet->getStyle("T1")->getAlignment()->setWrapText(true);
        $sheet->setCellValue("T2", "Название файла");
        $sheet->getStyle("T2")->getAlignment()->setWrapText(true);
        
        $sheet->getColumnDimension("U")->setWidth(35);
        $sheet->setCellValue("U2", "Ссылка для скачивания");
        $sheet->getStyle("U2")->getAlignment()->setWrapText(true);
    }
    
    $sheet->getColumnDimension("V")->setWidth(17);
    $sheet->mergeCells("V1:V2");
    $sheet->setCellValue("V1", "Дата умерщвления, естественной смерти/причины");
    $sheet->getStyle("V1")->getAlignment()->setWrapText(true);
    
    $sheet->getColumnDimension("V")->setWidth(17);
    $sheet->mergeCells("W1:W2");
    $sheet->setCellValue("W1", "Приют");
    $sheet->getStyle("W1")->getAlignment()->setWrapText(true);
    
    $sheet->getColumnDimension("X")->setWidth(17);
    $sheet->mergeCells("X1:X2");
    $sheet->setCellValue("X1", "№ вольера");
    $sheet->getStyle("X1")->getAlignment()->setWrapText(true);
    
    $query = "SELECT 
    application.id AS id,
    application.data AS data,
    application.street AS street,
    application.loc_type AS loc_type,
    application.locality AS locality,
    application.breed AS breed, 
    application.age AS age, 
    application.weight AS weight, 
    application.num_aviary AS num_aviary, 
    application.color AS color,
    application.date_start_vosstanovlenie AS date_start_vosstanovlenie,
    application.date_start_karantin AS date_start_karantin,
    DATE_FORMAT(application.date_vipusk,'%d.%m.%Y') AS date_vipusk,
    DATE_FORMAT(application.date_die,'%d.%m.%Y') AS date_die,
    animal_kind.name_kind AS kind,
    animal_gender.name_gender AS gender,
    application_status.id AS status_id,
    application_status.status_name AS status,
    vet_card.num_birka AS num_birka,
    vet_card.num_chip AS num_chip,
    DATE_FORMAT(vet_card.data,'%d.%m.%Y') AS vet_card_data,
    animal_shelters.name_shelter AS name_shelter,
    DATE_FORMAT(vet_degilmintizaciya.data,'%d.%m.%Y') AS vet_degilmintizaciya_data,
    DATE_FORMAT(vet_operation.data,'%d.%m.%Y') AS vet_operation_data,
    DATE_FORMAT(vet_vaccine.data,'%d.%m.%Y') AS vet_vaccine_data,
    DATE_FORMAT(animal_owners.data,'%d.%m.%Y') AS animal_owners_data,
    animal_owners.name_shelter AS a_name_shelter,
    animal_owners.adress AS a_adress,
    animal_owners.sourname_owner AS sourname_owner,
    animal_owners.name_owner AS name_owner,
    animal_owners.patronymic_owner AS patronymic_owner,
    animal_owners.phone AS phone
    FROM application 
    LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
    LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
    LEFT JOIN application_status ON application.id_status = application_status.id
    LEFT JOIN vet_card ON application.id = vet_card.id_application
    LEFT JOIN animal_shelters ON application.id_shelter = animal_shelters.id
    LEFT JOIN vet_degilmintizaciya ON vet_card.id_degelmintizaciya = vet_degilmintizaciya.id
    LEFT JOIN vet_operation ON vet_card.id_vaccine = vet_operation.id
    LEFT JOIN vet_vaccine ON vet_card.id_operation = vet_vaccine.id
    LEFT JOIN animal_owners ON application.id = animal_owners.id_application
    WHERE application.data >= '".$_GET['data_first']."' AND application.data <= '".$_GET['data_second']."' AND application.show='1' ".$show_contract." ".$id_shelter." ORDER BY application.data ASC";

    $res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
    $i = 3;
    $j = 1;

    $count_q = mysqli_fetch_array(mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM application WHERE application.data >= '".$_GET['data_first']."' AND application.data <= '".$_GET['data_second']."' AND application.show='1' ".$show_contract." ".$id_shelter." ORDER BY application.data ASC"));
    $count_r = $count_q[0]+2;
              
    $row1 = 0;
    $row2 = 0;
    while ($res = mysqli_fetch_array($res_query)) {
        
        if (ctype_digit($res['color'])) {
            $animal_color1 = mysqli_fetch_array(mysqli_query($SERVER, "SELECT name_color FROM animal_color WHERE id=".$res['color']));
            $animal_color = $animal_color1['name_color'];
        }
    	else
    	    $animal_color = $res['color']; 
        
        $query_file = mysqli_query($SERVER, "SELECT * FROM upload_files WHERE id_application = '".$res['id']."' AND type = 'vid' LIMIT 1");
        $name_file = "";
        $path_file = "";
        while ($res_file = mysqli_fetch_array($query_file)) {
          $name_file = $res_file['name_file'];
          $path_file = $res_file['path_file'];
        } 

        $query_file_vipusk = mysqli_query($SERVER, "SELECT * FROM upload_files WHERE id_application = '".$res['id']."' AND type = 'vid' AND vipusk = 1 LIMIT 1");
        $name_file_vipusk = "";
        $path_file_vipusk = "";
        while ($res_file_vipusk = mysqli_fetch_array($query_file_vipusk)) {
            $name_file_vipusk = $res_file_vipusk['name_file'];
            $path_file_vipusk = $res_file_vipusk['path_file'];
        } 
        
        $name = $res['name_owner'];
        $name1 = mb_substr($name, 0, 1);
        $patronymic = $res['patronymic_owner'];
        $patronymic1 = mb_substr($patronymic, 0, 1);
        
        $sheet->setCellValue("A".$i, $j);
        $sheet->getStyle("A".$i)->getAlignment()->setWrapText(true);
        
        $sheet->setCellValue("B".$i, date('d.m.Y', $res['data']));
        $sheet->getStyle("B".$i)->getAlignment()->setWrapText(true);
        
        $sheet->setCellValue("C".$i, $res['locality'] . ", " . $res['street'] . ";");
        $sheet->getStyle("C".$i)->getAlignment()->setWrapText(true);

        if (isset($_GET['without'])) {
            if ($row1 == 0) { 
                $sheet->mergeCells("D".$i.":"."E".$count_r);
                $sheet->setCellValue("D".$i, "Видео предоставляется по запросу службы ветеринарии или заказчика");
                $sheet->getStyle("D".$i)->getAlignment()->setWrapText(true)->setTextRotation(90);
                $row1 = 1;
            }
        } else {
            $sheet->setCellValue("D".$i, $name_file);
            $sheet->getStyle("D".$i)->getAlignment()->setWrapText(true);
            
            $sheet->setCellValue("E".$i, "https://baikalvet.ru" . $path_file . $name_file);
            $sheet->getStyle("E".$i)->getAlignment()->setWrapText(true);
        }
        
        $sheet->setCellValue("F".$i, $res['kind']);
        $sheet->getStyle("F".$i)->getAlignment()->setWrapText(true);
        
        $sheet->setCellValue("G".$i, $res['breed']);
        $sheet->getStyle("G".$i)->getAlignment()->setWrapText(true);
        
        $sheet->setCellValue("H".$i, $res['gender']);
        $sheet->getStyle("H".$i)->getAlignment()->setWrapText(true);
        
        $sheet->setCellValue("I".$i, $animal_color);
        $sheet->getStyle("I".$i)->getAlignment()->setWrapText(true);
        
        $sheet->setCellValue("J".$i, $res['age']);
        $sheet->getStyle("J".$i)->getAlignment()->setWrapText(true);
        
        $sheet->setCellValue("K".$i, $res['weight']);
        $sheet->getStyle("K".$i)->getAlignment()->setWrapText(true);
        
        $sheet->setCellValue("L".$i, $res['num_birka']);
        $sheet->getStyle("L".$i)->getAlignment()->setWrapText(true);
        
        $sheet->setCellValue("M".$i, $res['num_chip']);
        $sheet->getStyle("M".$i)->getAlignment()->setWrapText(true);
        
        $sheet->setCellValue("N".$i, $res['vet_degilmintizaciya_data']);
        $sheet->getStyle("N".$i)->getAlignment()->setWrapText(true);
        
        $sheet->setCellValue("O".$i, $res['vet_vaccine_data']);
        $sheet->getStyle("O".$i)->getAlignment()->setWrapText(true);
        
        $sheet->setCellValue("P".$i, $res['vet_operation_data']);
        $sheet->getStyle("P".$i)->getAlignment()->setWrapText(true);
        
        $sheet->setCellValue("Q".$i, $res['animal_owners_data']);
        $sheet->getStyle("Q".$i)->getAlignment()->setWrapText(true);
        
        $sheet->setCellValue("R".$i, $res['sourname_owner'] . " " . $name1 . $res['a_name_shelter'] . ". " . $res['a_adress'] . $patronymic1 . ". " . $res['phone']);
        $sheet->getStyle("R".$i)->getAlignment()->setWrapText(true); 
        
        $sheet->setCellValue("S".$i, $res['date_vipusk']);
        $sheet->getStyle("S".$i)->getAlignment()->setWrapText(true);

        if (isset($_GET['without'])) {
            if ($row2 == 0) { 
                $sheet->mergeCells("T".$i.":"."U".$count_r);
                $sheet->setCellValue("T".$i, "Видео предоставляется по запросу службы ветеринарии или заказчика");
                $sheet->getStyle("T".$i)->getAlignment()->setWrapText(true)->setTextRotation(90);
                $row2 = 1;
            }
        } else {
            $sheet->setCellValue("T".$i, $name_file_vipusk);
            $sheet->getStyle("T".$i)->getAlignment()->setWrapText(true);
            
            $sheet->setCellValue("U".$i, "https://baikalvet.ru" . $path_file_vipusk . $name_file_vipusk);
            $sheet->getStyle("U".$i)->getAlignment()->setWrapText(true);
        }
        
        $sheet->setCellValue("V".$i, $res['date_die']);
        $sheet->getStyle("V".$i)->getAlignment()->setWrapText(true);
        
        $sheet->setCellValue("W".$i, $res['name_shelter']);
        $sheet->getStyle("WV".$i)->getAlignment()->setWrapText(true);
        
        $sheet->setCellValue("X".$i, $res['num_aviary']);
        $sheet->getStyle("X".$i)->getAlignment()->setWrapText(true);
        
        $sheet->getStyle("A" . $i . ":" . "C" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle("F" . $i . ":" . "S" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle("V" . $i . ":" . "X" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle("D" . $i . ":" . "E" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("T" . $i . ":" . "U" . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A" . $i . ":" . "X" . $i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        
        $sheet->getStyle("A" . $i . ":" . "X" . $i)->applyFromArray($border);
        
        $i = $i + 1;
        $j = $j + 1;
    }
    
    header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=reestr_animals.xlsx");
     
    $objWriter = new PHPExcel_Writer_Excel2007($xls);
    $objWriter->save('php://output'); 
    exit();	
?>