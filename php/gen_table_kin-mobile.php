<?php
	require_once("config.php");
    require_once("timezone.php");
    require_once("functions.php");
    $time_now = time();
    $today = date("Y-m-d\TH:i:s");
    $rowsData = array();
    function format_interval(DateInterval $interval) {
        $result = "";
        if ($interval->d) { $result .= $interval->format("%d д. "); }
        if ($interval->h) { $result .= $interval->format("%h ч. "); }
        if ($interval->i) { $result .= $interval->format("%i мин. "); }
    
        return $result;
    };
	if (isset($_GET['gen_table'])) {
		if (isset($_GET['show_more']))
			$limit_constr = $_POST['start_from'].', '.$_POST['row_nums'];
		else
			$limit_constr = $_SESSION['row_limit'];
		
		$shelter_constr = $_SESSION['filter_id_shelter'] == "all" ? "" : "AND id_shelter=".$_SESSION['filter_id_shelter'];
// 		$show_status_constr = $_SESSION['show_status'] == "all" ? "AND (`id_status`=1 OR `id_status`=11 OR `id_status`=7 OR `id_status`=8)" : "AND id_status=".$_SESSION['show_status'];

		$show_contract = $_SESSION['show_contract'] == "all" ? "" : "AND id_contract=".$_SESSION['show_contract'];
		$filter_first_date = $_SESSION['filter_first_date'] == "first" ? "" : strtotime($_SESSION['filter_first_date']);
		$filter_last_date = $_SESSION['filter_last_date'] == "last" ? "" : strtotime($_SESSION['filter_last_date']);
		$locality_construct = $_SESSION['filter_locality'] == "all" ? "" : "AND locality='".$_SESSION['filter_locality']."'";
		$show_status_constr = $_SESSION['show_status'] == "all" ? "AND (`id_status`=1 OR `id_status`=11 OR `id_status`=7 OR `id_status`=8)" : "AND id_status=".$_SESSION['show_status'];
		$show_status_constr1 = $_SESSION['show_status'] == "all" ? "OR (`id_status`=11)" : "";
		$show_status_constr2 = $_SESSION['show_status'] == "11" ? "OR (`id_status`=11)" : "";
		$filter_aviarys = $_SESSION['filter_aviarys'] == "" ? "" : "AND `num_aviary`='".$_SESSION['filter_aviarys']."'";

		/*$query = "SELECT * FROM `application` WHERE application.data BETWEEN ".$filter_first_date." AND ".$filter_last_date." AND
		`id_sostoyanie`='1' AND `show`='1' AND ( (`id_user`='".$_SESSION['id_user']."' ".$show_status_constr." ) ".$show_status_constr1." ".$show_status_constr2." ) ".$locality_construct." ".$shelter_constr." ".$show_contract." ORDER BY `".$_SESSION['order_by']."` ".$_SESSION['sort_by']." LIMIT ".$limit_constr;*/
		$query = "SELECT * FROM `application` WHERE `id_region`='".$_SESSION['id_region']."' AND application.data BETWEEN ".$filter_first_date." AND ".$filter_last_date." AND
		`id_sostoyanie`='1' AND `show`='1' AND ( (`id_user`='".$_SESSION['id_user']."' ".$show_status_constr." ) ".$show_status_constr1." ".$show_status_constr2." ) ".$locality_construct." ".$shelter_constr." ".$filter_aviarys." ".$show_contract." ORDER BY `".$_SESSION['order_by']."` ".$_SESSION['sort_by']." LIMIT ".$limit_constr;
		
		$res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
		$row_count = 0;
		while ($res = mysqli_fetch_array($res_query)) {
			$row_count++;
			$res_image = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `upload_files` WHERE `id_user_group`='4' AND `id_application`='".$res['id']."' AND `type`='img' ORDER BY `id` ASC"));
			$res_kind = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `animal_kind` WHERE `id`=".$res['id_kind']));
			$res_status= mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `application_status` WHERE `id`=".$res['id_status']));
			$res_shelter= mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `animal_shelters` WHERE `id`=".$res['id_shelter']));
			$res_gender= mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `animal_gender` WHERE `id`=".$res['id_gender']));
			
			$end = date('Y-m-d H:i', $res['date_start_karantin']+777600);
			$interval_first_date = new DateTime($today);
			$interval_second_date = new DateTime($end);
			$difference = $interval_first_date->diff($interval_second_date);
			$interval = format_interval($difference);
			
			if(!empty($res_image)) {
				$path = $res_image['path_thumb'].$res_image['name_thumb'];
				$url = "https://baikalvet.ru".$path;
                $urlHeaders = @get_headers($url);
                // проверяем ответ сервера на наличие кода: 200 - ОК
                if(strpos($urlHeaders[0], '200')) {
                    $img = $path;
				}
				else {
				    $img = $res_image['path_file'].$res_image['name_thumb'];
				}
		    }   
			else
				$img = "assets/images/no-image.png";
				
			if (ctype_digit($res['color'])) 
        	    $animal_color = mysqli_fetch_array(mysqli_query($SERVER, "SELECT name_color FROM animal_color WHERE id=".$res['color']));
        	else
        	    $animal_color = mysqli_fetch_array(mysqli_query($SERVER, "SELECT name_color FROM animal_color WHERE id=0"));  
			$gender = mysqli_fetch_array(mysqli_query($SERVER, "SELECT `name_gender` FROM `animal_gender` WHERE `id`=".$res['id_gender']));
			$rowsData[] = "<tr data-href='/animal_card.php?application={$res['id']}' class='application-mobile'>
                			<td>
                			    <div class='block-head'>
                			        <div class='id-color block-head-num'>{$_SESSION['num_tr']}</div>
                			        <div class='block-head-date'>".date('d.m.Y', $res['data'])."</div>
                			        <div class='block-head-adress'>{$res['loc_type']} {$res['locality']}, {$res['street']}</div>";
                                    if($res_status['id'] == 7 || $res_status['id'] == 8)
                                        $rowsData[] = "<div class='block-head-status die-color'>";
                                    else
                                        $rowsData[] = "<div class='block-head-status'>";
                    			        if($res_status['id'] == 2) 
                            				$rowsData[] .= "{$res_status['status_name']}<br> <div class='status-time'>до ".date("d.m.Y", ($res['date_start_karantin']+777600))."</div>";
                            			else
                            				$rowsData[] .= "{$res_status['status_name']}";
            $rowsData[] = "         </div>
                                </div>
                                <div class='block-content'>
                                    <div class='block-content-img'><img src='{$img}' class='table-img'></div>
                                    <div class='block-content-info' style='display:flex;'>
                                        <ul style='width: 100px;'>
                                            <li>{$res_kind['name_kind']}</li>
											<li>{$res_gender['name_gender']}</li>
                                            <li>{$res['breed']}</li>
                                            <li>возраст: {$res['age']}</li>
                                        </ul>
										<ul style='width: 125px;'>
											<li>окрас: {$animal_color['name_color']}</li>
											<li>{$res_shelter['name_shelter']}</li>
											<li>вольер: {$res['num_aviary']}</li>
										</ul>
                                    </div>
                                </div>
                                <div class='block-comment'>{$res['comment']}</div>
                            </td>
                        </tr>";
			
			$_SESSION['num_tr'] = $_SESSION['num_tr'] + 1;
		}
		$html = "";
	    foreach($rowsData as $row){
	        $html .= $row;
	    }
	    echo json_encode(array(
	        'result'    => 'success',
	        'html'      => $html,
	        'row_count' => $row_count
	    ));
	}
	if (isset($_GET['update_table_config'])) {
	    $_SESSION['num_tr'] = 1;
		$_SESSION['filter_id_shelter'] = intval($_POST['id_shelter']);
// 		$_SESSION['show_contract'] = intval($_POST['show_contract']);
		
		switch ($_POST['show_status']) {
			case '0':
				$_SESSION['show_status'] = 'all';
				break;
			case '1':
				$_SESSION['show_status'] = '1';
				break;
			case '2':
				$_SESSION['show_status'] = '2';
				break;
			case '11':
				$_SESSION['show_status'] = '11';
				break;
			case '7':
				$_SESSION['show_status'] = '7';
				break;
			case '8':
				$_SESSION['show_status'] = '8';
				break;
			default:
				$_SESSION['show_status'] = 'all';
				break;
		}
		if ($_POST['show_contract'] == 0) {
            $_SESSION['show_contract'] = 'all';
        }
        $res_query = mysqli_query($SERVER, "SELECT id AS id FROM contract WHERE show_contract=1 AND status='открыт' AND id_region='".$_SESSION['id_region']."'") or die("Ошибка " . mysqli_error($SERVER));
		while ($res = mysqli_fetch_array($res_query)) {
            if ($_POST['show_contract'] == $res['id']) {
                $_SESSION['show_contract'] = $res['id'];
            } 
		}
		switch ($_POST['order_by']) {
			case '1':
				$_SESSION['order_by'] = 'data';
				break;
			case '2':
				$_SESSION['order_by'] = 'locality';
				break;
			case '3':
				$_SESSION['order_by'] = 'street';
				break;
			case '4':
				$_SESSION['order_by'] = 'id_kind';
				break;
			case '5':
				$_SESSION['order_by'] = 'breed';
				break;			
			case '6':
				$_SESSION['order_by'] = 'age';
				break;
			case '7':
				$_SESSION['order_by'] = 'color';
				break;
			default:
				$_SESSION['order_by'] = 'data';
				break;
		}
		switch ($_POST['sort_by']) {
			case 'DESC':
				$_SESSION['sort_by'] = 'DESC';
				break;
			case 'ASC':
				$_SESSION['sort_by'] = 'ASC';
				break;
			default:
				$_SESSION['sort_by'] = 'DESC';
				break;
		}
		switch ($_POST['row_limit']) {
			case '25':
				$_SESSION['row_limit'] = '25';
				break;
			case '50':
				$_SESSION['row_limit'] = '50';
				break;
			case '75':
				$_SESSION['row_limit'] = '75';
				break;
			case '100':
				$_SESSION['row_limit'] = '100';
				break;
			default:
				$_SESSION['row_limit'] = '25';
				break;
		}
		switch ($_POST['filter_locality']) {
			case 'all':
				$_SESSION['filter_locality'] = "all";
				break;
			default:
				$_SESSION['filter_locality'] = $_POST['filter_locality'];
				break;
		}
		if ($_POST['filter_first_date'] == "first") {
			$_SESSION['filter_first_date'] = $sql = mysqli_fetch_array(mysqli_query($SERVER, "SELECT MIN(`data`) AS filter_first_date FROM `application`"));
		}
		else {
			$_SESSION['filter_first_date'] = $_POST['filter_first_date'];
			
		}
		if ($_POST['filter_last_date'] == "last") {
			$_SESSION['filter_last_date'] = $sql = mysqli_fetch_array(mysqli_query($SERVER, "SELECT MAX(`data`) AS filter_last_date FROM `application`"));
		}
		else {
			$_SESSION['filter_last_date'] = $_POST['filter_last_date'];
			
		}
		if ($_POST['filter_aviarys'] == "")
			$_SESSION['filter_aviarys'] = "";
		else
			$_SESSION['filter_aviarys'] = $_POST['filter_aviarys'];
		echo json_encode(array('res'    => 'success'));
	}
?>