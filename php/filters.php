<?php
    require_once("config.php");
    require_once("timezone.php");
    require_once("functions.php");
    if (isset($_GET['shelter_changed'])){//ГЕНЕРАТОР ФИЛЬТРА ДЛЯ ВОЛЬЕРА В ЗАВИСИМОСТИ ОТ ВЫБРАННОГО ПРИЮТА
        $new_shelter = $_POST['new_shelter'];
        $selected_aviarys = array();
        $html = "";
        $shelter_constr = $_SESSION['filter_id_shelter'] == "all" ? "" : "WHERE `id_shelter`=".$_SESSION['filter_id_shelter'];
        $sql = "SELECT * FROM `aviarys_shelters` ".$shelter_constr;
        $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
        $html .= '<option value="all">Все вольеры</option>';
        while($row = mysqli_fetch_array($res))
        {
            $selected_aviarys[] = '<option value="'.$row['id'].'">'.$row['number'].'</option>';
        }
        foreach($selected_aviarys as $aviary){
	        $html .= $aviary;
	    }
        echo json_encode(array(
	        'result'    => 'success',
	        'html'      => $html
	    ));
    }
?>