<?php
    require_once("config.php");
    require_once("timezone.php");
    require_once("functions.php");
    $time_now = time();
    $today = time("Y-m-d\TH:i");
    $rowsData = array();
    function format_interval(DateInterval $interval) {
        $result = "";
        if ($interval->d) { $result .= $interval->format("%d д. "); }
        if ($interval->h) { $result .= $interval->format("%h ч. "); } 
        if ($interval->i) { $result .= $interval->format("%i мин. "); }

        return $result;
    };
    if (isset($_GET['gen_journal_table'])) {
        $journal_filter_first_date = $_SESSION['journal_filter_first_date'] == "first" ? "" : strtotime($_SESSION['journal_filter_first_date']);
        $journal_filter_last_date = $_SESSION['journal_filter_last_date'] == "last" ? "" : strtotime($_SESSION['journal_filter_last_date']);
        if (($journal_filter_first_date != "first") && ($journal_filter_first_date != "") && ($journal_filter_last_date != "last") && ($journal_filter_last_date != ""))
            $journal_filter_date_constr = "AND `date` BETWEEN '".$journal_filter_first_date."' AND '".$journal_filter_last_date."' ";
        else
            $journal_filter_date_constr = "";
        $journal_order_by_constr = " ORDER BY ".$_SESSION['journal_order_by']." ";
        $journal_sort_by_constr = " ".$_SESSION['journal_sort_by']." ";
        $journal_row_limit_constr = " LIMIT ".$_SESSION['journal_start_from'].", ".$_SESSION['journal_row_limit']." ";
        $journal_id_region_constr = $_SESSION['id_region'] == 'all' ? "" : " AND `id_region`=".$_SESSION['id_region']." ";
        $journal_search_request_cosntr = $_SESSION['journal_search_request'] == "" ? "" : " AND (street LIKE '%".$_SESSION['journal_search_request']."%' OR locality LIKE '%".$_SESSION['journal_search_request']."%' OR applicant_name LIKE '%".$_SESSION['journal_search_request']."%' OR applicant_phone LIKE '%".$_SESSION['journal_search_request']."%')";
        if ($_SESSION['journal_status'] == "all")
            $journal_status_constr = "";
        else
            $journal_status_constr = " AND `status`=".$_SESSION['journal_status']." ";
        //echo $_SESSION['journal_id_executor'];
        switch ($_SESSION['journal_id_executor']) {
            case 'without':
                $journal_id_executor_constr = " `id_kinolog` IS NULL ";//все без исполнителя
                break;
            case 'all':
                $journal_id_executor_constr = " `id` ";//колхоз, чтобы была выборка по всем 
                break;
            default:
                $journal_id_executor_constr = " `id_kinolog`=".$_SESSION['journal_id_executor']." ";
                break;
        }
        $count_query = "SELECT `id` FROM `journal` WHERE `show`=1 AND ".$journal_id_executor_constr.$journal_search_request_cosntr.$journal_id_region_constr.$journal_status_constr.$journal_filter_date_constr.$journal_order_by_constr.$journal_sort_by_constr;
        $count_rows_in_base = mysqli_num_rows(mysqli_query($SERVER, $count_query));
        $query = "SELECT * FROM `journal` WHERE `show`=1 AND ".$journal_id_executor_constr.$journal_search_request_cosntr.$journal_id_region_constr.$journal_status_constr.$journal_filter_date_constr.$journal_order_by_constr.$journal_sort_by_constr.$journal_row_limit_constr;
        $journal_current_page = ($_SESSION['journal_start_from']+$_SESSION['journal_row_limit'])/$_SESSION['journal_row_limit'];
        $query = mysqli_query($SERVER, $query);
        $rowscounter = 0;
        switch ($_POST['device']) {
            case 'mobile':
                while ($res = mysqli_fetch_array($query)) {
                    $creator = mysqli_fetch_array(mysqli_query($SERVER, "SELECT `sourname`, `name`, `patronymic` FROM `users` WHERE `id`=".$res['id_creator']));
                    $rowsData[] = "<div class='journal-mobile-row' data='{$res['id']}'>
                                    <div class='block-head'>
                                        <div class='id-color'>{$res['id']}</div>
                                        <p>".date('d.m.Y H:i', $res['date'])."</p>
                                    </div>
                                    <p><b>Место обращения:</b> {$res['loc_type']} {$res['locality']}</p>
                                    <p><b>ФИО заявителя:</b> {$res['applicant_name']}</p>
                                    <p><b>Телефон заявителя:</b> {$res['applicant_phone']}</p>
                                    <p><b>Количество животных:</b> {$res['count_animals']}</p>
                                    <p><b>Описание:</b> {$res['description']}</p>
                                    <p><b>Зарегистрировал:</b> {$creator['sourname']} {$creator['name']} {$creator['patronymic']}</p>
                                    <p>";
                    if ($_SESSION['id_users_group'] == '1') {
                        $rowsData[] =   "Статус: <select class='journal-input journal-input-gray edit_status'>";
                        if ($res['status'] == '0')
                            $rowsData[] .= "<option selected disabled>Не завершена</option>
                                            <option data-status='1' id-journal-row='".$res['id']."'>В работе</option>
                                            <option data-status='2' id-journal-row='".$res['id']."'>Завершена</option>";
                        if ($res['status'] == '1')
                            $rowsData[] .= "<option data-status='0' id-journal-row='".$res['id']."'>Не завершена</option>
                                            <option selected disabled>В работе</option>
                                            <option data-status='2' id-journal-row='".$res['id']."'>Завершена</option>";
                        if ($res['status'] == '2')
                            $rowsData[] .= "<option data-status='0' id-journal-row='".$res['id']."'>Не завершена</option>
                                            <option data-status='1' id-journal-row='".$res['id']."'>В работе</option>
                                            <option selected disabled>Завершена</option>";
                        $rowsData[] .= "</select>
                                    </p>
                                    <p>Исполнитель: 
                                        <select class='journal-input journal-input-gray edit_executor'>";
                        $sess_exec = $_SESSION['id_region'] == "all" ? "1" : "`code_region`=".$_SESSION['id_region'];
                        $executors = mysqli_query($SERVER, "SELECT `id`, `name`, `sourname`, `patronymic` FROM `users` WHERE `id_users_group`=4 AND ".$sess_exec);
                        if ($res['id_kinolog'] == NULL)
                                $rowsData[] .= "<option disabled selected>Нет исполнителя</option>";
                        while ($executor = mysqli_fetch_array($executors)) {
                            if ($res['id_kinolog'] == $executor['id'])
                                $active = "selected disabled";
                            else
                                $active = "";
                            $rowsData[] .= "<option ".$active." data-executor='".$executor['id']."' id-journal-row='".$res['id']."'>".$executor['sourname']." ".$executor['name']." ".$executor['patronymic']."</option>";
                        }
                        $rowsData[] .= "</select>";
                    }
                    if ($_SESSION['id_users_group'] == '4') {
                        switch ($res['status']) {
                            case '0':
                                $rowsData[] .= "Действие: <button class='btn btn-danger btn-sm to_work' id-row='".$res['id']."'>Взять в работу</button>";
                                break;
                            case '1':
                                $rowsData[] .= "Действие: <button class='btn btn-primary btn-sm show_linked_card_form' id-row='".$res['id']."'>Завершить</button>";
                                break;
                            case '2':
                                $rowsData[] .= "Действие: <button disabled class='btn btn-success btn-sm'>Завершена</button>";
                                break;
                            default:
                                $rowsData[] .= "Действие: <button class='btn btn-danger btn-sm to_work'>Взять в работу</button>";
                                break;
                        }
                    }
                    $rowsData[] .= "<p>";
                    $get_application = mysqli_query($SERVER, "SELECT * FROM `application` WHERE `id_journal_application`=".$res['id']);
                    $rowsData[] .= 'Просмотр связанных карточек: <button type="button" class="btn btn-default btn-md open_request_info" id-row='.$res['id'].'>
                                        '.mysqli_num_rows($get_application).'&nbsp<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                    </button>';            
                    $rowsData[] .= "</p>
                                        <button class='delete-journal btn btn-default' id-row='".$res['id']."'>Удалить</button>
                                </div>";
                    $rowscounter++;
                }
                break;
            case 'desktop':
                while ($res = mysqli_fetch_array($query)) {
                    $creator = mysqli_fetch_array(mysqli_query($SERVER, "SELECT `sourname`, `name`, `patronymic` FROM `users` WHERE `id`=".$res['id_creator']));
                    $rowsData[] = "<tr class='journal_row' data='{$res['id']}'>
                                    <td><div class='id-color'>{$res['id']}</div></td>
                                    <td>".date('d.m.Y H:i', $res['date'])."</td>
                                    <td>{$res['loc_type']} {$res['locality']}</td>
                                    <td>{$res['applicant_name']}</td>
                                    <td>{$res['applicant_phone']}</td>
                                    <td>{$res['count_animals']}</td>
                                    <td>{$res['description']}</td>
                                    <td>{$creator['sourname']}<br/>{$creator['name']}<br/>{$creator['patronymic']}</td>
                                    <td>";
                    if ($_SESSION['id_users_group'] == '1') {
                        $rowsData[] =   "<select class='journal-input journal-input-gray edit_status'>";
                        if ($res['status'] == '0')
                            $rowsData[] .= "<option selected disabled>Не завершена</option>
                                            <option data-status='1' id-journal-row='".$res['id']."'>В работе</option>
                                            <option data-status='2' id-journal-row='".$res['id']."'>Завершена</option>";
                        if ($res['status'] == '1')
                            $rowsData[] .= "<option data-status='0' id-journal-row='".$res['id']."'>Не завершена</option>
                                            <option selected disabled>В работе</option>
                                            <option data-status='2' id-journal-row='".$res['id']."'>Завершена</option>";
                        if ($res['status'] == '2')
                            $rowsData[] .= "<option data-status='0' id-journal-row='".$res['id']."'>Не завершена</option>
                                            <option data-status='1' id-journal-row='".$res['id']."'>В работе</option>
                                            <option selected disabled>Завершена</option>";
                        $rowsData[] .= "</select>
                                    </td>
                                    <td>
                                        <select class='journal-input journal-input-gray edit_executor'>";
                        $sess_exec = $_SESSION['id_region'] == "all" ? "1" : "`code_region`=".$_SESSION['id_region'];
                        $executors = mysqli_query($SERVER, "SELECT `id`, `name`, `sourname`, `patronymic` FROM `users` WHERE `id_users_group`=4 AND ".$sess_exec);
                        if ($res['id_kinolog'] == NULL)
                                $rowsData[] .= "<option disabled selected>Нет исполнителя</option>";
                        while ($executor = mysqli_fetch_array($executors)) {
                            if ($res['id_kinolog'] == $executor['id'])
                                $active = "selected disabled";
                            else
                                $active = "";
                            $rowsData[] .= "<option ".$active." data-executor='".$executor['id']."' id-journal-row='".$res['id']."'>".$executor['sourname']." ".$executor['name']." ".$executor['patronymic']."</option>";
                        }
                        $rowsData[] .= "</select>";
                    }
                    if ($_SESSION['id_users_group'] == '4') {
                        switch ($res['status']) {
                            case '0':
                                $rowsData[] .= "<button class='btn btn-danger btn-sm to_work' id-row='".$res['id']."'>Взять в работу</button>";
                                break;
                            case '1':
                                $rowsData[] .= "<button class='btn btn-primary btn-sm show_linked_card_form' id-row='".$res['id']."'>Завершить</button>";
                                break;
                            case '2':
                                $rowsData[] .= "<button disabled class='btn btn-success btn-sm'>Завершена</button>";
                                break;
                            default:
                                $rowsData[] .= "<button class='btn btn-danger btn-sm to_work'>Взять в работу</button>";
                                break;
                        }
                    }
                    $rowsData[] .= "<td>";
                    $get_application = mysqli_query($SERVER, "SELECT * FROM `application` WHERE `id_journal_application`=".$res['id']);
                    $rowsData[] .= '<button type="button" class="btn btn-default btn-md open_request_info" id-row='.$res['id'].'>
                                        '.mysqli_num_rows($get_application).'&nbsp<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                    </button>';            
                    $rowsData[] .= "</td>
                                    <td><button class='delete-journal btn btn-default' id-row='".$res['id']."'>Удалить</button></td>
                                </tr>";
                    $rowscounter++;
                }
                break;
            default:
                $rowsData[] = "Произошла ошибка определения устройства для генерации таблицы/списка. Пожауйста, попробуйте обновить страницу, если ошибка не уйдёт - обратититесь к разработчикам";
                break;
        }
        
        $html = "";
	    foreach($rowsData as $row){
	        $html .= $row;
	    }
        echo json_encode(array(
	        'result'    => 'success',
            'count_rows_in_base'=> $count_rows_in_base,
            'journal_current_page' => $journal_current_page,
	        'html'      => $html
	    ));
    }
    if (isset($_GET['update_table_config'])) {
        switch ($_POST['journal_order_by']) {
            case 'number':
                $_SESSION['journal_order_by']= "id";
                break;
            case 'date':
                $_SESSION['journal_order_by']= "date";
                break;
            case 'name':
                $_SESSION['journal_order_by']= "applicant_name";
                break;
            case 'phone':
                $_SESSION['journal_order_by']= "applicant_phone";
                break;
            case 'count':
                $_SESSION['journal_order_by']= "id_creator";
                break;
            case 'status':
                $_SESSION['journal_order_by']= "status";
                break;
            case 'id_executor':
                $_SESSION['journal_order_by']= "id_kinolog";
                break;
            default:
                $_SESSION['journal_order_by']= "date";
                break;
        }
        switch ($_POST['journal_sort_by']) {
            case 'ASC':
                $_SESSION['journal_sort_by']= "ASC";
                break;
            case 'DESC':
                $_SESSION['journal_sort_by']= "DESC";
                break;
            default:
            $_SESSION['journal_sort_by']= "ASC";
                break;
        }
        switch ($_POST['journal_row_limit']) {
            case '50':
                $_SESSION['journal_row_limit'] = "50";
                break;
            case '100':
                $_SESSION['journal_row_limit'] = "100";
                break;
            case '150':
                $_SESSION['journal_row_limit'] = "150";
                break;
            case '200':
                $_SESSION['journal_row_limit'] = "200";
                break;
            default:
                $_SESSION['journal_row_limit'] = "50";
                break;
        }
        switch ($_POST['journal_status']) {
            case 'all':
                $_SESSION['journal_status'] = "all";
                break;
            case '0':
                $_SESSION['journal_status'] = "0";
                break;
            case '1':
                $_SESSION['journal_status'] = "1";
                break;
            case '2':
                $_SESSION['journal_status'] = "2";
                break;
            default:
                $_SESSION['journal_row_limit'] = "all";
                break;
        }
        $_SESSION['journal_filter_first_date'] = $_POST['journal_filter_first_date'];
        $_SESSION['journal_filter_last_date'] = $_POST['journal_filter_last_date'];
        switch ($_POST['journal_id_executor']) {
            case 'without':
                $_SESSION['journal_id_executor'] = "without";
                break;
            case 'all':
                $_SESSION['journal_id_executor'] = "all";
                break;
            case '':
                $_SESSION['journal_id_executor'] = "all";
                break;
            default:
                $_SESSION['journal_id_executor'] = $_POST['journal_id_executor'];
                break;
        }
        echo json_encode(array(
            'result'    =>  'success'
        ));
    }
    if (isset($_GET['journal_filter_reset'])) {
        $_SESSION['journal_order_by']= 'date';
        $_SESSION['journal_sort_by']= 'ASC';
        $_SESSION['journal_row_limit'] = '50';
        $_SESSION['journal_filter_first_date'] = 'first';
        $_SESSION['journal_filter_last_date'] = 'last';
        $_SESSION['journal_start_from'] = '0';
        $_SESSION['journal_status'] = 'all';
        $_SESSION['journal_id_executor'] = 'without';
        $_SESSION['journal_search_request'] = '';
        echo json_encode(array(
	        'result'    => 'success',
        ));
    }
    if (isset($_GET['journal_change_page'])) {
        $journal_need_page = $_POST['journal_need_page'];
        $_SESSION['journal_start_from'] = $journal_need_page*$_SESSION['journal_row_limit']-$_SESSION['journal_row_limit']; 
        echo json_encode(array(
	        'result'    => 'success',
        ));
    }
    if (isset($_GET['journal_search'])) {
        if (!ctype_space($_POST['journal_search_request']))
            $_SESSION['journal_search_request'] = $_POST['journal_search_request'];

        echo json_encode(array(
	        'result'    => 'success',
            'request'   => $_POST['journal_search_request']
        ));
    }
    if (isset($_GET['add_row'])) {
        $today_day_time = time();
        $journal_locality = prepare($_POST['journal_locality']);
        $journal_street = prepare($_POST['journal_street']);
        $journal_applicant_name = prepare($_POST['journal_applicant_name']);
        $journal_applicant_phone = prepare($_POST['journal_applicant_phone']);
        $journal_animals_count = prepare($_POST['journal_animals_count']);
        $journal_description = prepare($_POST['journal_description']);
        $loc_type = prepare($_POST['journal_loc_type']).". ";  
        $journal_date = $_POST['journal_date'];
        if ($loc_type == "undefined.") 
            $loc_type = "";
        if ($_SESSION['id_region'] == "all") {
            $default_user_region = mysqli_query($SERVER, "SELECT `code_region` FROM `users` WHERE `id` = ".$_SESSION['id_user']);
			$query_res = mysqli_fetch_array($default_user_region);
			switch ($query_res['code_region']) {
				case 1:
					$journal_region = "1";
					break;
				case 2:
					$journal_region = "2";
					break;
				default:
                    $journal_region = "1";
					break;
			}
        }
        else
            $journal_region = $_SESSION['id_region'];
        $query = "INSERT INTO `journal` (`date`, `loc_type`, `locality`, `street`, `id_region`, `description`, `id_creator`, `applicant_phone`, `applicant_name`, `count_animals`, `status`, `id_kinolog`) VALUES ('".$journal_date."', '".$loc_type."', '".$journal_locality.", ".$journal_street."' , '".$journal_street."', '".$journal_region."', '".$journal_description."', '".$_SESSION['id_user']."', '".$journal_applicant_phone."', '".$journal_applicant_name."', '".$journal_animals_count."', '0', null)";
        $get_kinologs = mysqli_query($SERVER, "SELECT * FROM `users` WHERE `code_region`='".$_SESSION['id_region']."' AND `id_users_group`='4' AND `show`='1' AND `podtverzhdeno`='1'");
        while ($kinolog = mysqli_fetch_array($get_kinologs)) {
            $add_notification = "INSERT INTO `notifications` (`date`, `type`, `id_user`, `show`, `text`)
                VALUES ('".$time_now."', 'kinolog_new_task', '".$kinolog['id']."', '1', 'Появились новые задачи по отлову без исполнителя. Перейдите в раздел <<Мои задачи>>, чтобы увидеть их')";
            if (!mysqli_query($SERVER, $add_notification))
                file_put_contents("logs/journal_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$query." - Ошибка"."\n", FILE_APPEND);
            else {
                file_put_contents("logs/journal_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$query." - Успех"."\n", FILE_APPEND);
            }
        }
        if (!mysqli_query($SERVER, $query))
            file_put_contents("logs/journal_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$query." - Ошибка"."\n", FILE_APPEND);
        else {
            file_put_contents("logs/journal_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$query." - Успех"."\n", FILE_APPEND);
            echo json_encode(array(
                'result'    => 'success'
            ));
        }
    }
    if (isset($_GET['edit_status'])) {//обновление у админа
        $query = "UPDATE `journal` SET `status`=".$_POST['new_status']." WHERE `id`=".$_POST['id_row'];
        if (!mysqli_query($SERVER, $query))
            file_put_contents("logs/journal_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$query." - Ошибка"."\n", FILE_APPEND);
        else {
            file_put_contents("logs/journal_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$query." - Успех"."\n", FILE_APPEND);
            echo json_encode(array(
                'result'    => 'success'
            ));
        }
    }
    if (isset($_GET['edit_executor'])) {
        $query = "UPDATE `journal` SET `id_kinolog`=".$_POST['new_executor']." WHERE `id`=".$_POST['id_row'];
        if (!mysqli_query($SERVER, $query))
            file_put_contents("logs/journal_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$query." - Ошибка"."\n", FILE_APPEND);
        else {
            file_put_contents("logs/journal_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$query." - Успех"."\n", FILE_APPEND);
            $row = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `journal` WHERE `id`=".$_POST['id_row']));
            $add_notification = "INSERT INTO `notifications` (`date`, `type`, `id_user`, `show`, `text`) VALUES ('".$time_now."', 'kinolog_new_task', '".$row['id_kinolog']."', '1', 'У вас новое задание по отлову животного. Перейдите в раздел <<Мои задачи>>, чтобы увидеть его')";
            if (!mysqli_query($SERVER, $add_notification))
                file_put_contents("logs/notifications_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$add_notification." - Ошибка"."\n", FILE_APPEND);
            else
                file_put_contents("logs/notifications_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$add_notification." - Успех"."\n", FILE_APPEND);
            echo json_encode(array(
                'result'    => 'success'));
        }
    }
    if (isset($_GET['to_work'])) {//обновление у кинолога
        $query = "UPDATE `journal` SET `status`='1', `id_kinolog`='".$_SESSION['id_user']."' WHERE `id`=".$_POST['id_row'];

        if (!mysqli_query($SERVER, $query))
            file_put_contents("logs/journal_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$query." - Ошибка"."\n", FILE_APPEND);
        else {
            file_put_contents("logs/journal_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$query." - Успех"."\n", FILE_APPEND);
            echo json_encode(array(
                'result'    => 'success'
            ));
        }
    }
    if (isset($_GET['get_my_animal_cards'])){
        $get_cards = mysqli_query($SERVER, "SELECT application.id AS id,
                                            application.data AS data,
                                            application.street AS street,
                                            application.loc_type AS loc_type,
                                            application.locality AS locality,
                                            application.num_aviary AS num_aviary,
                                            animal_kind.name_kind AS kind,
                                            application.breed AS breed, 
                                            application.age AS age, 
                                            application.weight AS weight, 
                                            application.height AS height, 
                                            application.color AS color
                                            FROM `application`
                                            LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
                                            LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
                                            LEFT JOIN application_status ON application.id_status = application_status.id
                                            WHERE `id_user`='".$_SESSION['id_user']."' AND `show`=1 AND `id_journal_application` IS NULL AND (`id_status`=1 OR `id_status`=11 OR `id_status`=7 OR `id_status`=8) ORDER BY data DESC");
        $html = "";
        while ($card = mysqli_fetch_array($get_cards)) {
            $rowsData[] .= 
                "<tr class='card_on_list' id-journal-row='".$card['id']."'>
                    <td><div class='id-color'>".$card['id']."</div></td>
                    <td>".date("d-m-Y\ H:i", $card['data'])."</td>
                    <td>".$card['loc_type']." ,".$card['locality']." ,".$card['street']."</td>
                    <td>".$card['num_aviary']."</td>
                    <td>".$card['kind']."</td>
                    <td>".$card['breed']."</td>
                    <td>".$card['weight']."</td>
                    <td>".$card['height']."</td>
                    <td>".$card['color']."</td>
                    <!--<td>
                        <a target='_blank' href='animal_card.php?application=".$card['id']."'>
                            Просмотр
                        </a>
                    </td>-->
                    <td>
                        <input class='select-card' type='checkbox' multiple/>
                    </td>
                </tr>";
        }
        foreach($rowsData as $row){
            $html .= $row;
        }
        if ($html == "")
            $html .= "<div class='row'>Уведомлений нет</div>";
        echo json_encode(array(
            'result'    => 'success',
            'html'      => $html
        ));
    }
    if (isset($_GET['complete'])) {//обновление у кинолога
        //$id_selected_cards = $_POST['id_selected_cards'];
        $completed_application = $_POST['completed_application'];
        $query = "UPDATE `journal` SET `status`='2' WHERE `id`=".$completed_application;
        $error = false;
        if (!mysqli_query($SERVER, $query)) {
            file_put_contents("logs/journal_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$query." - Ошибка"."\n", FILE_APPEND);
            $error = true;
        }
        else {
            file_put_contents("logs/journal_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$query." - Успех"."\n", FILE_APPEND);
        }
        /*for ($i=0; $i < count($id_selected_cards); $i++) { 
            $update_application = "UPDATE `application` SET `id_journal_application`='".$completed_application."' WHERE `id`=".$id_selected_cards[$i];
            if (!mysqli_query($SERVER, $update_application)) {
                file_put_contents("logs/journal_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$update_application." - Ошибка"."\n", FILE_APPEND);
                $error = true;
            }
            else {
                file_put_contents("logs/journal_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$update_application." - Успех"."\n", FILE_APPEND);
            }
        }*/
        
        if ($error == false)
            echo json_encode(array(
                'result'    => 'success'
            ));
    }
    if (isset($_GET['get_linked_applications'])) {
        $html = "";
        $linked_applications = mysqli_query($SERVER, "SELECT * FROM `application` WHERE `id_journal_application`=".$_POST['id_row']);
        while ($card = mysqli_fetch_array($linked_applications)) {
            $img = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `upload_files` WHERE `id_application`=".$card['id']));
            //$region = mysqli_fetch_array(mysqli_query($SERVER, ""));
            $region = mysqli_fetch_array(mysqli_query($SERVER, "SELECT `name_region` FROM `region` WHERE `id`=".$card['id_region']));
            $kind = mysqli_fetch_array(mysqli_query($SERVER, "SELECT `name_kind` FROM `animal_kind` WHERE `id`=".$card['id_kind']));
            $gender = mysqli_fetch_array(mysqli_query($SERVER, "SELECT `name_gender` FROM `animal_gender` WHERE `id`=".$card['id_gender']));
            $color = mysqli_fetch_array(mysqli_query($SERVER, "SELECT `name_color` FROM `animal_color` WHERE `id`=".$card['color']));
            $rowsData[] .=  '<div class="row request_row">
                                <div class="col-xs-12 col-md-2">
                                    <img src="'.$img['path_thumb'].$img['name_thumb'].'" alt="Изображение">
                                </div>
                                <div class="caption">
                                    <h3>Учётное дело №'.$card['id'].' от '.date("d.m.Y\ H:i", $card['data']).'</h3>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <h4>Адресс: '.$region['name_region'].', '.$card['loc_type'].$card['locality'].', ул.'.$card['street'].'</h4>
                                    <h4>Вид: '.$kind['name_kind'].'</h4>
                                    <h4>Пол: '.$gender['name_gender'].'</h4>
                                    <h4>Окрас: '.$color['name_color'].'</h4>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                    <h4>Возраст: '.$card['age'].'</h4>
                                    <h4>Вес: '.$card['weight'].'</h4>
                                    <h4>Высота: '.$card['height'].'</h4>
                                    <p>
                                        <a target="_blank" href="animal_card.php?application='.$card['id'].'" class="btn btn-default" role="button">Просмотр</a>
                                    </p>
                                </div>
                            </div>';
        }
        foreach($rowsData as $row){
            $html .= $row;
        }
        echo json_encode(array(
            'result'    => 'success',
            'html'      => $html
        ));
    }
    if (isset($_GET['delete_journal_row'])) {
        $id_row = $_POST['id_row'];
        $query = "UPDATE `journal` SET `show`='0' WHERE `id`=".$id_row;
        if (!mysqli_query($SERVER, $query)) {
            file_put_contents("logs/journal_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$query." - Ошибка"."\n", FILE_APPEND);
        }
        else {
            file_put_contents("logs/journal_log.txt", date("Y-m-d\TH:i:s", time())." Пользователь ".$_SESSION['id_user'].", запрос ".$query." - Успех"."\n", FILE_APPEND);
        }       
        echo json_encode(array(
            'result'    => 'success'
        ));
    }
?>