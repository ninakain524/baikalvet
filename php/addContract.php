<?php
	require_once('config.php');
    require_once("timezone.php");
    require_once("functions.php");
	setlocale(LC_ALL, 'ru_RU.utf8');
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	mb_http_output('UTF-8');
	mb_language('uni');
	
	if (isset($_POST["customer"])) {
	    $path = "";
	    if (!empty($_FILES['doc'])) {
    	    if ($_FILES && $_FILES["doc"]["error"]== UPLOAD_ERR_OK)
            {
                $path = '/assets/files/contracts/'.$_FILES["doc"]["name"];
                $name = $_SERVER['DOCUMENT_ROOT'] . $path;
                move_uploaded_file($_FILES["doc"]["tmp_name"], $name);
                // echo $name. " Файл загружен";
            }
	    }
	    $result_id = $SERVER -> query("SELECT id FROM contract ORDER BY id DESC LIMIT 1");
        $row_id = mysqli_fetch_array($result_id);
        $id_contract = $row_id['id']+1;
        
        $query = "INSERT INTO `contract` (
            `id`, `customer`, `name_contract`, `deadline`, `term_services`, `number`, `id_region`, 
            `date_contract`, `comment`, `full_name`, `person_customer`, `index_a`, `adress`, `inn`, 
            `kpp`, `ogrn`, `phone`, `num_application`, `name_tz`, `name_organization`, `document`) VALUES (
            '".$id_contract."',
            '".trim($_POST['customer'])."',
            '".trim($_POST['name'])."',
            '".trim($_POST['deadline'])."',
            '".trim($_POST['term_services'])."',
            '".trim($_POST['number'])."',
            '".trim($_POST['region'])."',
            '".trim($_POST['date_contract'])."',
            '".trim($_POST['comment'])."',
            '".trim($_POST['full-name'])."',
            '".trim($_POST['person_customer'])."',
            '".trim($_POST['index_a'])."',
            '".trim($_POST['adress'])."',
            '".trim($_POST['inn'])."',
            '".trim($_POST['kpp'])."',
            '".trim($_POST['ogrn'])."',
            '".trim($_POST['phone'])."',
            '".trim($_POST['num_application'])."',
            '".trim($_POST['name_tz'])."',
            '".trim($_POST['name_organization'])."',
            '".$path."')";

        if (mysqli_query($SERVER, $query)) {
            addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Добавил контракт - " . $query);
        }

        $SERVER -> close();
        header('location: ../contract.php' );
        exit;
    }
    else   
        echo 'error';

?>