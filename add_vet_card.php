<?php
  require_once("php/config.php");
  require_once("php/timezone.php");
  if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
  require_once("template/head.html");
  require_once("template/header.php");
  require_once("php/functions.php");
  // Часовой пояс Якутск
  $today_day_time = date("Y-m-d\TH:i:s");
  
  $card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".$_GET['application']));
  $animal_kind = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_kind WHERE id=".$card['id_kind']));
  $animal_gender = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_gender WHERE id=".$card['id_gender']));
  $animal_shelter = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_shelters WHERE id=".$card['id_shelter']));
  $card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".$_GET['application']));
?>
<script type="text/javascript" src="js/veterinar.js"></script>
<section class="content">
	<div class="container">
		<div id="add_application" class="content-form"> 
			<div class="row">
				<h1>Карточка ветеринара</h1>

        		<div class="col-12 col-sm-4">
                  <div class="form-group">
                    <label for="date_vet">Дата проведения лечебных мероприятий</label>
                    <input class="field width-add field-date" type="datetime-local" name="date_vet" id="date_vet" autocomplete="on" value="<?php echo $today_day_time; ?>" required />  
                  </div>
                </div>
        
                <div class="col-12 col-sm-4">
                  <div class="form-group">
                    <label for="num_birka">№ неснимаемой/несмываемой метки</label>
                    <input class="field width-add" type="number" name="num_birka" id="num_birka" />
                  </div>
                </div>
        		        
                <div class="col-12 col-sm-4">
                  <div class="form-group">
                    <label for="num_chip">№ микрочипа</label>
                    <input class="field width-add" type="number" name="num_chip" id="num_chip" />
                  </div>
                </div>
	        </div>
	        
    	    <div class="row">
                <h4>Дегельминтизация</h4>
        		        
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label for="date_degel">Дата дегельминтизации</label>
                    <input class="field width-add field-date" type="datetime-local" name="date_degel" id="date_degel" autocomplete="on" value="<?php echo $today_day_time; ?>" required />  
                  </div>
                </div>
                      
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label for="degel_preparat">Препарат</label>
                    <input class="field width-add" type="text" name="degel_preparat" id="degel_preparat" />
                  </div>
                </div> 
             </div> 
                
            <div class="row">
                <h4>Вакцинация против бешенства и лептоспироза</h4>
                
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label for="date_vac">Дата вакцинации</label>
                    <input class="field width-add field-date" type="datetime-local" name="date_vac" id="date_vac" autocomplete="on" value="<?php echo $today_day_time; ?>" required />  
                  </div>
                </div>
                  
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label for="vac_preparat">Препарат</label>
                    <input class="field width-add" type="text" name="vac_preparat" id="vac_preparat" />
                  </div>
                </div> 
            </div>
            
            <div class="row">
                <h4>Стерилизация / кастрация</h4>
                
                <div class="col-12 col-sm-4">
                  <div class="form-group">
                    <label for="date_operation">Дата операции</label>
                    <input class="field width-add field-date" type="datetime-local" name="date_operation" id="date_operation" autocomplete="on" value="<?php echo $today_day_time; ?>" required />  
                  </div>
                </div>
                <div class="col-12 col-sm-4">
                  <div class="form-group">
                    <label for="operation_vet">Ответственный специалист за проведение всех мероприятий</label>
                    <select class="field width-add" id="operation_vet" name="operation_vet" required>
                      <?php
                        $sql = "SELECT * FROM `users` WHERE `id_users_group` = 3;";
                        $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                        echo '<option value='.$_SESSION['id_user'].' selected readonly hidden>'.$_SESSION['sourname_user']. " " .$_SESSION['name_user'].'</option>';
                
                        while($row = mysqli_fetch_array($res))
                        {
                          echo '<option value="'. $row['id'] .'">'. $row['sourname'] . " " . $row['name']. '</option>';
                        }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="col-12 col-sm-4">
                  <div class="form-group">
                    <label for="date_operation">№ вольера животного</label>
                    <input class="field width-add" type="text" name="num_aviary" id="num_aviary" value="<?php echo $card['num_aviary']; ?>"/>  
                  </div>
                </div>
            </div>  
      
            <div class="row">
                <h3>Медиафайлы</h3>
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label for="mediafiles">Фото и видео</label>
                    <input  type="file" class="form-control" name="mediafiles" id="mediafiles" accept="image/jpeg, image/png, image/jpg, video/mp4, video/avi, video/3gp, video/mov" multiple>
                  </div>
                </div>
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <button id="create_vet_card" class="btn field-submit width-submit" data="<?php echo $_GET['application'];?>">Сохранить</button>
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
  require_once("template/footer.html");
?>