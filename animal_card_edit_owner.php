<?php
	require_once("php/config.php");
  require_once('php/functions.php');
  require_once("php/functions.php");
	if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
	require_once("template/head.html");
	require_once("template/header.php");
  $today_day_time = date("Y-m-d\TH:i");
	
	if ($_GET['application'] != null && $_GET['application'] != "") {
    	$card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".$_GET['application']));
    	$card_date_z = mysqli_fetch_array(mysqli_query($SERVER, "SELECT DATE_FORMAT(date_zaseleno,'%d.%m.%Y %H:%i') AS date_zaseleno, DATE_FORMAT(date_vipusk,'%d.%m.%Y %H:%i') AS date_vipusk FROM application WHERE id=".$_GET['application']));
    	$card_date_z_edit = mysqli_fetch_array(mysqli_query($SERVER, "SELECT date_zaseleno, date_vipusk FROM application WHERE id=".$_GET['application']));
    	$animal_kind = mysqli_fetch_array(mysqli_query($SERVER, "SELECT name_kind FROM animal_kind WHERE id=".$card['id_kind']));
    	$animal_gender = mysqli_fetch_array(mysqli_query($SERVER, "SELECT name_gender FROM animal_gender WHERE id=".$card['id_gender']));
    	
      if (ctype_digit($card['color'])) 
    	    $animal_color = mysqli_fetch_array(mysqli_query($SERVER, "SELECT name_color FROM animal_color WHERE id=".$card['color']));
    	else
    	    $animal_color = mysqli_fetch_array(mysqli_query($SERVER, "SELECT name_color FROM animal_color WHERE id=0"));
    	
      $animal_shelter = mysqli_fetch_array(mysqli_query($SERVER, "SELECT name_shelter FROM animal_shelters WHERE id=".$card['id_shelter']));
    	$status = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application_status WHERE id=".$card['id_status']));
    	$region = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM region WHERE id=".$card['id_region']));
      $sostoyanie = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application_sostoyanie WHERE id=".$card['id_sostoyanie']));
      $card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".$_GET['application']));
      $user = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$card['id_user']));
      $contract = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM contract WHERE id=".$card['id_contract']));
	}
	// var_dump(date_default_timezone_get());
	setlocale(LC_ALL, 'ru_RU.utf8');
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	mb_http_output('UTF-8');
	mb_language('uni');
  // var_dump(date_default_timezone_get());
?>

<!--Редактировать карточку-->
  <div id="hide-form-popup-edit-card" class="hide-form-popup-edit-card hideform fancy-animate">
    <form id="popup-edit-card" method="post" action="/php/update_application.php?edit_application=<?php echo ($_GET['application']) ?>">
        <h3>Редактирование основных данных</h3>
        <? if ( $_SESSION['id_users_group'] == "1") { ?>
        <div class="col-12 col-sm-6">
            <div class="form-group">
                <label for="id_status">Статус</label>
                <select class="field width-add" id="id_status" name="id_status" required>
                  <?php
                    $sql = "SELECT * FROM `application_status`;";
                    $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                    echo '<option value='.$card["id_status"].' selected readonly hidden>'.$status["status_name"].'</option>';
    
                    while($row = mysqli_fetch_array($res))
                    {
                      echo '<option value="'. $row['id'] .'">'. $row['status_name'] .'</option>';
                    }
                  ?>
                </select>
            </div>
        </div>
        
        <div class="col-12 col-sm-6">
            <div class="form-group">
                <label for="region">Регион отлова</label>
    		    <select class="field" id="region_code" name="id_region" required autofocus>
                    <?php
                      $sql_region = "SELECT * FROM `region`;";
                      $res_region = mysqli_query($SERVER, $sql_region) or die("Ошибка " . mysqli_error($SERVER));
                      echo '<option value='.$card["id_region"].' selected readonly hidden>'.$region["name_region"].'</option>';
    
                      while($row_region = mysqli_fetch_array($res_region))
                      {
                        echo '<option value="'. $row_region['id'] .'">'. $row_region['name_region'] .'</option>';
                      }
                    ?>
                  </select>
            </div>
        </div>
        <? } ?>
        <? if ( $_SESSION['id_users_group'] == "4") { ?>              
        <div class="col-12 col-sm-6">
            <div class="form-group">
                <label for="id_status">Статус</label>
                <select class="field width-add" id="id_status" name="id_status" required>
                  <?php
                    $sql = "SELECT * FROM `application_status` WHERE id=1 OR id=2;";
                    $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                    echo '<option value='.$card["id_status"].' selected readonly hidden>'.$status["status_name"].'</option>';
    
                    while($row = mysqli_fetch_array($res))
                    {
                      echo '<option value="'. $row['id'] .'">'. $row['status_name'] .'</option>';
                    }
                  ?>
                </select>
            </div>
        </div>
        <? } ?>

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="city">Местность отлова</label>
            <input class="field width-add" type="text" name="city" id="city" value="<?echo $card['locality']?>" required />
          </div>
        </div> 
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="street">Улица</label>
            <input class="field width-add" type="text" name="street" id="street" value="<? echo $card['street'] ?>"/>
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="id_contract">Контракт</label>
            <select class="field width-add" id="id_contract" name="id_contract" required>
              <?php
                if ( $_SESSION['id_users_group'] == "1" ) 
                    $sql = "SELECT * FROM `contract` WHERE `show_contract`=1 AND status='открыт';";
                else
                    $sql = "SELECT * FROM `contract` WHERE `show_contract`=1 AND status='открыт' AND `id_region`='".$_SESSION['id_region']."';";
                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                echo '<option value='.$card["id_contract"].' selected readonly hidden>'.$contract["name_contract"].'</option>';

                while($row = mysqli_fetch_array($res))
                {
                  echo '<option value="'. $row['id'] .'">'.$row['name_contract'].'</option>';
                }
              ?>
            </select>
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="date">Дата отлова</label>
            <input class="field width-add field-date" type="datetime-local" name="date" id="date" autocomplete="on" value="<?php echo date("Y-m-d\TH:i", $card['data']) ?>" />  
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="date-karantin">Дата начала карантина</label>
            <input class="field width-add field-date" type="datetime-local" name="date-karantin" id="date-karantin" value="<?php if($card['date_start_karantin'] == 0) echo date("Y-m-d\TH:i", $card['data']); else echo date("Y-m-d\TH:i", $card['date_start_karantin']); ?>" />  
          </div>
        </div>

        <? if ( $_SESSION['id_users_group'] == "1") { 
          $rs = mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM vet_card WHERE id_application=".$_GET['application']);
          $result = mysqli_fetch_array($rs);
          if ($result[0] > 0) { ?>
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date-karantin">Дата начала восстановления</label>
                <input class="field width-add field-date" type="datetime-local" name="date-vosst" id="date-vosst" value="<?php if($card['date_start_vosstanovlenie'] != 0 && $card['date_start_vosstanovlenie'] != NULL) echo date("Y-m-d\TH:i", $card['date_start_vosstanovlenie']); ?>" />  
              </div>
            </div>
        <? } } ?>

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="id_kind">Вид животного</label>
            <select class="field width-add" id="id_kind" name="id_kind" >
              <?php
                $sql = "SELECT * FROM `animal_kind`;";
                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                echo '<option value='.$card["id_kind"].' selected readonly hidden>'.$animal_kind["name_kind"].'</option>';

                while($row = mysqli_fetch_array($res))
                {
                  echo '<option value="'. $row['id'] .'">'. $row['name_kind'] .'</option>';
                }
              ?>
            </select>
          </div>
        </div>

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="breed">Порода животного</label>
            <input class="field width-add" type="text" name="breed" id="breed" value="<?echo $card['breed']?>" />
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="gender">Пол животного</label>
            <select class="field width-add" id="gender" name="gender">
              <?php
                $sql = "SELECT * FROM `animal_gender`;";
                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                echo '<option value='.$card["id_gender"].' selected readonly hidden>'.$animal_gender["name_gender"].'</option>';

                while($row = mysqli_fetch_array($res))
                {
                  echo '<option value="'. $row['id'] .'">'. $row['name_gender'] .'</option>';
                }
              ?>
            </select>
          </div>
        </div> 
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="color">Окрас животного</label>
            <select class="field width-add" id="color" name="color">
              <?php
                $sql = "SELECT * FROM `animal_color`;";
                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                echo '<option value='.$card["color"].' selected readonly hidden>'.$animal_color["name_color"].'</option>';

                while($row = mysqli_fetch_array($res))
                {
                  echo '<option value="'. $row['id'] .'">'. $row['name_color'] .'</option>';
                }
              ?>
            </select>
          </div>
        </div> 
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="age">Возраст животного</label>
            <input class="field width-add" type="text" name="age" id="age" value="<?echo $card['age']?>" />
          </div>
        </div>

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="weight">Вес животного</label>
            <input class="field width-add" type="text" name="weight" id="weight" value="<?echo $card['weight']?>" />
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="height">Высота в холке</label>
            <input class="field width-add" type="text" name="height" id="height" value="<?echo $card['height']?>" />
          </div>
        </div>
        
        <? if ( $_SESSION['id_users_group'] == "1" ) { ?>
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="kin">Ответственный кинолог</label>
                <select class="field width-add" id="kin" name="kin">
                  <?php
                    $sql_k = "SELECT * FROM `users` WHERE `show` = 1 AND `podtverzhdeno` = 1 AND `id_users_group` = 4;";
                    $res_k = mysqli_query($SERVER, $sql_k) or die("Ошибка " . mysqli_error($SERVER));
                    echo '<option value='.$user['id'].' selected readonly hidden>'.$user['sourname']." ".$user['name'].'</option>';
                    while($row_k = mysqli_fetch_array($res_k))
                    {
                        echo '<option value="'. $row_k['id'] .'">'.$row_k['sourname']." ".$row_k['name'].'</option>';
                    }
                  ?>
                </select>
              </div>
            </div> 
        <? } ?>

        <div class="col-12 col-sm-12">
          <div class="form-group">
            <label for="shelter">Приют</label>
            <select class="field width-add" id="shelter_edit" name="shelter">
              <?php
                $sql = "SELECT * FROM `animal_shelters` WHERE actual = 1;";
                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                echo '<option value='.$card["id_shelter"].' selected readonly hidden>'.$animal_shelter["name_shelter"].'</option>';

                while($row = mysqli_fetch_array($res))
                {
                  echo '<option value="'. $row['id'] .'">'. $row['name_shelter'] .'</option>';
                }
              ?>
            </select>
          </div>
        </div>
        
        <div class="col-12 col-sm-12">
          <div class="form-group">
            <label for="aviary">№ вольера</label>
            <input class="field width-add" type="text" name="aviary" id="num_aviary" value="<?echo $card['num_aviary']?>" />
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="date_z">Дата заселения в вольер</label>
            <input class="field width-add field-date" type="datetime-local" name="date_z" id="date_z" autocomplete="on" value="<?php if($card_date_z_edit['date_zaseleno'] != "00.00.0000 00:00:00" && $card_date_z_edit['date_zaseleno'] != NULL) echo date("Y-m-d\TH:i", strtotime($card_date_z_edit['date_zaseleno'])); else echo date("Y-m-d\TH:i", $card['data']); ?>" /> 
            <!-- <input class="field width-add field-date" type="datetime-local" name="date_z" id="date_z" autocomplete="on" value="<?php if($card_date_z_edit['date_zaseleno'] != "00.00.0000 00:00:00" && $card_date_z_edit['date_zaseleno'] != NULL) echo $card_date_z_edit['date_zaseleno']; else echo date("Y-m-d\TH:i", $card['data']+ 7200); ?>" />  -->
          
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="date_v">Дата выбытия из вольера</label>
            <input class="field width-add field-date" type="datetime-local" name="date_v" id="date_v" autocomplete="on" value="<?php echo $card_date_z_edit['date_vipusk'] ?>" />  
          </div>
        </div>
        
        <div class="col-12 col-sm-12">
          <div class="form-group">
            <label for="comment">Комментарий</label>
            <textarea class="field" name="comment" id="comment" autocomplete="off"><? echo $card['comment'] ?></textarea>
          </div>
        </div>
        
        <input class="btn button-auth field-submit field-width" type="submit" value="Сохранить" autofocus />
        
    </form>
  </div>
<!--/Редактировать карточку-->

<!--Редактировать фото-->
  <div id="hide-form-popup-edit-foto" class="hide-form-popup-edit-foto hideform fancy-animate">
    <form id="popup-edit-foto" data="<? echo $_GET['application'] ?>">
        <h3>Редактирование фото</h3>
        
        <div class="col-12 col-sm-12">
          <div class="form-group">
            <div name="mediafiles">
        	<?php
  		 		$query = "SELECT * FROM `upload_files` WHERE `id_application` = '".$_GET['application']."' AND `id_user_group`=4 AND `type`='img' ORDER BY id";
              	$res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
                while ($res = mysqli_fetch_array($res_query)) {
          			$img = $res['path_thumb'].$res['name_thumb'];
          			echo
          				"<div class='media-item'>
          					<img src='{$img}' width='100px'>
          					<a class='del_file' href='#' onclick='event.preventDefault()' data='{$res['id']}'></a>
          				</div>";
                }
                echo
      				'<input  type="file" class="form-control" name="media-on-edit" id="media-on-edit" accept="image/jpeg, image/png, image/jpg" multiple>';
            ?>
            </div>
          </div>
        </div>
        
        <button id="update-foto" type="button" class="btn field-submit width-submit">Сохранить</button>
        
    </form>
  </div>
<!--/Редактировать фото-->

<!--Редактировать видео-->
  <div id="hide-form-popup-edit-video" class="hide-form-popup-edit-video hideform fancy-animate">
    <form id="popup-edit-video">
        <h3>Редактирование видео</h3>
        
        <div class="col-12 col-sm-12">
          <div class="form-group">
            <div name="mediafiles">
        	<?php
              	$query = "SELECT * FROM `upload_files` WHERE `id_application` = '".$_GET['application']."' AND `id_user_group`=4 AND `type`='vid' ORDER BY id";
              	$res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
                while ($res = mysqli_fetch_array($res_query)) {
              		
              		$vid = $res['path_file'].$res['name_file'];
          			echo 
          				"<div class='media-item'>
          					<video src='{$vid}' width='170px' muted></video>
          					<a class='del_file' href='#' onclick='event.preventDefault()' data='{$res['id']}'>
          					</a>
          				</div>";
                }
                echo
      				'<input type="file" class="form-control" name="media-on-edit" id="media-on-edit" accept="video/mp4, video/avi, video/3gp, video/mov" multiple>';
            ?>
            </div>
          </div>
        </div>
        
        <button id="update-video" type="button" class="btn field-submit width-submit">Сохранить</button>
        
    </form>
  </div>
<!--/Редактировать видео-->

<!--Заполнить карточку ветеринара-->
  <div id="hide-form-popup-add-card-vet" class="hide-form-popup-add-card-vet hideform fancy-animate">
    <form id="popup-add-card-vet" method="post" action="/php/vet_card.php?add_vet_card=<? echo ($_GET['application']) ?>">
        <h3>Карточка ветеринара</h3>
        
        <div class="col-12 col-sm-12">
          <div class="form-group">
            <label for="date_vet">Дата проведения лечебных мероприятий и дата начала восстановления будут установлены после загрузки фото/видео подтверждения мероприятий<br>
          </div>
        </div>

        <!-- <div class="col-12 col-sm-12">
          <div class="form-group">
            <label for="date_vet">Дата проведения лечебных мероприятий<br>
            <b>(дегельминтизации, вакцинации против бешенства и лептоспироза, стерилизации/кастрации)</b></label>
            <input class="field width-add field-date" type="datetime-local" name="date_vet" id="date_vet" autocomplete="on" value="<?php echo $today_day_time; ?>" required />  
          </div>
        </div> -->
        
        <div class="col-12 col-sm-12">
          <div class="form-group">
            <label for="operation_vet">Ответственный специалист за проведение всех мероприятий</label>
            <select class="field width-add" id="operation_vet" name="operation_vet">
              <?php
                $sql = "SELECT * FROM `users` WHERE `id_users_group` = 3;";
                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                if ($_SESSION['id_users_group'] == "3")
                    echo '<option value='.$_SESSION['id_user'].' selected readonly hidden>'.$_SESSION['sourname_user']. " " .$_SESSION['name_user'].'</option>';
                else
                    echo '<option value="" selected readonly hidden>Выберите ветеринара</option>';
        
                while($row = mysqli_fetch_array($res))
                {
                  echo '<option value="'. $row['id'] .'">'. $row['sourname'] . " " . $row['name']. '</option>';
                }
              ?>
            </select>
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="num_birka">№ метки</label>
            <input class="field width-add" type="number" name="num_birka_add" id="num_birka_add" />
          </div>
        </div>
		        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="num_chip">№ микрочипа</label>
            <input class="field width-add" type="number" name="num_chip_add" id="num_chip_add" />
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="degel_preparat">Препарат дегельминтизации</label>
            <input class="field width-add" type="text" name="degel_preparat" id="degel_preparat" />
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="vac_preparat">Препарат для вакцинации</label>
            <input class="field width-add" type="text" name="vac_preparat" id="vac_preparat" />
          </div>
        </div> 
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="date_operation">№ вольера животного</label>
            <input class="field width-add" type="text" name="num_aviary_vet" id="num_aviary_vet" value="<?php echo $card['num_aviary']; ?>"/>  
          </div>
        </div>

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="gender">Пол животного</label>
            <select class="field width-add" id="gender_vet" name="gender_vet">
              <?php
                $sql = "SELECT * FROM `animal_gender`;";
                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                echo '<option value='.$card["id_gender"].' selected readonly hidden>'.$animal_gender["name_gender"].'</option>';

                while($row = mysqli_fetch_array($res))
                {
                  echo '<option value="'. $row['id'] .'">'. $row['name_gender'] .'</option>';
                }
              ?>
            </select>
          </div>
        </div> 

        <div class="col-12 col-sm-12">
          <div class="form-group">
            <label for="comment">Комментарий</label>
            <textarea class="field" name="comment_vet" id="comment_vet" autocomplete="off"></textarea>
          </div>
        </div>
        
        <input class="btn button-auth field-submit field-width" type="submit" value="Сохранить" autofocus />
        <!--<button id="create_vet_card" class="btn field-submit width-submit" data="<?php echo $_GET['application'];?>">Сохранить</button>-->
        
    </form>
  </div>
<!--/Заполнить карточку ветеринара-->

<!--Редактировать карточку ветеринара-->
<? 
    $rs = mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM vet_card WHERE id_application=".$_GET['application']);
    $result = mysqli_fetch_array($rs);
    if ($result[0] > 0) {
?>
  <div id="hide-form-popup-edit-card-vet" class="hide-form-popup-edit-card-vet hideform fancy-animate">
    <form id="popup-edit-card-vet" method="post" action="/php/vet_card.php?update_vet_card=<? echo ($_GET['application']) ?>">
        <?
            $vet_card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_card WHERE id_application=".$_GET['application']));
            $vet_degilmintizaciya = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_degilmintizaciya WHERE id_application=".$_GET['application']));
            $vet_operation = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_operation WHERE id_application=".$_GET['application']));
            $vet_vaccine = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_vaccine WHERE id_application=".$_GET['application']));
            $card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".$_GET['application']));
            $user_vet = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$vet_card['id_veterinar']));
        ?>
        
        <h3>Редактирование карточки ветеринара</h3>
        
        <div class="col-12 col-sm-12">
          <div class="form-group">
            <label for="date_vet">Дата проведения лечебных мероприятий и дата начала восстановления будут установлены после загрузки фото/видео подтверждения мероприятий<br>
          </div>
        </div>

        <!-- <div class="col-12 col-sm-12">
          <div class="form-group">
            <label for="date_vet">Дата проведения лечебных мероприятий<br>
            <b>(дегельминтизации, вакцинации против бешенства и лептоспироза, стерилизации/кастрации)</b></label>
            <input class="field width-add field-date" type="datetime-local" name="date_vet_edit" id="date_vet_edit" autocomplete="on" value="<?php echo (new DateTime($vet_card['data']))->format('Y-m-d\TH:i'); ?>" required />  
          </div>
        </div> -->
        
        <div class="col-12 col-sm-12">
          <div class="form-group">
            <label for="operation_vet">Ответственный специалист</label>
            <select class="field width-add" id="operation_vet_edit" name="operation_vet_edit">
              <?php
                $sql = "SELECT * FROM `users` WHERE `id_users_group` = 3;";
                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                if ($_SESSION['id_users_group'] == "3")
                    echo '<option value='.$_SESSION['id_user'].' selected readonly hidden>'.$_SESSION['sourname_user']. " " .$_SESSION['name_user'].'</option>';
                else
                    echo '<option value='.$user_vet["id"].' selected readonly hidden>'. $user_vet['sourname'] . " " . $user_vet['name']. '</option>';

                while($row = mysqli_fetch_array($res))
                {
                  echo '<option value="'. $row['id'] .'">'. $row['sourname'] . " " . $row['name']. '</option>';
                }
              ?>
            </select>
          </div>
        </div>

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="num_birka">№ неснимаемой/несмываемой метки</label>
            <input class="field width-add" type="number" name="num_birka_edit" id="num_birka_edit" value="<?php echo $vet_card['num_birka']; ?>"/>
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="num_chip">№ микрочипа</label>
            <input class="field width-add" type="number" name="num_chip-edit" id="num_chip_edit" value="<?php echo $vet_card['num_chip']; ?>"/>
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="degel_preparat">Препарат дегельминтизации</label>
            <input class="field width-add" type="text" name="degel_preparat_edit" id="degel_preparat_edit" value="<?php echo $vet_degilmintizaciya['preparat']; ?>"/>
          </div>
        </div> 
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="vac_preparat">Препарат для вакцинации</label>
            <input class="field width-add" type="text" name="vac_preparat_edit" id="vac_preparat_edit" value="<?php echo $vet_vaccine['preparat']; ?>"/>
          </div>
        </div>
        
        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="date_operation">№ вольера животного</label>
            <input class="field width-add" type="text" name="num_aviary_edit" id="num_aviary_edit" value="<? echo $card['num_aviary']; ?>"/>  
          </div>
        </div>

        <div class="col-12 col-sm-6">
          <div class="form-group">
            <label for="gender">Пол животного</label>
            <select class="field width-add" id="gender_vet_edit" name="gender_vet_edit">
              <?php
                $sql = "SELECT * FROM `animal_gender`;";
                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                echo '<option value='.$card["id_gender"].' selected readonly hidden>'.$animal_gender["name_gender"].'</option>';

                while($row = mysqli_fetch_array($res))
                {
                  echo '<option value="'. $row['id'] .'">'. $row['name_gender'] .'</option>';
                }
              ?>
            </select>
          </div>
        </div> 

        <div class="col-12 col-sm-12">
          <div class="form-group">
            <label for="comment">Комментарий</label>
            <textarea class="field" name="comment_vet_edit" id="comment_vet_edit" autocomplete="off"><? echo $vet_card['comment'] ?></textarea>
          </div>
        </div>
        
        <input class="btn button-auth field-submit field-width" type="submit" value="Сохранить" autofocus />
        <!--<button id="update_vet_card" class="btn field-submit width-submit" data="<? echo $_GET['application']; ?>">Сохранить</button>-->
        
    </form>
  </div>
<? } ?>
<!--/Редактировать карточку ветеринара-->

<!--Редактировать фото ветеринара-->
  <div id="hide-form-popup-edit-foto-vet" class="hide-form-popup-edit-foto-vet hideform fancy-animate">
    <form id="popup-edit-foto-vet">
        <h3>Редактирование фото</h3>
        
        <div class="col-12 col-sm-12">
          <div class="form-group">
            <div name="mediafiles-vet">
        	<?php
  		 		$query = "SELECT * FROM `upload_files` WHERE `id_application` = '".$_GET['application']."' AND `id_user_group`=3 AND `type`='img' ORDER BY id";
              	$res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
                while ($res = mysqli_fetch_array($res_query)) {
          			$img = $res['path_thumb'].$res['name_thumb'];
          			echo
          				"<div class='media-item'>
          					<img src='{$img}' width='100px'>
          					<a class='del_file' href='#' onclick='event.preventDefault()' data='{$res['id']}'></a>
          				</div>";
                }
                echo
      				'<input type="file" class="form-control" name="media-on-edit-vet" id="media-on-edit-vet" accept="image/jpeg, image/png, image/jpg" multiple>';
            ?>
            </div>
          </div>
        </div>
        
        <button id="update-foto-vet" type="button" class="btn field-submit width-submit">Сохранить</button>
        
    </form>
  </div>
<!--/Редактировать фото ветеринара-->

<!--Редактировать видео ветеринара-->
  <div id="hide-form-popup-edit-video-vet" class="hide-form-popup-edit-video-vet hideform fancy-animate">
    <form id="popup-edit-video-vet">
        <h3>Редактирование видео</h3>
        
        <div class="col-12 col-sm-12">
          <div class="form-group">
            <div name="mediafiles-vet">
        	<?php
              	$query = "SELECT * FROM `upload_files` WHERE `id_application` = '".$_GET['application']."' AND `id_user_group`=3 AND `type`='vid' ORDER BY id";
              	$res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
                while ($res = mysqli_fetch_array($res_query)) {
              		
              		$vid = $res['path_file'].$res['name_file'];
          			echo 
          				"<div class='media-item'>
          					<video src='{$vid}' width='170px' muted></video>
          					<a class='del_file' href='#' onclick='event.preventDefault()' data='{$res['id']}'>
          					</a>
          				</div>";
                }
                echo
      				'<input type="file" class="form-control" name="media-on-edit-vet" id="media-on-edit-vet" accept="video/mp4, video/avi, video/3gp, video/mov" multiple>';
            ?>
            </div>
          </div>
        </div>
        
        <button id="update-video-vet" type="button" class="btn field-submit width-submit">Сохранить</button>
        
    </form>
  </div>
<!--/Редактировать видео ветеринара-->

<!--Редактировать фото выпуска-->
  <div id="hide-form-popup-edit-foto-vipusk" class="hide-form-popup-edit-foto-vipusk hideform fancy-animate">
    <form id="popup-edit-foto-vipusk">
        <h3>Редактирование фото</h3>
        
        <div class="col-12 col-sm-12">
          <div class="form-group">
            <div name="mediafiles-vipusk">
        	<?php
  		 		$query = "SELECT * FROM `upload_files` WHERE `id_application` = '".$_GET['application']."' AND `type`='img' AND vipusk = '1' ORDER BY id";
              	$res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
                while ($res = mysqli_fetch_array($res_query)) {
          			$img = $res['path_thumb'].$res['name_thumb'];
          			echo
          				"<div class='media-item'>
          					<img src='{$img}' width='100px'>
          					<a class='del_file' href='#' onclick='event.preventDefault()' data='{$res['id']}'></a>
          				</div>";
                }
                echo
      				'<input type="file" class="form-control" name="media-on-edit-vipusk" id="media-on-edit-vipusk" accept="image/jpeg, image/png, image/jpg" multiple>';
            ?>
            </div>
          </div>
        </div>
        <button id="update-foto-vipusk" type="button" class="btn field-submit width-submit">Сохранить</button>
    </form>
    
  </div>
<!--/Редактировать фото выпуска-->

<!--Редактировать видео выпуска-->
  <div id="hide-form-popup-edit-video-vipusk" class="hide-form-popup-edit-video-vipusk hideform fancy-animate">
    <form id="popup-edit-video-vipusk">
        <h3>Редактирование видео</h3>
        
        <div class="col-12 col-sm-12">
          <div class="form-group">
            <div name="mediafiles-vipusk">
        	<?php
              	$query = "SELECT * FROM `upload_files` WHERE `id_application` = '".$_GET['application']."' AND `type`='vid' AND vipusk = '1' ORDER BY id";
              	$res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
                while ($res = mysqli_fetch_array($res_query)) {
              		
              		$vid = $res['path_file'].$res['name_file'];
          			echo 
          				"<div class='media-item'>
          					<video src='{$vid}' width='170px' muted></video>
          					<a class='del_file' href='#' onclick='event.preventDefault()' data='{$res['id']}'>
          					</a>
          				</div>";
                }
                echo
      				'<input type="file" class="form-control" name="media-on-edit-vipusk" id="media-on-edit-vipusk" accept="video/mp4, video/avi, video/3gp, video/mov" multiple>';
            ?>
            </div>
          </div>
        </div>
        
        <button id="update-video-vipusk" type="button" class="btn field-submit width-submit">Сохранить</button>
        
    </form>
  </div>
<!--/Редактировать видео выпуска-->

<!--Подтверждение изменения статуса-->
  <div id="hide-form-popup-status" class="hide-form-popup-status hideform fancy-animate">
    <?
      $vet_date = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_card WHERE id_application=".$_GET['application']));
      $start_vet_date = strtotime($vet_date['data']);
    ?> 
    <form id="popup-status" method="post" action="/php/edit_status.php?edit_status=<?php echo ($_GET['application']) ?>&start_vet_date=<? echo $start_vet_date ?>" enctype="multipart/form-data">           
    <h3>Изменить статус на "На восстановлении"</h3>
      <h4>Датой начала восстановления будет указана дата проведения мероприятий</h4>
      <!--<p>После изменения статуса записи на "На восстановлении" редактирование карточки будет недоступно.</p>-->
      <input class="btn button-auth field-submit field-width" type="submit" value="Поместить на восстановление" autofocus />
    </form>
  </div>
<!--/Подтверждение изменения статуса-->

<!--Животное погибло-->
  <div id="hide-form-popup-die" class="hide-form-popup-die hideform fancy-animate">
    <form id="popup-die" method="post" action="/php/edit_status.php?edit_status_die=<?php echo ($_GET['application']) ?>" enctype="multipart/form-data">
      <h3>Подтвердите изменение статуса</h3>
      <p>После изменения статуса записи на "Животное погибло" редактирование карточки будет недоступно.</p>
      <input class="btn button-auth field-submit field-width" type="submit" value="Подтвердить" autofocus />
    </form>
  </div>
<!--/Животное погибло-->

<!--Изменить вольер-->
  <div id="hide-form-popup-num_aviary" class="hide-form-popup-num_aviary hideform fancy-animate">
    <form id="popup-num_aviary" method="post" action="/php/edit_status.php?num_aviary=<?php echo ($_GET['application']) ?>" enctype="multipart/form-data">
      <h3>Изменить номер вольера</h3>
      <div class="form-group">
            <input class="field width-add" type="text" name="num_aviary" id="num_aviary" />
      </div>
      <input class="btn button-auth field-submit field-width" type="submit" value="Подтвердить" autofocus />
    </form>
  </div>
<!--/Изменить вольер-->

<!--Перезаселить-->
  <div id="hide-form-popup-perezaseleno" class="hide-form-popup-perezaseleno hideform fancy-animate">
    <form id="popup-perezaseleno" method="post" action="/php/edit_status.php?perezaseleno=<?php echo ($_GET['application']) ?>" enctype="multipart/form-data">
      <h3>Перезаселить животное</h3>
      <p>Статус животного будет изменен на "На карантине" и возобновлен отсчет 10 дней.</p>
      <p>Номер вольера</p>
      <div class="form-group">
            <input class="field width-add" type="text" name="num_aviary" id="num_aviary" />
      </div>
      <input class="btn button-auth field-submit field-width" type="submit" value="Подтвердить" autofocus />
    </form>
  </div>
<!--/Перезаселить-->

<!--Передано владельцу-->
  <div id="hide-form-popup-vladelets" class="hide-form-popup-vladelets hideform fancy-animate">
    <form id="popup-vladelets" method="post" action="/php/edit_status.php?edit_status_vladelets=<?php echo ($_GET['application']) ?>" enctype="multipart/form-data">
      <h3>Данные владельца</h3>
      <div class="form-group">
            <input class="field width-add field-date" type="datetime-local" name="date" autocomplete="on" value="<?php echo $today_day_time; ?>" required />  
      </div>
      <div class="form-group">
            <input class="field width-add" type="text" name="sourname" placeholder="Фамилия" />  
      </div>
      <div class="form-group">
            <input class="field width-add" type="text" name="name" placeholder="Имя" />  
      </div>
      <div class="form-group">
            <input class="field width-add" type="text" name="patronymic" placeholder="Отчество" />  
      </div>
      <div class="form-group">
            <input class="field width-add" type="tel" name="phone" id="phone" placeholder="Телефон" />  
      </div>
      <div class="form-group">
            <input class="field width-add" type="number" name="seriya" placeholder="Серия паспорта" />  
      </div>
      <div class="form-group">
            <input class="field width-add" type="number" name="num" placeholder="Номер паспорта" />  
      </div>
      <div class="form-group">
            <input class="field width-add" type="text" name="vidan" placeholder="Выдан" />  
      </div>
      <input class="btn button-auth field-submit field-width" type="submit" value="Подтвердить" autofocus />
    </form>
  </div>
<!--/Передано владельцу-->

<!--Передано в приют-->
  <div id="hide-form-popup-shelter" class="hide-form-popup-vladelets hideform fancy-animate">
    <form id="popup-shelter" method="post" action="/php/edit_status.php?edit_status_shelter=<?php echo ($_GET['application']) ?>" enctype="multipart/form-data">
      <h3>Данные приюта</h3>
      <div class="form-group">
            <input class="field width-add field-date" type="datetime-local" name="date" autocomplete="on" value="<?php echo $today_day_time; ?>" required />  
      </div>
      
      <!--<div class="form-group">-->
      <!--      <input class="field width-add" type="text" name="name" placeholder="Название приюта" />  -->
      <!--</div>-->
      
      <div class="form-group">
        <select class="field width-add" id="shelter" name="shelter">
          <?php
            $sql = "SELECT * FROM `animal_shelters` WHERE actual = 1;";
            $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
            echo '<option value="" selected readonly hidden>Выберите приют</option>';

            while($row = mysqli_fetch_array($res))
            {
              echo '<option value="'. $row['id'] .'">'. $row['name_shelter'] .'</option>';
            }
          ?>
        </select>
      </div>
      
      <div class="form-group">
            <input class="field width-add" type="text" name="num" placeholder="Номер вольера" />  
      </div>
      <div class="form-group">
            <input class="field width-add" type="text" name="adress" placeholder="Адрес" />  
      </div>
      <div class="form-group">
            <input class="field width-add" type="tel" name="phone" id="phone" placeholder="Телефон" />  
      </div>
      <input class="btn button-auth field-submit field-width" type="submit" value="Подтвердить" autofocus />
    </form>
  </div>
<!--/Передано владельцу-->

<!--Животное умерщвлено-->
  <div id="hide-form-popup-die1" class="hide-form-popup-die1 hideform fancy-animate">
    <form id="popup-die1" method="post" action="/php/edit_status.php?edit_status_die1=<?php echo ($_GET['application']) ?>" enctype="multipart/form-data">
      <h3>Подтвердите изменение статуса</h3>
      <p>После изменения статуса записи на "Умерщвлено" редактирование карточки будет недоступно.</p>
      <input class="btn button-auth field-submit field-width" type="submit" value="Подтвердить" autofocus />
    </form>
  </div>
<!--/Животное умерщвлено-->

<!--Выбрать ветеринара-->
  <div id="hide-form-popup-select-vet" class="hide-form-popup-select-vet hideform fancy-animate">
    <form id="popup-select-vet" method="post" action="../libs/phpword/generate_word_zakluchenie2.php?application=<? echo ($_GET['application']) ?>" enctype="multipart/form-data">
        <h3>Выберите ветеринара для первичного заключения</h3>
        <div class="col-12">
          <div class="form-group">
            <select class="field width-add" id="vet-select" name="vet-select" required>
              <?php
                $sql_k = "SELECT * FROM `users` WHERE `show` = 1 AND `podtverzhdeno` = 1 AND `id_users_group` = 3;";
                $res_k = mysqli_query($SERVER, $sql_k) or die("Ошибка " . mysqli_error($SERVER));
                echo '<option value="" selected readonly hidden>Выберите ветеринара</option>';
                while($row_k = mysqli_fetch_array($res_k))
                {
                    echo '<option value="'. $row_k['id'] .'">'.$row_k['sourname']." ".$row_k['name'].'</option>';
                }
              ?>
            </select>
          </div>
        </div> 
        <input class="btn button-auth field-submit field-width" type="submit" value="Сформировать" autofocus />
    </form>
  </div>
<!--/Выбрать ветеринара-->

<?
  //проверка статуса заявки и вывод соответствующих кнопок
  $check_status = mysqli_fetch_array(mysqli_query($SERVER, "SELECT id_status FROM application WHERE id=".$card['id']));
?>

<section class="content" id="application" data="<? echo $_GET['application'] ?>">
	<div class="container">
		<div class="row block-main">
			<div class="col-md-12">
				<h1>Учетное дело животного без владельца 
          <? if($_SESSION['id_users_group'] == "1") echo("№".$card['id'])?></h1>
				<div class="content-title"></div>
			</div>
			<div class="col-12 col-md-6">
			  <? if ($card['id_shelter'] == "3" || $card['id_shelter'] == "2") { ?>
        <table class="table table-bordered table-hover table-striped tab-card tab-card-left">
          <tbody>
            <tr>
                <th>Статус:</th>
                <td><span class="edit_element"><?echo $status['status_name']?></span></td>
            </tr>
            <tr>
                <th>Дата отлова: </th>
                <td><span class="edit_element"><?echo date('d.m.Y H:i', $card['data']);?></span></td>
            </tr>
            <tr>
                <th>Дата начала карантина:</th>
                <td><? if ($card['date_start_karantin'] == 0) echo ""; else echo date('d.m.Y H:i', $card['date_start_karantin']); ?>
            </tr>
            <tr>
                <th>Дата окончания карантина:</th>
                <td><? if ($card['date_start_karantin'] == 0) echo ""; else echo date("d.m.Y H:i", ($card['date_start_karantin']+777600)) ?></td>
            </tr>
            <tr>
                <th>Дата начала восстановления:</th>
                <td><? if ($card['date_start_vosstanovlenie'] == 0) echo ""; else echo date('d.m.Y H:i', $card['date_start_vosstanovlenie']); ?>
            </tr>
            <tr>
                <th>Дата окончания восстановления:</th>
                <td><? if ($card['date_start_vosstanovlenie'] == 0) echo ""; else echo date("d.m.Y H:i", ($card['date_start_vosstanovlenie']+777600)) ?></td>
            </tr>
            <tr>
                <th>Регион отлова:</th>
                <td><span class="edit_element"><?echo $region["name_region"] ?></span></td>
            </tr>
            <tr>
                <th>Населенный пункт отлова:</th>
                <td><span class="edit_element"><?echo $card['locality']?></span></td>
            </tr>
            <tr>
                <th>Улица: </th>
                <td><span class="edit_element"><?echo $card['street']?></span></td>
            </tr>
            
          </tbody>
        </table>
        <table class="table table-bordered table-hover table-striped tab-card tab-card-right">
          <tbody>
            <tr>
                <th>Контракт:</th>
                <td><span class="edit_element"><?echo $contract['name_contract']?></span></td>
            </tr>
            <tr>
              <th>Кинолог: </th>
              <td><span class='edit_element'><?echo $user['sourname']. " " .$user['name'] ?></span></td>
            </tr>
            <tr>
              <th>Окрас: </th>
              <td><span class="edit_element"><?echo $animal_color["name_color"]?></span></td>
            </tr>
            <tr>
              <th>Порода: </th>
              <td><span class="edit_element"><?echo $card['breed']?></span></td>
            </tr>
            <tr>
              <th>Вид животного: </th>
              <td><span class="edit_element"><?echo $animal_kind['name_kind']?></span></td>
            </tr>
            <tr>
              <th>Пол: </th>
              <td><span class="edit_element"><?echo $animal_gender['name_gender']?></span></td>
            </tr>
            <tr>
              <th>Возраст: </th>
              <td><span class="edit_element"><?echo $card['age']?></span></td>
            </tr>
            <tr>
              <th>Высота в холке: </th>
              <td><span class="edit_element"><?echo $card['height']?></span></td>
            </tr>
            <tr>
              <th>Вес: </th>
              <td><span class="edit_element"><?echo $card['weight']?></span></td>
            </tr>
            <tr>
                <th>Комментарий: </th>
                <td><span class="edit_element"><?echo $card['comment']?></span></td>
            </tr>
          </tbody>
        </table>
				<? } else { ?>
        <table class="table table-bordered table-hover table-striped tab-card tab-card">
          <tbody>
            <tr>
              <th>Статус:</th>
              <td><span class="edit_element"><?echo $status['status_name']?></span></td>
            </tr>	   
            <tr>
              <th>Контракт:</th>
              <td><span class="edit_element"><?echo $contract['name_contract']?></span></td>
            </tr>
            <tr>
              <th>Дата отлова: </th>
              <td><span class="edit_element"><?echo date('d.m.Y H:i', $card['data']);?></span></td>
            </tr>
            <tr>
              <th>Дата начала карантина:</th>
              <td><? if ($card['date_start_karantin'] == 0) echo ""; else echo date('d.m.Y H:i', $card['date_start_karantin']);?>
            </tr>
            <tr>
              <th>Дата окончания карантина:</th>
              <td><? if ($card['date_start_karantin'] == 0) echo ""; else echo date("d.m.Y H:i", ($card['date_start_karantin']+777600)) ?></td>
            </tr>
            <tr>
                <th>Дата начала восстановления:</th>
                <td><? if ($card['date_start_vosstanovlenie'] == 0) echo ""; else echo date('d.m.Y H:i', $card['date_start_vosstanovlenie']); ?>
            </tr>
            <tr>
                <th>Дата окончания восстановления:</th>
                <td><? if ($card['date_start_vosstanovlenie'] == 0) echo ""; else echo date("d.m.Y H:i", ($card['date_start_vosstanovlenie']+777600)) ?></td>
            </tr>
            <tr>
              <th>Регион отлова:</th>
              <td><span class="edit_element"><?echo $region["name_region"] ?></span></td>
            </tr>
            <tr>
              <th>Населенный пункт отлова:</th>
              <td><span class="edit_element"><?echo $card['locality']?></span></td>
            </tr>
            <tr>
              <th>Улица: </th>
              <td><span class="edit_element"><?echo $card['street']?></span></td>
            </tr>
            <tr>
              <th>Приют: </th>
              <td><span class="edit_element"><?echo $animal_shelter['name_shelter']?></span></td>
            </tr>
            <tr>
              <th>Дата заселения в вольер: </th>
              <td>
                  <span class="edit_element"> <?
                    $Cdate_z = date("d.m.Y H:i", strtotime($card_date_z['date_zaseleno']));
                    // $Cdate_z = $card_date_z['date_zaseleno'];
                      if($Cdate_z == "00.00.0000 00:00:00") $Cdate_z = "";
                            echo $Cdate_z ?>
                  </span>
              </td>
            </tr>
            <tr>
              <th>№ вольера: </th>
              <td><span class="edit_element"><?echo $card['num_aviary']?></span></td>
            </tr>
            <tr>
              <th>Дата выбытия из вольера: </th>
              <td>
                  <span class="edit_element"> <?
                    $Cdate_v = $card_date_z['date_vipusk'];
                      if($Cdate_v == "00.00.0000 00:00:00") $Cdate_v = "";
                            echo $Cdate_v ?>
                  </span>
              </td>
            </tr>
            <tr>
              <th>Окрас: </th>
              <td><span class="edit_element"><?echo $animal_color["name_color"]?></span></td>
            </tr>
            <tr>
              <th>Порода: </th>
              <td><span class="edit_element"><?echo $card['breed']?></span></td>
            </tr>
            <tr>
              <th>Вид животного: </th>
              <td><span class="edit_element"><?echo $animal_kind['name_kind']?></span></td>
            </tr>
            <tr>
              <th>Пол: </th>
              <td><span class="edit_element"><?echo $animal_gender['name_gender']?></span></td>
            </tr>
            <tr>
              <th>Возраст: </th>
              <td><span class="edit_element"><?echo $card['age']?></span></td>
            </tr>
            <tr>
              <th>Высота в холке: </th>
              <td><span class="edit_element"><?echo $card['height']?></span></td>
            </tr>
            <tr>
              <th>Вес: </th>
              <td><span class="edit_element"><?echo $card['weight']?></span></td>
            </tr>
            <tr>
              <th>Кинолог: </th>
              <td><span class='edit_element'><?echo $user['sourname']. " " .$user['name'] ?></span></td>
            </tr>
            <tr>
              <th>Комментарий: </th>
              <td><span class="edit_element"><?echo $card['comment']?></span></td>
            </tr>
              <? if ($check_status['id_status'] == 1 && $_SESSION['id_users_group'] == "4") {?>
                <tr>
                  <td colspan="5" class="edit-app"><? 
                    if ($check_status['id_status'] == 1 || $check_status['id_status'] ==2 ){
                        echo "<a href='?application={$_GET['application']}&edit-application={$_GET['application']}'>
                        <span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> Редактировать
                      </a>";
                  }?>
                  </td>
                </tr> 
              <? } ?>
              <? if ($_SESSION['id_users_group'] == "1") {?>
                <tr>
                  <td colspan="5" class="edit-app">
                    <?php 
                      echo "<a href='?application={$_GET['application']}&edit-application={$_GET['application']}'>
                        <span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> Редактировать
                      </a>";
                  ?>
                  </td>
                </tr>
              <? } ?>
          </tbody>
        </table>
				<? } ?>
				<? if ($card['id_shelter'] == "3" || $card['id_shelter'] == "2") { ?>
    				<div class="info-shelter">
    				  <div class="form-group">
    				    <? if ($card['id_shelter'] == "3") { ?>
                <div class="avairs-3">
                  <table class="avairs-tab">
                    <tbody>
                      <tr>
                        <td class="small">18</td>
                        <td class="no-aviary"></td>
                        <td class="small">19</td>
                        <td rowspan="6" class="big">24</td>
                      </tr>
                      <tr>
                        <td class="small">17</td>
                        <td class="no-aviary"></td>
                        <td class="small">20</td>
                      </tr>
                      <tr>
                        <td class="small">16</td>
                        <td class="no-aviary"></td>
                        <td class="small">21</td>
                      </tr>
                      <tr>
                        <td class="small">15</td>
                        <td class="no-aviary"></td>
                        <td class="no-aviary"></td>
                      </tr>
                      <tr>
                        <td class="small">14</td>
                        <td class="no-aviary"></td>
                        <td class="small">22</td>
                      </tr>
                      <tr>
                        <td class="small">13</td>
                        <td class="no-aviary"></td>
                        <td class="small">23</td>
                      </tr>
                      <tr>
                        <td class="small">12</td>
                        <td class="no-aviary"></td>
                      </tr>
                      <tr>
                        <td class="small">11</td>
                        <td class="no-aviary"></td>
                      </tr>
                      <tr>
                        <td class="no-aviary"></td>
                      </tr>
                      <tr>
                        <td class="no-aviary"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table class="avairs-tab avairs-tab1">
                    <tbody>
                      <tr>
                        <td class="house">Вет.<br>блок</td>
                        <td class="no-aviary vertical"></td>
                        <td class="small vertical">1</td>
                        <td class="small vertical">2</td>
                        <td class="small vertical">3</td>
                        <td class="small vertical">4</td>
                        <td class="small vertical">5</td>
                        <td class="small vertical">6</td>
                        <td class="small vertical">7</td>
                        <td class="small vertical">8</td>
                        <td class="small vertical">9</td>
                        <td class="small vertical">10</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <? } else if ($card['id_shelter'] == "2") { ?>
                <div class="avairs-2">
                  <table class="avairs-tab">
                    <tbody>
                      <tr>
                        <td class="small">1</td>
                        <td class="small">2</td>
                        <td class="small">3</td>
                        <td class="small">4</td>
                        <td class="small">5</td>
                        <td class="small">6</td>
                        <td class="small">7</td>
                        <td class="small">8</td>
                        <td class="small">9</td>
                        <td class="small">10</td>
                        <td class="small">11</td>
                        <td class="small">12</td>
                      </tr>
                      <tr>
                        <td class="small">13</td>
                        <td class="small">14</td>
                        <td class="small">15</td>
                        <td class="small">16</td>
                        <td class="small">17</td>
                        <td class="small">18</td>
                        <td class="small">19</td>
                        <td class="small">20</td>
                        <td class="small">21</td>
                        <td class="small">22</td>
                        <td class="small">23</td>
                        <td class="small">24</td>
                      </tr>
                      <tr>
                        <td class="no-aviary"></td>
                      </tr>
                      <tr>
                        <td class="small">25</td>
                        <td class="small">26</td>
                        <td class="small">27</td>
                        <td class="small">28</td>
                        <td class="small">29</td>
                        <td class="small">30</td>
                        <td class="small">31</td>
                        <td class="small">32</td>
                        <td class="small">33</td>
                        <td class="small">34</td>
                        <td class="small">35</td>
                        <td class="small">36</td>
                      </tr>
                      <tr>
                        <td class="small">37</td>
                        <td class="small">38</td>
                        <td class="small">39</td>
                        <td class="small">40</td>
                        <td class="small">41</td>
                        <td class="small">42</td>
                        <td class="small">43</td>
                        <td class="small">44</td>
                        <td class="small">45</td>
                        <td class="small">46</td>
                        <td class="small">47</td>
                        <td class="small">48</td>
                      </tr>
                      <tr>
                        <td class="no-aviary"></td>
                      </tr>
                      <tr>
                        <td class="small">49</td>
                        <td class="small">50</td>
                        <td class="small">51</td>
                        <td class="small">52</td>
                        <td class="small">53</td>
                        <td class="small">54</td>
                        <td class="small">55</td>
                        <td class="small">56</td>
                        <td class="small">57</td>
                        <td class="small">58</td>
                        <td class="small">59</td>
                        <td class="small">60</td>
                      </tr>
                      <tr>
                        <td class="small">61</td>
                        <td class="small">62</td>
                        <td class="small">63</td>
                        <td class="small">64</td>
                        <td class="small">65</td>
                        <td class="small">66</td>
                        <td class="small">67</td>
                        <td class="small">68</td>
                        <td class="small">69</td>
                        <td class="small">70</td>
                        <td class="small">71</td>
                        <td class="small">72</td>
                      </tr>
                      <tr>
                        <td class="no-aviary"></td>
                      </tr>
                      <tr>
                        <td class="small">73</td>
                        <td class="small">74</td>
                        <td class="small">75</td>
                        <td class="small">76</td>
                        <td class="small">77</td>
                        <td class="small">78</td>
                        <td class="small">79</td>
                        <td class="small">80</td>
                        <td class="small">81</td>
                        <td class="small">82</td>
                      </tr>
                      <tr>
                        <td class="small">83</td>
                        <td class="small">84</td>
                        <td class="small">85</td>
                        <td class="small">86</td>
                        <td class="small">87</td>
                        <td class="small">88</td>
                        <td class="small">89</td>
                        <td class="small">90</td>
                        <td class="small">91</td>
                        <td class="small">92</td>
                      </tr>
                      <tr>
                        <td class="no-aviary"></td>
                      </tr>
                      <tr>
                        <td class="small">93</td>
                        <td class="small">94</td>
                        <td class="small">95</td>
                        <td class="small">96</td>
                        <td class="small">97</td>
                        <td class="small">98</td>
                        <td class="small">99</td>
                        <td class="small">100</td>
                        <td class="small">101</td>
                        <td class="small">102</td>
                      </tr>
                      <tr>
                        <td class="small">103</td>
                        <td class="small">104</td>
                        <td class="small">105</td>
                        <td class="small">106</td>
                        <td class="small">107</td>
                        <td class="small">108</td>
                        <td class="small">109</td>
                        <td class="small">110</td>
                        <td class="small">111</td>
                        <td class="small">112</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <? } ?>
              </div>
                        
              <table class="table table-bordered table-hover table-striped tab-card">
                  <tbody>
                    <tr>
                        <th>Приют: </th>
                        <td><span class="edit_element"><?echo $animal_shelter['name_shelter']?></span></td>
                    </tr>
                    <tr>
                        <th>Дата заселения в вольер: </th>
                        <td>
                            <span class="edit_element"> <?
                              $Cdate_z = date("d.m.Y H:i", strtotime($card_date_z['date_zaseleno']));
                                if($Cdate_z == "00.00.0000 00:00:00") $Cdate_z = "";
                                      echo $Cdate_z ?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>№ вольера: </th>
                        <td><span class="edit_element"><?echo $card['num_aviary']?></span></td>
                    </tr>
                    <tr>
                        <th>Количество животных в вольере: </th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Дата выбытия из вольера: </th>
                        <td>
                            <span class="edit_element"> <?
                              $Cdate_v = $card_date_z['date_vipusk'];
                                if($Cdate_v == "00.00.0000 00:00:00") $Cdate_v = "";
                                      echo $Cdate_v ?>
                            </span>
                        </td>
                    </tr>
                  </tbody>
              </table>
            </div>
            <table class="table table-bordered table-hover table-striped tab-card">
            <? if ($check_status['id_status'] == 1 && $_SESSION['id_users_group'] == "4") {?>
              <tr>
                <td colspan="5" class="edit-app"><? 
                  if ($check_status['id_status'] == 1 || $check_status['id_status'] ==2 ){
                      echo "<a href='?application={$_GET['application']}&edit-application={$_GET['application']}'>
                      <span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> Редактировать
                    </a>";
                }?>
                </td>
              </tr>
            <? } ?>
            <? if ($_SESSION['id_users_group'] == "1") {?>
              <tr>
                <td colspan="5" class="edit-app">
                  <?php 
                    echo "<a href='?application={$_GET['application']}&edit-application={$_GET['application']}'>
                      <span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> Редактировать
                    </a>";
                ?>
                </td>
              </tr>
            <? } ?>
    				</table>
    			<? } ?>
			</div>
			<div class="col-12 col-md-6 media-block" style="margin: 0 auto; text-align: center;">
        <? $res_image = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM upload_files WHERE id_application = '".$_GET['application']."' AND type = 'img' AND `id_user_group`=4"));
		      if(!empty($res_image)){ ?>
				<!--Пошёл слайдер, будь он неладен-->
				<div id="carousel" class="carousel slide" data-ride="carousel" data-interval="1000000">
			  		<div class="carousel-inner">
				    	<? 
				    		$qIMAGES = mysqli_query($SERVER, "SELECT * FROM `upload_files` WHERE id_application = '".$_GET['application']."' AND type = 'img' AND `id_user_group`=4");
		     				$active = true;
			     			while ($IMAGES = mysqli_fetch_array($qIMAGES)) {
			     				$img = $IMAGES['path_file'].$IMAGES['name_file'];
			     				$slide = $active ? "active" : "";
			     				echo '<div class="item '.$slide.' center-block">
    					       			<img src="'.$img.'" alt="Изображение животного" class="center-block" style="z-index: 3">
    						     	</div>';
						     	
					     		$active = false;
			     			}
			     		?>
				  	</div>
				  	<!-- Элементы управления -->
				  	<a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
				    	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    	<span class="sr-only">Предыдущий</span>
				  	</a>
				  	<a class="right carousel-control" href="#carousel" role="button" data-slide="next">
				    	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    	<span class="sr-only">Следующий</span>
				  	</a>
				</div><? 
				} 
				if ( ($check_status['id_status'] == 1 && $_SESSION['id_users_group'] == "4") ||  $_SESSION['id_users_group'] == "1")
				    echo ("<a href='?application={$_GET['application']}&edit-foto={$_GET['application']}'>
    			    		<span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> Редактировать фото
    			    	   </a>");
				?>
				<div class="vid block-vid"><?
				
          $rs_vid = mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM upload_files WHERE id_application='".$_GET['application']."' AND type = 'vid' AND `id_user_group`=4");
          $result_vid = mysqli_fetch_array($rs_vid);
                  
          $rs_img = mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM upload_files WHERE id_application='".$_GET['application']."' AND type = 'img' AND `id_user_group`=4");
          $result_img = mysqli_fetch_array($rs_img);

          if ( ($result_vid[0] == 1 && $result_img[0] == 0) || $_SESSION['width'] == 1) 
            $style_vid = "style= 'width: 100%;'";
          else
            $style_vid = "style= 'width: 50%;'";
        
					$qVIDEOS = mysqli_query($SERVER, "SELECT * FROM `upload_files` WHERE id_application = '".$_GET['application']."' AND type = 'vid' AND `id_user_group`=4");
	        while ($VIDEOS = mysqli_fetch_array($qVIDEOS)) {
            $vid = $VIDEOS['path_file'].$VIDEOS['name_file'];
            if ($VIDEOS['name_file'] != "") {
              echo "<div class='vid-item' {$style_vid}>
                      <div class='vid-wrap'>
                                  <video id='vid-screen' src='{$vid}' controls width='500vw'></video>	
                              </div>
                            </div>";
            }  
	     		}?>
				</div><?
				if ( ($check_status['id_status'] == 1 && $_SESSION['id_users_group'] == "4") ||  $_SESSION['id_users_group'] == "1")
				    echo ("<a href='?application={$_GET['application']}&edit-video={$_GET['application']}'>
    					<span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> Редактировать видео
    				</a>");?>
			</div>
			<? if ( $_SESSION['id_users_group'] == "4") { // Кинолог ?>
    			<!-- <div class="col-md-12">
    				<div class="button-card">
    					<?
    	      	// 		$check_status = mysqli_fetch_array(mysqli_query($SERVER, "SELECT id_status FROM application WHERE id=".$card['id']));					
    					// if ($check_status['id_status'] == 1) {
    					// 	echo '<input id="to_karantin" data="'.$card['id'].'" name="submit" type="submit" class="btn button-auth field-submit1 btn-card" value="Поместить на карантин"/>';
    					// }
    					
    					?>
    				</div>
    			</div> -->
    		<?php } ?>
  		</div>
  		
    	<?php 
        $check_vet_card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT id FROM vet_card WHERE id_application=".$card['id']));
        if ( (($_SESSION['id_users_group'] == "4" || $_SESSION['id_users_group'] == "2") && !empty($check_vet_card)) || $_SESSION['id_users_group'] == "1"  || $_SESSION['id_users_group'] == "3") {
	    ?>
        <div class="row block-content-card"> 
	        <div class="col-md-12 tab-vet-edit">
    		    <p style="text-align:left;">Данные о проведенных в отношении животного мероприятиях по профилактике и лечению болезней:<br>
    		    Клинический осмотр проведен в день поступления животного в приют.</p>
    		    <?
                    if ($_SESSION['id_users_group'] == "3" || $_SESSION['id_users_group'] == "1"){
                        if ( empty($check_vet_card) ) {
                            echo '<a href="?application='.$_GET['application'].'&add-card-vet='.$_GET['application'].'">
                        		    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Заполнить таблицу
                        	    </a>';
                        }
                        else {
                            echo '<a href="?application='.$_GET['application'].'&edit-card-vet='.$_GET['application'].'">
                        		    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Редактировать таблицу
                        	    </a>';
                        }
                    } 
    			
    			$query = "SELECT 
                        vet_card.id AS id,
                        DATE_FORMAT(vet_card.data,'%d.%m.%Y %H:%i') AS data,
                        vet_card.num_birka AS num_birka,
                        vet_card.num_chip AS num_chip,
                        vet_card.comment AS comment,
                        vet_degilmintizaciya.preparat AS vet_degilmintizaciya_preparat, 
                        DATE_FORMAT(vet_degilmintizaciya.data,'%d.%m.%Y') AS vet_degilmintizaciya_data, 
                        vet_degilmintizaciya.id_veterinar AS vet_degilmintizaciya_id_veterinar, 
                        vet_vaccine.preparat AS vet_vaccine_preparat, 
                        DATE_FORMAT(vet_vaccine.data,'%d.%m.%Y') AS vet_vaccine_data, 
                        vet_vaccine.id_veterinar AS vet_vaccine_id_veterinar, 
                        DATE_FORMAT(vet_operation.data,'%d.%m.%Y') AS vet_operation_data, 
                        vet_operation.id_veterinar AS vet_operation_id_veterinar, 
                        users.sourname AS sourname, 
                        users.name AS name, 
                        users.patronymic AS patronymic
                        FROM vet_card 
                        LEFT JOIN vet_degilmintizaciya ON vet_card.id_degelmintizaciya = vet_degilmintizaciya.id
                        LEFT JOIN vet_vaccine ON vet_card.id_vaccine = vet_vaccine.id
                        LEFT JOIN vet_operation ON vet_card.id_operation = vet_operation.id
                        LEFT JOIN users ON vet_card.id_veterinar = users.id
                        WHERE vet_card.id_application = ".$_GET['application']." ORDER BY vet_card.id";
                        
    			?>
    			<table class="table table-bordered table-hover table-striped mobile-vet" style="background: #f7fbff;">
				  	<tbody><?
				  	
				  		$res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
                        while ($res = mysqli_fetch_array($res_query)) {
                            
                            $name = $res['name'];
                            $name1 = mb_substr($name, 0, 1);
                            $patronymic = $res['patronymic'];
                            $patronymic1 = mb_substr($patronymic, 0, 1);
                            
                            echo "<tr><td>Дата проведения лечебно-профилактических мероприятий (дегельминтизации, вакцинации против бешенства и лептоспироза, стерилизации/кастрации)</td><td class='td-width'><span class='edit_element'>{$res['data']}</span></td></tr>
                                <tr><td>Ответственный специалист</td><td><span class='edit_element'>{$res['sourname']} {$name1}. {$patronymic1}.</span></td></tr>
                                <tr><td>Информация о неснимаемой и несмываемой метке</td><td><span class='edit_element'>{$res['num_birka']}</span></td></tr>
                                <tr><td>Номер микрочипа</td><td><span class='edit_element'>{$res['num_chip']}</span></td></tr>
                                <tr><td>Препарат дегельминтизации</td><td><span class='edit_element'>{$res['vet_degilmintizaciya_preparat']}</span></td></tr>
                                <tr><td>Препарат вакцинации</td><td><span class='edit_element'>{$res['vet_vaccine_preparat']}</span></td></tr>
                                <tr><td>Комментарий</td><td><span class='edit_element'>{$res['comment']}</span></td></tr>
                                ";
            			}?>
				  	</tbody>
				</table>
				
    			<div class="content-right-table table-responsive desktop-vet">
    		        <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab tab-vet">
                  <tr class="tab-col1">
                    <th>Дата проведения лечебно-профилактических мероприятий (дегельминтизации, вакцинации против бешенства и лептоспироза, стерилизации/кастрации)</th>
                    <th>Ответственный специалист</th>
                    <th>Информация о неснимаемой и несмываемой метке</th>
                    <th>Номер микрочипа</th>
                    <th>Препарат дегельминтизации</th>
                    <th>Препарат вакцинации</th>
                    <th>Комментарий</th>
                  </tr>
                    <?
                    if ( empty($check_vet_card) ) {
                         echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                    }
                    else{
        			    $res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
                        while ($res = mysqli_fetch_array($res_query)) {
                            
                            $name = $res['name'];
                            $name1 = mb_substr($name, 0, 1);
                            $patronymic = $res['patronymic'];
                            $patronymic1 = mb_substr($patronymic, 0, 1);
                            
                          echo "<tr data-href='/animal_card.php?application={$res['id']}' class='application'>
                            <td>{$res['data']}</td>
                            <td>{$res['sourname']} {$name1}. {$patronymic1}.</td>
                            <td>{$res['num_birka']}</td>
                            <td>{$res['num_chip']}</td>
                            <td>{$res['vet_degilmintizaciya_preparat']}</td>
                            <td>{$res['vet_vaccine_preparat']}</td>
                            <td>{$res['comment']}</td>
                            </tr>";
            			}
        			}?>
                </table>
                </div>
            </div>
	    
	   	    <div class="col-md-12 vet-block">
	   	        <p>Доказательные фото и видео материалы о проведенных профилактических мероприях в отношении животного.</p>
	   	    </div>
	   		
			    <?php 
		        $res_image = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM upload_files WHERE `id_user_group`=3 AND id_application = '".$_GET['application']."' AND type = 'img'"));
		        if(!empty($res_image)){ ?>
				    <div class="col-12 col-sm-6" style="margin: 0 auto;">
					    <div id="carousel2" class="carousel slide" data-ride="carousel" data-interval="1000000">
				  		<div class="carousel-inner">
					    	<?php 
					    		$qIMAGES = mysqli_query($SERVER, "SELECT * FROM `upload_files` WHERE `id_user_group`=3 AND id_application = '".$_GET['application']."' AND type = 'img'");
			     				$active = true;
				     			while ($IMAGES = mysqli_fetch_array($qIMAGES)) {
				     				$img = $IMAGES['path_file'].$IMAGES['name_file'];
				     				$slide = $active ? "active" : "";
				     				echo '<div class="item '.$slide.' center-block">
	    					       			<img src="'.$img.'" alt="Изображение животного" class="center-block" style="z-index: 3">
	    						     	</div>';
							     	
						     		$active = false;
				     			}
				     		?>
					  	</div>
					  	<!-- Элементы управления -->
					  	<a class="left carousel-control" href="#carousel2" role="button" data-slide="prev">
					    	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					    	<span class="sr-only">Предыдущий</span>
					  	</a>
					  	<a class="right carousel-control" href="#carousel2" role="button" data-slide="next">
					    	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					    	<span class="sr-only">Следующий</span>
					  	</a>
					</div>
                 
				  </div><? 
		        }
		        
				// if ( $_SESSION['id_users_group'] == "1" || $_SESSION['id_users_group'] == "3" ) {
				//     echo ("<a href='?application={$_GET['application']}&edit-foto-vet={$_GET['application']}'>
    // 					<span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> Редактировать фото
    // 				</a>");
    //             }
				?>
			
			<div class="col-12 col-sm-6" style="margin: 0 auto;">
				<div class="vid">
				    <?php
        				   
        			$rs_vid = mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM upload_files WHERE `id_user_group`=3 AND id_application='".$_GET['application']."' AND type = 'vid'");
                    $result_vid = mysqli_fetch_array($rs_vid);
                    
                    $rs_img = mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM upload_files WHERE `id_user_group`=3 AND id_application='".$_GET['application']."' AND type = 'img'");
                    $result_img = mysqli_fetch_array($rs_img);
                    
                    
                    
                    if ( $result_vid[0] == 1 || $_SESSION['width'] == 1) 
                        $style_vid = "style= 'width: 100%;'";
                    else
                        $style_vid = "style= 'width: 50%;'";
        			
					$qVIDEOS = mysqli_query($SERVER, "SELECT * FROM `upload_files` WHERE `id_user_group`=3 AND id_application = '".$_GET['application']."' AND type = 'vid'");
	                while ($VIDEOS = mysqli_fetch_array($qVIDEOS)) {
	     				$vid = $VIDEOS['path_file'].$VIDEOS['name_file'];
	     				if ($VIDEOS['name_file'] != "") {
	     					echo "<div class='vid-item' {$style_vid}>
	     					        <div class='vid-wrap'>
		                   	            <video src='{$vid}' controls width='500vw'></video>	
		                   	        </div>
		                   	     </div>";
	     				}
						else
							echo "<div class=''>
	                   	       <img src='template/no_video.jpg'>
	                   	     </div>";   
	     			} 
				    ?>
				</div>
			</div>
			<? 
    			if ( $_SESSION['id_users_group'] == "1" || $_SESSION['id_users_group'] == "3" ) {
    			    echo ("<div class='col-md-12'>");
    			    echo ("<a href='?application={$_GET['application']}&edit-foto-vet={$_GET['application']}'>
    					<span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> Редактировать фото
    				</a>");
    				echo ("<a href='?application={$_GET['application']}&edit-video-vet={$_GET['application']}' style='float: right;'>
        					<span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> Редактировать видео
        				</a>");
        			echo ("</div>");
          }
          $result = mysqli_fetch_array(mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM vet_card WHERE id_application=".$_GET['application']));
          if ($result[0] > 0) {
            // if ( $card['id_status'] == 10 && ($_SESSION['id_users_group'] == "3" || $_SESSION['id_users_group'] == "1")) {
            //   echo "<center><a href='?application={$_GET['application']}&status_vosstanovlenie={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Поместить на восстановление</a></center>";
            // }
          }
          ?>
   	    </div>
	    <? } ?> 
		<div class="row">
		    <div class="col-md-12">
    		    <?
    		    if ( $_SESSION['id_users_group'] == "2" && $card['id_status'] != 1 ) {
    			    echo "<div class='col-md-12'>
    			            <div class='button-card'>
    		                   <a href='?application={$_GET['application']}&status_vladelets={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Возвращено владельцу</a>
    		                   <a href='?application={$_GET['application']}&status_shelter={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Передано в приют</a>";
    		                   if ( $_SESSION['id_users_group'] != "1" ) {
    		                     echo "<a href='?application={$_GET['application']}&num_aviary={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Изменить номер вольера</a>";
    		                   }
    		        echo "  </div>
    		            </div>"; 
		    ?>
		    </div>
		</div>
		<? } ?>
	   	
	   	<div class="row"> 
	   	    <div class='col-md-12'>
	   	        <div class='button-card but-vet'>
    	        <?php
    	      //       if ( $card['id_status'] == 10 && ($_SESSION['id_users_group'] == "3" || $_SESSION['id_users_group'] == "1")) {
    				//     echo "<a href='?application={$_GET['application']}&status_vosstanovlenie={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Поместить на восстановление</a>";
    				// }
    				if ( ($_SESSION['id_users_group'] == "3" || $_SESSION['id_users_group'] == "1") && $card['id_status'] != 7 && $card['id_status'] != 8) {
              if ($_SESSION['id_users_group'] == "3")
                  echo "<a href='../libs/phpword/generate_word_zakluchenie2.php?application={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Заключение первичное</a>";
              else
                  echo "<a href='?application={$_GET['application']}&select-vet={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Заключение первичное</a>";
    				}
    				$result_vet_card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM vet_card WHERE id_application=".$_GET['application']));
            if (  $result_vet_card[0] > 0 && ($_SESSION['id_users_group'] == "3" || $_SESSION['id_users_group'] == "1") && $card['id_status'] != 7 && $card['id_status'] != 8 ) {
                echo "<a href='../libs/phpword/generate_word_zakluchenie.php?application={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Заключение вторичное</a>";    
            }   
    				if ( $_SESSION['id_users_group'] == "2" && $card['id_status'] != 7 && $card['id_status'] != 8 && $card['id_status'] != 5 && $card['id_status'] != 12) {
    				    echo "<a href='?application={$_GET['application']}&num_aviary={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Изменить номер вольера</a>";
    				}
    				if ( $card['id_status'] == 11 && ($_SESSION['id_users_group'] == "2" || $_SESSION['id_users_group'] == "1") && $card['id_status'] != 7 && $card['id_status'] != 8) {
    				    echo "<a href='?application={$_GET['application']}&status_perezaseleno={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Перезаселено</a>";
    				}
    				if ( ($_SESSION['id_users_group'] == "2" || $_SESSION['id_users_group'] == "1") && $card['id_status'] != 7 && $card['id_status'] != 8 && $card['id_status'] != 5 && $card['id_status'] != 12) {
    				    echo "<a href='?application={$_GET['application']}&status_vladelets={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Возвращено владельцу</a>";
    				    echo "<a href='?application={$_GET['application']}&status_shelter={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Передано в приют</a>";
    				}
    				if ( ($_SESSION['id_users_group'] == "2" || $_SESSION['id_users_group'] == "1" || $_SESSION['id_users_group'] == "3") && $card['id_status'] != 7 && $card['id_status'] != 8) {
    				    echo "<a href='?application={$_GET['application']}&status_die={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Погибло</a>";
    				}
    				// if ( ($_SESSION['id_users_group'] == "2" || $_SESSION['id_users_group'] == "1" || $_SESSION['id_users_group'] == "3") && $card['id_status'] == 3 && $card['id_status'] != 7 && $card['id_status'] != 8) {
    				if ( ($_SESSION['id_users_group'] == "2" || $_SESSION['id_users_group'] == "1" || $_SESSION['id_users_group'] == "3") && $card['id_status'] != 7 && $card['id_status'] != 8) {
    				    echo "<a href='?application={$_GET['application']}&status_die1={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Умерщвлено</a>";
    				}
    				if ( $_SESSION['id_users_group'] == "1" && $card['id_status'] == 12 ) {
    				    echo "<a href='../libs/phpword/generate_word_akt_shelter.php?application={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Акт выбытия животного без владельца</a>";
    				    // По причине передачи в приют
    				}
    				if ( $_SESSION['id_users_group'] == "1" && $card['id_status'] == 4 ) {
    				    echo "<a href='../libs/phpword/generate_word_akt.php?application={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Акт выбытия животного из приюта</a>";
    				    // Это учетное дело печатается админом после выбытия животного из приюта
    				}
    				if ( $_SESSION['id_users_group'] == "1") {
    				    echo "<a href='../libs/phpword/uchetnoe_delo_word.php?application={$_GET['application']}' class='btn button-auth field-submit1 btn-card'>Учетное дело животного</a>";
                echo "<a href='#' class='btn button-auth field-submit1 btn-card'>Скачать медиафайлы</a>";
    				}
    			?>
    			</div>
    		</div>
			<div class='col-md-12'><?
			    if ( $card['id_status'] == 5 ) {?>
			    	<p>Данные о владельце животного</p>
				    <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab tab-vet">
                          <tr>
                            <th>Дата передачи</th>
                            <th>ФИО</th>
                            <th>Телефон</th>
                            <th>Серия паспорта</th>
                            <th>Номер паспорта</th>
                            <th>Выдан</th>
                          </tr>
                        <?
					    $query = "SELECT * FROM animal_owners WHERE id_application = ".$_GET['application'];
        
                        $res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
                        while ($res = mysqli_fetch_array($res_query)) {
                          echo "<tr>
                            <td>{$res['data']}</td>
                            <td>{$res['sourname_owner']} {$res['name_owner']} {$res['patronymic_owner']}</td>
                            <td>{$res['phone']}</td>
                            <td>{$res['passport_seriya']}</td>
                            <td>{$res['passport_num']}</td>
                            <td>{$res['passport_vidan']}</td>
                            </tr>";
            			}?>
                    
    	            </table><?
			    }
			    
			    if ( $card['id_status'] == 12 ) {?>
				    <p>Данные о новом приюте</p>
				    <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab tab-vet">
                          <tr>
                            <th>Дата передачи</th>
                            <th>Название приюта</th>
                            <th>Адрес</th>
                            <th>Телефон</th>
                          </tr>
                        <?
					    $query = "SELECT * FROM animal_owners WHERE id_application = ".$_GET['application'];
        
                        $res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
                        while ($res = mysqli_fetch_array($res_query)) {
                          echo "<tr>
                            <td>{$res['data']}</td>
                            <td>{$res['name_shelter']}</td>
                            <td>{$res['adress']}</td>
                            <td>{$res['phone']}</td>
                            </tr>";
            			}?>
            		</table><?
                }?>
	        </div><? 
	        if ( $_SESSION['id_users_group'] == "4" || $_SESSION['id_users_group'] == "1") { // Кинолог ?>
    			<div class="col-md-12">
    				<div class="button-card">
    					<?
    	      			$check_status = mysqli_fetch_array(mysqli_query($SERVER, "SELECT id_status FROM application WHERE id=".$card['id']));					
    					if ($check_status['id_status'] == 11) {
    						echo '<input id="free" data="'.$card['id'].'" name="submit" type="submit" class="btn button-auth field-submit1 btn-card" value="Животное выпущено"/>';
    					}
    					if ($check_status['id_status'] == 7 || $check_status['id_status'] == 8) {
    						echo '<input id="buried" data="'.$card['id'].'" name="submit" type="submit" class="btn button-auth field-submit1 btn-card" value="Животное захоронено"/>';
    					}
    					
    					?>
    				</div>
    			</div><?
    		} ?>
    	</div> <?
    		if ( ($_SESSION['id_users_group'] == "1") || ($_SESSION['id_users_group'] != "1" && $card['id_status'] == 4) ) { ?>
    			<div class="row block-content-card"> 
        			<div class="col-md-12">
        			    <p>Доказательные фото и видео выпуска животного</p>
        			</div>
    				<?
        		        $res_image = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM upload_files WHERE id_application = '".$_GET['application']."' AND type = 'img' AND vipusk = 1"));
        		        if(!empty($res_image)){?>
        		            <div class="col-12 col-sm-6" style="margin: 0 auto;">
        					    <div id="carousel3" class="carousel slide" data-ride="carousel" data-interval="1000000">
        				  		<div class="carousel-inner">
        					    	<?php 
        					    		$qIMAGES = mysqli_query($SERVER, "SELECT * FROM `upload_files` WHERE id_application = '".$_GET['application']."' AND type = 'img' AND vipusk = 1");
        			     				$active = true;
        				     			while ($IMAGES = mysqli_fetch_array($qIMAGES)) {
        				     				$img = $IMAGES['path_file'].$IMAGES['name_file'];
        				     				$slide = $active ? "active" : "";
        				     				echo '<div class="item '.$slide.' center-block">
        	    					       			<img src="'.$img.'" alt="Изображение животного" class="center-block" style="z-index: 3">
        	    						     	</div>';
        							     	
        						     		$active = false;
        				     			}
        				     		?>
        					  	</div>
        					  	<!-- Элементы управления -->
        					  	<a class="left carousel-control" href="#carousel3" role="button" data-slide="prev">
        					    	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        					    	<span class="sr-only">Предыдущий</span>
        					  	</a>
        					  	<a class="right carousel-control" href="#carousel3" role="button" data-slide="next">
        					    	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        					    	<span class="sr-only">Следующий</span>
        					  	</a>
        					</div>
        					</div><? 
        		        }
        		        
        				
        				?>
        			
        			<div class="col-12 col-sm-6" style="margin: 0 auto;">
        				<div class="vid">
        				    <?php
        				    
        				    $rs_vid = mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM upload_files WHERE id_application='".$_GET['application']."' AND type = 'vid' AND vipusk = 1");
                            $result_vid = mysqli_fetch_array($rs_vid);
                            
                            $rs_img = mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM upload_files WHERE id_application='".$_GET['application']."' AND type = 'img' AND vipusk = 1");
                            $result_img = mysqli_fetch_array($rs_img);
                            
                            
                            
                            if ( $result_vid[0] == 1 || $_SESSION['width'] == 1) 
                                $style_vid = "style= 'width: 100%;'";
                            else
                                $style_vid = "style= 'width: 50%;'";
	     			
        					$qVIDEOS = mysqli_query($SERVER, "SELECT * FROM `upload_files` WHERE id_application = '".$_GET['application']."' AND type = 'vid' AND vipusk = 1");
        	                while ($VIDEOS = mysqli_fetch_array($qVIDEOS)) {
        	     				$vid = $VIDEOS['path_file'].$VIDEOS['name_file'];
        	     				if ($VIDEOS['name_file'] != "") {
        	     					echo "<div class='vid-item' {$style_vid}>
        	     					        <div class='vid-wrap'>
        		                   	            <video src='{$vid}' controls width='500vw'></video>	
        		                   	        </div>
        		                   	     </div>";
        	     				}  
        	     			} 
        	     		// 	if ( $_SESSION['id_users_group'] == "1" || $_SESSION['id_users_group'] == "4" ) {
            				//     echo ("<a href='?application={$_GET['application']}&edit-video-vipusk={$_GET['application']}'>
                // 					<span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> Редактировать видео
                // 				</a>");
                //             }
        				    ?>
        				</div>
        			</div>
        			<? 
        			if ( $_SESSION['id_users_group'] == "1" || $_SESSION['id_users_group'] == "4" ) {
        			    echo ("<div class='col-md-12'>");
    				    echo ("<a href='?application={$_GET['application']}&edit-foto-vipusk={$_GET['application']}'>
        					<span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> Редактировать фото
        				</a>");
        				echo ("<a href='?application={$_GET['application']}&edit-video-vipusk={$_GET['application']}' style='float: right;'>
            					<span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> Редактировать видео
            				</a>");
            			echo ("</div>");
                    }?>
        		</div><?
		    } ?>
    		
	    </div>
	</div>
    <? 
    // } 
    ?>
</div>
</section>
<?php
    
    if (isset($_GET['select-vet'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-select-vet',
              type : 'inline'
           });
      </script>
    <?php
    }
    
    if (isset($_GET['edit-application'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-edit-card',
              type : 'inline'
           });
      </script>
    <?php
    }
    
    if (isset($_GET['edit-foto'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-edit-foto',
              type : 'inline'
           });
      </script>
    <?php
    }
    
    if (isset($_GET['edit-video'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-edit-video',
              type : 'inline'
           });
      </script>
    <?php
    }
    
    if (isset($_GET['edit-card-vet'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-edit-card-vet',
              type : 'inline'
           });
      </script>
    <?php
    }
    
    if (isset($_GET['add-card-vet'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-add-card-vet',
              type : 'inline'
           });
      </script>
    <?php
    }
    
    if (isset($_GET['edit-foto-vet'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-edit-foto-vet',
              type : 'inline'
           });
      </script>
    <?php
    }
    
    if (isset($_GET['edit-video-vet'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-edit-video-vet',
              type : 'inline'
           });
      </script>
    <?php
    }
    
    if (isset($_GET['edit-foto-vipusk'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-edit-foto-vipusk',
              type : 'inline'
           });
      </script>
    <?php
    }
    
    if (isset($_GET['edit-video-vipusk'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-edit-video-vipusk',
              type : 'inline'
           });
      </script>
    <?php
    }
    
    if (isset($_GET['status_vosstanovlenie'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-status',
              type : 'inline'
           });
      </script>
    <?php
    }
    if (isset($_GET['status_die'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-die',
              type : 'inline'
           });
      </script>
    <?php
    }
    if (isset($_GET['num_aviary'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-num_aviary',
              type : 'inline'
           });
      </script>
    <?php
    }
    if (isset($_GET['status_perezaseleno'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-perezaseleno',
              type : 'inline'
           });
      </script>
    <?php
    }
    if (isset($_GET['status_vladelets'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-vladelets',
              type : 'inline'
           });
      </script>
    <?php
    }
    if (isset($_GET['status_shelter'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-shelter',
              type : 'inline'
           });
      </script>
    <?php
    }
    if (isset($_GET['status_die1'])) { 
    ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-die1',
              type : 'inline'
           });
      </script>
    <?php
    }
    
    if ($_SESSION['id_users_group'] == 3 || $_SESSION['id_users_group'] == 1) {
		echo '<script type="text/javascript" src="js/veterinar.js"></script>
		      <script type="text/javascript" src="js/veterinar_tab.js"></script>';
	}  
	echo '<script type="text/javascript" src="js/vipusk.js"></script>';
	
    require_once("template/footer.html");
?>

<script>

    // $(document).ready(function() {
    //     if (document.getElementById("shelter_edit").value == "3") {
    //         document.getElementById("avairs-3").style.display = "block";   
    //     }
    //     console.log(doc.value);
    // });

    // window.onload = function() {
		
// 		document.getElementById("get_images").onclick = function() {
		    
// 		  //  let href_vid = document.querySelector("#vid-screen").data;
		    
// 		  //  window.open(href_vid, '_blank');
//             console.log(document.querySelector("#vid-screen"));
// 			html2canvas(document.querySelector("#vid-screen")).then(function(canvas) {

// 				var my_screen = canvas;

// 				document.getElementById("result").appendChild(my_screen);

// 			});

// 		};
	
// 	}

</script>