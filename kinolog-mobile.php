<?php
  require_once("php/config.php");
  if (!isset($_SESSION['timezone'])){
		echo "Не определён часовой пояс пользователя. Авторизируйтесь заново <a href='auth/login.php'>ЗДЕСЬ</a>";
		exit();
	}
  require_once("php/timezone.php");
  if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
  {
      header('location: auth/login.php' );
      exit();
  }
  require_once("php/update_status.php");
  require_once("template/head.html");
  require_once("template/header.php");
  require_once("php/functions.php");
  $_SESSION['width'] = 1;
  $_SESSION['num_tr'] = 1;
  
  $today = date("Y-m-d\TH:i:s");

  $last_query = " AND (`id_status`=1 OR `id_status`=2 OR `id_status`=11 OR `id_status`=7 OR `id_status`=8) ";
  
  function format_interval(DateInterval $interval) {
      $result = "";
      if ($interval->d) { $result .= $interval->format("%d д. "); }
      if ($interval->h) { $result .= $interval->format("%h ч. "); }
      if ($interval->i) { $result .= $interval->format("%i мин. "); }
  
      return $result;
  };
  if ($_SESSION['filter_first_date'] == "first") {
    $sql = "SELECT MIN(`data`) AS filter_first_date FROM `application`";
    $res = mysqli_fetch_array(mysqli_query($SERVER, $sql));
    $filter_first_date = date("Y-m-d\TH:i:s", $res['filter_first_date']);
  }
  else{
    $filter_first_date = $_SESSION['filter_first_date'];
  }

  // var_dump($_SESSION['filter_last_date']);
  // if ($_SESSION['filter_last_date'] == "last") {
    $sql = "SELECT MAX(`data`) AS filter_last_date FROM `application`";
    $res = mysqli_fetch_array(mysqli_query($SERVER, $sql));
    $filter_last_date = date("Y-m-d\TH:i:s", $res['filter_last_date']);
  // }
  // else{
  //   $filter_last_date = $_SESSION['filter_last_date'];
  // }
?>



<!-- Content -->
<section class="content-mobile">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="content-right" class="content-right">
          <div id="content-right-table" class="content-right-table table-responsive">
            <div class="menu-table-min">
              <!--<div class="table-caption">Кинолог<div class="rule-answer"><a href="#">?</a></div></div>-->
                <div class="menu-table-filter mob-filter-parent">
                    <div class="mob-filter">
                      <div class="mobile-label">
                        <p>Начиная с</p>
                        <input class="field-filter field-date" id="filter_first_date" name="filter_first_date"  type="datetime-local" autocomplete="on" value="<?echo $filter_first_date?>">
                      </div>
                      <div class="mobile-label">
                        <p>Заканчивая</p>
                        <input class="field-filter field-date" id="filter_last_date" name="filter_last_date"  type="datetime-local" autocomplete="on" value="<?echo $filter_last_date?>">
                      </div>
                    </div>
                    <div class="mob-filter">
                        <div class="mobile-label">
                          <p>Место отлова</p>
                          <select class="field-filter" id="filter_locality" name="filter_locality" >
                          <?php
                            $sql = "SELECT DISTINCT locality AS filter_locality FROM `application`";
                            $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                            echo '<option value="all">Все населенные пункты</option>';
                            while($row = mysqli_fetch_array($res))
                            {
                              $selected = $_SESSION['filter_locality'] == $row['filter_locality'] ? 'selected' : '';
                              echo '<option value="'.$row['filter_locality'].'" '.$selected.'>'.$row['filter_locality'].'</option>';
                            }
                          ?>
                        </select>
                        </div>
                    </div>
                    <div class="mob-filter">
                        <div class="mobile-label">
                          <p>Статус</p>
                          <select class="field-filter" id="show_status" >                    
                            <option  value="0" <? if ($_SESSION['show_status'] == 0) echo 'selected'; ?>> все статусы</option>
                            <option  value="1" <? if ($_SESSION['show_status'] == 1) echo 'selected'; ?>>работает кинолог</option>
                            <!--<option  value="2" <? if ($_SESSION['show_status'] == 2) echo 'selected'; ?>>на карантине</option>-->
                            <option  value="11" <? if ($_SESSION['show_status'] == 11) echo 'selected'; ?>>готово к выпуску</option>
                            <option  value="7" <? if ($_SESSION['show_status'] == 7) echo 'selected'; ?>>погибло</option>
                            <option  value="8" <? if ($_SESSION['show_status'] == 8) echo 'selected'; ?>>умерщвлено</option>
                          </select>
                        </div>
                        
                        <div class="mobile-label">
                            <p>Приют</p>
                            <select class="field-filter" id="id_shelter" name="id_shelter" >
                            <?
                            //   $sql = "SELECT * FROM `animal_shelters` WHERE `actual`='1' AND `id_region`='".$_SESSION['id_region']."';";
                              $sql = "SELECT * FROM `animal_shelters` WHERE `actual`='1';";
                              $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                              echo '<option value="all">Все приюты</option>';
                              while($row = mysqli_fetch_array($res))
                              {
                                $selected = $_SESSION['filter_id_shelter'] == $row['id'] ? 'selected' : '';
                                echo '<option value="'.$row['id'].'" '.$selected.'>'.$row['name_shelter'].'</option>';
                              }
                            
                            ?>
                          </select>
                        </div>
                    </div>
                    <div class="mob-filter">
                        <div class="mobile-label">
                            <p>Контракт</p>
                            <select class="field-filter" id="show_contract" name="show_contract" >
                            <?
                              $sql1 = "SELECT * FROM `contract` WHERE `show_contract`=1 AND status='открыт' AND id_region='".$_SESSION['id_region']."' ORDER BY contract.id DESC;";
                              $res1 = mysqli_query($SERVER, $sql1) or die("Ошибка " . mysqli_error($SERVER));
                              echo '<option value="all">Все контракты</option>';
                    
                              while($row1 = mysqli_fetch_array($res1))
                              {
                                $selected1 = $_SESSION['show_contract'] == $row1['id'] ? 'selected' : '';
                                echo '<option value="'.$row1['id'].'" '.$selected1.'>'.$row1['name_contract'].'</option>';
                              }
                            ?>
                        </select>
                        </div>
                    </div>
                    <div class="mob-filter">
                        <div class="mobile-label">
                          <p>Вольер</p>
                          <input type="number" class="field-filter input" placeholder="Все вольеры" id="filter_aviarys" value="<?echo $_SESSION['filter_aviarys']?>">          
                        </div>
                        <div class="mobile-label">
                          <p>Сортировать по</p>
                          <select class="field-filter"  id="order_by">
                            <option value="1" <? if ($_SESSION['order_by'] == 'data') echo 'selected'; ?>>дата</option>
                            <option value="2" <? if ($_SESSION['order_by'] == 'locality') echo 'selected'; ?>>город</option>
                            <option value="3" <? if ($_SESSION['order_by'] == 'street') echo 'selected'; ?>>улица</option>
                            <option value="4" <? if ($_SESSION['order_by'] == 'id_kind') echo 'selected'; ?>>вид</option>
                            <option value="5" <? if ($_SESSION['order_by'] == 'breed') echo 'selected'; ?>>порода</option>
                            <option value="6" <? if ($_SESSION['order_by'] == 'age') echo 'selected'; ?>>возраст</option>
                            <option value="7" <? if ($_SESSION['order_by'] == 'color') echo 'selected'; ?>>окрас</option>
                          </select>    
                        </div>
                        <div class="mobile-label">
                          <p>Направление</p>
                          <select class="field-filter"  id="sort_by">
                            <option value="DESC" <? if ($_SESSION['sort_by'] == 'DESC') echo 'selected'; ?>>по убыванию</option>
                            <option value="ASC" <? if ($_SESSION['sort_by'] == 'ASC') echo 'selected'; ?>>по возрастанию</option>
                          </select>
                        </div>
                        <label for="row_limit" class="hide-mobile" style="display:none;">Показывать: </label>
                        <select class="field-filter hide-mobile" style="display:none;"  id="row_limit">
                            <option value="25" <? if ($_SESSION['row_limit'] == '25') echo 'selected'; ?>>25</option>
                        </select>
                    </div>
                    <div class="mob-filter">
                        <button id="apply_filter" onclick="apply_filter()">Применить фильтры</button>
                        <button id="reset_filter" onclick="reset_filter()">Сбросить фильтры</button>
                    </div>
                </div>
            </div>
            <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab tab-mobile"></table>
            <button hidden id="show_more" onclick="show_more()">Показать ещё</button>
          </div>  
        </div>
      </div> 
    </div> 
  </div>
</section>
<!-- /Content -->
<?php
  require_once("template/footer.html");
?>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    apply_filter();
  });
  
  $('body').on('click', '.application-mobile', function(e){
      e.prevenDefault;
      document.location.href = $(this).data('href');
  });
  
  var start_from = $('#row_limit').val();
  function gen_table(){
    $.ajax({
      type: 'POST',
      url: 'php/gen_table_kin-mobile.php?gen_table',
      dataType: 'json',
      beforeSend: function(){
        //создаем div
        var loading = $("<div>", {
          "class" : "ds-loading"
        });
        $('.ds-loading').fadeIn(1000, function(){
          setTimeout(function(){
            $('.ds-loading').fadeIn(500);
          }, 5000);
        });
        //выравним div по центру страницы
        $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
        //добавляем созданный div в конец документа
        $("body").append(loading);
      },
      success: function(res){
        $('.application-mobile').remove();
        $('#table-zayavka').append(res.html);
        if (parseInt(res.row_count) < parseInt($('#row_limit').val()))
          $('#show_more').fadeOut();
        else
          $('#show_more').fadeIn();
          //уничтожаем div
        $('.ds-loading').fadeOut(1000, function(){
          setTimeout(function(){
            $('.ds-loading').fadeOut(500);
          }, 5000);
        });
        $(".ds-loading").detach();
      },
    });
  }
  
  function apply_filter(){
    var show_status = $('#show_status').val();
    let show_contract = $('#show_contract').val();
    let id_shelter = $('#id_shelter').val();
    var row_limit = $('#row_limit').val();
    var order_by = $('#order_by').val();
    var sort_by = $('#sort_by').val();
    var filter_first_date = $('#filter_first_date').val();
    var filter_last_date = $('#filter_last_date').val();
    var filter_locality = $('#filter_locality').val();
    var filter_aviarys = $('#filter_aviarys').val();
    $.ajax({
      type: 'POST',
      url: 'php/gen_table_kin-mobile.php?update_table_config',
      dataType: 'json',
      data: {
        show_status,
        show_contract,
        id_shelter,
        row_limit,
        order_by,
        sort_by,
        filter_first_date,
        filter_last_date,
        filter_locality,
        filter_aviarys
      },
      success: function(res){
        if (res.res == "success") {
          $('#show_more').fadeIn();
          gen_table();
        }
        else {
          alert("Возникла ошибка. Страница будет перезагружена");
          location.reload();
        }
      },
      error: function(res){
        console.log(res);
      }
    });
  }
  
  function show_more(){
    var row_nums = 25;
    $.ajax({
      type: 'POST',
      url: 'php/gen_table_kin-mobile.php?gen_table&show_more',
      dataType: 'json',
      data: {
        start_from,
        row_nums
      },
      beforeSend: function(){
        //создаем div
        var loading = $("<div>", {
          "class" : "ds-loading"
        });
        $('.ds-loading').fadeIn(1000, function(){
          setTimeout(function(){
            $('.ds-loading').fadeIn(500);
          }, 5000);
        });
        //выравним div по центру страницы
        $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
        //добавляем созданный div в конец документа
        $("body").append(loading);
      },
      success: function(res){
        if (res.result == "success"){
          $('#table-zayavka').append(res.html);
          start_from = Number(start_from) + 25;
          if (parseInt(res.row_count) < parseInt($('#row_limit').val()))
            $('#show_more').fadeOut();
          else
            $('#show_more').fadeIn();
            //уничтожаем div
          $('.ds-loading').fadeOut(1000, function(){
            setTimeout(function(){
              $('.ds-loading').fadeOut(500);
            }, 5000);
          });
          $(".ds-loading").detach();
        }
      },
      error: function(res){
        console.log(res);
      }
    });
  }
</script>