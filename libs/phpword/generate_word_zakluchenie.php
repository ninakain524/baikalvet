<?php 
    require_once('require_word.php');
    
    $id = $_GET['application']; //текущий id

    $numCard = "0";      
    $card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".$id));
    $user = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$card['id_user']));
    $kind = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_kind WHERE id=".$card['id_kind']));
    $gender = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_gender WHERE id=".$card['id_gender']));
    $contract = mysqli_fetch_array(mysqli_query($SERVER, "SELECT contract.number AS number, DATE_FORMAT(contract.date_contract,'%d.%m.%Y') AS date_contract, contract.name_tz AS name_tz, contract.num_application AS num_application FROM contract WHERE id=".$card['id_contract']));
    
    if ($card['id_contract'] != "0" && $card['id_contract'] != "" && $card['id_contract'] != NULL) 
        $numCard = getNumberCard($id, $card['id_contract'], $SERVER);
         
    if (ctype_digit($card['color'])) {
        $animal_color1 = mysqli_fetch_array(mysqli_query($SERVER, "SELECT name_color FROM animal_color WHERE id=".$card['color']));
        $animal_color = $animal_color1['name_color'];
    }
	else
	    $animal_color = $card['color']; 
    	    
    $vet_card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_card WHERE id_application=".$_GET['application']));
    $vet_degilmintizaciya = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_degilmintizaciya WHERE id_application=".$_GET['application']));
    $vet_d = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$vet_degilmintizaciya['id_veterinar']));
    $vet_operation = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_operation WHERE id_application=".$_GET['application']));
    $vet_o = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$vet_operation['id_veterinar']));
    $vet_vaccine = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_vaccine WHERE id_application=".$_GET['application']));
    $vet_v = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$vet_vaccine['id_veterinar']));
            
    $vet = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$vet_card['id_veterinar']));
    $name1 = mb_substr($vet['name'], 0, 1);
    $patronymic1 = mb_substr($vet['patronymic'], 0, 1);
    
    $name_d = mb_substr($vet['name'], 0, 1);
    $patronymic_d = mb_substr($vet['patronymic'], 0, 1);
    
    $name_o = mb_substr($vet['name'], 0, 1);
    $patronymic_o = mb_substr($vet['patronymic'], 0, 1);
    
    $name_v = mb_substr($vet['name'], 0, 1);
    $patronymic_v = mb_substr($vet['patronymic'], 0, 1);
    
    if(!empty($card['id_user']) && $card['id_user'] != 0 && $card['id_user'] != "") {
        $names = $user['name'];
        $names1 = mb_substr($names, 0, 1);
        $patronymics = $user['patronymic'];
        $patronymics1 = mb_substr($patronymics, 0, 1);
    }
    
    $phpWord = new  \PhpOffice\PhpWord\PhpWord(); 
    $_doc = new \PhpOffice\PhpWord\TemplateProcessor('template_4.docx');
    $_doc->setValue('data', $contract['date_contract']);  
    $_doc->setValue('num', $contract['number']);
    $_doc->setValue('num_app', $contract['num_application']);
    $_doc->setValue('city_app', $contract['name_tz']);
    $_doc->setValue('vet', $vet['sourname'].' '.$name1.'. '.$patronymic1.'.');
    if(!empty($card['id_user']) && $card['id_user'] != 0 && $card['id_user'] != "") 
        $_doc->setValue('rab', $user['sourname'].' '.$names1.'. '.$patronymics1.'.');
    else
        $_doc->setValue('rab', '');
    
    $_doc->setValue('id', $numCard); 
    $_doc->setValue('data_z', date('d.m.Y', $card['data']));
    $_doc->setValue('vid', $kind['name_kind']); 
    $_doc->setValue('poroda', $card['breed']); 
    $_doc->setValue('gender', $gender['name_gender']); 
    $_doc->setValue('color', $animal_color); 
    $_doc->setValue('age', $card['age']); 
    $_doc->setValue('weight', $card['weight']);
    
    $_doc->setValue('comment', $card['comment']);
    
    $_doc->setValue('data_mer', (new DateTime($vet_card['data']))->format('d.m.Y'));
    $_doc->setValue('metka', $vet_card['num_birka']);
    $_doc->setValue('chip', $vet_card['num_chip']);
    $_doc->setValue('data_d', (new DateTime($vet_degilmintizaciya['data']))->format('d.m.Y'));
    $_doc->setValue('preparat_d', $vet_degilmintizaciya['preparat']);
    $_doc->setValue('vet_d', $vet['sourname'].' '.$name_d.'. '.$patronymic_d.'.');
    
    $_doc->setValue('data_v', (new DateTime($vet_vaccine['data']))->format('d.m.Y'));
    $_doc->setValue('preparat_v', $vet_vaccine['preparat']);
    $_doc->setValue('vet_v', $vet['sourname'].' '.$name_v.'. '.$patronymic_v.'.');
    
    $_doc->setValue('data_o', (new DateTime($vet_operation['data']))->format('d.m.Y'));
    $_doc->setValue('vet_o', $vet['sourname'].' '.$name_o.'. '.$patronymic_o.'.');
    

    $img_Dir_Str = "/assets/files/documents/";
    $img_Dir = $_SERVER['DOCUMENT_ROOT']."/". $img_Dir_Str; 
    @mkdir($img_Dir, 0777);
    $file = str_replace("/","-", "Zakluchenie-vtorichnoe-".date("d-m-Y")).".docx";
    
    $_doc->saveAs($img_Dir.$file);
    
    $file_name = $img_Dir.$file;
    
    header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
    header('Content-Disposition: attachment;filename="'.$file.'"');
    header('Cache-Control: max-age=0');
    readfile($file_name);
?>