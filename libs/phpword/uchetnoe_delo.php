<?php 
    require_once('require_word.php');

    $name_zip = "uchetnie-dela-".date("d-m-Y").".zip";
    $zip = new ZipArchive(); //Создаём объект для работы с ZIP-архивами
    $zip->open($name_zip, ZIPARCHIVE::CREATE); //Открываем (создаём) архив
    
    $array_id = [];
    $array_files = [];

    $numCard = "0"; 
    
    $data_first = $_GET['data_first'];
    $data_second = $_GET['data_second'];
    $getContract = $_GET['contract'];
    $getShelter = $_GET['shelter'];
    $getKinolog = $_GET['kinolog'];
    $getAdress = $_GET['adress'];

    $part_query_kinolog = $part_query_shelter = $part_query_contract = $part_query_adress = "";

    if ($getContract != "all") 
        $part_query_contract = "AND application.id_contract='".$getContract."'";

    if ($getKinolog != "all") 
        $part_query_kinolog = "AND application.id_user='".$getKinolog."'";

    if ($getShelter != "all") 
        $part_query_shelter = "AND application.id_shelter='".$getShelter."'";
    
    if ($getAdress != "all") {
        $adressQuery = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".intval($getAdress)));
        $part_query_adress = " AND application.loc_type='".$adressQuery['loc_type']."' AND application.locality='".$adressQuery['locality']."' AND application.street='".$adressQuery['street']."' ";
    }

    $query = "SELECT 
        application.id AS id,
        application.data AS data,
        application.street AS street,
        application.locality AS locality,
        application.breed AS breed, 
        application.age AS age, 
        application.weight AS weight, 
        application.color AS color,
        application.date_start_vosstanovlenie AS date_start_vosstanovlenie,
        application.date_start_karantin AS date_start_karantin,
        animal_kind.name_kind AS kind,
        animal_gender.name_gender AS gender,
        application_status.id AS status_id,
        application_status.status_name AS status,
        vet_card.num_birka AS num_birka,
        vet_card.num_chip AS num_chip,
        DATE_FORMAT(vet_card.data,'%d.%m.%Y') AS vet_card_data,
        animal_shelters.name_shelter AS name_shelter
        FROM application 
        LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
        LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
        LEFT JOIN application_status ON application.id_status = application_status.id
        LEFT JOIN vet_card ON application.id_vet_card = vet_card.id
        LEFT JOIN animal_shelters ON application.id_shelter = animal_shelters.id
        WHERE application.data >= '".$data_first."' AND application.data <= '".$data_second."' AND application.show='1' ".$part_query_kinolog." ".$part_query_shelter." ".$part_query_contract." ".$part_query_adress." ORDER BY application.data ASC";

    $res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
    while ($res = mysqli_fetch_array($res_query)) {
        array_push($array_id, $res['id']);
    }
     $j = 1;
    for ($i = 0; $i < count($array_id); $i++) {
        $id = $array_id[$i];

        $vet_num_chip = $vet_num_birka = $vet_data = $vet_d_preparat = $vet_d_data = $vet_v_preparat = $vet_v_data = $vet_o_data = $sourname = $name1 = $patronymic1 = $img = " ";
        
        $card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".$id));
        $user = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$card['id_user']));
        $kind = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_kind WHERE id=".$card['id_kind']));
        $gender = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_gender WHERE id=".$card['id_gender']));
        $contract = mysqli_fetch_array(mysqli_query($SERVER, "SELECT contract.customer AS customer, contract.number AS number, DATE_FORMAT(contract.date_contract,'%d.%m.%Y') AS date_contract FROM contract WHERE id=".$card['id_contract']));
    
        if ($card['id_contract'] != "0" && $card['id_contract'] != "" && $card['id_contract'] != NULL) 
            $numCard = getNumberCard($id, $card['id_contract'], $SERVER);
        

        if (ctype_digit($card['color'])) {
            $animal_color1 = mysqli_fetch_array(mysqli_query($SERVER, "SELECT name_color FROM animal_color WHERE id=".$card['color']));
            $animal_color = $animal_color1['name_color'];
        }
        else
            $animal_color = $card['color']; 
            
        $img_sql = mysqli_query($SERVER, "SELECT * FROM `upload_files` WHERE id_application = '".$id."' AND type = 'img' AND `id_user_group`=4 LIMIT 1");
        while ($img_res = mysqli_fetch_array($img_sql)) {
            $img = $img_res['path_file'].$img_res['name_file'];
        }
        
        $rs_vet_card = mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM vet_card WHERE id_application=".$id);
        $result_vet_card = mysqli_fetch_array($rs_vet_card);
        if ($result_vet_card[0] > 0) {
            $vet_card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_card WHERE id_application=".$id));
            $vet_num_birka = $vet_card['num_birka'];
            $vet_num_chip = $vet_card['num_chip'];
            $vet_data = date("d.m.Y", strtotime($vet_card['data']));
            
            $vet_d = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_degilmintizaciya WHERE id=".$vet_card['id_degelmintizaciya']));
            $vet_d_preparat = $vet_d['preparat'];
            $vet_d_data = date("d.m.Y", strtotime($vet_d['data']));
            
            $vet_v = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_vaccine WHERE id=".$vet_card['id_vaccine']));
            $vet_v_preparat = $vet_v['preparat'];
            $vet_v_data = date("d.m.Y", strtotime($vet_v['data']));
            
            $vet_o = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_operation WHERE id=".$vet_card['id_operation'])); 
            $vet_o_data = date("d.m.Y", strtotime($vet_o['data']));
            
            $vet = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$vet_card['id_veterinar']));
            $sourname = $vet['sourname'];
            $name = $vet['name'];
            $name1 = mb_substr($name, 0, 1);
            $patronymic = $vet['patronymic'];
            $patronymic1 = mb_substr($patronymic, 0, 1);
        }
    
    
        $phpWord = new \PhpOffice\PhpWord\PhpWord(); 
        if ($card['id_region'] == "1")
            $_doc = new \PhpOffice\PhpWord\TemplateProcessor('template_5_zab.docx'); 
        else
            $_doc = new \PhpOffice\PhpWord\TemplateProcessor('template_5.docx');
        $_doc->setValue('num', $contract['number']);
        $_doc->setValue('city', $contract['customer']);
        $_doc->setValue('date_c', $contract['date_contract']);
        $_doc->setValue('data', date('d.m.Y', $card['data']));
        $_doc->setValue('adress', $card['street']);
        $_doc->setValue('id', $numCard); 
        $_doc->setValue('vid', $kind['name_kind']); 
        $_doc->setValue('p', $card['breed']); 
        $_doc->setValue('gender', $gender['name_gender']); 
        $_doc->setValue('color', $animal_color); 
        $_doc->setValue('age', $card['age']); 
        $_doc->setValue('weight', $card['weight']);
        $_doc->setValue('met', $vet_num_birka);
        $_doc->setValue('chip', $vet_num_chip);
        $_doc->setValue('datav', $vet_data);
        $_doc->setValue('deg', $vet_d_preparat);
        $_doc->setValue('datad', $vet_d_data);
        $_doc->setValue('vac', $vet_v_preparat);
        $_doc->setValue('datav', $vet_v_data);
        $_doc->setValue('datao', $vet_o_data);
        $_doc->setValue('vet', $sourname.' '.$name1.'. '.$patronymic1.'.');
    
        $_doc->setValue('data1', $card['date_vipusk']);
        $_doc->setValue('datav', $card['data']);
    
        $img_Dir_Str = "/assets/files/documents/";
        $img_Dir = $_SERVER['DOCUMENT_ROOT']."/". $img_Dir_Str; 
        @mkdir($img_Dir, 0777);
        $file = str_replace("/","-", "uchetnoe-delo-".date("d-m-Y"))."-".$j.".docx";
        
        $_doc->saveAs($img_Dir.$file);
        
        $file_name = $img_Dir.$file;

        $zip->addFile($file_name,basename($file_name)); 
        
        array_push($array_files, $file_name);
        $j = $j + 1;
    }

    $zip->close(); //Завершаем работу с архивом

    header('Content-Type: application/zip');
    header('Content-Disposition: attachment;filename="'.$name_zip.'"');
    readfile($name_zip);
    unlink($name_zip);
    
    for ($i = 0; $i < count($array_files); $i++) {
        unlink($array_files[$i]);
    }

?>