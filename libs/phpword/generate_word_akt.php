<?php 
    require_once('require_word.php');

    $vet_num_chip = $vet_num_birka = $vet_data = $vet_d_preparat = $vet_d_data = $vet_v_preparat = $vet_v_data = $vet_o_data = $sourname = $name1 = $patronymic1 = $img = " ";
    $id = $_GET['application'];
    
    $numCard = "0"; 

    $card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".$id));
    $user = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$card['id_user']));
    $kind = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_kind WHERE id=".$card['id_kind']));
    $gender = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_gender WHERE id=".$card['id_gender']));
    $contract = mysqli_fetch_array(mysqli_query($SERVER, "SELECT contract.customer AS customer, contract.number AS number, DATE_FORMAT(contract.date_contract,'%d.%m.%Y') AS date_contract FROM contract WHERE id=".$card['id_contract']));
    
    if ($card['id_contract'] != "0" && $card['id_contract'] != "" && $card['id_contract'] != NULL) 
        $numCard = getNumberCard($id, $card['id_contract'], $SERVER);

    if (ctype_digit($card['color'])) {
        $animal_color1 = mysqli_fetch_array(mysqli_query($SERVER, "SELECT name_color FROM animal_color WHERE id=".$card['color']));
        $animal_color = $animal_color1['name_color'];
    }
	else
	    $animal_color = $card['color']; 
	    
    $img_sql = mysqli_query($SERVER, "SELECT * FROM `upload_files` WHERE id_application = '".$id."' AND type = 'img' AND `id_user_group`=4 LIMIT 1");
 	while ($img_res = mysqli_fetch_array($img_sql)) {
 		$img = $img_res['path_file'].$img_res['name_file'];
 	}
    
    $rs_vet_card = mysqli_query($SERVER, "SELECT COUNT(*) AS `id` FROM vet_card WHERE id_application=".$id);
    $result_vet_card = mysqli_fetch_array($rs_vet_card);
    if ($result_vet_card[0] > 0) {
        $vet_card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_card WHERE id_application=".$id));
        $vet_num_birka = $vet_card['num_birka'];
        $vet_num_chip = $vet_card['num_chip'];
        $vet_data = date("d.m.Y", strtotime($vet_card['data']));
        
        $vet_d = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_degilmintizaciya WHERE id=".$vet_card['id_degelmintizaciya']));
        $vet_d_preparat = $vet_d['preparat'];
        $vet_d_data = date("d.m.Y", strtotime($vet_d['data']));
        
        $vet_v = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_vaccine WHERE id=".$vet_card['id_vaccine']));
        $vet_v_preparat = $vet_v['preparat'];
        $vet_v_data = date("d.m.Y", strtotime($vet_v['data']));
        
        $vet_o = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_operation WHERE id=".$vet_card['id_operation'])); 
        $vet_o_data = date("d.m.Y", strtotime($vet_o['data']));
        
        $vet = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$vet_card['id_veterinar']));
        $sourname = $vet['sourname'];
        $name = $vet['name'];
        $name1 = mb_substr($name, 0, 1);
        $patronymic = $vet['patronymic'];
        $patronymic1 = mb_substr($patronymic, 0, 1);
    }
    
    $phpWord = new  \PhpOffice\PhpWord\PhpWord(); 
    if ($card['id_region'] == "1")
        $_doc = new \PhpOffice\PhpWord\TemplateProcessor('template_3_zab.docx');
    else
        $_doc = new \PhpOffice\PhpWord\TemplateProcessor('template_3.docx');
    $_doc->setValue('num', $contract['number']);
    $_doc->setValue('city', $contract['customer']);
    $_doc->setValue('date_c', $contract['date_contract']);
    $_doc->setValue('data', date('d.m.Y', $card['data']));
    $_doc->setValue('adress', $card['street']);
    $_doc->setValue('id', $numCard); 
    $_doc->setValue('vid', $kind['name_kind']); 
    $_doc->setValue('p', $card['breed']); 
    $_doc->setValue('gender', $gender['name_gender']); 
    $_doc->setValue('color', $animal_color); 
    $_doc->setValue('age', $card['age']); 
    $_doc->setValue('weight', $card['weight']);
    $_doc->setValue('met', $vet_num_birka);

    if ($card['id_region'] != "1")
        $_doc->setValue('chip', $vet_num_chip);
    $_doc->setValue('datav', $vet_data);
    $_doc->setValue('deg', $vet_d_preparat);
    $_doc->setValue('datad', $vet_d_data);
    $_doc->setValue('vac', $vet_v_preparat);
    $_doc->setValue('datav', $vet_v_data);
    $_doc->setValue('datao', $vet_o_data);
    $_doc->setValue('vet', $sourname.' '.$name1.'. '.$patronymic1.'.');
    
    $_doc->setValue('data1', date("d.m.Y", strtotime($card['date_vipusk'])));
    $_doc->setValue('datav', $card['data']);
    
    // $_doc->addImage('Example.jpg', array('width'=>100, 'height'=>100, 'align'=>'right'));

    $img_Dir_Str = "/assets/files/documents/";
    $img_Dir = $_SERVER['DOCUMENT_ROOT']."/". $img_Dir_Str; 
    @mkdir($img_Dir, 0777);
    $file = str_replace("/","-", "Akt-vibitiya-".date("d-m-Y")).".docx";
    
    $_doc->saveAs($img_Dir.$file);
    
    $file_name = $img_Dir.$file;
    
    header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
    header('Content-Disposition: attachment;filename="'.$file.'"');
    header('Cache-Control: max-age=0');
    readfile($file_name);
?>