<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite80b8b0ca4aa2df8c470a7efbf373b34
{
    public static $prefixLengthsPsr4 = array (
        'D' => 
        array (
            'DocxMerge\\' => 10,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'DocxMerge\\' => 
        array (
            0 => __DIR__ . '/..' . '/krustnic/docx-merge/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
        'DocxMerge\\DocxMerge' => __DIR__ . '/..' . '/krustnic/docx-merge/src/DocxMerge.php',
        'DocxMerge\\DocxMerge\\Docx' => __DIR__ . '/..' . '/krustnic/docx-merge/src/DocxMerge/Docx.php',
        'DocxMerge\\DocxMerge\\Prettify' => __DIR__ . '/..' . '/krustnic/docx-merge/src/DocxMerge/Prettify.php',
        'DocxMerge\\libraries\\TbsZip' => __DIR__ . '/..' . '/krustnic/docx-merge/src/libraries/TbsZip.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite80b8b0ca4aa2df8c470a7efbf373b34::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite80b8b0ca4aa2df8c470a7efbf373b34::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInite80b8b0ca4aa2df8c470a7efbf373b34::$classMap;

        }, null, ClassLoader::class);
    }
}
