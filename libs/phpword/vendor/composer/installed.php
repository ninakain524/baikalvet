<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => NULL,
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => NULL,
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'krustnic/docx-merge' => array(
            'pretty_version' => 'v1.0.1',
            'version' => '1.0.1.0',
            'reference' => '4bfcbcce3af57273f246b05f6ace616b5905c665',
            'type' => 'library',
            'install_path' => __DIR__ . '/../krustnic/docx-merge',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
