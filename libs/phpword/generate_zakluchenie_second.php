<?php 
    require_once('require_word.php');
    
    $name_zip = "zaklucheniya-".date("d-m-Y").".zip";
    $zip = new ZipArchive(); //Создаём объект для работы с ZIP-архивами
    $zip->open($name_zip, ZIPARCHIVE::CREATE); //Открываем (создаём) архив
    
    $array_id = [];
    $array_files = [];

    $numCard = "0"; 
    
    $data_first = $_GET['data_first'];
    $data_second = $_GET['data_second'];
    $getContract = $_GET['contract'];
    $getShelter = $_GET['shelter'];
    $getKinolog = $_GET['kinolog'];
    $getAdress = $_GET['adress'];

    $part_query_kinolog = $part_query_shelter = $part_query_contract = $part_query_adress = "";

    if ($getContract != "all") 
        $part_query_contract = "AND application.id_contract='".$getContract."'";

    if ($getKinolog != "all") 
        $part_query_kinolog = "AND application.id_user='".$getKinolog."'";

    if ($getAdress != "all") {
        $adressQuery = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".intval($getAdress)));
        $part_query_adress = " AND application.loc_type='".$adressQuery['loc_type']."' AND application.locality='".$adressQuery['locality']."' AND application.street='".$adressQuery['street']."' ";
    }

    if ($getShelter != "all") 
        $part_query_shelter = "AND application.id_shelter='".$getShelter."'";
    
    $query = "SELECT 
        application.id AS id,
        application.data AS data,
        application.street AS street,
        application.locality AS locality,
        application.breed AS breed, 
        application.age AS age, 
        application.weight AS weight, 
        application.color AS color,
        application.date_start_vosstanovlenie AS date_start_vosstanovlenie,
        application.date_start_karantin AS date_start_karantin,
        animal_kind.name_kind AS kind,
        animal_gender.name_gender AS gender,
        application_status.id AS status_id,
        application_status.status_name AS status,
        vet_card.num_birka AS num_birka,
        vet_card.num_chip AS num_chip,
        DATE_FORMAT(vet_card.data,'%d.%m.%Y') AS vet_card_data,
        animal_shelters.name_shelter AS name_shelter
        FROM application 
        LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
        LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
        LEFT JOIN application_status ON application.id_status = application_status.id
        RIGHT JOIN vet_card ON application.id_vet_card = vet_card.id
        LEFT JOIN animal_shelters ON application.id_shelter = animal_shelters.id
        WHERE application.data >= '".$data_first."' AND application.data <= '".$data_second."' AND application.show='1' ".$part_query_kinolog." ".$part_query_shelter." ".$part_query_contract." ".$part_query_adress." ORDER BY application.data ASC";

    $res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
    while ($res = mysqli_fetch_array($res_query)) {
        array_push($array_id, $res['id']);
    }
     $j = 1;
    for ($i = 0; $i < count($array_id); $i++) {
        $id = $array_id[$i];
        // $id = 98;
        
        $card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".$id));
        $user = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$card['id_user']));
        $kind = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_kind WHERE id=".$card['id_kind']));
        $gender = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_gender WHERE id=".$card['id_gender']));
        $contract = mysqli_fetch_array(mysqli_query($SERVER, "SELECT contract.num_application AS num_application, contract.number AS number, DATE_FORMAT(contract.date_contract,'%d.%m.%Y') AS date_contract FROM contract WHERE id=".$card['id_contract']));
        
        if ($card['id_contract'] != "0" && $card['id_contract'] != "" && $card['id_contract'] != NULL) 
            $numCard = getNumberCard($id, $card['id_contract'], $SERVER);
      
        if (ctype_digit($card['color'])) {
            $animal_color1 = mysqli_fetch_array(mysqli_query($SERVER, "SELECT name_color FROM animal_color WHERE id=".$card['color']));
            $animal_color = $animal_color1['name_color'];
        }
    	else
    	    $animal_color = $card['color']; 
        
        $vet_card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_card WHERE id_application=".$id));
        $vet_degilmintizaciya = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_degilmintizaciya WHERE id_application=".$id));
        $vet_d = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$vet_degilmintizaciya['id_veterinar']));
        $vet_operation = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_operation WHERE id_application=".$id));
        $vet_o = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$vet_operation['id_veterinar']));
        $vet_vaccine = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_vaccine WHERE id_application=".$id));
        $vet_v = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$vet_vaccine['id_veterinar']));
                
        $vet = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$vet_card['id_veterinar']));
        $name1 = mb_substr($vet['name'], 0, 1);
        $patronymic1 = mb_substr($vet['patronymic'], 0, 1);
        
        $name_d = mb_substr($vet['name'], 0, 1);
        $patronymic_d = mb_substr($vet['patronymic'], 0, 1);
        
        $name_o = mb_substr($vet['name'], 0, 1);
        $patronymic_o = mb_substr($vet['patronymic'], 0, 1);
        
        $name_v = mb_substr($vet['name'], 0, 1);
        $patronymic_v = mb_substr($vet['patronymic'], 0, 1);
        
        if(!empty($card['id_user']) && $card['id_user'] != 0 && $card['id_user'] != "") {
            $names = $user['name'];
            $names1 = mb_substr($names, 0, 1);
            $patronymics = $user['patronymic'];
            $patronymics1 = mb_substr($patronymics, 0, 1);
        }
        
        $phpWord = new  \PhpOffice\PhpWord\PhpWord(); 
        $_doc = new \PhpOffice\PhpWord\TemplateProcessor('template_4.docx');
        $_doc->setValue('data', $contract['date_contract']); 
        $_doc->setValue('num_app', $contract['num_application']); 
        $_doc->setValue('num', $contract['number']);
        $_doc->setValue('city_app', $card['locality']);
        $_doc->setValue('vet', $vet['sourname'].' '.$name1.'. '.$patronymic1.'.');
        
        if(!empty($card['id_user']) && $card['id_user'] != 0 && $card['id_user'] != "") 
            $_doc->setValue('rab', $user['sourname'].' '.$names1.'. '.$patronymics1.'.');
        else
            $_doc->setValue('rab', '');
        
        $_doc->setValue('id', $numCard); 
        $_doc->setValue('data_z', date('d.m.Y', $card['data'])); 
        $_doc->setValue('vid', $kind['name_kind']); 
        $_doc->setValue('poroda', $card['breed']); 
        $_doc->setValue('gender', $gender['name_gender']); 
        $_doc->setValue('color', $animal_color); 
        $_doc->setValue('age', $card['age']); 
        $_doc->setValue('weight', $card['weight']);
        
        $_doc->setValue('comment', $card['comment']);
        
        $_doc->setValue('data_mer', (new DateTime($vet_card['data']))->format('d.m.Y'));
        $_doc->setValue('metka', $vet_card['num_birka']);
        $_doc->setValue('chip', $vet_card['num_chip']);
        $_doc->setValue('data_d', (new DateTime($vet_degilmintizaciya['data']))->format('d.m.Y'));
        $_doc->setValue('preparat_d', $vet_degilmintizaciya['preparat']);
        $_doc->setValue('vet_d', $vet_d['sourname'].' '.$name_d.'. '.$patronymic_d.'.');
        
        $_doc->setValue('data_v', (new DateTime($vet_vaccine['data']))->format('d.m.Y'));
        $_doc->setValue('preparat_v', $vet_vaccine['preparat']);
        $_doc->setValue('vet_v', $vet_v['sourname'].' '.$name_v.'. '.$patronymic_v.'.');
        
        $_doc->setValue('data_o', (new DateTime($vet_operation['data']))->format('d.m.Y'));
        $_doc->setValue('vet_o', $vet_o['sourname'].' '.$name_o.'. '.$patronymic_o.'.');
        
    
        $img_Dir_Str = "/assets/files/documents/";
        $img_Dir = $_SERVER['DOCUMENT_ROOT']."/". $img_Dir_Str; 
        @mkdir($img_Dir, 0777);
        $file = str_replace("/","-", "Zakluchenie-vtorichnoe-".date("d-m-Y"))."-".$j.".docx";
        
        $_doc->saveAs($img_Dir.$file);
        
        $file_name = $img_Dir.$file;
        
        // header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        // header('Content-Disposition: attachment;filename="'.$file.'"');
        // header('Cache-Control: max-age=0');
        // readfile($file_name);
    
        $zip->addFile($file_name,basename($file_name)); 
        
        array_push($array_files, $file_name);
        $j = $j + 1;
    }
    
    $zip->close(); //Завершаем работу с архивом
    
    header('Content-Type: application/zip');
    header('Content-Disposition: attachment;filename="'.$name_zip.'"');
    readfile($name_zip);
    unlink($name_zip);
    
    for ($i = 0; $i < count($array_files); $i++) {
        unlink($array_files[$i]);
    }
?>