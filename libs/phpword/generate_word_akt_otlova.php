<?php 
    require_once('require_word.php');
    
    $data_first = $_GET['data_first'];
    $data_second = $_GET['data_second'];
    $getContract = $_GET['contract'];
    $getShelter = $_GET['shelter'];
    $getKinolog = $_GET['kinolog'];
    $getAdress = $_GET['adress'];

    $part_query_kinolog = $part_query_shelter = $part_query_contract = $part_query_adress = "";

    if ($getContract != "all") 
        $part_query_contract = "AND id_contract='".$getContract."'";

    if ($getKinolog != "all") 
        $part_query_kinolog = "AND id_user='".$getKinolog."'";

    if ($getAdress != "all") {
        $adressQuery = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".$getAdress));
        $part_query_adress = " AND application.loc_type='".$adressQuery['loc_type']."' AND application.locality='".$adressQuery['locality']."' AND application.street='".$adressQuery['street']."' ";
    }

    if ($getShelter != "all") 
        $part_query_shelter = "AND id_shelter='".$getShelter."'";
    
    $mesto = $_POST['mesto'];
    $kinolog = $_POST['kinolog'];
    $count_animals_get = $_POST['count_animals_get'];
    $count_animals_shelter = $_POST['count_animals_shelter'];
    $admin_shelter = $_POST['admin_shelter'];
    $count_animals_die = $_POST['count_animals_die'];
    $comment = $_POST['comment'];
    $zakazchik_name = $_POST['zakazchik_name'];
    $zakazchik_role = $_POST['zakazchik_role'];
        
    $card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `application` WHERE `data` >= '".$data_first."' AND `data` <= '".$data_second."' AND `show`='1' ".$part_query_kinolog." ".$part_query_shelter." ".$part_query_contract." ".$part_query_adress." ORDER BY `id` DESC"));
    $user = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$card['id_user']));
    $contract = mysqli_fetch_array(mysqli_query($SERVER, "SELECT contract.number AS number, DATE_FORMAT(contract.date_contract,'%d.%m.%Y') AS date_contract, contract.name_tz AS name_tz, contract.num_application AS num_application FROM contract WHERE id=".$card['id_contract']));

    $gata_day = $contract['date_contract'];
    if ($gata_day == "00.00.0000") $gata_day = "";
    $name = $user['name'];
    $name1 = mb_substr($name, 0, 1);
    $patronymic = $user['patronymic'];
    $patronymic1 = mb_substr($patronymic, 0, 1);
    
    $phpWord = new  \PhpOffice\PhpWord\PhpWord(); 
    $_doc = new \PhpOffice\PhpWord\TemplateProcessor('template.docx');
    $_doc->setValue('num_app', $contract['num_application']);
    $_doc->setValue('city_app', $contract['name_tz']);

    $_doc->setValue('data_day', $gata_day); 
    $_doc->setValue('num', $contract['number']); 
    $_doc->setValue('adress', $mesto); 
    $_doc->setValue('kinolog', $kinolog); 
    $_doc->setValue('count_animals_get', $count_animals_get); 
    $_doc->setValue('count_animals_shelter', $count_animals_shelter); 
    $_doc->setValue('admin_shelter', $admin_shelter); 
    $_doc->setValue('count_animals_die', $count_animals_die); 
    $_doc->setValue('comment', $comment); 
    $_doc->setValue('zakazchik_name', $zakazchik_name); 
    $_doc->setValue('zakazchik_role', $zakazchik_role);
    
    $img_Dir_Str = "/assets/files/documents/";
    $img_Dir = $_SERVER['DOCUMENT_ROOT']."/". $img_Dir_Str; 
    @mkdir($img_Dir, 0777);
    $file = str_replace("/","-", "Akt-otlova-".date("d-m-Y")).".docx";
    
    $_doc->saveAs($img_Dir.$file);
    
    $file_name = $img_Dir.$file;
    
    header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
    header('Content-Disposition: attachment;filename="'.$file.'"');
    header('Cache-Control: max-age=0');
    readfile($file_name);
?>