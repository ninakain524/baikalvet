<?php 
    require_once('require_word.php');
    require "vendor/autoload.php";

    $name_zip = "Akti-otlova-".date("d-m-Y").".zip";
    $zip = new ZipArchive(); //Создаём объект для работы с ZIP-архивами
    $zip->open($name_zip, ZIPARCHIVE::CREATE); //Открываем (создаём) архив
    
    $array_id = $array_files = $arrayCount = $adressArray = $countA = $arrayDate = [];

    $numCard = "0"; 
    
    $data_first = $_GET['data_first']; //начальная дата 00:00
    $data_second = $_GET['data_second']; //конечная дата 23:59
    $getContract = $_GET['contract'];
    $getShelter = $_GET['shelter'];
    $getKinolog = $_GET['kinolog'];
    $getAdress = $_GET['adress'];

    $name_merge_file = "Akti-otlova-".date("d_m_Y", $data_first)."-".date("d_m_Y", $data_second).".docx";

    $period = new DatePeriod(
        new DateTime(date("d.m.Y H:i:s", $data_first)),
        new DateInterval('P1D'),
        new DateTime(date("d.m.Y H:i:s", $data_second))
        
    );
        
    $dates = array();
    foreach ($period as $key => $value) {
        $dates[] = $value->format('Y-m-d');     
    }

    $id_contract = 0;

    $part_query_kinolog = $part_query_shelter = $part_query_contract = $part_query_adress = "";

    if ($getContract != "all") 
        $part_query_contract = "AND id_contract='".$getContract."'";

    if ($getKinolog != "all") 
        $part_query_kinolog = "AND id_user='".$getKinolog."'";

    if ($getAdress != "all") {
        $adressQuery = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".intval($getAdress)));
        $part_query_adress = " AND application.loc_type='".$adressQuery['loc_type']."' AND application.locality='".$adressQuery['locality']."' AND application.street='".$adressQuery['street']."' ";
    }
    if ($getShelter != "all") 
        $part_query_shelter = "AND id_shelter='".$getShelter."'";
    
    $admin_shelter = $_POST['admin_shelter'];
    $count_animals_die = $_POST['count_animals_die'];
    $comment = $_POST['comment'];
    $zakazchik_name = $_POST['zakazchik_name'];
    $zakazchik_role = $_POST['zakazchik_role'];
        
    $query = mysqli_query($SERVER, "SELECT * FROM `application` WHERE `data` >= '".$data_first."' AND `data` <= '".$data_second."' AND `show`='1' ".$part_query_kinolog." ".$part_query_shelter." ".$part_query_contract." ".$part_query_adress." ORDER BY `id` ASC");
    while ($row = mysqli_fetch_array($query)) {
        array_push($arrayCount, $row['id_user']);
        array_push($arrayDate, date("Y-m-d", $row['data']));
    }
    $kin_name = array_unique($arrayCount);
    $dates = array_unique($arrayDate);
    $w = 1;
    foreach($dates as $day) {
        $j = 1;
        $day_start = strtotime($day."T00:00:00");
        $day_end = strtotime($day."T23:59:59");
        if(array_search($day, $dates) !== NULL) {
            foreach($kin_name as $kinolog_id) {
                $i = 0;
                $adressArray = [];
                $card = mysqli_query($SERVER, "SELECT 
                application.id AS id, application.data AS data, application.street AS street, application.loc_type AS loc_type,
                application.locality AS locality, application.breed AS breed, application.age AS age, application.id_user AS id_user, 
                application.weight AS weight, application.color AS color, application.date_start_vosstanovlenie AS date_start_vosstanovlenie,
                application.date_start_karantin AS date_start_karantin, animal_kind.name_kind AS kind, animal_gender.name_gender AS gender,
                application_status.id AS status_id, application_status.status_name AS status, vet_card.num_birka AS num_birka,
                vet_card.num_chip AS num_chip, DATE_FORMAT(vet_card.data,'%d.%m.%Y') AS vet_card_data, animal_shelters.name_shelter AS name_shelter,
                contract.number AS contract_number, contract.customer AS contract_customer, contract.name_contract AS name_contract,
                DATE_FORMAT(contract.date_contract,'%d.%m.%Y') AS date_contract, users.sourname AS sourname, users.name AS name,
                users.patronymic AS patronymic, application.id_contract AS id_contract FROM `application` 
                LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
                LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
                LEFT JOIN application_status ON application.id_status = application_status.id
                LEFT JOIN vet_card ON application.id = vet_card.id_application
                LEFT JOIN animal_shelters ON application.id_shelter = animal_shelters.id
                LEFT JOIN users ON application.id_user = users.id
                LEFT JOIN contract ON application.id_contract = contract.id
                WHERE application.id_user ='".intval($kinolog_id)."' 
                AND application.data >= '".$day_start."' AND application.data <= '".$day_end."' AND application.show='1' 
                ".$part_query_kinolog." ".$part_query_shelter." ".$part_query_contract." ".$part_query_adress." ORDER BY application.data ASC");
                
                $adress1 = $item_adress = $street = $kinolog_name = $count = "";
                while ($row = mysqli_fetch_array($card)) {
                    if (($row['street'] != "") && ($street == $row['street'])){
                        $item_adress = "";
                        $street = "";
                    }
                    else{
                        $street = $row['street'];
                        $item_adress = $row['loc_type'] . "." . $row['locality'] . ", " . $street . "; "; 
                    }
                    array_push($adressArray, $item_adress);

                    $name = $row['name'];
                    $name1 = mb_substr($name, 0, 1);
                    $patronymic = $row['patronymic'];
                    $patronymic1 = mb_substr($patronymic, 0, 1);
                    $kinolog_name = $row['sourname']. " " .$name1. ". " .$patronymic1. ".";
                    $id_contract = $row['id_contract'];
                }
                if($kinolog_name != "") {
                $adress1 = implode('', array_unique($adressArray)); //удалить дубликаты и в строку

                $countQuery = mysqli_fetch_array(mysqli_query($SERVER, "SELECT COUNT(1) FROM application 
                LEFT JOIN animal_kind ON application.id_kind = animal_kind.id
                LEFT JOIN animal_gender ON application.id_gender = animal_gender.id
                LEFT JOIN application_status ON application.id_status = application_status.id
                LEFT JOIN vet_card ON application.id = vet_card.id_application
                LEFT JOIN animal_shelters ON application.id_shelter = animal_shelters.id
                LEFT JOIN contract ON application.id_contract = contract.id
                WHERE application.id_user = '".intval($kinolog_id)."' AND application.data >= '".$day_start."' AND application.data <= '".$day_end."' AND application.show='1'".$part_query_kinolog."".$part_query_shelter."".$part_query_contract."".$part_query_adress."ORDER BY application.data ASC"));
                $count = $countQuery[0];

                $id = $arrayCount[$i];
                $contract = mysqli_fetch_array(mysqli_query($SERVER, "SELECT contract.number AS number, DATE_FORMAT(contract.date_contract,'%d.%m.%Y') AS date_contract, contract.name_tz AS name_tz, contract.num_application AS num_application FROM contract WHERE id=".$id_contract));
            
                $gata_day = $contract['date_contract'];
                if ($gata_day == "00.00.0000") $gata_day = "";
                
                $phpWord = new  \PhpOffice\PhpWord\PhpWord(); 
                $_doc = new \PhpOffice\PhpWord\TemplateProcessor('template.docx');
                $_doc->setValue('num_app', $contract['num_application']);
                $_doc->setValue('city_app', $contract['name_tz']);

                $_doc->setValue('data_day', date("d.m.Y", strtotime($day))); 
                $_doc->setValue('num', $w); 
                $_doc->setValue('adress', $adress1); 
                $_doc->setValue('kinolog', $kinolog_name); 
                $_doc->setValue('count_animals_get', $count); 
                $_doc->setValue('count_animals_shelter', $count); 
                $_doc->setValue('admin_shelter', $admin_shelter); 
                $_doc->setValue('count_animals_die', $count_animals_die); 
                $_doc->setValue('comment', $comment); 
                $_doc->setValue('zakazchik_name', $zakazchik_name); 
                $_doc->setValue('zakazchik_role', $zakazchik_role);
                
                $img_Dir_Str = "/assets/files/documents/";
                $img_Dir = $_SERVER['DOCUMENT_ROOT']."/". $img_Dir_Str; 
                @mkdir($img_Dir, 0777);
                $file = str_replace("/","-", "Akt-otlova-".date("d-m-Y"))."-".$j."-".$w.".docx";
                
                $_doc->saveAs($img_Dir.$file);
                
                $file_name = $img_Dir.$file;
                
                    // $zip->addFile($file_name,basename($file_name)); 
                
                array_push($array_files, $file_name);
                $j = $j + 1;
            }
            }
            $w = $w + 1;
        }
    }
    // var_dump($array_files);
    
    $path_name_merge = $img_Dir.$name_merge_file;
    use DocxMerge\DocxMerge;
    $dm = new DocxMerge();
    $dm->merge( $array_files, $path_name_merge );

    $zip->addFile($path_name_merge,basename($path_name_merge)); 

    $zip->close(); //Завершаем работу с архивом
    
    header('Content-Type: application/zip');
    header('Content-Disposition: attachment;filename="'.$name_zip.'"');
    readfile($name_zip);
    unlink($name_zip);
    
    for ($i = 0; $i < count($array_files); $i++) {
        unlink($array_files[$i]);
    }
?>