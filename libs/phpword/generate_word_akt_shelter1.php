<?php 
    require_once('require_word.php');
    
    $id = $_GET['application'];
    
    $numCard = "0";  
    $card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".$id));
    $user = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$card['id_user']));
    $kind = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_kind WHERE id=".$card['id_kind']));
    $gender = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_gender WHERE id=".$card['id_gender']));
    $contract = mysqli_fetch_array(mysqli_query($SERVER, "SELECT contract.number AS number, DATE_FORMAT(contract.date_contract,'%d.%m.%Y') AS date_contract, contract.name_tz AS name_tz, contract.num_application AS num_application FROM contract WHERE id=".$card['id_contract']));
    
    if ($card['id_contract'] != "0" && $card['id_contract'] != "" && $card['id_contract'] != NULL) 
        $numCard = getNumberCard($id, $card['id_contract'], $SERVER);

    if (ctype_digit($card['color'])) {
        $animal_color1 = mysqli_fetch_array(mysqli_query($SERVER, "SELECT name_color FROM animal_color WHERE id=".$card['color']));
        $animal_color = $animal_color1['name_color'];
    }
	else
	    $animal_color = $card['color']; 
    
    $vet_card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM vet_card WHERE id=".$card['id_vet_card']));
    $vet = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$vet_card['id_veterinar']));
        
    $name = $vet['name'];
    $name1 = mb_substr($name, 0, 1);
    $patronymic = $vet['patronymic'];
    $patronymic1 = mb_substr($patronymic, 0, 1);
    
    $phpWord = new  \PhpOffice\PhpWord\PhpWord(); 
    $_doc = new \PhpOffice\PhpWord\TemplateProcessor('template_1.docx');
    $_doc->setValue('data', $contract['date_contract']);  
    $_doc->setValue('num', $contract['number']);
    $_doc->setValue('num_app', $contract['num_application']);
    $_doc->setValue('city_app', $contract['name_tz']);
    $_doc->setValue('id', $numCard); 
    $_doc->setValue('vid', $kind['name_kind']); 
    $_doc->setValue('poroda', $card['breed']); 
    $_doc->setValue('gender', $gender['name_gender']); 
    $_doc->setValue('color', $animal_color); 
    $_doc->setValue('age', $card['age']); 
    $_doc->setValue('weight', $card['weight']);
    $_doc->setValue('data_s', date("d.m.Y"));  
    
    $_doc->setValue('comment', $card['comment']);
    
    $_doc->setValue('vet', $vet['sourname'].' '.$name1.' '.$patronymic1);

    $img_Dir_Str = "/assets/files/documents/";
    $img_Dir = $_SERVER['DOCUMENT_ROOT']."/". $img_Dir_Str; 
    @mkdir($img_Dir, 0777);
    $file = str_replace("/","-", "Akt-vibitiya-".date("d-m-Y")).".docx";
    
    $_doc->saveAs($img_Dir.$file);
    
    $file_name = $img_Dir.$file;
    
    header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
    header('Content-Disposition: attachment;filename="'.$file.'"');
    header('Cache-Control: max-age=0');
    readfile($file_name);
?>