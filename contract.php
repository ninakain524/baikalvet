<?php
    require_once("php/config.php");
    require_once("php/timezone.php");
    require_once("php/functions.php");
    if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
    require_once("php/update_status.php");
    require_once("template/head.html");
    require_once("template/header.php");
    
    $today = date("Y-m-d");
?>

<!--Заполнить контракт-->
    <div id="hide-form-popup-add-contract" class="hide-form-popup-add-contract hideform fancy-animate">
        <form id="popup-add-contract" enctype="multipart/form-data" method="post" action="/php/addContract.php" >
            <h3>Новый контракт</h3>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="customer">Заказчик</label>
                <input class="field width-add" type="text" name="customer" id="customer" />
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">Руководитель заказчика (ФИО)</label>
                <input class="field width-add field-date" type="text" name="person_customer" id="person_customer" autocomplete="on"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="deadline">Срок сдачи</label>
                <input class="field width-add" type="text" name="deadline" id="deadline" />
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="term_services">Срок оказания услуг</label>
                <input class="field width-add field-date" type="date" name="term_services" id="term_services" autocomplete="on" value="<?php echo $today; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="name">Рабочее наименование</label>
                <input class="field width-add" type="text" name="name" id="name" required/>
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="name">Полное наименование</label>
                <input class="field width-add" type="text" name="full-name" id="full-name" />
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="number">Номер контракта</label>
                <input class="field width-add" type="text" name="number" id="number" />
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">Дата контракта</label>
                <input class="field width-add field-date" type="date" name="date_contract" id="date_contract" autocomplete="on" value="<?php echo $today; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label for="region">Регион</label>
        		    <select class="field" id="region" name="region" required autofocus>
                        <?php
                          $sql_region = "SELECT * FROM `region`;";
                          $res_region = mysqli_query($SERVER, $sql_region) or die("Ошибка " . mysqli_error($SERVER));
                          echo '<option value="" selected readonly hidden>Выберите регион</option>';
        
                          while($row_region = mysqli_fetch_array($res_region))
                          {
                            echo '<option value="'. $row_region['id'] .'">'. $row_region['name_region'] .'</option>';
                          }
                        ?>
                    </select> 
                </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">Название огранизации</label>
                <input class="field width-add field-date" type="text" name="name_organization" id="name_organization" autocomplete="on"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">Индекс</label>
                <input class="field width-add field-date" type="number" name="index_a" id="index_a" autocomplete="on"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">Адрес</label>
                <input class="field width-add field-date" type="text" name="adress" id="adress" autocomplete="on"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">ИНН</label>
                <input class="field width-add field-date" type="number" name="inn" id="inn" autocomplete="on"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">КПП</label>
                <input class="field width-add field-date" type="number" name="kpp" id="kpp" autocomplete="on"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">ОГРН</label>
                <input class="field width-add field-date" type="number" name="ogrn" id="ogrn" autocomplete="on"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">Телефон</label>
                <input class="field width-add field-date" type="text" name="phone" id="phone" autocomplete="on"  />  
              </div>
            </div>

            <div class="col-12 col-sm-12">
              <div class="form-group">
                <label for="doc">Файл договора</label>
                <input type="file" class="form-control" name="doc" id="doc" accept=".doc, .docx, .pdf" />
              </div>
            </div>

            <div class="col-12 col-sm-12">
              <h3>Данные для печатных форм (верхний правый угол)</h3>
            </div>

            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="num_application">Номер приложения</label>
                <input class="field width-add field-date" type="text" name="num_application" id="num_application"  />  
              </div>
            </div>

            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="name_tz">Название контракта/договора</label>
                <input class="field width-add field-date" type="text" name="name_tz" id="name_tz"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-12">
              <div class="form-group">
                <label for="comment">Комментарий</label>
                <textarea class="field width-add" name="comment" id="comment"></textarea>
              </div>
            </div>
            
            <div class="col-12 col-sm-12">
                <input class="btn button-auth field-submit field-width" type="submit" value="Сохранить" autofocus />
            </div>
        </form>
    </div>
<!--/Заполнить контракт-->

<!--Редактировать контракт-->
    <? if (isset($_GET['edit-contract'])) { ?>
    <div id="hide-form-popup-edit-contract" class="hide-form-popup-edit-contract hideform fancy-animate">
        <form id="popup-edit-contract" method="post" enctype="multipart/form-data" action="/php/editContract.php?contract=<? echo ($_GET['edit-contract']) ?>">
            <?  $contract = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM contract WHERE id=".$_GET['edit-contract']));
                $region = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM region WHERE id=".$contract['id_region']));?>
            
            <h3>Редактирование контракта</h3>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="customer_edit">Заказчик</label>
                <input class="field width-add" type="text" name="customer_edit" id="customer_edit" value="<?php echo $contract['customer']; ?>" />
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">Руководитель заказчика (ФИО)</label>
                <input class="field width-add field-date" type="text" name="person_customer_edit" id="person_customer_edit" value="<?php echo $contract['person_customer']; ?>" autocomplete="on"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="deadline_edit">Срок сдачи</label>
                <input class="field width-add" type="text" name="deadline_edit" id="deadline_edit" value="<?php echo $contract['deadline']; ?>" />
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="term_services_edit">Срок оказания услуг</label>
                <input class="field width-add field-date" type="date" name="term_services_edit" id="term_services_edit" autocomplete="on" value="<?php echo $contract['term_services']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="name">Рабочее наименование</label>
                <input class="field width-add" type="text" name="name" id="name" value="<?php echo $contract['name_contract']; ?>" required/>
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="name">Полное наименование</label>
                <input class="field width-add" type="text" name="full-name_edit" id="full-name_edit" value="<?php echo $contract['full_name']; ?>" />
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="number_edit">Номер контракта</label>
                <input class="field width-add" type="text" name="number_edit" id="number_edit" value="<?php echo $contract['number']; ?>" />
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract_edit">Дата контракта</label>
                <input class="field width-add field-date" type="date" name="date_contract_edit" id="date_contract_edit" autocomplete="on" value="<?php echo $contract['date_contract']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label for="region">Регион</label>
        		    <select class="field" id="region" name="region" required autofocus>
                        <?php
                          $sql_region = "SELECT * FROM `region`;";
                          $res_region = mysqli_query($SERVER, $sql_region) or die("Ошибка " . mysqli_error($SERVER));
                          echo '<option value="'. $contract['id_region'] .'" selected readonly hidden>'.$region['name_region'].'</option>';
        
                          while($row_region = mysqli_fetch_array($res_region))
                          {
                            echo '<option value="'. $row_region['id'] .'">'. $row_region['name_region'] .'</option>';
                          }
                        ?>
                    </select> 
                </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">Название огранизации</label>
                <input class="field width-add field-date" type="text" name="name_organization_edit" id="name_organization_edit" autocomplete="on" value="<?php echo $contract['name_organization']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">Индекс</label>
                <input class="field width-add field-date" type="number" name="index_a_edit" id="index_a_edit" autocomplete="on" value="<?php echo $contract['index_a']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">Адрес</label>
                <input class="field width-add field-date" type="text" name="adress_edit" id="adress_edit" autocomplete="on" value="<?php echo $contract['adress']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">ИНН</label>
                <input class="field width-add field-date" type="number" name="inn_edit" id="inn_edit" autocomplete="on" value="<?php echo $contract['inn']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">КПП</label>
                <input class="field width-add field-date" type="number" name="kpp_edit" id="kpp_edit" autocomplete="on" value="<?php echo $contract['kpp']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">ОГРН</label>
                <input class="field width-add field-date" type="number" name="ogrn_edit" id="ogrn_edit" autocomplete="on" value="<?php echo $contract['ogrn']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="date_contract">Телефон</label>
                <input class="field width-add field-date" type="text" name="phone_edit" id="phone_edit" autocomplete="on" value="<?php echo $contract['phone']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label for="region">Статус</label>
        		    <select class="field" id="status" name="status"  autofocus>
                        <? echo '<option value="'. $contract['status'] .'" selected readonly hidden>'.$contract['status'].'</option>'; ?>
                        <option value="открыт">открыт</option>
                        <option value="закрыт">закрыт</option>
                    </select> 
                </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="doc">Файл договора</label>
                <input type="file" class="form-control" name="doc_edit" id="doc_edit" accept=".doc, .docx, .pdf" />
              </div>
            </div>
            
            <div class="col-12 col-sm-12">
              <h3>Данные для печатных форм (верхний правый угол)</h3>
            </div>

            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="num_application_edit">Номер приложения</label>
                <input class="field width-add field-date" type="text" name="num_application_edit" id="num_application_edit" value="<?php echo $contract['num_application']; ?>"  />  
              </div>
            </div>

            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="name_tz_edit">Название контракта/договора</label>
                <input class="field width-add field-date" type="text" name="name_tz_edit" id="name_tz_edit" value="<?php echo $contract['name_tz']; ?>"  />  
              </div>
            </div>
            
            <div class="col-12 col-sm-12">
              <div class="form-group">
                <label for="comment_edit">Комментарий</label>
                <textarea class="field width-add" name="comment_edit" id="comment_edit" value="<?php echo $contract['comment']; ?>"></textarea>
              </div>
            </div>
            
            <div class="col-12 col-sm-12">
              <input class="btn button-auth field-submit field-width" type="submit" value="Сохранить" autofocus />
            </div>
            
        </form>
    </div>
    <? } ?>
<!--/Редактировать контракт-->

<!-- Content -->
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="content-right" class="content-right">
          <div id="content-right-table" class="content-right-table table-responsive">
            <div class="menu-table">
              <div class="table-caption">Контракты<div class="rule-answer"><a href="#">?</a></div></div>  
              
              <div class="menu-table-filter">
                  <ul class='subsubsub'>
                    <li>
                        <a href="?add-contract">Добавить</a>
                    </li>
                  </ul>
                  <button class="btn-deletebtn" id="delete_selected"><i class="fa fa-trash"></i>Удалить</button>
              </div>
            </div>
            <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab tab-contract">
              <tr class="tab-col1">
                <th>№</th>
                <th style="width: 120px;">Регион</th>
                <th>Заказчик</th>
                <th>Рабочее<br>наименование</th>
                <th>Полное<br>наименование</th>
                <th>Срок сдачи</th>
                <th>Срок оказания услуг</th>
                <th>Номер</th>
                <th>Дата контракта</th>
                <th>Комментарий</th>
                <th>Статус</th>
                <th>Действия</th>
              </tr>

              <?php  
                $query = "SELECT 
                    contract.id AS id,
                    contract.customer AS customer,
                    contract.name_contract AS name_contract,
                    contract.deadline AS deadline,
                    DATE_FORMAT(contract.term_services,'%d.%m.%Y') AS term_services,
                    contract.number AS number,
                    DATE_FORMAT(contract.date_contract,'%d.%m.%Y') AS date_contract,
                    contract.comment AS comment,
                    contract.status AS status,
                    contract.id_region AS id_region,
                    region.name_region AS name_region,
                    contract.full_name AS full_name
                    FROM contract 
                    LEFT JOIN region ON contract.id_region = region.id 
                    WHERE contract.show_contract=1 ORDER BY contract.id DESC";

                $res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
                
                $i = 1;
                while ($res = mysqli_fetch_array($res_query)) {
                    echo "<tr class='application' data='".$res['id']."' data-href='contract_card.php?contract=".$res['id']."'>
                    <td><div class='id-color'>{$i}</div></td>
                    <td>{$res['name_region']}</td>
                    <td>{$res['customer']}</td>
                    <td>{$res['name_contract']}</td>
                    <td>{$res['full_name']}</td>
                    <td>{$res['deadline']}</td>
                    <td>{$res['term_services']}</td>
                    <td>{$res['number']}</td>
                    <td>{$res['date_contract']}</td>
                    <td>{$res['comment']}</td>
                    <td>{$res['status']}</td>
                    <td><a href='?edit-contract={$res['id']}'>Редактировать</a></td>";
                    $i = $i + 1;
                }
              ?>
            </table>

          </div>  
        </div>
      </div> 
    </div> 
  </div>
</section>

    <? if (isset($_GET['add-contract'])) { ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-add-contract',
              type : 'inline'
           });
      </script>
    <? } ?>
    
    <? if (isset($_GET['edit-contract'])) { ?>
      <script type="text/javascript">
          $.fancybox.open({
              src  : '#hide-form-popup-edit-contract',
              type : 'inline'
           });
      </script>
    <? } ?>

<script>
    $(document).on("dblclick", 'tr[data-href]',function(e) {
        window.open($(this).data('href'), '_self');
    });
    $(document).on("click", 'tr[data]',function(e) {
        if ($(this).hasClass('del_app_checkbox')) {
          $(this).css("background-color", "#fff");
          $(this).removeClass('del_app_checkbox');
        }
        else{
          $(this).css("background-color", "#c0ccda");
          $(this).addClass('del_app_checkbox');
        }
    });
  
    $('body').on('click', '#delete_selected', function(){
        var checked = [];
        $('.del_app_checkbox').each(function() {
            checked.push($(this).attr('data'));
        });
        if (checked.length > 0) {
            if (confirm("Удалить выбранные контракты?")){
                $.ajax({
                  type: 'POST',
                  url: 'php/admin.php?delete_contracts',
                  dataType: 'json',
                  data: {checked : JSON.stringify(checked)},
                  cache: false,
                  success: function(res){
                    console.log(res)
                    if (res.res == "success") {
                      console.log(res.res);
                      location.reload();
                    }
                    else {
                      alert("Возникла ошибка. Страница будет перезагружена");
                      console.log(res.res);//location.reload();
                    }
                  },
                  error: function(res){
                    console.log(res);
                  }
                });
            }
        }
        else{
            alert("Пользователи не выбраны.");
        }
    })
    // Phone
    window.addEventListener("DOMContentLoaded", function() {
    	function setCursorPosition(pos, elem) {
    		elem.focus();
    		if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
    		else if (elem.createTextRange) {
    			var range = elem.createTextRange();
    			range.collapse(true);
    			range.moveEnd("character", pos);
    			range.moveStart("character", pos);
    			range.select()
    		}
    	}
    	function mask(event) {
    		var matrix = "+7 (___) ___-__-__",
    		i = 0,
    		def = matrix.replace(/\D/g, ""),
    		val = this.value.replace(/\D/g, "");
    		if (def.length >= val.length) val = def;
    		this.value = matrix.replace(/./g, function(a) {
    			return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
    		});
    		if (event.type == "blur") {
    			if (this.value.length == 2) this.value = ""
    		} else setCursorPosition(this.value.length, this)
    	};	
    
    	var input = document.querySelector("#phone");
    	input.addEventListener("input", mask, false);
    	input.addEventListener("focus", mask, false);
    	input.addEventListener("blur", mask, false);
    	
    	var input1 = document.querySelector("#phone_edit");
    	input1.addEventListener("input", mask, false);
    	input1.addEventListener("focus", mask, false);
    	input1.addEventListener("blur", mask, false);
    });
    // /Phone
</script>
<!-- /Content -->

<?php
  require_once("template/footer.html");
?>