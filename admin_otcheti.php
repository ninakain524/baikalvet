<?php
    require_once("php/config.php");
    require_once("php/timezone.php");
    if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
    require_once("php/update_status.php");
    require_once("template/head.html");
    require_once("template/header.php");
    require_once("php/functions.php");
    $today = date("Y-m-d\TH:i:s");
    $today1 = date("Y-m-d");
    
    function format_interval(DateInterval $interval) {
        $result = "";
        if ($interval->d) { $result .= $interval->format("%d д. "); }
        if ($interval->h) { $result .= $interval->format("%h ч. "); }
        if ($interval->i) { $result .= $interval->format("%i мин. "); }
    
        return $result;
    };
?>

<!-- Content -->
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="content-right" class="content-right">
          <div id="content-right-table" class="content-right-table table-responsive">
            <div class="menu-table">
              <div class="table-caption">Администратор<div class="rule-answer"><a href="#">?</a></div></div>
                <div class="menu-table-filter">
                    <select class="field-filter" id="show_contract" name="show_contract">
                    <? 
                        switch ($_SESSION['id_region']) {
                          case 'all':
                              $region = '';
                              break;
                          case '':
                              $region = '';
                              break;
                          default:
                              $region = ' AND id_region='.$_SESSION['id_region'];
                              break;
                        }
                        $sql1 = "SELECT 
                            contract.id AS id,
                            contract.name_contract AS name_contract
                            FROM `contract` WHERE `show_contract`=1".$region." ORDER BY contract.id DESC;";
                        $res1 = mysqli_query($SERVER, $sql1) or die("Ошибка " . mysqli_error($SERVER));
                        echo '<option value="all">Все контракты</option>';
                        
                        while($row1 = mysqli_fetch_array($res1))
                        {
                            $selected = $_SESSION['show_contract_otchet'] == $row1['id'] ? 'selected' : '';
                            echo '<option value="'.$row1['id'].'" '.$selected.'>'.$row1['name_contract'].'</option>';
                        }
                    ?>
                  </select>
                  <select class="field-filter" id="id_shelter" name="id_shelter">
                    <?php
                      $sql = "SELECT * FROM `animal_shelters` WHERE `actual`='1';";
                      $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                      echo '<option value="all">Все приюты</option>';
                      while($row = mysqli_fetch_array($res))
                      {
                        $selected = $_SESSION['filter_id_shelter'] == $row['id'] ? 'selected' : '';
                        echo '<option value="'.$row['id'].'" '.$selected.'>'.$row['name_shelter'].'</option>';
                      }
                    
                    ?>
                  </select>  
                  <label for="sort_by" class="hide-mobile">Выберите период: </label>
                  <input type="date" name="data_first" id="data_first" class="field-filter" value="<?php echo $today1; ?>" autocomplete="off"/>
                  <input type="date" name="data_second" id="data_second" class="field-filter" value="<?php echo $today1; ?>" autocomplete="off"/>
                  <input id="display_period" name="display-set" type="submit" value="Показать" />
                </div>
            </div>
            <div id="content-table">
                <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab tab-vet">
                  <tr class="tab-col1">
                    <th rowspan="2">№</th>
                    <th rowspan="2">Дата отлова</th>
                    <th rowspan="2">Место отлова</th>
                    <!-- <th rowspan="2">Населенный пункт отлова</th>
                    <th rowspan="2">Улица отлова</th> -->
                    <th rowspan="2">Приют</th>
                    <th rowspan="2">№ вольера</th>
                    <th colspan="6">Описание животного</th>
                    <th rowspan="2">Статус</th>
                  </tr>
                  <tr class="tab-col1">
                    <th>Вид</th>
                    <th>Порода</th>
                    <th>Пол</th>
                    <th>Окрас</th>
                    <th>Возраст ≈</th>
                    <th>Вес ≈</th>
                  </tr>
                </table>
            </div>
          </div>  
        </div>
      </div> 
    </div> 
  </div>
</section>
<!-- /Content -->
<script type="text/javascript">
  $('tr[data-href]').on("click", function() {
    document.location = $(this).data('href');
  });
  
  $("#display_period").click(function() {
        var data_def = "";
        var data_first1 = "";
        var data_first = "";
        var data_second1 = "";
        var data_second =  "";
        var show_contract = $('#show_contract').val();
        var id_shelter = $('#id_shelter').val();
        $("#content-table").empty();
        
        data_def = document.getElementById('data_first').value;
        data_first1 = document.getElementById('data_first').value + "T00:00:00";
        data_first = Date.parse(data_first1)/1000 - 21600;
        data_second1 = document.getElementById('data_second').value + "T23:59:59";
        data_second =  Date.parse(data_second1)/1000 - 21600;
        $("#content-table").html("Загрузка...").show();
        $.ajax({
          type: "POST",
          url: "../php/period_admin.php",
          data: {
            data_first1: data_first,
            data_second1: data_second,
            data_def1: data_def,
            show_contract,
            id_shelter,
          },
          success: function(response) {
            $("#content-table").html(response).show();
          }
        });
    });
</script>
<?php
  require_once("template/footer.html");
?>