    
    let id_selected_cards = [];
    let completed_application;
    let start_from = $('#row_limit').val();
    $(document).ready(function() {
        gen_journal_table();
    });
    function gen_journal_table(){
        if($("#journal-mobile-view").length>0) 
            var device = "mobile";
        else
            var device = "desktop";
        $.ajax({
            type: 'POST',
            url: 'php/journal.php?gen_journal_table',
            dataType: 'json',
            data: {
                device,
        },
        beforeSend: function(){
            //создаем div
            var loading = $("<div>", {
            "class" : "ds-loading"
            });
            $('.ds-loading').fadeIn(1000, function(){
            setTimeout(function(){
                $('.ds-loading').fadeIn(500);
            }, 5000);
            });
            //выравним div по центру страницы
            $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
            //добавляем созданный div в конец документа
            $("body").append(loading);
        },
        success: function(res){
            switch (device) {
                case 'mobile':
                    $('.journal-mobile-row').remove();
                    $('#journal-mobile-view').append(res.html);
                    break;
                case 'desktop':
                    $('.journal_row').remove();
                    $('#table-zayavka').append(res.html);
                    break;
                default:
                    break;
            }
            
            var journal_current_page = res.journal_current_page;
            var pages = Math.ceil(res.count_rows_in_base/$('#journal_row_limit').val());
            $('.pagination').empty();
            for (i = 1; i<= pages; i++) {
                if (i==journal_current_page)
                    var disabled = 'disabled';
                else
                var disabled = '';
                $('.pagination').append('<a onclick="journal_change_page('+i+')" class="btn btn-sm btn-primary active" role="button" aria-pressed="true" href="#"  '+disabled+'>'+(i)+'</a>&nbsp');
            }
            //уничтожаем div
            $('.ds-loading').fadeOut(1000, function(){
                setTimeout(function(){
                    $('.ds-loading').fadeOut(500);
                }, 5000);
            });
            $(".ds-loading").detach();
        },
        error: function(res){
            console.log(res)
        }
        });
    }
    function journal_filter_apply(){
        var journal_filter_first_date = $('#journal_filter_first_date').val();
        var journal_filter_last_date = $('#journal_filter_last_date').val();
        var journal_order_by = $('#journal_order_by').val();
        var journal_sort_by = $('#journal_sort_by').val();
        var journal_row_limit = $('#journal_row_limit').val();
        var journal_id_executor = $('#journal_id_executor').val();
        var journal_status = $('#journal_status').val();
        var journal_id_executor = $('#journal_id_executor').val();
        $.ajax({
            type: 'POST',
            url: 'php/journal.php?update_table_config',
            dataType: 'json',
            data: {
                journal_filter_first_date,
                journal_filter_last_date,
                journal_order_by,
                journal_sort_by,
                journal_row_limit,
                journal_id_executor,
                journal_status,
                journal_id_executor
            },
            success: function(res){
                //console.log(res.result)
                if (res.result == "success") {
                    $('#show_more').fadeIn();
                    gen_journal_table();
                }
                else {
                    alert("Возникла ошибка. Страница будет перезагружена");
                    location.reload();
                }
            },
            error: function(res){
                console.log("Ошибка в функции фильтра")
                console.log(res);
            }
        });
    }
    function journal_filter_reset() {
        $.ajax({
            type: 'POST',
            url: 'php/journal.php?journal_filter_reset',
            dataType: 'json',
            success: function(res){
                location.reload();
            },
            error: function(res){
                console.log("Ошибка в функции фильтра")
                console.log(res);
            }
        });
    }
    function journal_change_page(journal_need_page) {
        console.log("AJAX: "+journal_need_page)
        $.ajax({
            type: 'POST',
            url: 'php/journal.php?journal_change_page',
            dataType: 'json',
            data:{
                journal_need_page,
            },
            success: function(res){
                gen_journal_table();
            },
            error: function(res){
                console.log("Ошибка: ")
                console.log(res);
            }
        });
    }
    $('.search_click').on('click', function() {
        journal_search_request = $('#journal_search_request').val();
        
        $.ajax({
            type: 'POST',
            url: 'php/journal.php?journal_search',
            dataType: 'json',
            data:{
                journal_search_request,
            },
            success: function(res){
                gen_journal_table();
            },
            error: function(res){
                console.log("Ошибка: ")
                console.log(res);
            }
        });
    });
    function add_row(){
        var journal_locality = CheckInput('journal_locality') ? $('#journal_locality').val() : false;
        var journal_loc_type = $('#journal_locality').attr('loc_type'); 
        var journal_street = $('#journal_street').val();
        var journal_applicant_name = $('#journal_applicant_name').val();
        var journal_applicant_phone = $('#journal_applicant_phone').val();
        var journal_animals_count = $('#journal_animals_count').val();
        var journal_description = $('#journal_description').val();
        var temp_date =  $('#journal_date').val();
        var journal_date = Date.parse(temp_date)/1000;
        if (!journal_locality){
            alert('Заполните населенный пункт отлова');
            return false;
            }  
        $.ajax({
            type: 'POST',
            url: 'php/journal.php?add_row',
            dataType: 'json',
            data:{
                journal_locality,
                journal_street,
                journal_applicant_name,
                journal_applicant_phone,
                journal_animals_count,
                journal_description,
                journal_loc_type,
                journal_date
            },
            success: function(res){
                location.reload();                
            },
            error: function(res){
                console.log("Ошибка: ")
                console.log(res);
            }
        });
    }
    $(document).on('change', '.edit_status', function(event) {
        var new_status = $(this).find(":selected").attr("data-status");
        var id_row = $(this).find(":selected").attr("id-journal-row");
        $.ajax({
            type: 'POST',
            url: 'php/journal.php?edit_status',
            dataType: 'json',
            data:{
                new_status,
                id_row,
            },
            success: function(res){
                gen_journal_table();
            },
            error: function(res){
                console.log("Ошибка: ")
                console.log(res);
            }
        });
    });
    $(document).on('change', '.edit_executor', function(event) {
        var new_executor = $(this).find(":selected").attr("data-executor");
        var id_row = $(this).find(":selected").attr("id-journal-row");
        $.ajax({
            type: 'POST',
            url: 'php/journal.php?edit_executor',
            dataType: 'json',
            data:{
                new_executor,
                id_row,
            },
            success: function(res){
                gen_journal_table();
            },
            error: function(res){
                console.log("Ошибка: ")
                console.log(res);
            }
        });
    });
    $(function () {
        var $city = $("#journal_locality");
        var $tooltip = $('.tooltip');
        $.fias.setDefault({
            parentInput: '.js-form-address',
            verify: true,
            select: function (obj) {
                setLabel($(this), obj.type);
                $tooltip.hide();
            },
            check: function (obj) {
                var $input = $(this);

                if (obj) {
                    setLabel($input, obj.type);
                    $tooltip.hide();
                }
                else {
                    showError($input, 'Введено неверно');
                }
            },
            checkBefore: function () {
                var $input = $(this);

                if (!$.trim($input.val())) {
                    $tooltip.hide();
                    return false;
                }
            },
            change: function (obj) {
                if(obj && obj.parents){
                    $.fias.setValues(obj.parents, '.js-form-address');
                }

                if(obj && obj.zip){
                    $('[name="zip"]').val(obj.zip);
                }
                $('#journal_locality').attr('loc_type', obj.typeShort);
            },
        });
        $city.fias('type', $.fias.type.city);
        $city.fias('withParents', true);
        function setLabel($input, text) {
            text = text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
            $input.parent().find('label').text(text);
        }
        function showError($input, message) {
            $tooltip.find('span').text(message);
            var inputOffset = $input.offset(),
                inputWidth = $input.outerWidth(),
                inputHeight = $input.outerHeight();
            var tooltipHeight = $tooltip.outerHeight();
            $tooltip.css({
                left: (inputOffset.left + inputWidth + 10) + 'px',
                top: (inputOffset.top + (inputHeight - tooltipHeight) / 2 - 1) + 'px'
            });
            $tooltip.show();
        }
    });
    $(document).on('click', '.to_work', function(event) {
        var id_row = $(this).attr('id-row');
        $.ajax({
            type: 'POST',
            url: 'php/journal.php?to_work',
            dataType: 'json',
            data:{
                id_row,
            },
            success: function(res){
                gen_journal_table();
            },
            error: function(res){
                console.log("Ошибка: ")
                console.log(res);
            }
        });
    });
    /*$(document).on('click', '.show_linked_card_form', function(event) {
        $.fancybox.open({
            src  : '#linked_card_form',
            type : 'inline'
        });
        completed_application = $(this).attr('id-row');
        $.ajax({
            type: 'POST',
            url: 'php/journal.php?get_my_animal_cards',
            dataType: 'json',
            success: function(res){
                $('.card_on_list').remove();
                $('#cards_list').append(res.html);
            },
            error: function(res){
                console.log("Ошибка: ")
                console.log(res);
            }
        });
    });*/
    /*$(document).on('click', '.card_on_list', function(event) {
        if ($(this).find("input").prop("checked")!=true) {
            //$(this).css({"background-color": "#337ab7", "color": "#FFFFFF"});
            $(this).addClass("selected_card_on_list");
            $(this).find("input").prop("checked", true);
            //id_selected_cards.push($(this).attr('id-journal-row'));
        }
        else {
            //$(this).css({"background-color": "#eeeeee", "color": "#000000"});
            $(this).removeClass("selected_card_on_list");
            $(this).find("input").prop("checked", false);
            //id_selected_cards.push($(this).attr('id-journal-row'));
        }
    });*/
    $(document).on('click', '.show_linked_card_form', function(event) {
        //$(document).on('click', '#complete', function(event) {
        /*$('.selected_card_on_list').each(function() {
            id_selected_cards.push($(this).attr('id-journal-row'));
        });*/
        completed_application = $(this).attr('id-row');
        $.ajax({
        type: 'POST',
            url: 'php/journal.php?complete',
            dataType: 'json',
            data: {
                //id_selected_cards,
                completed_application,
            },
            success: function(res){
                location.reload()
            },
            error: function(res){
                console.log("Ошибка: ")
                console.log(res);
            }
        });
    });
    $(document).on('click', '.open_request_info', function(event) {
        $.fancybox.open({
            src  : '#request_info',
            type : 'inline'
        });
        var id_row = $(this).attr('id-row');
        $.ajax({
            type: 'POST',
            url: 'php/journal.php?get_linked_applications',
            dataType: 'json',
            data: {
                id_row,
            },
            success: function(res){
                $('.request_row').remove();
                $('#request_list').append(res.html);
            },
            error: function(res){
                console.log("Ошибка: ")
                console.log(res);
            }
        });
    });
    $(document).on('click', '.delete-journal', function(event) {
        if (confirm("Вы уверены?")) {
            var id_row = $(this).attr('id-row');
            $.ajax({
                type: 'POST',
                url: 'php/journal.php?delete_journal_row',
                dataType: 'json',
                data: {
                    id_row,
                },
                success: function(res){
                    gen_journal_table();
                },
                error: function(res){
                    console.log("Ошибка: ")
                    console.log(res);
                }
            });
        }
    });