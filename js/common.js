// Показать пароль
$('body').on('click', '.password-control', function(){
  if ($('#password-input1').attr('type') == 'password'){
    $(this).addClass('view');
    $('#password-input1').attr('type', 'text');
  } else {
    $(this).removeClass('view');
    $('#password-input1').attr('type', 'password');
  }
  return false;
});

window.setInterval(get_notifications_count, 30000);
$(document).ready(function() {  
  var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
  $.ajax({
    type: 'POST',
    url: '../php/timezone.php?apply_timezone',
    dataType: 'json',
    data: {
      timezone,
    },
    success: function(res){
      console.log("Часовой пояс обработан: "+timezone)
    },
    error: function(res){
      console.log("Ошибка часового пояса: "+res.res)
    }
  });
  // Генерация пароля
  $("#generate").click(function(){
    $("#password-input1").val("");
    $("#password-input1").val(gen_password());
    return false;
  });

  // Генерация пароля
  $("#generate").click(function(){
    $("#password-input1").val("");
    $("#password-input1").val(gen_password());
    return false;
  });

  
  $("#generate2").click(function(){
    $("#password-input1").val("");
    $("#password-input1").val(gen_password());
    return false;
  });

  function gen_password(){
    len = 8;
    var password = "";
    var symbols = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz0123456789";
    for (var i = 0; i < len; i++){
        password += symbols.charAt(Math.floor(Math.random() * symbols.length));     
    }
    return password;
  }
  // /Генерация пароля

});

//Редактирование
  $(".del_file").click(function(){
    files_to_delete.push($(this).attr("data"));
    $(this).parent(".media-item").remove();
  });
  var media_to_load;
  $('body').on('change', '#media-on-edit', function(event) {
        delete media_to_load;
        media_to_load = this.files;
  
  });
  let files_to_delete = [];
  $('#update').on('click', function() { 
    var mediaFormData = new FormData();
    $.each(media_to_load, function(key, value){
      mediaFormData.append(key, value);
    });
    var id = $('#add_application').attr('data');
    var city = CheckInput('city') ? $('#city').val() : false;
    var street = CheckInput('street') ? $('#street').val() : false;
    var id_kind = CheckInput('id_kind') ? $('#id_kind').val() : "";
    var breed = CheckInput('breed') ? $('#breed').val() : "";
    var gender = CheckInput('gender') ? $('#gender').val() : "";
    var color = CheckInput('color') ? $('#color').val() : "";
    var age = CheckInput('age') ? $('#age').val() : "";
    var shelter = CheckInput('shelter') ? $('#shelter').val() : "";
    var weight = CheckInput('weight') ? $('#weight').val() : "";
    var num_aviary = CheckInput('num_aviary') ? $('#num_aviary').val() : "";
    var comment = CheckInput('comment') ? $('#comment').val() : "";
    if(document.getElementById("id_status") != null)
        var id_status = CheckInput('id_status') ? $('#id_status').val() : "";
    if(document.getElementById("id_sostoyanie") != null)
        var id_sostoyanie = CheckInput('id_sostoyanie') ? $('#id_sostoyanie').val() : "";
        
    if (!city){
         alert('Заполните населенный пункт отлова');
         return false;
    }
    
    var ajax_query = 'update&id='+id+'&street='+street+'&id_kind='+id_kind+'&breed='+breed+'&gender='+gender+'&color='+color+'&age='+age+'&weight='+weight+'&shelter='+shelter+'&num_aviary='+num_aviary+'&comment='+comment+'&street='+street+'&city='+city+'&files_to_delete='+files_to_delete+'&id_status='+id_status+'&id_sostoyanie='+id_sostoyanie;
        
    $.ajax({            
      url: 'php/update_application.php?'+ajax_query,
      type: 'POST',
      data: mediaFormData,   
      cache: false,
      dataType: 'json',
      processData: false,
      contentType: false,
      beforeSend: function(){
        //создаем div
        var loading = $("<div>", {
          "class" : "ds-loading"
        });
        $('.ds-loading').fadeIn(1000, function(){
          setTimeout(function(){
            $('.ds-loading').fadeIn(500);
          }, 5000);
        });
        //выравним div по центру страницы
        $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
        //добавляем созданный div в конец документа
        $("body").append(loading);
      },
      complete: function() {
        //уничтожаем div
        $('.ds-loading').fadeOut(1000, function(){
          setTimeout(function(){
            $('.ds-loading').fadeOut(500);
          }, 5000);
        });
        $(".ds-loading").detach();
      },
      success: function(res){
        //   alert("Изменения внесены");
          window.location.href = "animal_card.php?application="+id;
      },
      error: function(res){
        //   alert("Изменения внесены");
          window.location.href = "animal_card.php?application="+id;
      }
    });
    files_to_delete = [];
  });
//конец редактирования


//Редактирование фото
    $(".del_file").click(function(){
        files_to_delete_foto.push($(this).attr("data"));
        $(this).parent(".media-item").remove();
    });
    
    var media_to_load;
    $('body').on('change', '#media-on-edit', function(event) {
        delete media_to_load;
        media_to_load = this.files;  
    });
    
    let files_to_delete_foto = [];
    $('#update-foto').on('click', function() { 
        var mediaFormData = new FormData();
        $.each(media_to_load, function(key, value){
          mediaFormData.append(key, value);
        });
        
        var id = $('#application').attr('data');
        var ajax_query = 'update-foto&id='+id+'&files_to_delete='+files_to_delete_foto;
        $.ajax({            
          url: 'php/update_application.php?'+ajax_query,
          type: 'POST',
          data: mediaFormData,   
          cache: false,
          dataType: 'json',
          processData: false,
          contentType: false,
          beforeSend: function(){
            //создаем div
            var loading = $("<div>", {
              "class" : "ds-loading"
            });
            $('.ds-loading').fadeIn(1000, function(){
              setTimeout(function(){
                $('.ds-loading').fadeIn(500);
              }, 5000);
            });
            //выравним div по центру страницы
            $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
            //добавляем созданный div в конец документа
            $("body").append(loading);
          },
          complete: function() {
            //уничтожаем div
            $('.ds-loading').fadeOut(1000, function(){
              setTimeout(function(){
                $('.ds-loading').fadeOut(500);
              }, 5000);
            });
            $(".ds-loading").detach();
          },
          success: function(res){
              window.location.href = "animal_card.php?application="+id;
          },
          error: function(res){
              window.location.href = "animal_card.php?application="+id;
          }
        });
        files_to_delete_foto = [];
    });
// /Редактирование фото

//Редактирование видео
    $(".del_file").click(function(){
        files_to_delete_video.push($(this).attr("data")); 
        $(this).parent(".media-item").remove();
    });
    var media_to_load_video;
    $('body').on('change', '#media-on-edit', function(event) {
        delete media_to_load_video;
        media_to_load_video = this.files;  
    });
    let files_to_delete_video = [];
    $('#update-video').on('click', function() { 
        var mediaFormData_video = new FormData();
        $.each(media_to_load_video, function(key, value){
          mediaFormData_video.append(key, value);
        });
        
        var id = $('#application').attr('data');
        
        var ajax_query = 'update-video&id='+id+'&files_to_delete='+files_to_delete_video;
            
        $.ajax({            
          url: 'php/update_application.php?'+ajax_query,
          type: 'POST',
          data: mediaFormData_video,   
          cache: false,
          dataType: 'json',
          processData: false,
          contentType: false,
          beforeSend: function(){
            //создаем div
            var loading = $("<div>", {
              "class" : "ds-loading"
            });
            $('.ds-loading').fadeIn(1000, function(){
              setTimeout(function(){
                $('.ds-loading').fadeIn(500);
              }, 5000);
            });
            //выравним div по центру страницы
            $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
            //добавляем созданный div в конец документа
            $("body").append(loading);
          },
          complete: function() {
            //уничтожаем div
            $('.ds-loading').fadeOut(1000, function(){
              setTimeout(function(){
                $('.ds-loading').fadeOut(500);
              }, 5000);
            });
            $(".ds-loading").detach();
          },
          success: function(res){
              window.location.href = "animal_card.php?application="+id;
          },
          error: function(res){
              window.location.href = "animal_card.php?application="+id;
          }
        });
        files_to_delete_video = [];
    });
// /Редактирование видео

//изменение статусов
    $('body').on('click', '#to_karantin', function(){
    var id_card = $(this).attr('data');
    $.ajax({
      type: 'POST',
      url: 'php/update_application.php?to_karantin',
      data: {
        id_card
      },
      success: function(res){
          location.reload();
      }
    });
    });
    $('body').on('click', '#free', function(){
    var id_card = $(this).attr('data');
    $.ajax({
      type: 'POST',
      url: 'php/update_application.php?free',
      data: {
        id_card
      },
      success: function(res){
          location.reload();
      }
    });
    });
    $('body').on('click', '#buried', function(){
        var id_card = $(this).attr('data');
        $.ajax({
          type: 'POST',
          url: 'php/update_application.php?buried',
          data: {
            id_card
          },
          success: function(res){
              location.reload();
          }
        });
      });
//конец изменение статусов

//заключение
    $('body').on('click', '#zakluchenie', function(){
    var id_card = $(this).attr('data');
    window.open('php/otchet_zakluchenie.php?zakluchenie=' + id_card);
  });
// /заключение

//добавление
  var files;
  $('body').on('change', '#mediafiles', function(event) {
    if (this.files.length <= 10){
      delete files;
      files = this.files;
    }
    else{
      alert("Выберите не более 10 файлов!");
      $(this).val("");
    }
  });
  // // Добавление карточки
  // $('#add_card').on('click', function() {
  //   var filesData = new FormData();
  //   $.each(files, function(key, value){
  //     filesData.append(key, value);
  //   });
  //   var city =  CheckInput('city') ? $('#city').val() : false;
  //   var id_kind =  CheckInput('id_kind') ? $('#id_kind').val() : "";
  //   var breed =  CheckInput('breed') ? $('#breed').val() : "";
  //   var gender =  CheckInput('gender') ? $('#gender').val() : "";
  //   var color =  CheckInput('color') ? $('#color').val() : "";
  //   var age =  CheckInput('age') ? $('#age').val() : "";
  //   var weight =  CheckInput('weight') ? $('#weight').val() : "";
  //   var shelter =  CheckInput('shelter') ? $('#shelter').val() : "";
  //   var num_aviary =  CheckInput('num_aviary') ? $('#num_aviary').val() : "";
  //   var comment =  CheckInput('comment') ? $('#comment').val() : "";
  //   var data_a =  CheckInput('date') ? $('#date').val() : "";
  //   if(document.getElementById("kinolog") != null)
  //       var kinolog =  CheckInput('kinolog') ? $('#kinolog').val() : "";
  //   var street =  CheckInput('street') ? $('#street').val() : false;
    
  //   var data_ts = Date.parse(data_a)/1000;

  //   // if (!street || !city || !id_kind || !breed || !gender || !color || !age || !weight || !shelter || !num_aviary || !street){
  //       if (!street || !city){
  //         alert('Заполните населенный пункт отлова и улицу');
  //         return false;
  //       }
  //   // alert(data_ts);
  //   var ajax_query = 'add_application&street='+street+'&id_kind='+id_kind+'&breed='+breed+'&gender='+gender+'&color='+color+'&age='+age+'&weight='+weight+'&shelter='+shelter+'&num_aviary='+num_aviary+'&comment='+comment+'&street='+street+'&city='+city+'&data='+data_ts+'&kinolog='+kinolog;
  //   $.ajax({
  //       url: 'php/application.php?'+ajax_query,
  //       type: 'POST',
  //       data: filesData,   
  //       cache: false,
  //       dataType: 'json',
  //       processData: false,
  //       contentType: false,
  //       beforeSend: function(){
  //         //создаем div
  //         var loading = $("<div>", {
  //           "class" : "ds-loading"
  //         });
  //         $('.ds-loading').fadeIn(1000, function(){
  //           setTimeout(function(){
  //             $('.ds-loading').fadeIn(500);
  //           }, 5000);
  //         });
  //         //выравним div по центру страницы
  //         $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
  //         //добавляем созданный div в конец документа
  //         $("body").append(loading);
  //       },
  //       complete: function() {
  //         //уничтожаем div
  //         $('.ds-loading').fadeOut(1000, function(){
  //           setTimeout(function(){
  //             $('.ds-loading').fadeOut(500);
  //           }, 5000);
  //         });
  //         $(".ds-loading").detach();
  //       },
  //       success: function(res){
  //       //   alert("Успешно добавлено");
  //         window.location.href = "index.php";
  //       },
  //       error: function(res){
  //       //   alert("Успешно добавлено");
  //         window.location.href = "index.php";
  //       }
  //   });
    
  // });
//конец добавления

function CheckInput(input){
  var checkstr = $('#'+input).val();
  if (checkstr.length < 1){
    $('#'+input).addClass("Empty");
    return false;
  }
  else {
    $('#'+input).removeClass("Empty");
    return true;
  }
}

// fias
$(function () {
    var $zip = $('[name="zip"]'),
        $city = $('[name="city"]'),
        // $street = $('[name="street"]'),
        $building = $('[name="building"]');

    var $tooltip = $('.tooltip');

    $.fias.setDefault({
        parentInput: '.js-form-address',
        verify: true,
        select: function (obj) {
            setLabel($(this), obj.type);
            $tooltip.hide();
        },
        check: function (obj) {
            var $input = $(this);

            if (obj) {
                setLabel($input, obj.type);
                $tooltip.hide();
            }
            else {
                showError($input, 'Введено неверно');
            }
        },
        checkBefore: function () {
            var $input = $(this);

            if (!$.trim($input.val())) {
                $tooltip.hide();
                return false;
            }
        },
        change: function (obj) {
            if(obj && obj.parents){
                $.fias.setValues(obj.parents, '.js-form-address');
            }

            if(obj && obj.zip){
                $('[name="zip"]').val(obj.zip);
            }
            $('#city').attr('loc_type', obj.typeShort);
        },
    });

    $city.fias('type', $.fias.type.city);
    // $street.fias('type', $.fias.type.street);
    $building.fias('type', $.fias.type.building);

    // $district.fias('withParents', true);
    $city.fias('withParents', true);
    // $street.fias('withParents', true);


    // Отключаем проверку введённых данных для строений
    $building.fias('verify', false);

    // Подключаем плагин для почтового индекса
    // $zip.fiasZip('.js-form-address');

    function setLabel($input, text) {
        text = text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
        $input.parent().find('label').text(text);
    }

    function showError($input, message) {
        $tooltip.find('span').text(message);

        var inputOffset = $input.offset(),
            inputWidth = $input.outerWidth(),
            inputHeight = $input.outerHeight();

        var tooltipHeight = $tooltip.outerHeight();

        $tooltip.css({
            left: (inputOffset.left + inputWidth + 10) + 'px',
            top: (inputOffset.top + (inputHeight - tooltipHeight) / 2 - 1) + 'px'
        });

        $tooltip.show();
    }

});



$(function() {
    let header = $('.fixed-filters');
    let hederHeight = header.height(); // вычисляем высоту шапки
    // console.log(hederHeight);
    $(window).scroll(function() {
       if($(this).scrollTop() > 170) {
        header.addClass('header_fixed');
        $('body').css({
           'paddingTop': hederHeight+'px' // делаем отступ у body, равный высоте шапки
        });
       } else {
        header.removeClass('header_fixed');
        $('body').css({
         'paddingTop': 0 // удаляю отступ у body, равный высоте шапки
        })
       }
    });
});


$('body').on('click', '.a_location', function(event) {
  var new_a_location = $(this).attr('data');
  $.ajax({
    type: 'POST',
    url: 'php/admin.php?change_location',
    data: {
      new_a_location
    },
    success: function(res){
      if (res == "success"){
        location.reload();
      }
    },
    error: function(res){
      alert("Ошибка при смене региона");
    }
  });
});
function reset_filter() {
  $.ajax({
    type: 'POST',
    url: '../php/functions.php?reset_filter',
    success: function(res){
      console.log("Фильтр сброшен")
      location.reload();
    },
    error: function(res){
      alert("Ошибка сброса фильтра: "+res.res);
    }
  });
}