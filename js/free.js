$(document).ready(function() {
  gen_free_table();
});
var files_all = [];
var files_foto;
$('body').on('change', '#mediafiles-foto', function(event) {
    delete files_foto;
    files_foto = this.files;
    for (let i = 0; i < files_foto.length; i++) {
        files_all.push(files_foto[i]);
    }
});

var files_video;
$('body').on('change', '#mediafiles-video', function(event) {
  delete files_video;
  files_video = this.files;
  for (let j = 0; j < files_video.length; j++) {
     files_all.push(files_video[j]);
  }
});
$('#add_free_application').on('click', function() {
    var filesData = new FormData();
    $.each(files_all, function(key, value){
      filesData.append(key, value);
    });
    var city =  $('#city').val();
    var loc_type = $('#city').attr('loc_type'); 
    var date =  $('#date').val();
    var street =  $('#street').val();
    if (city == ""){
        alert('Заполните населенный пункт отлова');
        return false;
    }
    var ajax_query = 'add_free_application&street='+street+'&city='+city+'&date='+date+'&loc_type='+loc_type;
    $.ajax({
        url: 'php/free.php?'+ajax_query,
        type: 'POST',
        data: filesData,   
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function(){
          //создаем div
          var loading = $("<div>", {
            "class" : "ds-loading"
          });
          $('.ds-loading').fadeIn(1000, function(){
            setTimeout(function(){
              $('.ds-loading').fadeIn(500);
            }, 5000);
          });
          //выравним div по центру страницы
          $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
          //добавляем созданный div в конец документа
          $("body").append(loading);
        },
        complete: function() {
          //уничтожаем div
          $('.ds-loading').fadeOut(1000, function(){
            setTimeout(function(){
              $('.ds-loading').fadeOut(500);
            }, 5000);
          });
          $(".ds-loading").detach();
        },
        success: function(res){
            // alert("Сохранено успешно");
            window.location.href = "index.php";
        },
        error: function(res){
            console.log("Ошибка: ")
            console.log(res);
        }
    });
    
});
function gen_free_table(){
  $.ajax({
  type: 'POST',
  url: 'php/free.php?get_free_applications',
  dataType: 'json',
  beforeSend: function(){
    //создаем div
    var loading = $("<div>", {
    "class" : "ds-loading"
    });
    $('.ds-loading').fadeIn(1000, function(){
    setTimeout(function(){
        $('.ds-loading').fadeIn(500);
    }, 5000);
    });
    //выравним div по центру страницы
    $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
    //добавляем созданный div в конец документа
    $("body").append(loading);
  },
  success: function(res){
    $('#free_list').html("");
    $('#free_list').append(res.html);
    //уничтожаем div
    $('.ds-loading').fadeOut(1000, function(){
      setTimeout(function(){
          $('.ds-loading').fadeOut(500);
      }, 5000);
      });
      $(".ds-loading").detach();
  },
  error: function(res){
    console.log(res)
  }
  });
}
$('body').on('click', '.delete-files', function(event) {
  var id_file = $(this).closest('tr').attr('id-file');
  $.ajax({
    type: 'POST',
    url: 'php/free.php?delete_free_application',
    dataType: 'json',
    data:{
      id_file,
    },
    success: function(res){
      switch (res.result) {
        case 'success':
          gen_free_table();
          break;
        case 'error_del':
          console.log("Ошибка удаления файла с сервера");
          break;
        case 'error_mysql':
          console.log("Ошибка удаления файла с БД>");
          break;
        default:
          break;
      }
    },
    error: function(res){
        console.log("Ошибка: ")
        console.log(res);
    }
});
})