function open_notifications(){
    $.fancybox.open({
      src  : '#notifications',
      type : 'inline'
    });
    $.ajax({
        type: 'POST',
        url: '../php/notifications.php?get_notifications',
        dataType: 'json',
        success: function(res){
            $("#spisok").html(res.html);
        },
        error: function(res){
            console.log("Ошибка: ")
            console.log(res);
        }
    });
  }
  function get_notifications_count(){
    $.ajax({
        type: 'POST',
        url: '../php/notifications.php?get_notifications_count',
        dataType: 'json',
        success: function(res){
          if (res.counter_notifications == 0){
            $('.indicator').css({"display": "none"});
            $('.noti_count').html("");
          }
          else{
            $('.indicator').css({"display": "flex"});
            $('.noti_count').html(res.counter_notifications);
          }
        },
        error: function(res){
          console.log("Ошибка: ")
          console.log(res);
        }
    });
  }
  $(document).on('click', '.hide_notification', function(event) {
    $(this).parent().fadeOut();
    var id_row = $(this).parent().attr('data-id');
    $.ajax({
        type: 'POST',
        url: '../php/notifications.php?hide_notification',
        dataType: 'json',
        data: {
          id_row,
        },
        success: function(res){
          $(this).parent().fadeOut();
          get_notifications_count();
        },
        error: function(res){
          console.log("Ошибка: ")
          console.log(res);
        }
    });
  })

    
  $("#close-day").click(function(){ 
      if (screen.width > 480) location.href='../otchet_kinolog.php';
      else  location.href='../otchet_kinolog-mobile.php';
  });
  $("#close-day-mobile").click(function(){ 
      if (screen.width > 480) location.href='../otchet_kinolog.php';
      else location.href='../otchet_kinolog-mobile.php';
  });
