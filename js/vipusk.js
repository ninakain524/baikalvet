//добавление
var files;
  $('body').on('change', '#mediafiles-vipusk', function(event) {
    if (this.files.length <= 10){
      delete files;
      files = this.files;
    }
    else{
      alert("Выберите не более 10 файлов!");
      $(this).val("");
    }
  });

var media_to_load;
$('body').on('change', '#media-on-edit-vipusk', function(event) {
// if ($('.media-item').length < 10) {
//   	if (this.files.length <= (10-$('.media-item').length)) {
		delete media_to_load;
		media_to_load = this.files;
//   	}
//   	else{
// 		alert("Максимальное количество файлов - 10. Вы можете выбрать еще "+(10-$('.media-item').length));
// 		$(this).val("");
//   	}
// }
// else {
//   	alert("Максимальное количество файлов - 10. Удалите ненужные и повторите снова");
//   	$(this).val("");
// }   
});

//Редактирование фото
    $(".del_file").click(function(){
        files_to_delete_foto_vipusk.push($(this).attr("data"));
        $(this).parent(".media-item").remove();
    });
    
    let files_to_delete_foto_vipusk = [];
    $('#update-foto-vipusk').on('click', function() { 
        var mediaFormData = new FormData();
        $.each(media_to_load, function(key, value){
          mediaFormData.append(key, value);
        });
        
        var id = $('#application').attr('data');
        var ajax_query = 'update-foto-vipusk&id='+id+'&files_to_delete='+files_to_delete_foto_vipusk;
        $.ajax({            
          url: 'php/vipusk_card.php?'+ajax_query,
          type: 'POST',
          data: mediaFormData,   
          cache: false,
          dataType: 'json',
          processData: false,
          contentType: false,
          beforeSend: function(){
            //создаем div
            var loading = $("<div>", {
              "class" : "ds-loading"
            });
            $('.ds-loading').fadeIn(1000, function(){
              setTimeout(function(){
                $('.ds-loading').fadeIn(500);
              }, 5000);
            });
            //выравним div по центру страницы
            $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
            //добавляем созданный div в конец документа
            $("body").append(loading);
          },
          complete: function() {
            //уничтожаем div
            $('.ds-loading').fadeOut(1000, function(){
              setTimeout(function(){
                $('.ds-loading').fadeOut(500);
              }, 5000);
            });
            $(".ds-loading").detach();
          },
          success: function(res){
              window.location.href = "animal_card.php?application="+id;
          },
          error: function(res){
              window.location.href = "animal_card.php?application="+id;
          }
        });
        files_to_delete_foto_vipusk = [];
    });
// /Редактирование фото

//Редактирование видео
    $(".del_file").click(function(){
        files_to_delete_video_vipusk.push($(this).attr("data"));
        $(this).parent(".media-item").remove();
    });
    var media_to_load_video;
    $('body').on('change', '#media-on-edit-vipusk', function(event) {
    // if ($('.media-item').length < 10) {
    //   if (this.files.length <= (10-$('.media-item').length)) {
        delete media_to_load_video;
        media_to_load_video = this.files;
    //   }
    //   else{
    //     alert("Максимальное количество файлов - 10. Вы можете выбрать еще "+(10-$('.media-item').length));
    //     $(this).val("");
    //   }
    // }
    // else {
    //   alert("Максимальное количество файлов - 10. Удалите ненужные и повторите снова");
    //   $(this).val("");
    // }   
    });
    let files_to_delete_video_vipusk = [];
    $('#update-video-vipusk').on('click', function() { 
        var mediaFormData_video = new FormData();
        $.each(media_to_load_video, function(key, value){
          mediaFormData_video.append(key, value);
        });
        
        var id = $('#application').attr('data');
        
        var ajax_query = 'update-video-vipusk&id='+id+'&files_to_delete='+files_to_delete_video_vipusk;
            
        $.ajax({            
          url: 'php/vipusk_card.php?'+ajax_query,
          type: 'POST',
          data: mediaFormData_video,   
          cache: false,
          dataType: 'json',
          processData: false,
          contentType: false,
          beforeSend: function(){
            //создаем div
            var loading = $("<div>", {
              "class" : "ds-loading"
            });
            $('.ds-loading').fadeIn(1000, function(){
              setTimeout(function(){
                $('.ds-loading').fadeIn(500);
              }, 5000);
            });
            //выравним div по центру страницы
            $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
            //добавляем созданный div в конец документа
            $("body").append(loading);
          },
          complete: function() {
            //уничтожаем div
            $('.ds-loading').fadeOut(1000, function(){
              setTimeout(function(){
                $('.ds-loading').fadeOut(500);
              }, 5000);
            });
            $(".ds-loading").detach();
          },
          success: function(res){
              window.location.href = "animal_card.php?application="+id;
          },
          error: function(res){
              window.location.href = "animal_card.php?application="+id;
          }
        });
        files_to_delete_video_vipusk = [];
    });
// /Редактирование видео