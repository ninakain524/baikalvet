//добавление
var files_all = [];
var files_foto;

$('body').on('change', '#mediafiles-foto', function(event) {
    delete files_foto;
    files_foto = this.files;
    for (let i = 0; i < files_foto.length; i++) {
        files_all.push(files_foto[i]);
    }
});

var files_video;
$('body').on('change', '#mediafiles-video', function(event) {
  delete files_video;
  files_video = this.files;
  for (let j = 0; j < files_video.length; j++) {
     files_all.push(files_video[j]);
  }
});
  
  // Добавление карточки
  $('#add_card1').on('click', function() {
    var filesData = new FormData();
    
    $.each(files_all, function(key, value){
      filesData.append(key, value);
    });
    
    var city =  CheckInput('city') ? $('#city').val() : false;
    var loc_type = $('#city').attr('loc_type'); 
    var id_kind =  CheckInput('id_kind') ? $('#id_kind').val() : 0;
    var id_contract =  CheckInput('id_contract') ? $('#id_contract').val() : 0;
    var breed =  CheckInput('breed') ? $('#breed').val() : "";
    var gender =  CheckInput('gender') ? $('#gender').val() : 0;
    var color =  CheckInput('color') ? $('#color').val() : 0;
    var age =  CheckInput('age') ? $('#age').val() : "";
    var weight =  CheckInput('weight') ? $('#weight').val() : "";
    var height =  CheckInput('height') ? $('#height').val() : "";
    var shelter =  CheckInput('shelter') ? $('#shelter').val() : 0;
    var num_aviary =  CheckInput('num_aviary') ? $('#num_aviary').val() : 0;
    // alert (num_aviary);
    var comment =  CheckInput('comment') ? $('#comment').val() : "";
    var data_a =  CheckInput('date') ? $('#date').val() : "";
    // var date_aviary =  CheckInput('date_aviary') ? $('#date_aviary').val() : "";
    if(document.getElementById("kinolog") != null)
        var kinolog =  CheckInput('kinolog') ? $('#kinolog').val() : 0;
    if(document.getElementById("region") != null) 
        var region =  CheckInput('region') ? $('#region').val() : 0;
    var street =  CheckInput('street') ? $('#street').val() : "";
    var data_ts = Date.parse(data_a)/1000;

    var date = new Date(data_a);//Запрет установки времени отлова после 21:00. 
    if ((date.getHours() >= 21) && (date.getMinutes() >= 0)) {
      alert('Введите корректное время отлова');
      return false;
    }
    if (!city){
      alert('Заполните населенный пункт отлова');
      return false;
    }   
    if (region == 0){
      alert('Заполните регион');
      return false;
    } 
        
    var ajax_query = 'add_application&street='+street+'&id_kind='+id_kind+'&id_contract='+id_contract+'&breed='+breed+'&gender='+gender+'&color='+color+'&age='+age+'&weight='+weight+'&height='+height+'&shelter='+shelter+'&num_aviary='+num_aviary+'&comment='+comment+'&street='+street+'&city='+city+'&data='+data_ts+'&kinolog='+kinolog+'&region='+region+'&loc_type='+loc_type;

    $.ajax({
        url: 'php/application.php?'+ajax_query,
        type: 'POST',
        data: filesData,   
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function(){
          //создаем div
          var loading = $("<div>", {
            "class" : "ds-loading"
          });
          $('.ds-loading').fadeIn(1000, function(){
            setTimeout(function(){
              $('.ds-loading').fadeIn(500);
            }, 5000);
          });
          //выравним div по центру страницы
          $(loading).css("top", ($(window).height()/2)-($(loading).height()/2)).css("left", ($(document).width()/2)-($(loading).width()/2));
          //добавляем созданный div в конец документа
          $("body").append(loading);
        },
        complete: function() {
          //уничтожаем div
          $('.ds-loading').fadeOut(1000, function(){
            setTimeout(function(){
              $('.ds-loading').fadeOut(500);
            }, 5000);
          });
          $(".ds-loading").detach();
        },
        success: function(res){
          if(res == 0)
            alert("При создании произошла ошибка, попробуйте снова");
          else
            window.location.href = "index.php";
        },
        error: function(res){
          if(res == 0)
            alert("При создании произошла ошибка, попробуйте снова");
          else
            window.location.href = "index.php";
        }
    });
  });
//конец добавления