<?php
    require_once("php/config.php");
    if (!isset($_SESSION['timezone'])){
      echo "Не определён часовой пояс пользователя. Авторизируйтесь заново <a href='auth/login.php'>ЗДЕСЬ</a>";
      exit();
    }
    require_once("php/timezone.php");
    if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    } 
    require_once("php/update_status.php");
    require_once("template/head.html");
    require_once("template/header.php");
    require_once("php/functions.php");
    $today = time();
    $today1 = time();

    $last_query = " AND (`id_status`=1 OR `id_status`=2 OR `id_status`=11 OR `id_status`=7 OR `id_status`=8) ";
    
    function format_interval(DateInterval $interval) {
        $result = "";
        if ($interval->d) { $result .= $interval->format("%d д. "); }
        if ($interval->h) { $result .= $interval->format("%h ч. "); }
        if ($interval->i) { $result .= $interval->format("%i мин. "); }
    
        return $result;
    };
?>

<!-- Content -->
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="content-right" class="content-right">
          <div id="content-right-table" class="content-right-table table-responsive">
            <div class="menu-table">
              <div class="table-caption">Кинолог<div class="rule-answer"><a href="#">?</a></div></div>
                <div class="menu-table-filter">
                  <label for="sort_by" class="hide-mobile">Выберите период: </label>
                  <input type="date" name="data_first" id="data_first" class="field-filter" value="<?php echo date("Y-m-d", $today1); ?>" autocomplete="off"/>
                  <input type="date" name="data_second" id="data_second" class="field-filter" value="<?php echo date("Y-m-d", $today1); ?>" autocomplete="off"/>
                  <input id="display_period_kinolog" name="display-set" type="submit" value="Показать" autofocus />
                </div>
            </div>
            <div id="content-table">
              
            </div>
          </div>  
        </div>
      </div> 
    </div> 
  </div>
</section>
<!-- /Content -->
<script type="text/javascript">
  $('tr[data-href]').on("click", function() {
    document.location = $(this).data('href');
  });
  
  $("#display_period_kinolog").click(function() {
        var data_def = document.getElementById('data_first').value;
        var data_first1 = document.getElementById('data_first').value + "T00:00:00";
        var data_first = Date.parse(data_first1)/1000 - 21600;
        var data_second1 = document.getElementById('data_second').value + "T23:59:59";
        var data_second =  Date.parse(data_second1)/1000 - 21600;
        $("#content-table").html("Загрузка...").show();
        $.ajax({
          type: "POST",
          url: "../php/period_kinolog.php",
          data: {
            data_first1: data_first,
            data_second1: data_second,
            data_def1: data_def,
          },
          success: function(response) {
            $("#content-table").html(response).show();
          }
        });
    });
</script>
<?php
  require_once("template/footer.html");
?>