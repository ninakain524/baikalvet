<?php
    require_once("php/config.php");
    require_once("php/timezone.php");
    require_once("php/functions.php");
    if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
    require_once("php/update_status.php");
    require_once("template/head.html");
    require_once("template/header.php");
    
    $today = date("Y-m-d\TH:i:s");
    $today1 = date("Y-m-d");
    
    function format_interval(DateInterval $interval) {
        $result = "";
        if ($interval->d) { $result .= $interval->format("%d д. "); }
        if ($interval->h) { $result .= $interval->format("%h ч. "); }
        if ($interval->i) { $result .= $interval->format("%i мин. "); }
    
        return $result;
    };
?>

<!-- Content -->
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="content-right" class="content-right">
          <div id="content-right-table" class="content-right-table table-responsive">
            <div class="menu-table">
              <div class="menu-table-filter">
                <label for="sort_by" class="hide-mobile">Выберите период: </label>
                <input type="date" name="data_first" id="data_first" class="field-filter" value="<?php echo $today1; ?>" autocomplete="off"/>
                <input type="date" name="data_second" id="data_second" class="field-filter" value="<?php echo $today1; ?>" autocomplete="off"/>
              </div>
            </div>
            <div class="menu-table row-menu" style="border-top-left-radius: 0; border-top-right-radius: 0;">
              <div class="menu-table-filter">
                <select class="field-filter" id="contract" name="contract" onchange="showAdress()">
                <? $sql_contract = "SELECT contract.id AS id, contract.name_contract AS name_contract FROM `contract` WHERE `show_contract`=1 ORDER BY contract.id DESC;";
                  $res_contract = mysqli_query($SERVER, $sql_contract) or die("Ошибка " . mysqli_error($SERVER));
                  echo '<option value="all" selected>Все контракты</option>';
                  while($row_contract = mysqli_fetch_array($res_contract))
                  {
                    echo '<option value="'.$row_contract['id'].'">'.$row_contract['name_contract'].'</option>';
                  } ?>
                </select>

                <select class="field-filter" id="adress" name="adress">
                  <option value='all' selected>Все места отлова</option>
                </select>

                <select class="field-filter" id="kinolog" name="kinolog">
                <? $sql_kinolog = "SELECT * FROM `users` WHERE `show` = 1 AND `podtverzhdeno` = 1 AND `id_users_group` = 4;";
                  $res_kinolog = mysqli_query($SERVER, $sql_kinolog) or die("Ошибка " . mysqli_error($SERVER));
                  echo '<option value="all" selected>Все кинологи</option>';
                  while($row_kinolog = mysqli_fetch_array($res_kinolog))
                  {
                    $name_kinolog = $row_kinolog['name'];
                    $name_kinolog1 = mb_substr($name_kinolog, 0, 1);
                    $patronymic_kinolog = $row_kinolog['patronymic'];
                    $patronymic_kinolog1 = mb_substr($patronymic_kinolog, 0, 1);
                    echo '<option value="'.$row_kinolog['id'].'">'.$row_kinolog['sourname']." ".$name_kinolog1.". ".$patronymic_kinolog1.".".'</option>';
                  } ?>
                </select>

                <select class="field-filter" id="shelter" name="shelter">
                  <? $sql = "SELECT * FROM `animal_shelters` WHERE `actual`='1';";
                    $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                    echo '<option value="all" selected>Все приюты</option>';
                    while($row = mysqli_fetch_array($res))
                    {
                      echo '<option value="'.$row['id'].'">'.$row['name_shelter'].'</option>';
                    } ?>
                </select>

                <select class="field-filter" id="type" name="type" required>
                  <option value="all" selected>Тип документа</option>
                  <option value="akt_otlova">Акт отлова</option>
                  <option value="first_zakluchenie">Заключение первичное</option>
                  <option value="second_zakluchenie">Заключение вторичное</option>
                  <option value="uchetnoe_delo">Учетное дело</option>
                  <option value="akt_vibitiya">Акт выбытия животного</option>
                </select>
                
                <input id="display_period" name="display-set" type="submit" value="Показать" />
              </div>

              <!-- <div class="menu-table-filter">
                <select class="field-filter" id="adress" name="adress" style="display:none;"> </select>
              </div> -->
            </div>
            <div id="content-table">
                <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab tab-vet">
                  <tr class="tab-col1">
                    <th rowspan="2">№</th>
                    <th rowspan="2">Дата</th>
                    <th rowspan="2">Населенный пункт</th>
                    <th rowspan="2">Улица</th>
                    <th rowspan="2">Приют</th>
                    <th colspan="6">Описание животного</th>
                    <th rowspan="2">Статус</th>
                  </tr>
                  <tr class="tab-col1">
                    <th>Вид</th>
                    <th>Порода</th>
                    <th>Пол</th>
                    <th>Окрас</th>
                    <th>Возраст</th>
                    <th>Вес</th>
                  </tr>
                </table>
            </div>
          </div>  
        </div>
      </div> 
    </div> 
  </div>
</section>
<!-- /Content -->
<script type="text/javascript">

  function showAdress() {
    let contract = $('#contract').val();
    if (contract == "all") {
      // document.querySelector("#adress").style.display = "none";
      selectElement = document.getElementById('adress');
      var i, L = selectElement.options.length - 1;
      for(i = L; i >= 0; i--) {
          selectElement.remove(i);
      }
      $('#adress').append("<option value='all' selected>Все места отлова</option>");  
    }
    else {
      // document.querySelector("#adress").style.display = "block";
      selectElement = document.getElementById('adress');
      var i, L = selectElement.options.length - 1;
      for(i = L; i >= 0; i--) {
          selectElement.remove(i);
      }
      
      $.ajax({
        type: "POST",
        url: "../php/addSelect.php",
        dataType: 'json',
        data: {
          contract,
        },
        success: function(response) {
          $('#adress').append(response.html);  
          console.log(response.html);
        }
      });
    }
  };

  $('tr[data-href]').on("click", function() {
    document.location = $(this).data('href');
  });
  
  $("#display_period").click(function() {
    var data_def = "";
    var data_first1 = "";
    var data_first = "";
    var data_second1 = "";
    var data_second =  "";
    var contract1 =  "";
    var adress =  "";
    var kinolog =  "";
    var shelter =  "";
    var type =  "";
    
    data_def = document.getElementById('data_first').value;
    data_first1 = document.getElementById('data_first').value + "T00:00:00";
    data_first = Date.parse(data_first1)/1000 - 21600;
    data_second1 = document.getElementById('data_second').value + "T23:59:59";
    data_second =  Date.parse(data_second1)/1000 - 21600;

    contract1 =  document.getElementById('contract').value;
    adress =  document.getElementById('adress').value;
    kinolog =  document.getElementById('kinolog').value;
    shelter =  document.getElementById('shelter').value;
    type =  document.getElementById('type').value;

    // if (contract1 == "all") {
    //   alert("Выберите контракт");
    // }
    // else {
      if (type == "all") {
        alert("Выберите тип документа");
      }
      else if (type == "akt_otlova" && contract1 == "all"){
        alert("Для печати акта отлова выберите контракт");
      }
      else {
        $("#content-table").html = "";
        $("#content-table").html("Загрузка...").show();
        $.ajax({
          type: "POST",
          url: "../php/period_zaklucheniya.php",
          data: {
            data_first,
            data_second,
            data_def,
            contract1,
            adress,
            kinolog,
            shelter,
            type,
          },
          success: function(response) {
            $("#content-table").html(response).show();
          }
        });
      }
    // }
  });
</script>
<?php
  require_once("template/footer.html");
?>