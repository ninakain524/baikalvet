<?php
    require_once("php/config.php");
    if (!isset($_SESSION['timezone'])){
      echo "Не определён часовой пояс пользователя. Авторизируйтесь заново <a href='auth/login.php'>ЗДЕСЬ</a>";
      exit();
    }
    require_once("php/timezone.php");
    if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
    require_once("php/update_status.php");
    require_once("template/head.html");
    require_once("template/header.php");
    require_once("php/functions.php");
    $today = date("Y-m-d\TH:i:s");
    $today1 = date("Y-m-d");
    
    $_SESSION['num_tr'] = 1;

    $last_query = " AND (`id_status`=1 OR `id_status`=2 OR `id_status`=11 OR `id_status`=7 OR `id_status`=8) ";
    
    function format_interval(DateInterval $interval) {
        $result = "";
        if ($interval->d) { $result .= $interval->format("%d д. "); }
        if ($interval->h) { $result .= $interval->format("%h ч. "); }
        if ($interval->i) { $result .= $interval->format("%i мин. "); }
    
        return $result;
    };
?>

<!-- Content -->
<section class="content-mobile">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="content-right" class="content-right">
          <div id="content-right-table" class="content-right-table table-responsive">
            <div class="menu-table-min">
                <div class="menu-table-filter">
                  <input type="date" name="data_first" id="data_first" class="field-filter-mobile d-mob" value="<?php echo $today1; ?>" autocomplete="off"/>
                  <input type="date" name="data_second" id="data_second" class="field-filter-mobile d-mob" value="<?php echo $today1; ?>" autocomplete="off"/>
                  <input id="display_period_kinolog-m" name="display-set" class="d-mob" type="submit" value="Показать" autofocus />
                </div>
            </div>
            <div id="content-table">
                <table id="table-zayavka" class="table-zayavka table table-striped table-bordered tab tab-mobile"><?
                    $show_status_constr = $_SESSION['show_status'] == "all" ? "AND (`id_status`=1 OR `id_status`=2 OR `id_status`=11 OR `id_status`=7 OR `id_status`=8)" : "AND id_status=".$_SESSION['show_status'];
            		$query = "SELECT * FROM `application` WHERE `id_sostoyanie`='1' AND `show`='1' AND `id_user`='".$_SESSION['id_user']."' ".$show_status_constr." ORDER BY `".$_SESSION['order_by']."` ".$_SESSION['sort_by'];
            		$res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
            		$row_count = 0;
                    while ($res = mysqli_fetch_array($res_query)) {
                        $row_count++;
            			$res_image = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `upload_files` WHERE `id_user`='".$_SESSION['id_user']."' AND `id_application`='".$res['id']."' AND `type`='img'"));
            			$res_kind = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `animal_kind` WHERE `id`=".$res['id_kind']));
            			$res_status= mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM `application_status` WHERE `id`=".$res['id_status']));
            			$end = date('Y-m-d H:i', $res['date_start_karantin']+777600);
            			$first_date = new DateTime($today);
            			$second_date = new DateTime($end);
            			$difference = $first_date->diff($second_date);
            			$interval = format_interval($difference);
            			
            			if(!empty($res_image))
            				$img = $res_image['path_thumb'].$res_image['name_thumb'];
            			else
            				$img = "assets/images/no-image.png";
            			    
            			echo "<tr data-href='/animal_card.php?application={$res['id']}' class='application-mobile'>
                            			<td>
                            			    <div class='block-head'>
                            			        <div class='id-color block-head-num'>{$_SESSION['num_tr']}</div>
                            			        <div class='block-head-date'>".date('d.m.Y', $res['data'])."</div>
                            			        <div class='block-head-adress'>{$res['locality']}, {$res['street']}</div>";
                                                if($res_status['id'] == 7 || $res_status['id'] == 8)
                                                    $rowsData[] = "<div class='block-head-status die-color'>";
                                                else
                                                    $rowsData[] = "<div class='block-head-status'>";
                                			        if($res_status['id'] == 2)
                                        				$rowsData[] .= "{$res_status['status_name']}<br> <div class='status-time'>до ".date("d.m.Y", ($res['date_start_karantin']+777600))."</div>";
                                        			else
                                        				$rowsData[] .= "{$res_status['status_name']}";
                        echo "         </div>
                                            </div>
                                            <div class='block-content'>
                                                <div class='block-content-img'><img src='{$img}' class='table-img'></div>
                                                <div class='block-content-info'>
                                                    <ul>
                                                        <li>{$res_kind['name_kind']}</li>
                                                        <li>{$res['breed']}</li>
                                                        <li>возраст: {$res['age']}</li>
                                                        <li>окрас: {$res['color']}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class='block-comment'>{$res['comment']}</div>
                                        </td>
                                    </tr>";
                        $_SESSION['num_tr'] = $_SESSION['num_tr'] + 1;
                      
                    }?>
                </table>
            </div>  
          </div>  
        </div>
      </div> 
    </div> 
  </div>
</section>
<!-- /Content -->
<script type="text/javascript">
  $('tr[data-href]').on("click", function() {
    document.location = $(this).data('href');
  });
  
  $("#display_period_kinolog-m").click(function() {
        var data_def = document.getElementById('data_first').value;
        var data_first1 = document.getElementById('data_first').value + "T00:00:00";
        var data_first = Date.parse(data_first1)/1000 - 21600;
        var data_second1 = document.getElementById('data_second').value + "T23:59:59";
        var data_second =  Date.parse(data_second1)/1000 - 21600;
        $("#content-table").html("Загрузка...").show();
        $.ajax({
          type: "POST",
          url: "../php/period_kinolog-mobile.php",
          data: {
            data_first1: data_first,
            data_second1: data_second,
            data_def1: data_def,
          },
          success: function(response) {
            $("#content-table").html(response).show();
          }
        });
    });
</script>
<?php
  require_once("template/footer.html");
?>