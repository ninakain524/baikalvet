<?php
  require_once("php/config.php");
  require_once("php/timezone.php");
  require_once("php/functions.php");
  if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
  require_once("template/head.html");
  require_once("template/header.php");

  $today_day = date("Y-m-d");
  $user = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users WHERE id=".$_GET['user_id']));
  $userg = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM users_group WHERE id=".$user['id_users_group']));
  $region = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM region WHERE id=".$user['code_region']));
  if ($user['id_users_group'] == 2)
    $shelter = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_shelters WHERE id=".$user['shelter_id']));
  
  if(isset($_POST['submit'])) {

        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $users_group = $_POST['users_group'];
        $sourname = $_POST['sourname'];
        $name = $_POST['name'];
        $patronymic = $_POST['patronymic'];
        $region = $_POST['region'];
        $shelter = $_POST['shelter'];
        $login = $_POST['login'];
        $part = "";
        if($_SESSION['super_admin'] == "1") {
          $password = $_POST['password'];
          $part = "password='".$password."',";
        }
        
        $queryLog = "UPDATE users SET 
        sourname = '".$sourname."',
        name ='".$name."',
        patronymic = '".$patronymic."',
        email='".$email."', 
        login='".$login."',
        ".$part."
        id_users_group ='".$users_group."',
        phone='".$phone."', 
        code_region='".$region."',
        shelter_id='".$shelter."'
        WHERE id=".$_GET['user_id'];
        if (mysqli_query($SERVER, $queryLog)) {
          addLogs($_SESSION['id_user']." ".$_SESSION['sourname_user']." ".$_SESSION['name_user'], "Редактировал пользователя - " . $queryLog);
        }
        
        mail($email,
            "Ваша учетная запись изменена администратором", 
            "ФИО: ".$sourname." ".$name." ".$patronymic. 
            "\nЛогин: ".$email.
            "\nТелефон: ".$phone);
    
        $SERVER -> close();
        echo "<script>window.location.href='/admin_users.php';</script>";
        exit();
    }
?>

<section class="content">
  <div class="container">
    <div id="add_application" class="content-form js-form-address"> 
      <div class="row">
        <form id="card_user" method="post" action="">
            <h1>Карточка пользователя</h1>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="sourname">Фамилия</label>
                <input class="field width-add" type="text" name="sourname" value="<?php echo $user['sourname']; ?>" required/>  
              </div>
            </div>
    
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="name">Имя</label>
                <input class="field width-add input" type="text" name="name" value="<?php echo $user['name']; ?>" required />
              </div>
            </div>  
             
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="patronymic">Отчество</label>
                <input class="field width-add field-date" type="text" name="patronymic" value="<?php echo $user['patronymic']; ?>" />  
              </div>
            </div>
    
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="email">e-mail</label>
                <input class="field width-add" type="email" name="email"  value="<?php echo $user['email']; ?>" required />
              </div>
            </div>  
    
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="phone">Телефон</label>
                <input class="field width-add" type="tel" name="phone" id="phone" value="<?php echo $user['phone']; ?>" />
              </div>
            </div> 

            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="users_group">Роль</label>
                <select class="field width-add" name="users_group">
                  <?php
                    $sql = "SELECT * FROM `users_group`;";
                    $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                    echo '<option value="'.$userg['id'].'" selected readonly hidden>'.$userg['name_group'].'</option>';
    
                    while($row = mysqli_fetch_array($res))
                    {
                      echo '<option value="'. $row['id'] .'">'. $row['name_group'] .'</option>';
                    }
                  ?>
                </select>
              </div>
            </div> 
    
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="region">Регион</label>
                <select class="field width-add" name="region">
                  <?php
                    $sql = "SELECT * FROM `region`;";
                    $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                    echo '<option value="'. $region['id'].'" selected readonly hidden>'. $region['name_region'].'</option>';
    
                    while($row = mysqli_fetch_array($res))
                    {
                      echo '<option value="'. $row['id'] .'">'. $row['name_region'] .'</option>';
                    }
                  ?>
                </select>
              </div>
            </div>
            
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="shelter">Приют (для админа приюта)</label>
                <select class="field width-add" name="shelter">
                  <?php
                    $sql = "SELECT * FROM animal_shelters WHERE actual = 1;";
                    $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
                    echo '<option value="'.$shelter['id'].'" selected readonly hidden>'.$shelter['name_shelter'].'</option>';
    
                    while($row = mysqli_fetch_array($res))
                    {
                      echo '<option value="'. $row['id'] .'">'. $row['name_shelter'] .'</option>';
                    }
                  ?>
                </select>
              </div>
            </div>
              
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label for="login">Логин</label>
                <input class="field width-add field-date" type="text" name="login" value="<?php echo $user['login']; ?>" required/>  
              </div>
            </div>
           <? if($_SESSION['super_admin'] == "1") { ?>
            <div class="col-12 col-sm-6">
                <div class="form-group">
                  <label for="password">Пароль</label>
                  <div class="form-group custom-input">
                    <div class="password">
                    <input class="field pass" type="password" id="password-input1" name="password"  minlength="8" value="<?php echo $user['password']; ?>" title="Пароль должен содержать минимум 8 символов" required autocomplete="off" />
                      <a href="#" class="password-control"></a>
                      <!--<p id="generate2" class="generate">Сгенерировать</p>-->
                    </div>
                  </div>
                </div>
            </div>
            <? } ?>
            <div class="col-12 col-sm-12">
              <div class="form-group">
                <input class="btn button-auth field-submit" name="submit" type="submit" value="Сохранить" autofocus />
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php
  require_once("template/footer.html");
?>