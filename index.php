<?php
  require_once("php/config.php");
  $_SESSION['error'] = "";

  if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
  {
    header('location: auth/login.php' );
    exit();
  }

  if ( $_SESSION['id_users_group'] == "1") { // Админ
    $_SESSION['width'] = 0;
    echo "<script language='javascript'>\n";
    echo "if (screen.width > 480) \n";
    echo "location.href='admin.php';\n";
    echo "else\n";
    echo "location.href='admin-mobile.php';\n";
    echo "</script>";
  }

  if ( $_SESSION['id_users_group'] == "2") { // Админ приюта
    
    echo "<script language='javascript'>\n";
    echo "if (screen.width > 480) \n";
    echo "location.href='admin_shelter.php';\n";
    echo "else\n";
    echo "location.href='adminShelter-mobile.php';\n";
    echo "</script>";
  }

  if ( $_SESSION['id_users_group'] == "3") { // Ветеринар
    
    echo "<script language='javascript'>\n";
    echo "if (screen.width > 480) \n";
    echo "location.href='veterinar.php';\n";
    echo "else\n";
    echo "location.href='veterinar-mobile.php';\n";
    echo "</script>";
    
    // header('location: veterinar.php' );
  }

  if ( $_SESSION['id_users_group'] == "4") { // Кинолог
  
    echo "<script language='javascript'> \n";
    echo "if (screen.width > 480) \n";
    echo "location.href='kinolog.php';\n";
    echo "else\n";
    echo "location.href='kinolog-mobile.php';\n";
    echo "</script>";

    // header('location: kinolog.php' );
  }
?>

<?php
  require_once("template/footer.html");
?>