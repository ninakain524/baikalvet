<?php
  require_once("php/config.php");
  require_once("php/timezone.php");
  require_once("php/functions.php");
  if (!isset($_SESSION['email']) OR ($_SESSION['email'] == false))
    {
        header('location: auth/login.php' );
        exit();
    }
  require_once("template/head.html");
  require_once("template/header.php");

  $card = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application WHERE id=".$_GET['application']));
  $animal_kind = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_kind WHERE id=".$card['id_kind']));
  $animal_gender = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_gender WHERE id=".$card['id_gender']));
  $animal_shelter = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM animal_shelters WHERE id=".$card['id_shelter']));
  $status = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application_status WHERE id=".$card['id_status']));
  $sostoyanie = mysqli_fetch_array(mysqli_query($SERVER, "SELECT * FROM application_sostoyanie WHERE id=".$card['id_sostoyanie']));
?>
<section class="content">
	<div class="container">
		<form id="add_application" data="<?echo $_GET['application']?>" class="content-form js-form-address"> 
			<div class="row">
				<h1>Редактирование карточки животного</h1>
				<div class="col-12 col-sm-6">
		          <div class="form-group">
		            <label for="region">Регион отлова</label>
		            <input class="field width-add" type="text" name="region" value="<?php echo $_SESSION['region']; ?>" required readonly/>  
		          </div>
		        </div>

		        <div class="col-12 col-sm-6">
		          <div class="form-group">
		            <label for="city">Населенный пункт/местность отлова</label>
		            <input class="field width-add" type="text" name="city" id="city" value="<?echo $card['locality']?>" required />
		          </div>
		        </div>  

		        <div class="col-12 col-sm-6">
		          <div class="form-group">
		            <label for="street">Улица</label>
		            <input class="field width-add" type="text" name="street" id="street" value="<? echo $card['street'] ?>" required />
		          </div>
		        </div>
		        
		        <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label for="date">Дата </label>
                    <input class="field width-add field-date" type="datetime-local" name="date" id="date" autocomplete="on" value="<?php echo $card['data'] ?>" required />  
                  </div>
                </div>

		        <div class="col-12 col-sm-6">
		          <div class="form-group">
		            <label for="id_kind">Вид животного</label>
		            <select class="field width-add" id="id_kind" name="id_kind" required>
		              <?php
		                $sql = "SELECT * FROM `animal_kind`;";
		                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
		                echo '<option value='.$card["id_kind"].' selected readonly hidden>'.$animal_kind["name_kind"].'</option>';

		                while($row = mysqli_fetch_array($res))
		                {
		                  echo '<option value="'. $row['id'] .'">'. $row['name_kind'] .'</option>';
		                }
		              ?>
		            </select>
		          </div>
		        </div>

		        <div class="col-12 col-sm-6">
		          <div class="form-group">
		            <label for="breed">Порода животного</label>
		            <input class="field width-add" type="text" name="breed" id="breed" value="<?echo $card['breed']?>" />
		          </div>
		        </div>

		        <div class="col-12 col-sm-6">
		          <div class="form-group">
		            <label for="gender">Пол животного</label>
		            <select class="field width-add" id="gender" name="gender">
		              <?php
		                $sql = "SELECT * FROM `animal_gender`;";
		                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
		                echo '<option value='.$card["id_gender"].' selected readonly hidden>'.$animal_gender["name_gender"].'</option>';

		                while($row = mysqli_fetch_array($res))
		                {
		                  echo '<option value="'. $row['id'] .'">'. $row['name_gender'] .'</option>';
		                }
		              ?>
		            </select>
		          </div>
		        </div> 

		        <div class="col-12 col-sm-6">
		          <div class="form-group">
		            <label for="color">Цвет животного</label>
		            <input class="field width-add" type="text" name="color" id="color" value="<?echo $card['color']?>"/>
		          </div>
		        </div>

		        <div class="col-12 col-sm-6">
		          <div class="form-group">
		            <label for="age">Возраст животного</label>
		            <input class="field width-add" type="text" name="age" id="age" value="<?echo $card['age']?>" />
		          </div>
		        </div>

		        <div class="col-12 col-sm-6">
		          <div class="form-group">
		            <label for="weight">Вес животного</label>
		            <input class="field width-add" type="text" name="weight" id="weight" value="<?echo $card['weight']?>" />
		          </div>
		        </div>

		        <div class="col-12 col-sm-6">
		          <div class="form-group">
		            <label for="shelter">Приют</label>
		            <select class="field width-add" id="shelter" name="shelter">
		              <?php
		                $sql = "SELECT * FROM `animal_shelters` WHERE actual = 1;";
		                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
		                echo '<option value='.$card["id_shelter"].' selected readonly hidden>'.$animal_shelter["name_shelter"].'</option>';

		                while($row = mysqli_fetch_array($res))
		                {
		                  echo '<option value="'. $row['id'] .'">'. $row['name_shelter'] .'</option>';
		                }
		              ?>
		            </select>
		          </div>
		        </div>

		        <div class="col-12 col-sm-6">
		          <div class="form-group">
		            <label for="aviary">№ вольера</label>
		            <input class="field width-add" type="text" name="aviary" id="num_aviary" value="<?echo $card['num_aviary']?>" />
		          </div>
		        </div>
		        
		        <?
		            if ($_SESSION['id_users_group'] == "1") {?>
		                <div class="col-12 col-sm-6">
        		          <div class="form-group">
        		            <label for="id_status">Статус</label>
        		            <select class="field width-add" id="id_status" name="id_status">
        		              <?php
        		                $sql = "SELECT * FROM `application_status`;";
        		                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
        		                echo '<option value='.$card["id_status"].' selected readonly hidden>'.$status["status_name"].'</option>';
        
        		                while($row = mysqli_fetch_array($res))
        		                {
        		                  echo '<option value="'. $row['id'] .'">'. $row['status_name'] .'</option>';
        		                }
        		              ?>
        		            </select>
        		          </div>
        		        </div>
        		        
        		        <div class="col-12 col-sm-6">
        		          <div class="form-group">
        		            <label for="id_sostoyanie">Состояние</label>
        		            <select class="field width-add" id="id_sostoyanie" name="id_sostoyanie">
        		              <?php
        		                $sql = "SELECT * FROM `application_sostoyanie`;";
        		                $res = mysqli_query($SERVER, $sql) or die("Ошибка " . mysqli_error($SERVER));
        		                echo '<option value='.$card["id_sostoyanie"].' selected readonly hidden>'.$sostoyanie["name_sostoyanie"].'</option>';
        
        		                while($row = mysqli_fetch_array($res))
        		                {
        		                  echo '<option value="'. $row['id'] .'">'. $row['name_sostoyanie'] .'</option>';
        		                }
        		              ?>
        		            </select>
        		          </div>
        		        </div>
		        <? } ?>

		        <div class="col-12 col-sm-12">
		          <div class="form-group">
		            <label for="comment">Комментарий</label>
		            <textarea class="field" name="comment" id="comment" autocomplete="off"><? echo $card['comment'] ?></textarea>
		          </div>
		        </div>

		        <div class="col-12 col-sm-12">
		          <div class="form-group">
		            <label for="mediafiles">Медиафайлы</label>
		            <div name="mediafiles">
	            	<?php
	      				if ($_SESSION['id_users_group'] == 4 || $_SESSION['id_users_group'] == 1)
                      		// $query = "SELECT * FROM `upload_files` WHERE id_user IN (SELECT id FROM `users` WHERE id_users_group = '4') AND id_application = '".$_GET['application']."' ORDER BY id";
                      		$query = "SELECT * FROM `upload_files` WHERE id_application = '".$_GET['application']."' ORDER BY id";
                      	$res_query = mysqli_query($SERVER, $query) or die("Ошибка " . mysqli_error($SERVER));
		                while ($res = mysqli_fetch_array($res_query)) {
		              		
		              		if ($res['type'] == "img") {
		              			$img = $res['path_thumb'].$res['name_thumb'];
		              			echo
		              				"<div class='media-item'>
		              					<img src='{$img}' width='100px'>
		              					<a class='del_file' href='#' onclick='event.preventDefault()' data='{$res['id']}'></a>
		              				</div>";
		              		}
		              		else{
		              			$vid = $res['path_file'].$res['name_file'];
		              			echo 
		              				"<div class='media-item'>
		              					<video src='{$vid}' width='100px'></video>
		              					<a class='del_file' href='#' onclick='event.preventDefault()' data='{$res['id']}'>
		              					</a>
		              				</div>";
		              		}
		                }
		                echo
              				'<input  type="file" class="form-control" name="media-on-edit" id="media-on-edit" accept="image/jpeg, image/png, image/jpg, video/mp4, video/avi, video/3gp, video/mov" multiple>';
		            ?>
		            </div>
		          </div>
		        </div>
		        <div class="col-12 col-sm-12">
		          <div class="form-group">
		            <button id="update" type="button" class="btn field-submit width-submit">Сохранить</button>
		          </div>
		        </div>


			</div>
		</form>
    </div>
</section>
<?php
	require_once("template/footer.html");
?>